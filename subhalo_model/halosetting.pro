pro halosetting,nparticles
common haloset,neq,npar,mset
neq=5
npar=nparticles
mset={msub:0.,mhost:0.,rs_sub:0.,rs_host:0.,csub:0.,chost:0.,$
      msubi:0.,rf:0.,lf:0.,ros_sub:0.,ros_host:0.,vcmax_host:0.,$
      Vvir_host:0.,ld:0.,td:1,mbaryon:0.}
if npar gt 1 then mset=replicate(temporary(mset),1,npar)

end
