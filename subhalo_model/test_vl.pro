pro test_vl
vlname=['one','two']
!key.hostz=1
mhost=1.77e12 & rpvir=389. & chost=chalo(mhost,0.)
rs_host=rpvir/chost
device,filename='halofunc/mfunc_vl.ps',/encapsul,/color
plot,findgen(10)-10,findgen(10),xrange=[-6,-2],yrange=[0.1,4]
for vl=0,1 do begin
  out=read_ascii('halofunc/plotdata/vl'+vlname[vl]+'subs.txt',comment_symbol='#',count=k)
  out=out.field01 
  help,out
  rxyz=sqrt(out[7,*]^2+out[8,*]^2+out[9,*]^2)
  rc=out[1,*]
  snr=where((rc gt 0)and(rc le rpvir),count)
  print,count
  mvev=alog10(out[5,snr]/mhost)
  print,min(mvev),max(mvev)
  h_ave=histo(mvev,-6,-3,11,dbin=dbin,locations=xev,/cumu,/inverse)
  print,h_ave
;  mfev=alog10(h_ave/dbin)-alog10(alog(10))
;  h=deriv(h_ave,xev)
;  print,h
;  mfev=alog10(h)-alog10(alog(10))
;  print,mfev
  mfev=alog10(h_ave)
  oplot,xev,mfev,color=!myct.c2[vl]
endfor

yfit=alog10(0.0064)-xev ;/alog(10)^2   ;-1
oplot,xev,yfit,linestyle=2
yfit=alog10(0.02884)-0.9*xev
oplot,xev,yfit,linestyle=3



end

