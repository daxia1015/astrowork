function rt_eq,rt,rs_sub,csub,msub,ros_sub,coeff,c_th
m_r=mhalo_r(rt,rs_sub,csub,msub,th=!key.th,c_th=c_th,ros_sub=ros_sub,hprofile=!hprofile)
f=rt^3-coeff*m_r
return,f
end
;=======================================================
function rt_eq_p,rt,rs_sub,csub,ros_sub,coeff,c_th
ro=ro_r(rt,rs_sub,csub,ros_sub,th=!key.th,c_th=c_th,hprofile=!hprofile)
fp=(3.-coeff*4*!pi*ro)*rt^2
return,fp
end
;==================================================================
;==================================================================
function r_tidal,mhost_r,ro_h_r,omega,r_orb,c_th,rmax,mset
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
coeff=omega^2+gaussc*(2*mhost_r/r_orb^3-4*!pi*ro_h_r)
coeff=gaussc/temporary(coeff)

  it=0 & itmax=2000 & eps=1e-3 & root=fix(r_orb) & root[*]=0
  rt=mset.rs_sub
;  rt=temporary(rt) > mset.rs_sub
;if !enprint then  print,'0 ',rt
  fp=rt_eq_p(rt,mset.rs_sub,mset.csub,mset.ros_sub,coeff,c_th)
  while 1 do begin
    sn1=where((root eq 0)and(fp le 0),n1)
    if n1 ne 0 then begin
      rt[sn1]=temporary(rt[sn1])*2 < temporary(rt[sn1])+1.
      fp[sn1]=rt_eq_p(rt[sn1],mset[sn1].rs_sub,mset[sn1].csub,$
                      mset[sn1].ros_sub,coeff[sn1],$
                      [c_th[0,sn1],c_th[1,sn1]])
    endif else break
  endwhile
  f=fp
;if !enprint then print,'1 ',rt
  while 1 do begin
    sn1=where(root eq 0,n1) ;& print,n1
    if n1 ne 0 then begin
      f[sn1]=rt_eq(rt[sn1],mset[sn1].rs_sub,mset[sn1].csub,$
                   mset[sn1].msub,mset[sn1].ros_sub,$
                   coeff[sn1],[c_th[0,sn1],c_th[1,sn1]])
      dr=abs(f)/fp & dr0=rt*eps > 0.01
      sn2=where((root eq 0)and(dr le dr0),n2)
      if n2 ne 0 then root[sn2]=1
      sn2=where((root eq 0)and(dr gt dr0)and(f lt 0),n2)
      sn3=where((root eq 0)and(dr gt dr0)and(f gt 0),n3)
      if n2 ne 0 then rt[sn2]=temporary(rt[sn2])+dr[sn2]/2 ;< 1.)
      if n3 ne 0 then rt[sn3]=temporary(rt[sn3])-dr[sn3]
      ;if !enprint then print,rt,dr,format='(2e15.6)'
      sn2=where((root eq 0)and(rt lt 0.1*mset.rs_sub),n2)
      sn3=where((root eq 0)and(rt gt rmax),n3)
      if n2 ne 0 then begin
	rt[sn2]=temporary(rt[sn2]) > 0.01*mset[sn2].rs_sub
	root[sn2]=2
      endif
      if n3 ne 0 then root[sn3]=3
      sn2=where((root eq 0)and(it gt itmax),n2)
      if n2 ne 0 then begin
        root[sn2]=4
	if n2 gt 1 then print,'rt=',rt[sn2]
      endif
      sn2=where(root eq 0,n2)
      if n2 ne 0 then begin
;        if !enprint then print,rt[sn2]
        fp[sn2]=rt_eq_p(rt[sn2],mset[sn2].rs_sub,mset[sn2].csub,$
                        mset[sn2].ros_sub,coeff[sn2],$
	                [c_th[0,sn2],c_th[1,sn2]])
      endif else break    
    endif else break
    it++
  endwhile
;  if it gt itmax then print,'iteration gt 1000'
;  if it gt 500 then print,'rt=',rt,dr,dr/rt,f,format='(a3,4e15.6)'
;  !itcount=!itcount > it
;if !enprint then print,rt,dr,dr/rt,f,format='(4e15.6)'
return,rt
end

;  while fp le 0. do begin
;    rt=rt*(2.+alog10(1-fp))
;    if rt ge !rt.Rbnd then begin
;      root=0
;      break
;    endif
;    fp=rt_eq_p(rt,coeff,c_th)
;  endwhile

;    if root then begin
;    f=rt_eq(rt,coeff,c_th)
;    dr=abs(f)/fp
;    while dr gt dr0 do begin
;      if f lt 0. then rt=rt+dr/2 else rt=rt-dr
;      if rt lt dr0 then begin
;        rt=rt>(dr0/10.)
;	break
;      endif	
;      if rt ge !rt.Rbnd then begin
;        root=0
;        break
;      endif 
;      fp=rt_eq_p(rt,coeff,c_th) & f=rt_eq(rt,coeff,c_th)
;      dr=abs(f)/fp
;    endwhile
;  endif
