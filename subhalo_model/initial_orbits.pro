;pro mah,z,zc,mxz,pder
;mxz=exp(-2*z/(1+zc))
;if n_params() ge 4 then pder=-2*z*mxz/(1+zc)^2
;end

function chalo,m,z,normalize=normalize ;,sub=sub,host=host
if keyword_set(normalize) then begin
  mr=m & ch=z
  alpha=0.13
  c=ch/mr^alpha
endif else begin
  ;alpha=0.13 & mstar=1e13 & A=9.6  ;Bullock 2001
  alpha=0.11 & mstar=1e14/0.73 & A=4.67  ;Netto 2007
  mu=m/mstar
  c=A/mu^alpha/(1.+z)
endelse
return,c
end


;--specific force act on subhalo by host halo at position r.
function f_g,r,rs_host,chost,mhost,hprofile=hprofile
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
m_r=mhalo_r(r,rs_host,chost,mhost,hprofile=hprofile)
f=gaussc*m_r/r^2
return,f
end


function energy_r,r,mhost,rs_host,chost,rpvir,v_r,circular=circular,hprofile=hprofile
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
ec=r
if keyword_set(circular) then begin
  m_r=mhalo_r(r,rs_host,chost,mhost,hprofile=hprofile)
  v_r2=gaussc*m_r/r
endif else v_r2=v_r^2
sn1=where(r lt rpvir,n1) & sn2=where(r ge rpvir,n2)
m=mhost & rs=rs_host & c=chost & rvir=rpvir
nhost=n_elements(mhost)
if n1 ne 0 then begin
  if nhost gt 1 then begin
    m=mhost[sn1] & rs=rs_host[sn1]
    c=chost[sn1] & rvir=rpvir[sn1]
  endif
  I_r_rvir=(-alog(1+c)/rvir+alog(1+r[sn1]/rs)/r[sn1])*gaussc*m/gx(c)
  ec[sn1]=v_r2[sn1]/2.-I_r_rvir-gaussc*m/rvir
endif
if n2 ne 0 then begin
  if nhost gt 1 then m=mhost[sn2]
  ec[sn2]=v_r2[sn2]/2.-gaussc*m/r[sn2]
endif
return,ec
end
;========================================================
;=========%%%=++++++++++++============+++++++++++++=======---------
function ini_tra,msub,mhost,chost,yita,epson,zi,ti,hprofile=hprofile
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
rvir=r_vir(mhost,zi) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
r_s_host=rvir/chost   ;initial r_s of subhalo and host halo
R_c=yita*rvir  ;& help,rvir            ;kpc, circular orbit(Energy) with R_c, the intial radial position of subhalo
;if R_c le 0. then print,'R_c',R_c
m_r=mhalo_r(R_c,r_s_host,chost,mhost,hprofile=hprofile) ;& help,m_r      ;M_sun, the mass of host halo within initial radius R_c
vc=sqrt(gaussc*m_r/R_c)  ;kpc Gyr^{-1}, initial velocity of subhalo
ei=energy_r(R_c,mhost,r_s_host,chost,rvir,/circular,hprofile=hprofile) ;& print,'ei=',ei
vi=sqrt(2*(ei+gaussc*mhost/rvir)) ;& ri=rvir
;print,epson*yita*vc/vi 
alpha=!pi-asin(epson*yita*vc/vi)          ;initial angle between velocity and radial direction
;vi=vc & ri=R_c
vr=vi*cos(alpha) ;& vth=vi*epson   ;vth=vi*sin(alpha)  kpc Gyr^{-1}
li=epson*R_c*vc ;& print,li               ;initial specific angular momentum of subhalo
;fg=gaussc*m_r/ri^2
;ki=vth*fg/vi^3                    ;kpc^-1       ;initial trajectory curvature
;x=mhost/msub & v_dti=vi < 180.  
;dt=(1+epson)*(dt+x^0.5/alog(1+x))/v_dti     ;initial time and stepsize
;print,'dti=',dt ;print,x,epson,vi,dt
;tra_i={t:ti,m:msub,l:li,energy:0.,vr:vr,vth:vth,v:vi,r:ri,theta:0.,k:ki,ID:1.}
tra_i=replicate(!halo.tras,n_elements(msub))
tra_i.t=ti & tra_i.m=msub & tra_i.l=li & tra_i.vr=vr & tra_i.r=rvir
;tra_i={t:ti,m:msub,l:li,vr:vr,r:ri,theta:0.,ID:1.}
;---initial trajectory status of this subhalo
return,tra_i
end

;======++++++===+++======+++++++======++++++==+++======+++++++++
function fepson,es,zenter=zenter,jiang=jiang
a=2.22
case 1 of
  keyword_set(zenter): f=gamma(2*a)*es^(a-1)*(1-es)^(a-1)/gamma(a)^2   ;probability density profile provided by Zenter
  keyword_set(jiang):  f=2.77*es^1.19*(1.55-es)^2.99  ;probability density profile provided by Jiang
  else: f=2.77*es^1.19*(1.55-es)^2.99     ;message,'keyword /zenter or /jiang should be setted!'
endcase
return,f
end
;~!@#$$%^&*()__+-====-099876543221``~!@@##$$%%^^&&**((())___=+/*---+.
;=====================================================================
function ini_energy,seed,nhalo,yitamin
eta=randomu(seed,nhalo)*0.4+0.6
while 1 do begin
  sn=where(eta lt yitamin,count)
  if count ne 0 then eta[sn]=randomu(seed,count)*0.4+0.6 else break
endwhile
return,eta
end
;===================================================================
;===================================================================
function ini_angmomenta,seed,nhalo,esmin,esmax
    ;n=500
    ;out=fltarr(2,n+1)
    ;openr,lun,'upsilon/upsilon.dat',/get_lun
    ;  readf,lun,out,format='(f9.6,2x,f9.6)'
    ;free_lun,lun
    ;epson=out[0,*]
    ;int_epson=out[1,*]
    func='fepson'
    C=qromb(func,0.,1.) ;& print,'C=',C
    n=500
    epson=findgen(n+1)/n
    int_epson=qromb(func,0.,epson)/C

    int_epson1=randomu(seed,nhalo)
    epson1=interpol(epson,int_epson,int_epson1)
    ;print,where(epson1 gt 1)
    ;print,where(epson1 lt 0.1)
    while 1 do begin
      sn=where((epson1 lt esmin)or(epson1 gt esmax),count)
      if count ne 0 then begin
        int=randomu(seed,count) ;& print,count
        epson1[sn]=interpol(epson,int_epson,int)
      endif else break
    endwhile
    return,epson1
end
;=================================================================
;=================================================================
pro ini_orbits,seed,nhalo,halon,hprofile=hprofile
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
mhost=halon.mhost
chost=chalo(mhost,halon.z)
rvir=r_vir(mhost,halon.z) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
r_s_host=rvir/chost   ;initial r_s of subhalo and host halo

esmin=0.1
yita=ini_energy(seed,nhalo,0.6)
while 1 do begin 
  while 1 do begin
    R_c=yita*rvir  ;& help,rvir            ;kpc, circular orbit(Energy) with R_c, the intial radial position of subhalo
    m_r=mhalo_r(R_c,r_s_host,chost,mhost,hprofile=hprofile) ;& help,m_r      ;M_sun, the mass of host halo within initial radius R_c
    vc=sqrt(gaussc*m_r/R_c)  ;kpc Gyr^{-1}, initial velocity of subhalo
    ei=energy_r(R_c,mhost,r_s_host,chost,rvir,/circular) ;& print,'ei=',ei
    vi2=2*(ei+gaussc*mhost/rvir)
    sn=where(vi2 le 0,count)
    if count ne 0 then yita[sn]=ini_energy(seed,count,yita[sn]) else break
  endwhile
  vi=sqrt(vi2) ;& ri=rvir
  esmax=vi/vc/yita
  sn1=where(esmax le esmin,count1)
  if count1 ne 0 then yita[sn1]=ini_energy(seed,count1,yita[sn1]) else break 
endwhile
halon.yita=yita
halon.es=ini_angmomenta(seed,nhalo,esmin,esmax)
end
