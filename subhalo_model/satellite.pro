function read_satellite,file,count=n,head=head

head='' & n=23
out=replicate({name:'',dsun:0.,dgc:0.,mag:0.,ID:0},n)

openr,lun,file,/get_lun
  readf,lun,head
  readf,lun,out,format='(a20,3f10.1,i6)'
  if eof(lun) then print,'end of '+file
free_lun,lun


return,out
end
