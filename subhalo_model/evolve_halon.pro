pro mset_host,zv
common haloset
;zv=reform(zv,1,npar)
mset.chost=chalo(mset.mhost,zv)
rvir_h=r_vir(mset.mhost,zv)
mset.rs_host=rvir_h/mset.chost  ;initial r_s of subhalo and host halo
mset.ros_host=ros_profile(zv,mset.chost,a,b,hprofile=!hprofile)
mset.rf=1e-3*mset.rs_host > 0.1
mset.vcmax_host=vcir(2.16,mset.chost,mset.mhost,rvir_h)
end
;==============================================================
function find_level,tnow,tlev
ilev=fix(tnow) 
sn0=where(tlev le max(tnow))
sn1=where(tlev le min(tnow))
for i=sn0[0],sn1[0] do begin
  if i eq 0 then sn=where(tnow ge tlev[i],count) else $
  sn=where((tnow ge tlev[i])and(tnow lt tlev[i-1]),count)
  if count ne 0 then ilev[sn]=i
endfor
return,ilev
end
;============================================================
;--this function generate the trajectory by given initial condition
;--it's designed for evolving merger tree
function tra_track,itree,halo,zlev,tlev,mhostz,n_levs0,dtmin,dtmax,nstep
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
common haloset ;,neq,mset,npar0,npar
tra_i=halo.tra & dt=halo.dt
tra=tra_i & t0=tra_i.t & eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
tf=tlev[halo.ilev-1] ;
;help,halo,tra_i,dt,t0,tf
vr0=tra_i.vr
if npar gt 1 then begin
  t0=rebin(tra_i.t,neq,npar)
  dt=rebin(temporary(dt),neq,npar)  
  eps=rebin(eps,neq,npar)
endif
while 1 do begin
  sn1=where(tra_i.m lt halo.mf,n1)
  if !key.bs and(n1 ne 0) then mset[sn1].td=0
  if !key.nex ne 0 then begin
    sn1=where(vr0*tra_i.vr le 0,n1)
    if n1 ne 0 then halo[sn1].iex=temporary(halo[sn1].iex)+1
    sn1=where(halo.iex ge !key.nex,n1)
    if n1 ne 0 then mset[sn1].td=0
  endif
  sn=where((tra_i.r lt mset.rf)or(~finite(tra_i.r))or $
           (tra_i.l lt mset.lf)or(~finite(tra_i.l))or $
           ((!key.bs eq 0)and(tra_i.m lt halo.mf))or $
	   (~finite(tra_i.m))or(tra_i.t ge tlev[0]),nout)
  if nout ne 0 then break
  vr0=tra_i.vr
  tra_i=tra_walk(tra_i,t0,dt,dtmin,dtmax,eps)  ;get the trajectory status of next step
  tra=[temporary(tra),tra_i]
  sn2=where(tra_i.t ge tf,n2)
  if n2 ne 0 then begin
    halo[sn2].ilev=find_level(tra_i[sn2].t,tlev)
    halo[sn2].z=zlev[halo[sn2].ilev]
    halo[sn2].mhost=mhostz[halo[sn2].ilev]	  
    tf[sn2]=tlev[halo[sn2].ilev-1 > 0]
    mset[sn2].mhost=halo[sn2].mhost
    mset_host,halo.z
  endif
endwhile
nstep=n_elements(tra[*,0])-1
tra_i[sn].ID=tra_ID(tra_i[sn],mset[sn].rf,mset[sn].lf,halo[sn].mf)
tra[nstep,sn].ID=tra_i[0,sn].ID
halo.tra=tra_i & halo.dt=dt[0,*] ;& halo.td=mset.td
return,tra
end
;============================================================
;============================================================
pro evolve_halon,itree,halon,zlev,tlev,mhostz,n_levs0,dtmin,dtmax,nhalo,trasv,$
                 nstepv,ntra,major=major
common haloset
iex=intarr(1,nhalo)
while 1 do begin
  sn=where((halon.tra.ID eq 1)and(halon.tra.t lt tlev[0]),n0)
  if n0 ne 0 then begin
    halosetting,n0 ;& print,'n_particles=',n0
;    !enprint=n0 eq 126
    halo=halon[sn]
    if n0 gt 1 then halo=reform(temporary(halo),1,n0)
    struct_assign,halo,mset,/nozero
    mset_host,halo.z
    tra=tra_track(itree,halo,zlev,tlev,mhostz,n_levs0,dtmin,dtmax,nstep)
    if n0 gt 1 then halo=reform(temporary(halo),n0)
    halon[sn]=halo
;    if nstep ge 1 then begin
      n_next=nstepv[sn]+nstep
;      if max(n_next) gt ntra then begin
;        nplus=(max(n_next)-ntra)*2
;        (*to_trasv)=[[(*to_trasv)],[replicate(!halo.tras,nhalo,nplus)]]
;        ntra=ntra+nplus
;      endif
;      for is=1,nstep do (*to_trasv)[sn,nstepv[sn]+is-1]=reform(tra[is,*],n0)
      nstepv[sn]=n_next
;    endif
  endif else break
endwhile
halon.tf=halon.tra.t
sn=where(halon.tra.ID lt 1,n0)
if n0 ne 0 then halon[sn].tf=tlev[halon[sn].ilev]
end




;function initialize_ihalo,k,halo,t_now,tsam,tra_f,trasv,enfit=enfit
;yita=halo.yita & epson=halo.es & dt=8.
;!m.chost=chalo(halo.mhost,halo.zi)
;tra_i=ini_tra(halo.msubi,halo.mhosti,!m.chost,yita,epson,t_now,Rvir_h,dt,hprofile=hprofile)
;halo.li=tra_i.l 
;lf=0.05*halo.li
;lf=lf < 100. & lf=lf > 10.
;halo.lf=lf
;halo.dt=dt
;tral=tra_s2l(tra_i,!m.mhost,!m.rs_host,!m.chost,Rvir_h,1,hprofile=hprofile)
;trasv[k,0]=tra_i
;if enfit then begin
;  mr=halo.msubi/halo.mhosti
;  if (mr ge min(!mrv))and(mr le max(!mrv)) then begin
;    tmerge=tfit(!mrv,tsam,mr,epson,yita)
;    if(tmerge le trun)then tral.ID=0 
;  endif
;  if((mr ge min(!mrv))and(mr le max(!mrv))and(trun gt 0.)and(tral.ID ne 0))then begin
;    tral=trafit(tra_i,mr,epson,yita,tra_f[*,*,*,ilev])
;;    tra_i.m=tra_i.m*exp(-trun*(ez(nplev[ilev].z)*dltcz(nplev[ilev].z)/dltc0)^0.5/3.)
;    tral.ID=2
;  endif
;endif
;halo.tra=tral
;return,halo
;end
;======================================================
;============================================================

