pro tzentner
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=[0.3,0.5,0.7] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)


nbs=3 & nmlr=5 & ld=4 & k=ny-1
tzentner=fltarr(nm,nu,ny,nmlr,nbs)
for bs=0,2 do begin
  for mlr=0,3 do begin
    tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
    tzentner[*,*,*,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                           rmax=1.,lf=0.01,extrapolate=0)
  endfor
endfor
bs=0 & char=1.2	 	    
device,filename='tmerge/tzentner.ps',/encapsul,/color,xsize=18.6,ysize=7 ;,xoffset=1.5,yoffset=10
multiplot,[nu,1],mxtitle='m(0)/M(0)',mxtitsize=char,mxtitoffset=0.4,$
xgap=0.025,/doyaxis,myposition=[0.06,0.2,0.99,0.95]
!p.charsize=char
xyouts,0.04,(!p.position[1]+!p.position[3])/2,'T!ddf!n [Gyr]',/normal,$
orientation=90,alignment=0.5
for j=0,nu-1 do begin  ;epson
  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=1,$
  xtickname=[' ','0.1',' ','0.2',' ','0.3']
  for mlr=0,3 do oplot,mratio,tzentner[*,j,k,mlr,bs],color=!myct.c5[mlr]

  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1
  multiplot,/doyaxis
endfor
multiplot,default

end
;========================================================
pro ldzentner
!key.hostz=0 & z=0. & csub=8.5
yita=1. & mhost=1e12  & epson=0.5 
mrv=[0.01,0.05,0.2] & nm=n_elements(mrv)
mrvname=string(mrv,format='(f4.2)')
ros_sub=ros_nfw(z,csub)
char=1.2
device,filename='orbits/ldzentner.ps',/encapsul,/color,xsize=10,ysize=9
!p.charsize=1.2
plot,findgen(10),findgen(10),xrange=[0,4],yrange=[1,23],$
/nodata,xtitle='t [Gyr]',ytitle='ln!mL!x (Z03)',$
position=[0.18,0.17,0.97,0.97]
for mlr=2,2 do begin
  dir='tradata/bs0rs0_ld3zentner_'+!wdir.mlr[mlr]+'/'
  for km=0,nm-1 do begin
    mr=mrv[km]
    tra_info=read_tra_info(dir,mr,mhost,mrk)
    tra=read_tra(dir,mrk,tra_info,epson,yita)
    nstep=n_elements(tra)
 
    msubi=mr*mhost
    rs_sub=r_s(csub,msubi,z) 
    mx=tra.m/msubi < 1.
    rx=Rbounding(mx,fltarr(nstep)+csub,th=1)
    Rbnd=rx*rs_sub
    Ixsub=0.10947*rx^3.989/(1+0.90055*rx^1.099+0.03568*rx^1.189+0.06403*rx^1.989)
    lnlda=alog(tra.r/Rbnd)+Ixsub/gx(rx)^2 ; & print,lnlda    
    oplot,tra.t,lnlda,color=!myct.c7[km],linestyle=km
  endfor
endfor
labeling,0.5,0.9,0.12,0.08,'m/M='+mrvname,/lineation,$
ct=!myct.c7,linestyle=indgen(5)
end
;=======================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro mfunc_zentner
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
;  m_un,  m_ev,tfunc,  rfunc,       rmfunc,      efunc
ml={mun:-4.,     mev:-4., t:0., r:alog10(0.01), ri:alog10(0.01), rm:alog10(0.01), e:0.01}
mh={mun:-0.35, mev:-1., t:1., r:alog10(1.),   ri:alog10(1.),   rm:alog10(1.) ,  e: 1.}
nbin={mun:11,  mev:11,  t:10, r:        10,   ri:        10,   rm:    10,       e: 10}
dbin=ml
for i=0,n_tags(ml)-1 do dbin.(i)=(mh.(i)-ml.(i))/(nbin.(i)-1)
treedir='treedata'+!wdir.d+!wdir.mp+!wdir.d
enfit=0
mpname=!wdir.mp & !key.hostz=1-enfit
rmax=1. & rxle001=0
mp=0 & major=1
ml.mun=alog10(mres/mphalo)+0.1

char=1.2
halofunc=halofunc_sam(treedir[mp],nbin,ml,mh,dbin,mpname[mp],$
                      [2,2],[4,4],[2,2],xh,$
                      rmax=rmax,major=major,enfit=enfit,rxle001=rxle001)
halofunc0=halofunc_sim(nbin,ml,mh,dbin,xh)

device,filename='halofunc/mfunczentner.ps',/color,/encapsul,$
xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
!p.position=[0.17,0.2,0.98,0.97]
plot,xh.mev,halofunc0.m.ev.giocoli[*,1],linestyle=2,$              ;evolved, data of Giocoli 2007
xrange=[5e-5,0.5],yrange=[1.001e-2,4e2],/xlog,/ylog,$
xtickname=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'],$
ytickname=['10!u-2!n','10!u-1!n','1','10','10!u2!n'],$
xtitle='m/M' ;,ytitle='dn/dln(m/M)'
xyouts,0.055,(!p.position[1]+!p.position[3])/2,'dN/dln(m/M)',$
/normal,orientation=90,alignment=0.5
oplot,xh.mun,halofunc.m.un,linestyle=1     ;unevolved merger tree.
y=halofunc.m.ev[*,4,2,2]
oplot,xh.mev,y,color=!myct.blue ;,linestyle=ld-2 ;,thick=mlr*4

labeling,0.05,0.25,0.1,0.08,['unevolved SHMF','evolved SHMF, SIM','Model, Z03,05'],$
ct=[0,0,!myct.blue],linestyle=[1,2,0],/lineation,charsize=1
end
;====================================================================
;========================================================
;pro trazentner
;common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;!key.hostz=0 & z=0. & csub=8.5 & chost=8.5
;yita=1. & mhost=1e12 & mr=0.05 & epson=0.5 & msubi=mr*mhost
;rs_sub=r_s(csub,msubi,z) & rs_host=r_s(chost,mhost,z)
;vcmax_host=vcir(2.16,chost,mhost,rs_host*chost)
;ros_sub=ros_nfw(z,csub) & ros_host=ros_nfw(z,chost)
;sny=[1,2,7]
;char=1.2 
;for mlr=0,4 do begin
;  device,filename='orbits/trazentner_'+!wdir.mlr[mlr]+'.ps',$
;  /encapsul,/color,xsize=18,ysize=20
;  multiplot,[2,3],mxtitle='t [Gyr]',xgap=0.05,mxtitsize=char,$
;  mxtitoffset=0.5,/doyaxis
;  !p.charsize=char
;  dir='tradata/bs0rs0_zentner_'+!wdir.mlr[mlr]+'/'
;  tra_info=read_tra_info(dir,mr,mhost,mrk)
;  tra=read_tra(dir,mrk,tra_info,epson,yita)
;  nstep=n_elements(tra)
;  for psi=0,2 do begin
;    trai=tra.(sny[psi])
;    plot,tra.t,trai/trai[0]
;    multiplot,/doyaxis
;  endfor
;  lp=-deriv(tra.t,tra.l)
;  plot,tra.t,lp/lp[0]
;  multiplot,/doyaxis

;  mx=tra.m/msubi < 1.
;  rx=Rbounding(mx,fltarr(nstep)+csub,th=1)
;  Rbnd=rx*rs_sub
;  Ixsub=0.10947*rx^3.989/(1+0.90055*rx^1.099+0.03568*rx^1.189+0.06403*rx^1.989)
;  lnlda=alog(tra.r/Rbnd)+Ixsub/gx(rx)^2 ; & print,lnlda    
;  plot,tra.t,lnlda
;  multiplot,/doyaxis
;  
;  ro_h_r=ro_r(tra.r,rs_host,chost,ros_host)
;  xh=tra.r/rs_host
;  sigma=vcmax_host*1.4393*xh^0.354/(1+1.1756*xh^0.725)
;  X=tra.v/sigma/sqrt(2.) ;& if(X gt 10.)then print,X
;  X=temporary(X) < 4.01
;  IX=erf(X)-2*X*exp(-X^2)/sqrt(!pi)
;  f=4*!pi*lnlda*gaussc^2*tra.m*ro_h_r*IX/tra.v^2   ;M_{sun}kpc Gyr^{-2}
;  plot,tra.t,f/max(f),/ylog
;  multiplot,/doyaxis
;endfor
;multiplot,/default
;labeling,0.01,0.22,0.15,0.035,'A='+strmid(!wdir.mlr,0,3),/lineation,$
;ct=!myct.c5,linestyle=indgen(5)
;end
;====================================================================
;====================================================================

