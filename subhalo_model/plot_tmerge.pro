pro plot_tmerge
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] 
epson=[0.3,0.5,0.7] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
;print,tfit.taffoni

nmlr=10 & nld=10 & nbs=10
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=1. & lf=0.01
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
get_ID=0; not(fix(strmid(!wdir.bs[bs],2,1)))
index=[[2,3,1],[5,6,2],[3,6,2],[4,6,2]]
for ii=0,3 do begin 
  bs=index[0,ii] & ld=index[1,ii] & mlr=index[2,ii]
  tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
  tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                   get_ID=get_ID,rmax=rmax,lf=lf,$
	                           extrapolate=0)
  if get_ID then ID_point[ld,mlr,bs]=IDp
endfor
help,tsam
;===merger time dependence on mass ratio
;==============================================================
psname=['nex4','bs1gs0.3p0.05']
k=ny-1 &  char=1.1 & mlr=2
for psi=0,1 do begin
  device,filename='tmerge/t_'+psname[psi]+'.ps',$
  /encapsul,/color,xsize=18,ysize=6 ;,xoffset=1.5,yoffset=10
  multiplot,[3,1],mxtitle='!mm!x!di!n',mxtitsize=1.2,$
  mxtitoffset=0.3,mytitle='T!ddf!n /Gyr',mytitsize=1.2,$
  mytitoffset=-2.5,xgap=0.025,/doyaxis,myposition=[0.05,0.22,0.99,0.97]
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  for j=0,nu-1 do begin  ;epson
    t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
    plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
    xtickname=[' ','0.1',' ','0.2',' ','0.3']
    if psi eq 0 then begin
      ;oplot,mratio,tfit.jiang[*,j,k],linestyle=1
      ;  oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
      oplot,mratio,tsam[*,j,k,6,2,5] ;,thick=8  ;M2, A=3.5
      ;  oplot,mratio,tsam[*,j,k,3,1,2],color=!myct.blue          ;M1, A=1
    endif else begin
      for bs=3,4 do $
      oplot,mratio,tsam[*,j,k,6,2,bs],thick=4*(5-bs)  ;Model "M2" with baryon
    endelse
    xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.2
    multiplot,/doyaxis
  endfor
  multiplot,/default
  if psi eq 0 then begin
    labeling,0.85,0.88,0.05,0.12,['M2','BK08'],$
    /lineation,linestyle=[0,2],charsize=char
  endif else begin
    xyouts,0.84,0.8,'Baryon',/normal,charsize=char
    labeling,0.85,0.77,0.05,0.12,['0.05','0.3','BK08'],$
    /lineation,linestyle=[0,0,2],thickv=[4,8,4],charsize=char
  endelse
endfor


end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ld=6 & mlr=2
;device,filename='tmerge/t_bs1gs0.3p0.05.ps',/encapsul,/color,xsize=18,ysize=6.5 ;,xoffset=1.5,yoffset=10
;multiplot,[3,1],mxtitle='m(0)/M(0)',mxtitsize=char,mxtitoffset=0.3,$
;mytitle='T!ddf!n /Gyr',mytitsize=char,mytitoffset=-2.5,$
;xgap=0.025,/doyaxis,myposition=[0.05,0.2,0.99,0.95]
;!p.charsize=char
;for j=0,nu-1 do begin  ;epson
;  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
;  plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
;  xtickname=[' ','0.1',' ','0.2',' ','0.3']
;  for bs=3,4 do begin
;    oplot,mratio,tsam[*,j,k,ld,mlr,bs],color=!myct.blue,thick=4*(5-bs)
;  endfor
;;  oplot,mratio,tsam[*,j,k,ld,mlr,5],color=!myct.blue
;  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1
;  multiplot,/doyaxis
;endfor
;multiplot,/default
;xyouts,0.84,0.79,'Baryon',/normal,color=!myct.blue,charsize=1.1
;labeling,0.85,0.77,0.05,0.12,['0.05f!db!n','0.3f!db!n','BK08'],/lineation,$
;linestyle=[0,0,2],ct=[198,198,0],thickv=[4,8,4],charsize=1.1

