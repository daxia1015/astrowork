pro heating_ps
!key.hostz=0
mi=1e10 & mx=[0.5,0.1,0.05,0.01] & c=8.5 & z=0.
n=n_elements(mx)
rs=r_s(c,mi,z) & !m.rs_sub=rs ;& print,rs
ros_sub=ros_nfw(z,c)
r=10^(findgen(101)*alog10(c*rs)/100)-0.99
x=r/rs
device,filename='heating/ro_r_hayashi.eps',/color,/encapsul,$
xsize=10,ysize=8
ro0=ro_r(r,rs,c,ros_sub)
plot,x,ro0,/xlog,/ylog,xtitle='r/r!ds!n',ytitle='!mr!x(r)',$
position=[0.21,0.2,0.98,0.98]
for i=0,n-1 do begin
  c_th=c_heating(mx[i])
  ro=ro_r(r,rs,c,ros_sub,th=1,c_th=c_th)
  oplot,x,ro,linestyle=i+1,color=!myct.c4[i]
endfor
labeling,0.01,0.42,0.14,0.09,['NFW','0.5','0.1','0.05','0.01'],/lineation,$
linestyle=indgen(n+1),ct=[0,!myct.c4]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro test_heating
!key.hostz=0
mi=1e11 & mx=[1e-3,0.01,0.05,0.1,0.3,0.5,0.8,1] & c=8.5 & z=0.
n=n_elements(mx)
rs=r_s(c,mi,z) & !m.rs_sub=rs ;& print,rs
ros_sub=ros_nfw(z,c)
r=10^(findgen(101)*alog10(c*rs)/100)-0.99
x=r/rs
device,filename='heating/m_r_th.ps',/color,/encapsul,xsize=15,ysize=12
mr0=mhalo_r(r,rs,c,mi)
plot,r,mr0/mi,yrange=[1e-3,1.1] ;,/ylog
for i=1,n-1 do begin
    !m.c_th=c_heating(mx[i])
    m=qromb('ror2',0.,x,eps=1e-3); & print,'m',m
    m=4*!pi*!m.c_th[1]*ros_sub*rs^3*m 
    oplot,r,m/mi,color=150+i*3
    oplot,r,mx[i]+fltarr(100),thick=1,linestyle=1,color=150+i*3

    m=mhalo_r(r,rs,c,mhalo,th=1,c_th=!m.c_th,ros_sub=ros_sub)
    oplot,r,m/mi,color=150+i*3,linestyle=2,thick=10
endfor

end
