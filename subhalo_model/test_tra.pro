pro test_lp
tradir='tradata/bs0rs0_M0mt_2.5mlr/'
mhost=1e12 & mr=0.1 & j=3 & k=2

tra_info=read_tra_info(tradir,mr,mhost,mrk)
tra=read_tra(tradir,mrk,tra_info,!epsonv[j],!yitav[k])
;n=tra_info[j].nstep
;omega=tra.l/tra.r^2 
;Torb=2*!pi/omega
;dt=tra[1:n-1].t-tra[0:n-2].t
;check=2.5*dt/Torb[0:n-2]
;print,where(check ge 1)
lf=tra.l/tra[0].l
tmin=truncation(lf,0.1,tra.t) & tmax=max(tra.t)
device,filename='orbits/lp.ps',/color,/encapsul,xsize=18,ysize=14.5
multiplot,[2,2],xgap=0.04,/doyaxis
xyouts,0.1,(!p.position[1]+!p.position[3])/2,'j(t)/j(0)',/normal,align=0.5,orien=90
for psi=0,1 do begin
  plot,tra.t,lf,xrange=[tmin*psi,tmax],yrange=[-0.05,1-psi*0.9]
;  y=smooth(lf,3)
;  oplot,tra.t,y,color=!myct.pink,thick=1
  multiplot,/doyaxis
endfor
xyouts,0.1,(!p.position[1]+!p.position[3])/2,'[dj(t)/dt]/[dj(t=0)/dt]',/normal,align=0.5,orien=90
for psi=0,1 do begin
  lp=-deriv(tra.t,tra.l)
  lpf=lp/lp[0]
;  temp=lpf[tra_info[j].nstep-7]
;  lpf[tra_info[j].nstep-6:tra_info[j].nstep-1]=temp
  plot,tra.t,lpf,xrange=[tmin*psi,tmax],yrange=[-0.1,max(lpf)^(1-psi)]
;  y=smooth(lpf,3) ;& print,width
;  oplot,tra.t,y,color=!myct.pink,thick=1

;  y=smooth(tra.l,3)
;  y=-deriv(tra.t,y)
;  y=y/y[0]
;  y=smooth(y,3) ;& print,width
;  oplot,tra.t,y,color=!myct.blue,thick=1
  multiplot,/doyaxis
endfor
multiplot,/default
end
;=====================================
;==============================================================
pro test_tra
!except=2 
!key.hostz=0 & !m.z=0
mhost=1.e12 & ti=0. & chost=8.5 & csub=8.5 & dtmin=0. & dtmax=0.4
eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
mf=1e4 & tf=45. & !key.bs=0 & halt=6-!key.bs 
rmax=1. & lfc=0.01 
ldv=[5.,4.,3.] & mlrv=[1.,2.5,3.5,6.,10.]
nld=n_elements(ldv) & nmlr=n_elements(mlrv)
halosetting,1
tlist=tlist_bk(np)
ntra=2*nld*nmlr*np
tag='tra'+strtrim(sindgen(ntra),2)
ktra=0
for k=0,0 do begin
  !ld.i=k
  for ld=0,nld-1 do begin
    !ld.x=ldv[ld] & print,'lnlda=',!ld.x,k
    for mlr=2,2 do begin
      !mlr=mlrv[mlr] ;& print,'mlr=',!mlr
      for i=9,9 do begin
        ;print,'bki=',i
        tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
                       tlist[i].yita,tlist[i].es,mf,ti,tf,0.1,dtmin,dtmax,$
                       eps,halt=halt,df=1,td=1,th=1,bs=!key.bs)
        print,max(tra.t),trefine(tra,rmax=rmax,lfc=lfc,/extrapolate)
        if ktra eq 0 then trav={tra0:tra} else trav=create_struct(trav,tag[ktra],tra)
        ktra++
      endfor
    endfor
  endfor
endfor
help,trav,/struc
sny=[1,2,7,2] & ytit=['m(t)/m(0)','j(t)/j(0)','r(t)/R!dvir!n','dj(t)/dt']
device,filename='bk08fit/tra_ld0_c.ps',/encapsul,/color,xsize=18,ysize=15
multiplot,[2,2],xgap=0.05,ygap=0.03,/doxaxis,/doyaxis
!p.charsize=1.2
for psi=0,3 do begin
  j=sny[psi]
  plot,findgen(100)*0.1,findgen(100)*0.01,/nodata,$
  xrange=[0+(psi eq 3)*0,15],yrange=[-0.05,1+(psi eq 3)*2],$
  ytitle=ytit[psi]
  for itra=0,ktra-1 do begin
    tra=trav.(itra)
    if psi eq 3 then begin
      lp=-deriv(tra.t,tra.l) ;& print,min(lp,index)
      oplot,tra.t,lp/lp[0],color=!myct.c7[itra]
    endif else oplot,tra.t,tra.(j)/tra[0].(j),color=!myct.c7[itra]
  endfor
  multiplot,/doxaxis,/doyaxis
endfor
multiplot,/default
end
