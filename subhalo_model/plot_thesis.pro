pro plot_thesis_intro
char=1.1 &  nmp=5
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=[0,15],major=1
help,halofunc.m.ev
mpv=indgen(nmp)+2
ld=7 & bs=0 


device,filename='thesis/intro/mfunc_intro.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.mun,halofunc0.m.un.giocoli,/nodata,$              ;evolved, data of Giocoli 2007
xrange=[5e-5,0.5],yrange=[2e-2,4e2],/xlog,/ylog,$
xtickname=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'],$
ytickname=['10!u-1!n','1','10','10!u2!n'],$
xtitle='m/M',position=[0.18,0.16,0.98,0.97];,ytitle='dn/dln(m/M)'
xyouts,0.06,0.58,'dN/dln(m/M)',/normal,orient=90,align=0.5,charsize=1.2

for i=0,1 do begin
  ihost=1+i*3
  oplot,xh.mun,halofunc.m.un[*,i*5],linestyle=i,color=!myct.red
  oplot,xh.mev,halofunc0.m.ev.giocoli[*,ihost],linestyle=i,color=!myct.blue
endfor
labeling,0.02,0.3,0.1,0.08,['Cluster, UN','MW, UN','Cluster, EV','MW, EV'],$
/lineation,linestyle=[1,0,1,0],charsize=1,$
ct=[!myct.red,!myct.red,!myct.blue,!myct.blue]
;========================================================
;========================================================
;=========================================================
ld=7  & mlr=2 & mpi=0
device,filename='thesis/intro/rfunc_intro.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.r,halofunc0.r.DM,/xlog,/ylog,$                       ;unevolved, fitted by Giocoli 2007
xrange=[0.05,1],yrange=[0.003,1.1],$
position=[0.21,0.17,0.96,0.97],/nodata,$
xtitle='r/R!dvir!n',ytitle='N(<r)/N(<R!dvir!n)'
oplot,xh.r,halofunc0.r.einasto200
;oplot,xr,halofunc0.r.diemand[*,mc],linestyle=2
;oplot,xr,halofunc0.r.einasto50,linestyle=3
;oplot,xh.r,halofunc0.r.DM,linestyle=5
oplot,xh.r,halofunc0.r.MW,psym=-6
;shadowing,xh.r,halofunc0.r.diemand[*,1],halofunc0.r.einasto50,density=8,thick=3
oplot,xh.r,halofunc.r.mcf[*,7,2,2,1,mpi],linestyle=1
oplot,xh.r,halofunc.r.mcf[*,7,2,0,0,mpi],linestyle=2


labeling,0.6,0.4,0.16,0.08,'MW',/lineation,/symboling,psym=6
labeling,0.6,0.31,0.16,0.08,['TB05b','Z05','SIM'],$
/lineation,linestyle=[1,2,0]
;=======================================================
;=======================================================
out=read_satellite('halofunc/plotdata/MW_satellite.dat')
a=0.684 & b=5.667 
mag=findgen(101)*0.2-20
Rlim=(3/(4*!pi*0.194))^(1./3.)*10^((-a*mag-b)/3+3)
device,file='thesis/intro/sate_intro.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char

plot,out.mag,out.dsun,xrange=[-20,0],yrange=[0,270],$
xtitle='M!dv!n',ytitle='d / kpc',position=[0.2,0.18,0.97,0.97],$
psym=6
;for i=0,1 do begin
;  sn=where(out.ID eq i*2+1998)
;  oplot,out[sn].mag,out[sn].dsun,psym=i+5
;endfor
oplot,mag,Rlim
;labeling,0.05,0.9,0.05,0.1,['Classical','Ultra-faint'],/symboling,psym=[5,6]
end
;========================================================
;=========================================================
pro plot_thesis_dft
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=!epsonv & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
;print,tfit.taffoni

nmlr=10 & nld=15 & nbs=15
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=1. & lf=0.01 & get_ID=0
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
bsv=[1,6,7,8,9]
for bs=6,10,4 do begin 
  !m.irs=float(strmid(!wdir.bs[bs],5,4))
  for ld=10,10 do begin
    for mlr=1,8 do begin
      if (mlr eq 2)or(mlr eq 3) then continue
      tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                       get_ID=get_ID,rmax=rmax,lf=lf,$
				       extrapolate=!m.irs eq 0)
    endfor
  endfor
endfor
help,tsam
char=1 & k=ny-1
;=====================================================
;tfit from BK08, J08, T03, LC93
ld=10 & mlrv=[4,1,5,6,7,8] & bs=6
;tick0=replicate(' ',6) & tick1=[' ','0.1',' ','0.2',' ','0.3']
;xtick=[[tick0],[tick0],[tick0],[tick1],[tick1],[tick1]]
xtit='R!dm!n!u-1!n=M!ds!n(0)/M!dh!n(0)' & ytit='T!ddf!n /Gyr'
psname='t'+['prior','mlr','mlr00','mlr3','mlr_th01']+'.eps'
for fig=0,4 do begin
  device,filename='thesis/dft/'+psname[fig],/encapsul,/color,xsize=16,ysize=11 ;,xoffset=1.5,yoffset=10
  multiplot,[3,2],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.5,$
  mytitle=ytit,mytitsize=1.1,mytitoffset=-2.3,$
  xgap=0.03,ygap=0.04,/doxaxis,/doyaxis,$
  myposition=[0.05,0.1,1,1]
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  for j=0,5 do begin  ;epson
    t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
    plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
    xtickname=[' ','0.1',' ','0.2',' ','0.3'],/nodata
    case fig of 
      0:begin
        oplot,mratio,tfit.lc[*,j,k],linestyle=1
        oplot,mratio,tfit.taffoni[*,j,k],linestyle=4
        oplot,mratio,tfit.jiang[*,j,k],linestyle=3
	oplot,mratio,t,linestyle=2
	charv=['LC93','T03','J08','BK08'] & lstyle=[1,4,3,2] & thickv=4
      end
      1:begin
      	for ii=1,3 do begin
          mlr=mlrv[ii]
          oplot,mratio,tsam[*,j,k,ld,mlr,bs],thick=ii*3
	  charv='A='+['1','2','3'] & lstyle=0 & thickv=[3,6,9]
        endfor
      end
      2:begin
        oplot,mratio,tfit.lc[*,j,k],linestyle=1
      	oplot,mratio,tsam[*,j,k,ld,4,bs] 
	charv=['A=0','LC93'] & lstyle=[0,1] & thickv=4
      end
      3:begin
        oplot,mratio,tfit.jiang[*,j,k],linestyle=3
	oplot,mratio,t,linestyle=2
      	oplot,mratio,tsam[*,j,k,ld,6,bs]       
	charv=['J08','BK08','A=3'] & lstyle=[3,2,0] & thickv=4
      end
      4:begin
      	for ii=1,3 do begin
          mlr=mlrv[ii]
          oplot,mratio,tsam[*,j,k,ld,mlr,bs],thick=ii*3
          oplot,mratio,tsam[*,j,k,ld,mlr,10],thick=ii*3,linestyle=1
	  charv='A='+['1','2','3'] & lstyle=0 & thickv=[3,6,9]
        endfor
      end
      else:print,'out of range'
    endcase
    xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
    multiplot,/doxaxis,/doyaxis
  endfor
  multiplot,/default
  labeling,0.85,0.38,0.05,0.05,charv,/lineation,linestyle=lstyle,thickv=thickv
endfor
;==========================================================
;==========================================================
bs=10
device,filename='thesis/dft/tmlr0.eps',/encapsul,/color,xsize=16,ysize=5.8 ;,xoffset=1.5,yoffset=10
multiplot,[3,1],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.,$
mytitle=ytit,mytitsize=1.1,mytitoffset=-1.5,$
xgap=0.03,myposition=[0.06,0.22,1,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
jv=[0,2,5]
for jj=0,2 do begin  ;epson
  j=jv[jj]
  t=tfit.lc[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mratio,t,yrange=[0.1*tmin,2*tmax],linestyle=1,$
  xtickname=[' ','0.1',' ','0.2',' ','0.3']
  oplot,mratio,tsam[*,j,k,ld,4,bs] 
  xyouts,0.28,1.7*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.85,0.85,0.05,0.12,['A=0','LC93'],/lineation,linestyle=[0,1]
;==========================================================
;==========================================================

xtit=['R!dm!n!u-1!n=M!ds!n(0)/M!dh!n(0)','!me!x (circularity)']
ytit=['T!ddf!n / T!ddf!n(R!dm!n=20)','T!ddf!n / T!ddf!n(!me!x=1)']
ld=10 & bs=10 & mlr=4
device,filename='thesis/dft/tdep0.eps',/encapsul,/color,xsize=16,ysize=7
multiplot,[2,1],xgap=0.05,myposition=[0.05,0.17,1,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
k=ny-1
for col=0,1 do begin
  if col eq 0 then begin
    x=mratio & i=0 & j=4
    y1=tfit.lc[*,j,k]/tfit.lc[i,j,k]
    y2=tsam[*,j,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    tick=[' ','0.1',' ','0.2',' ','0.3']
  endif else begin
    x=epson & i=0 & j=5
    y1=tfit.lc[i,*,k]/tfit.lc[i,j,k]
    y2=tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    tick=['0.2','0.4','0.6','0.8','1.0']
  endelse
  plot,x,y1,linestyle=1,xtitle=xtit[col],ytitle=ytit[col],$
  yrange=[0.1,1],xtickname=tick
  oplot,x,y2 ;,linestyle=ii
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.25,0.95,0.05,0.1,['A=0','LC93'],/lineation,linestyle=[0,1]
;========================================================
;========================================================
ld=10 & bs=6 & mlr=1 & j=nu-1 & k=ny-1
device,filename='thesis/dft/tdep1.eps',/encapsul,/color,xsize=11,ysize=9
!p.charsize=char & !x.charsize=char & !y.charsize=char
imv=[0,7] & lstyle=[5,4,3,2]
plot,epson,epson,xrange=[0.05,1],yrange=[0.1,1],/nodata,$
xtitle=xtit[1],ytitle=ytit[1],position=[0.14,0.13,0.96,0.98];,linestyle=lstyle[im]
for im=0,1 do begin
  i=imv[im]
  oplot,epson,tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs],psym=im+5
endfor
oplot,epson,tfit.bk[i,*,k]/tfit.bk[i,j,k],linestyle=2
oplot,epson,epson^0.4/epson[j]^0.4,linestyle=5    ;Colpi99
labeling,0.68,0.3,0.05,0.08,['R!dm!n!u-1!n=0.05','R!dm!n!u-1!n=0.3'],$
/symboling,psym=[5,6]
labeling,0.66,0.13,0.13,0.07,['C99','BK08'],/lineation,linestyle=[5,2]

;==========================================================
;==========================================================
;========================================================
;========================================================


end
;========================================================
;========================================================
pro plot_thesis_dft_tra
yita=1. & mhost=1e12 & mr=0.1 & epson=0.5  & bs=6 & ld=10
snc=[1,2] & mlrv=[4,1,5,6,7,8] & char=1
ytit=['M!ds!n(t)/M!ds!n(0)','j(t)/j(0)']
;=========================================================
device,/color,/encapsul,filename='thesis/dft/mj.eps',$
xsize=16,ysize=7

multiplot,[2,1],xgap=0.06,myposition=[0.05,0.19,1,0.97] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
for row=0,1 do begin
  plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
  xrange=[0,9.5],yrange=[0.01,1.05],xtitle='t / Gyr',$
  ytitle=ytit[row] ;,ylog=row eq 0
  
  j=snc[row]
  for ii=1,3 do begin
    mlr=mlrv[ii]
    for bs=6,10,4 do begin
    dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
    tra_info=read_tra_info(dir,mr,mhost,mrk)
    tra=read_tra(dir,mrk,tra_info,epson,yita)
    t=tra.t & y=tra.(j)/tra[0].(j)
    oplot,t,y,thick=ii*3,linestyle=bs/10
    endfor
  endfor
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.8,0.95,0.05,0.1,'A='+['1','2','3'],/lineation,$
thickv=[3,6,9]
;========================================================
;========================================================
@bk08fit/BK08j.dat
ijv=[2,4,5] & ipv=[14,8,7] ;& gav=[0.1,0.5,1.]
para1='R!dm!n!u-1!n='+['0.05','0.1','0.1']
para2='!me!x='+['0.65','0.65','0.46']

device,filename='thesis/dft/bk.eps',/encapsul,/color,xsize=16,ysize=6
multiplot,[3,1],mxtitle='t / Gyr',mxtitsize=1.1,$
mytitle='j(t)/j(0)',mytitsize=1.1,mytitoffset=-1,$
myposition=[0.1,0.2,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for col=0,2 do begin
  ij=ijv[col] & ip=ipv[col]
  plot,tseries,jv[*,ij],linestyle=2,xrange=[0,9.99],yrange=[0.01,1.05]
  for mlr=1,3 do begin 
    mlrs=strmid(strtrim(string(float(mlr)),2),0,3)
    file='tradata/raa/'+mlrs+'mlr_'+strtrim(string(ip),2)
    info=read_tra_any(file,tra=tra)
    oplot,tra.t,tra.l/tra[0].l,thick=mlr*3
  endfor
  xyouts,9.5,0.9,para1[col],align=1
  xyouts,9.2,0.78,para2[col],align=1
  multiplot 
endfor
multiplot,/default
labeling,0,0.4,0.05,0.1,['BK08','A='+['1','2','3']],$
/lineation,thickv=[4,3,6,9],linestyle=[2,0,0,0]

end
;==============================================================
;==============================================================
;==============================================================
pro plot_thesis_bk_j
tlist=tlist_bk(np)
@bk08fit/BK08j.dat
lns=['ln!mL!x=','ln!mL!x=-ln!mm!x+']
ipv=[16,15,14,13,8,7] ;& gav=[0.1,0.5,1.]
xtit='t / Gyr' & ytit='j(t)/j(0)' & trange=[0,9.99] & jrange=[1e-3,1.05]
;===================================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
i=4 & ip=ipv[i] & char=1.1
;plot badj for mlrv =============================
name=['ld1_0.00_1.0mlr_8','ld1_0.00_3.5mlr_8','ld1_0.00_10.mlr_8']
device,filename='thesis/fitld/badj_mlrv.eps',/encapsul,/color,xsize=10,ysize=9
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange,$
xtitle=xtit,ytitle=ytit,position=[0.18,0.16,0.98,0.97]
for j=0,2 do begin
  info=read_tra_any('bk08fit/tradata/badj/'+name[j],tra=tra)
  oplot,tra.t,tra.l/tra[0].l,thick=3+j*4
;  print,info.ld
endfor
labeling,0.02,0.3,0.1,0.08,['1','3.5','10'],/lineation,$
thickv=[3,7,11]
xyouts,0.05,0.36,'A='
;=============================================================
;plot badj for ldv  ==================================
name=['ld0_3.00_3.5mlr_8','ld0_4.00_3.5mlr_8','ld0_5.00_3.5mlr_8',$
      'ld1_0.00_3.5mlr_8','ld1_1.00_3.5mlr_8','ld1_2.00_3.5mlr_8']
charv=[['3','4','5'],['0','1','2']]
device,filename='thesis/fitld/badj_ldv.eps',/encapsul,$
/color,xsize=10,ysize=5.8
multiplot,[2,1],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.8,$
myposition=[0.17,0.25,0.98,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for ldi=0,1 do begin
  plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
  for j=0,2 do begin
    kname=j+ldi*3
    info=read_tra_any('bk08fit/tradata/badj/'+name[kname],tra=tra)
    oplot,tra.t,tra.l/tra[0].l,thick=3+j*4
;    print,info.ld
  endfor
  labeling,0.7,0.8,0.13,0.1,charv[*,ldi],/lineation,thickv=[3,7,11]
  xyouts,0.88,0.87,lns[ldi],alignment=1,charsize=1
  multiplot
endfor
multiplot,/default
;==================================================================
;==================================================================
device,filename='thesis/fitld/goodj1.eps',/encapsul,/color,xsize=10,ysize=9.5
multiplot,[2,2],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.8,$
myposition=[0.16,0.15,0.98,0.98]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for i=4,3,-1 do begin
  ip=ipv[i]
  for ldi=0,1 do begin
    plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
    file='bk08fit/tradata/nex4_best_ld'+string(ldi,format='(i1)') $
         +'/3.5mlr_'+strtrim(string(ip),2)
    info=read_tra_any(file,tra=tra) 
    oplot,tra.t,tra.l/tra[0].l
;    print,info.ld
    xyouts,9.6-(1-ldi)*0.3,0.9,lns[ldi]+string(info.ld,format='(f3.1)'),$
    charsize=1,align=1
    multiplot
  endfor
endfor
multiplot,/default
;===============================================================
;===============================================================
mrv=['0.025','0.05','R!dm!n!u-1!n=0.1'] & esv=['0.65','0.46']
device,filename='thesis/fitld/goodj2.eps',/encapsul,/color,xsize=10,ysize=13.5
multiplot,[2,3],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.5,$
myposition=[0.17,0.12,0.9,0.9]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for i=0,5 do begin
  plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
  ip=ipv[i] 
  file='bk08fit/tradata/nex4_fit_ld1/3.5mlr_'+strtrim(string(ip),2)
  info=read_tra_any(file,tra=tra)
  oplot,tra.t,tra.l/tra[0].l
;  print,info.ld
  if i mod 2 eq 1 then xyouts,0.98,(!p.position[1]+!p.position[3])/2,$
    mrv[i/2],align=0.5,orient=90,/normal
  if i le 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,$
    '!me!x='+esv[i],align=0.5,/normal
  xyouts,0.5,0.1,'C='+strtrim(string(info.ld,format='(f5.2)'),2)
  multiplot 
endfor
multiplot,/default

end
;===============================================================
;===============================================================
;===============================================================
;===============================================================
pro plot_thesis_tmerge
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] 
epson=[0.3,0.5,0.7] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
;print,tfit.taffoni

nmlr=10 & nld=10 & nbs=10
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=1. & lf=0.01
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
get_ID=0; not(fix(strmid(!wdir.bs[bs],2,1)))
index=[[2,3,1],[5,6,2],[3,6,2],[4,6,2]]
for ii=0,3 do begin 
  bs=index[0,ii] & ld=index[1,ii] & mlr=index[2,ii]
  tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
  tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                   get_ID=get_ID,rmax=rmax,lf=lf,$
	                           extrapolate=0)
  if get_ID then ID_point[ld,mlr,bs]=IDp
endfor
help,tsam
;===merger time dependence on mass ratio
;==============================================================
psname=['nex4','bs1gs0.3p0.05']
k=ny-1 &  char=1.1 & mlr=2
for psi=0,1 do begin
  device,filename='thesis/fitld/t_'+psname[psi]+'.eps',$
  /encapsul,/color,xsize=18,ysize=6 ;,xoffset=1.5,yoffset=10
  multiplot,[3,1],mxtitle='!mm!x!di!n',mxtitsize=1.2,$
  mxtitoffset=0.3,mytitle='T!ddf!n /Gyr',mytitsize=1.2,$
  mytitoffset=-2.5,xgap=0.025,/doyaxis,myposition=[0.05,0.22,0.99,0.97]
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  for j=0,nu-1 do begin  ;epson
    t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
    plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
    xtickname=[' ','0.1',' ','0.2',' ','0.3']
    if psi eq 0 then begin
      ;oplot,mratio,tfit.jiang[*,j,k],linestyle=1
      ;  oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
      oplot,mratio,tsam[*,j,k,6,2,5] ;,thick=8  ;M2, A=3.5
      ;  oplot,mratio,tsam[*,j,k,3,1,2],color=!myct.blue          ;M1, A=1
    endif else begin
      for bs=3,4 do $
      oplot,mratio,tsam[*,j,k,6,2,bs],thick=4*(5-bs)  ;Model "M2" with baryon
    endelse
    xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.2
    multiplot,/doyaxis
  endfor
  multiplot,/default
  if psi eq 0 then begin
    labeling,0.85,0.88,0.05,0.12,['M2','BK08'],$
    /lineation,linestyle=[0,2],charsize=char
  endif else begin
    xyouts,0.84,0.8,'Baryon',/normal,charsize=char
    labeling,0.85,0.77,0.05,0.12,['0.05','0.3','BK08'],$
    /lineation,linestyle=[0,0,2],thickv=[4,8,4],charsize=char
  endelse
endfor


end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;================================================================
pro plot_thesis_jmr
yita=1. & mhost=1e12 & mr=0.05 & epson=0.5  & bs=2
Mname=num2str(mr,format='(f4.2)')
epsonname=num2str(epson,format='(f3.1)')
snc=[1,7,2] & char=1.1
mytit=['m(t)/m(0)','r(t)/R!dvir!n','j(t)/j(0)']
tmax=12.999
;=========================================================
lddir=['ld1Mtmt0.0','ld0'+['3.0','4.0','5.0']]
charv=[['1','3.5','10'],['3','4','5']]
charv1=['A=','ln!mL!x=']

ldv=[[0,0],[1,3]] & mlrv=[[1,3],[2,2]]
device,/color,/encapsul,filename='thesis/fitld/mr.eps',$
xsize=16,ysize=13
multiplot,[2,2],mxtitle='t / Gyr',mxtitsize=char,$
mxtitoffset=0.5,myposition=[0.15,0.15,0.98,0.98] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
for row=0,1 do begin
  j=snc[row]
  xyouts,0.04,(!p.position[1]+!p.position[3])/2,mytit[row],$
  align=0.5,/normal,orient=90
  for col=0,1 do begin
    plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
    xrange=[0,9.5],yrange=[0.011,1.1+(row eq 1)*0.35],ylog=j eq 1
    kk=0
    for ld=ldv[0,col],ldv[1,col] do begin
      for mlr=mlrv[0,col],mlrv[1,col] do begin
        dir='tradata/'+!wdir.bs[bs]+'_'+lddir[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
        tra_info=read_tra_info(dir,mr,mhost,mrk)
        tra=read_tra(dir,mrk,tra_info,epson,yita)
        t=tra.t & y=tra.(j)/tra[0].(j)
        if j eq 7 then begin
          n=n_elements(tra) & tf=max(tra.t)<tmax
          t=findgen(n*3)*tf/n/3
          y=spline(tra.t,y,t)
        endif
        oplot,t,y,thick=3+kk*4
	kk++
      endfor
    endfor
;    print,charv[*,col]
    if row eq 0 then begin
      labeling,0.1,0.25,0.1,0.08,charv[*,col],/lineation,thickv=[3,7,11]
      xyouts,0.2,0.31,charv1[col],alignment=1
    endif
    multiplot
  endfor
endfor
multiplot,/default

end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;================================================================
pro plot_thesis_halo
mh=10^(findgen(5)+11)
neto07=4.67/(mh/1e14)^0.11
maccio07=10^(1.02-0.109*alog10(mh/1e12))

bullock01=9.6/(mh/1e13)^0.13
device,/color,/encapsul,filename='thesis/halo/c_m.eps',$
xsize=12,ysize=10

plot,mh,bullock01,/xlog,linestyle=2
oplot,mh,neto07



end
