pro refine_itree,halon,trasv,nstepv,nhalo,haloout,go_on=go_on
;common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
tra=halon.tra & sny=[1,2,4] ;& sns=[1,2,4]
for k=0,2 do begin
  yk=sny[k] ;& yks=sns[k]
  sn=where((finite(tra.(yk)) eq 0)or(tra.(yk) lt 0),count)
  if count ne 0 then begin
    tra[sn].t=halon[sn].tf
    tra[sn].ID=0.1
;    for j=0,count-1 do begin
;      trav=(*trasv)[sn[j],0:nstepv[sn[j]]-1]
;      tra[sn[j]].(yk)=spline(trav.t,trav.(yks),tra[sn[j]].t)
;      if tra[sn[j]].(yk) le 0 then  begin
        case k of
	  0 : tra[sn].(yk)=0.1*halon[sn].mf
	  1 : tra[sn].(yk)=0.01*halon[sn].li
	  2 : tra[sn].(yk)=0.09
	else: message,'out of range: k'
	endcase 
;      endif
;    endfor
  endif
endfor
if go_on eq 0 then haloout=replicate(!halo.out,nhalo)
struct_assign,halon,haloout,/nozero  
rvir_h=r_vir(halon.mhost,halon.z)
chost=chalo(halon.mhost,halon.z)
rs_host=rvir_h/chost
tral=tra_s2l(tra,halon.mhost,rs_host,chost,rvir_h,nhalo,hprofile=!hprofile)
haloout.tra=tral
haloout.nstep=halon.nstep+nstepv
haloout.rf=tra.r*rvir_h/r_vir(halon.mhosti,halon.zi)
end
;==\|||=[][]]]]]]]]]]]]]]]]]]]]]]]][[[[[[[[[[[[[[[][][]=======
;===========================================================
;=================================================================
function initialize_itree,itree,nhalo,treen,haloout,tlev,trasv,$
                          nstepv,ntra,dti,go_on=go_on
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
halon=replicate(!halo.in,nhalo)
if go_on then begin
  struct_assign,haloout,halon,/nozero
  halon.ilev=find_level(halon.tra.t,tlev)
  sn=where(halon.tra.ID eq 0.4,count) 
  if count ne 0 then halon[sn].tra.ID=1 ;& print,sn04
  sn=where((halon.tra.ID gt 0.4)and(halon.tra.ID lt 1),count)
  if count ne 0 then halon[sn].tra.ID=halon[sn].tra.ID-0.4
  halon.dt=dti*abs(halon.tra.vr)/100. 
;  print,halon.dt
;-----------------------------------------------------  
;  ntra=max(nstepv)
endif else begin
;  help,treen,halon,/struc
  struct_assign,treen,halon,/nozero  ;halon.sub=treen.sub & halon.host=treen.host
  halon.msubi=treen.msub & halon.mhosti=treen.mhost ;& halon.mhost=treen.mhost
  halon.zi=treen.z
  ti=tGy(halon.zi)
  chost=chalo(halon.mhost,halon.zi)
  halon.tra=ini_tra(halon.msubi,halon.mhost,chost,$
                    halon.yita,halon.es,halon.zi,ti,hprofile=!hprofile)
  halon.li=halon.tra.l 
  halon.dt=dti
;--------------------------------------------------------------
endelse
if !ld.i le 1 then begin
  if strpos(!wdir.ld[!ld.ld],'fit') gt 0 then begin
    curveld,[[halon.msubi/halon.mhosti],[halon.es],[halon.yita]],$
    c,fldx,pder,/cknown
    halon.ld=fldx  
  endif else halon.ld=float(strmid(!wdir.ld[!ld.ld],3+!ld.i*4,3))
endif
;print,halon.ld,fldx
lf=0.05*halon.li
lf=temporary(lf) < 100. & lf=temporary(lf) > 10.
halon.lf=lf
halon.csub=chalo(halon.msubi,halon.zi)
halon.rs_sub=r_s(halon.csub,halon.msubi,halon.zi)
halon.ros_sub=ros_profile(halon.zi,halon.csub,a,b,hprofile=!hprofile) ;& print,halon.ros_sub
if strpos(!wdir.bs[!bsi],'rs') gt 0 then halon.mf=halon.msubi*gx(!m.irs)/gx(halon.csub)
if strpos(!wdir.bs[!bsi],'gs') gt 0 then halon.mbaryon=halon.msubi*!m.irs*omegab/omega0
if !key.bs or (!m.irs eq 0) then halon.mf=1e4
;for jtree=itree[0],itree[1] do begin
;  if jtree eq itree[0] then ihalo=0 else $
;  ihalo=total(nhalos[itree[0]:jtree-1],/int)
;  halon[ihalo:ihalo+nhalos[jtree]-1].itree=jtree
;endfor
;ntra=300L-go_on*100
;trasv=replicate(!halo.tras,nhalo,ntra)
;trasv=ptr_new(trasv)
nstepv=lonarr(nhalo)
return,halon
end
;============================================================
;=============================================================
;----evolving one tree, generating halo net------------------
pro evolve_itree,itree,zlev,tlev,nhalo,mhostz,n_levs0,treen,tsam,tra_f,$
                 haloout,trasv,nstepv,dti,dtmax,$
		 go_on=go_on,major=major,enfit=enfit
;nhalo=nhalos[itree] ;& help,nhalo
halon=initialize_itree(itree,nhalo,treen,haloout,tlev,trasv,$
                       nstepv,ntra,dti,go_on=go_on)
;-------------------------------------------------------------------
if enfit then begin
  mr=halo.msubi/halo.mhosti
  if (mr ge min(!mrv))and(mr le max(!mrv)) then begin
    tmerge=tfit(!mrv,tsam,mr,epson,yita)
    if(tmerge le trun)then tral.ID=0 
  endif
  if((mr ge min(!mrv))and(mr le max(!mrv))and(trun gt 0.)and(tral.ID ne 0))then begin
    tral=trafit(tra_i,mr,epson,yita,tra_f[*,*,*,ilev])
    tra_i.m=tra_i.m*exp(-trun*(ez(zlev[ilev])*dltcz(zlev[ilev])/dltc0)^0.5/3.)
    tral.ID=2
  endif
endif else begin
  dtmin=0. 
  evolve_halon,itree,halon,zlev,tlev,mhostz,n_levs0,dtmin,dtmax,nhalo,trasv,$
               nstepv,ntra,major=major
endelse
;-------------------------------------------------------------------
refine_itree,halon,trasv,nstepv,nhalo,haloout,go_on=go_on
end
;===============~!@!#@#$@#^$%&^(*(_()+_)_))*((&^%%^#$%$!@#!===========
;=====================================================================
;======================================================================
;----evolving trees with one specific condition------------------
pro evolve_tree,treedir,tradir,mpname,dti,dtmax,godir,$
                go_on=go_on,major=major,enfit=enfit
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
tree_series,treedir,zlev,nhalos0,n_levs0
treen_series,treedir,nhalos,n_levs,major=major
;help,total(nhalos),long(total(nhalos)),long(double(total(nhalos))),total(nhalos,/int)
tlev=tGy(zlev)
if enfit then begin
  tsam=tmerge_sam('tradata/'+tradir,!mrv,1.e12,!epsonv,!yitav,a)
  trun=tlev[0]-tlev
  tra_f=tra_final('tradata/'+tradir,trun)
endif

hdir='halofunc/'+!wdir.fit[enfit]+!wdir.d+tradir
hname=hdir+'halonet.'+mpname+!wdir.major[major]
;track=(!bsi eq 1)or(!bsi eq 2)
;openw,lun2,'halofunc/evolving/halo_track',/get_lun
for itree=0,ntree-1 do begin
  print,'itree=',itree
  readtree,treedir,nhalos0,n_levs0,mhostz=mhostz,itree=itree
;  nhalo=nhalos[itree] ;& print,nhalo
  if go_on then begin
    haloout=read_halon(godir,mpname,ns,major=major,itree=itree)
;    trasv=read_halon_track(godir,mpname,ns,nstepv,major=major,itree=i,/matrix)
  endif else begin
    treen=readtreen(treedir,nhalos,itree=itree,major=major)
  endelse
  evolve_itree,itree,zlev,tlev,nhalos[itree],mhostz,n_levs0,treen,tsam,tra_f,$
               haloout,trasv,nstepv,dti,dtmax,$
	       go_on=go_on,major=major,enfit=enfit
  if itree eq 0 then begin
    openw,lun0,hname,/get_lun
    writeu,lun0,nhalos 
;    openw,lun1,hname+'_track',/get_lun
;    writeu,lun1,nhalos
  endif else begin
    openu,lun0,hname,/get_lun
    out_b=(n_tags(!halo.out)-1+n_tags(!halo.tral))*4
    point_b=total(nhalos[0:itree-1],/int)*out_b+ntree*4
    point_lun,lun0,point_b
;    openu,lun1,hname+'_track',/get_lun
;    point_b=total(nhalos[0:itree-1],/int)*4
;    point_lun,lun1,ntree*4+point_b
  endelse;& help,out,/struct
  writeu,lun0,haloout  
;  writeu,lun1,nstepv  
;  for k=0,nhalo-1 do if nstepv[k] ge 1 then $
;    writeu,lun2,(*trasv)[k,0:nstepv[k]-1]
  free_lun,lun0
endfor
;if track then begin
;  point_lun,lun2,0
;  copy_lun,lun2,lun1,/eof
;  free_lun,lun1,lun2
;endif
end
;=================================================================
;---==-evolving trees----===------===------====--------
pro evolve_trees,treedir,major=major
!key.df=1 & !key.th=1 & !key.td=1
mpname=strmid(treedir,9,15)
dtiv=  [0.01, 0.05, 0.05,  0.05,  0.1]
dtmaxv=[ 0.5,  0.5,  0.4,  0.3,  0.3]

for enfit=0,0 do begin
  !key.hostz=1-enfit
  dir1='halofunc/'+!wdir.fit[enfit]+!wdir.d
  for bs=5,5 do begin
    !bsi=bs
    !key.bs=fix(strmid(!wdir.bs[bs],2,1)) & print,'bs=',!wdir.bs[bs],bs
    if strpos(!wdir.bs[bs],'th0') gt 0 then !key.th=0 else !key.th=1
    px=strpos(!wdir.bs[bs],'x')
    if px gt 0 then !key.nex=fix(strmid(!wdir.bs[bs],px+1,1)) else !key.nex=0
    !m.irs=float(strmid(!wdir.bs[bs],5,4))
    go_on=0 ;(bs eq 1)or(bs eq 2) ;& help,go_on
    for ld=3,3 do begin 
      !ld.ld=ld
      !ld.i=fix(strmid(!wdir.ld[ld],2,1)) & print,'lnlda=',!wdir.ld[ld]
      for mlr=2,2 do begin
        if(strpos(!wdir.ld[ld],'fit') gt 0)and $
	((strpos(!wdir.bs[bs],'x4') eq -1)or $
	(strpos(!wdir.mlr[mlr],'3.5') eq -1))then continue
        !mlr=float(strmid(!wdir.mlr[mlr],0,3)) & print,'mlr=',!mlr
        dir0='_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
        tradir=!wdir.bs[bs]+dir0
        if go_on then godir=dir1+!wdir.bs[bs-1]+dir0 
        evolve_tree,treedir,tradir,mpname,dtiv[mlr],dtmaxv[mlr],godir,$
	            go_on=go_on,major=major,enfit=enfit
      endfor
    endfor
  endfor
endfor
end

;k=0L & en0=0 & en1=0 & en3=0
;for ilev=n_lev-1,0,-1 do begin
;  !m.z=nplev[ilev].z & t_now=tlev[ilev]
  ;wait,1
;  en2=1
;----give the initial orbital parameters for the subhalos when they are accreted
;  sn=where((halon.li eq 0)and(halon.zi eq !m.z),count)
;  if count ne 0 then begin
;    for kk=0,count-1 do begin
;      k=sn[kk] 
      ;print,'k=',k
;      if en2 then last_k=k
;      en2=0
;      halon[k]=initialize_ihalo(k,halon[k],t_now,tsam,tra_f,trasv,enfit=enfit)
;      k=k+1
;      if k eq nhalo then break
;      en0=1
;    endfor
;  endif
;-----evolving the subhalos after they are accreted==============
;    if en3 then last_k=nhalo

;  sn=where((halon.li ne 0)and(halon.tra.t lt t_now),count)
;  if count ne 0 then begin
;    for ii=0,count-1 do begin
;      i=sn[ii]
;    endfor
;  endif
;  if en0 then en1=1
;  if k eq nhalo then en3=1 ;now all the subhalo have been accreted.
;endfor
;if go_on then halon[where(halon.tra.ID eq 3)].tra.ID=1


;      yita=yitas[k] & epson=epsons[k] & dt=dti
;      !m.chost=chalo(halon[k].mhost,halon[k].z)
;      tra_i=ini_tra(halon[k].msubi,halon[k].mhost,!m.chost,yita,epson,t_now,Rvir_h,dt,hprofile=hprofile)
;      halon[k].li=tra_i.l 
;      halon[k].dt=dt
;      if enfit then begin
;        mr=halon[k].msubi/halon[k].mhost
;        if (mr ge min(!mrv))and(mr le max(!mrv)) then begin
;          tmerge=tfit(!mrv,tsam,mr,epson,yita)
;          if(tmerge le trun)then tra_i.ID=0 
;        endif
;        if((mr ge min(!mrv))and(mr le max(!mrv))and(trun gt 0.)and(tra_i.ID ne 0))then begin
;          tra_i=trafit(tra_i,mr,epson,yita,tra_f[*,*,*,ilev])
;;         tra_i.m=tra_i.m*exp(-trun*(ez(nplev[ilev].z)*dltcz(nplev[ilev].z)/dltc0)^0.5/3.)
;          tra_i.ID=2
;        endif
;      endif
;      halon[k].tra=tra_i



;      tra_i=halo.tra
;      if(tra_i.ID eq 1)and(tra_i.t lt t_now)then begin
;        ;print,ilev,i
;        !m.mhost=halo.mhost 
;	!m.chost=chalo(!m.mhost,nplev[ilev].z)
;	!m.rs_host=r_s(!m.chost,!m.mhost,nplev[ilev].z)  ;initial r_s of subhalo and host halo
;        !vcmax.host=vcir(2.16,!m.chost,!m.mhost,!m.chost*!m.rs_host)
;        !m.rs_sub=halo.rs_sub & !m.csub=halo.csub & !m.msubi=halo.msubi 
;	!ros_sub=halo.ros_sub
;	dt=halo.dt & dtmax=tlev[0]-tra_i.t ;& eps=halo.eps
;	!key.td=halo.td
	;!enprint= i eq 341
	;if !enprint then print,ilev,tra_i,halo.dt,format='(i2,f7.3,5e12.3,f4.1,f7.3)'
;        tra_i=tra_lev(tra_i,!m.mhost,dt,halo.li,halo.mf,t_now,dtmin,dtmax);,eps)
	;if !enprint then print,ilev,tra_i,halo.dt,format='(i2,f7.3,5e12.3,f4.1,f7.3)'

 ;       halo.dt=dt & halo.td=!key.td  ;& halo.eps=eps               ;adaptive stepsize adjustment based on the variation of trajectory curvature
;	print,'m(t)/m(0)=',tra_i.m/msubi
;        halo.tra=tra_i
;      endif
;      if major then begin          ;adjust the host halo
;        halo.host=nplev[(ilev-1)>0].fp_lev
;	halo.mhost=nplev[(ilev-1)>0].mhost
;      endif else begin
;        kparent=tree[halo.host-1].k_parent     ;adjust the host halo
;        if tree[kparent-1].f_p eq halo.host then begin
;          halo.host=kparent
;          halo.mhost=tree[kparent-1].mhost
 ;       endif
 ;     endelse
