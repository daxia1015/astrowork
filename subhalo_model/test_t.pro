pro test_e
mv=[1e9,1e10,1e11,1e12] & chost=8.5
n=500
device,filename='orbits/energyc.ps',/encapsul,/color,xsize=15,ysize=12
multiplot,[1,1],mxtitoffset=0.1,mytitoffset=0.02
plot,findgen(100)+1,findgen(100)+1,xtitle='rc/kpc',ytitle='-e(rc)',$
/xlog,/ylog,charsize=1,xrange=[0.01,2000],yrange=[1,1.4e5],/nodata
for i=0,3 do begin
  mhost=mv[i] 
  chost=chalo(mhost,0.)
  rpvir=r_vir(mhost,!m.z) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
  rs_host=rpvir/chost   ;initial r_s of subhalo and host halo
  rc=10^(findgen(n+1)*alog10(rpvir*10)/n)-0.99
  ec=energy_r(rc,mhost,rs_host,chost,rpvir,/circular)
  oplot,rc,-ec,linestyle=i
endfor
multiplot,/default
end  



pro test_t
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=[0.3,0.5,1.] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
print,tfit.taffoni

mlrv=['1.0','1.1','1.4','1.8','2.2','2.5']+'mlr'
nmlr=n_elements(mlrv) & nld=3 & nbs=5
print,mlrv,nmlr
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=2. & lf=0.01
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
for bs=0,0 do begin 
  ;print,bs
  get_ID=0; not(fix(strmid(!wdir.bs[bs],2,1)))
  for ld=2,2 do begin
    for mlr=0,nmlr-1 do begin
      ;print,bs,ld,mlr
      tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+mlrv[mlr]+!wdir.d
      tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                       get_ID=get_ID,rmax=rmax,lf=lf,$
				       extrapolate=bs ne 1)
      if get_ID then ID_point[ld,mlr,bs]=IDp
    endfor
  endfor
endfor
help,tsam
char=1.3 
device,filename='tmerge/t_mlrv.eps',/encapsul,/color,xsize=18,ysize=6.5 
multiplot,[nu,1],mxtitle='m(0)/M(0)',mytitle='merger time/Gyr',$
mxtitsize=char,mytitsize=char,xgap=0.02,/doyaxis
k=ny-1 & ld=2 & bs=0
for j=0,2 do begin
  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=1,xcharsize=0.8 ;,ycharsize=1,charsize=1
  oplot,mratio,tfit.jiang[*,j,k],linestyle=2
  oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
  for mlr=0,nmlr-1 do oplot,mratio,tsam[*,j,k,ld,mlr,bs],color=!myct.c7[mlr] ;,linestyle=line[bs]
  xyouts,0.23,0.88*tmax,'!4e!x='+epsonname[j]
  multiplot,/doyaxis
endfor
xyouts,0.05,0.93,!wdir.bs[bs],charsize=char,/normal
multiplot,/default
end







