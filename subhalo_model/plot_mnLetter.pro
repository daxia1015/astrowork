pro plot_tra_mpv

yita=1. &  epson=0.5  & bs=2 & ld=10 & mlr=2
mr=[0.01,0.05,0.1,0.3]
mr_l=[0.002,0.006,0.009,0.009]
mhost=float(strmid(!wdir.mp,9,7)) & help,mhost
Mname=num2str(mr,format='(f4.2)')
epsonname=num2str(epson,format='(f3.1)')
char=1.1 
mhostname='10!u'+['11','12','13','14','15']+'!nh!u-1!nM'+sunsymbol()

device,/color,/encapsul,filename='model2/mr1.ps',$
xsize=16,ysize=12
multiplot,[2,2],mxtitle='t / Gyr',mytitle='m(t)/M(z=0)',$
mxtitsize=char,mytitsize=char,mxtitoffset=0.5,mytitoffset=0.5,$
xgap=0.05,ygap=0.04,/doxaxis,/doyaxis,$
myposition=[0.08,0.1,1,1] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
for mri=0,3 do begin
  plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
  xrange=[0,8-mri],yrange=[mr_l[mri],mr[mri]*1.2],/ylog
  for hosti=1,5 do begin
    tra_info=read_tra_info(dir,mr[mri],mhost[hosti],mrk)
    tra=read_tra(dir,mrk,tra_info,epson,yita)
    oplot,tra.t,tra.m/mhost[hosti],linestyle=hosti,color=!myct.c5[hosti-1]
  endfor
; print,charv[*,col]
  multiplot,/doxaxis,/doyaxis
endfor
multiplot,/default
labeling,0.15,0.98,0.07,0.05,mhostname,linestyle=indgen(5)+1,$
thick=3,ct=!myct.c5,/lineation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mr=[[0.1,0.01,0.001,0.0001,0.00001],[0.5,0.05,0.005,0.0005,0.00005]]
m0=['','5'+textoidl('\times')]+'10!u10!nh!u-1!nM'+sunsymbol()
device,/color,/encapsul,filename='model2/mr2.ps',$
xsize=16,ysize=7
multiplot,[2,1],mxtitle='t / Gyr',mytitle='m(t)/m(0)',$
mxtitsize=char,mytitsize=char,mxtitoffset=0.5,mytitoffset=-0.7,$
xgap=0.04,$
myposition=[0.07,0.2,1,0.97] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
for mri=0,1 do begin
  plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
  xrange=[0,8],yrange=[0.09,1.1],/ylog
  for hosti=1,5 do begin
    ;mr= ; (5^mri)*(1.43e10/mhost[hosti])
;    print,mri,hosti
    tra_info=read_tra_info(dir,mr[hosti-1,mri],mhost[hosti],mrk)
    tra=read_tra(dir,mrk,tra_info,epson,yita)
    oplot,tra.t,tra.m/tra[0].m,linestyle=hosti,color=!myct.c5[hosti-1]
  endfor
  xyouts,7.5,0.7,'m(0)='+m0[mri],alignment=1
  multiplot,/doyaxis
endfor
multiplot,/default



end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro plot_mpv
mhostname='10!u'+['11','12','13','14','15']+'!nh!u-1!nM'+sunsymbol()
char=1.1 &  nmp=5
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=[0,15],major=1
help,halofunc.m.ev
mpv=indgen(nmp)+1
ld=3 & bs=0
;==============================================
;==============================================

device,filename='model2/mfunc_mlr.ps',/encapsul,/color,xsize=16,ysize=10

multiplot,[3,2],mxtitle='m/M',mytitle='dN/dln(m/M)',$
mxtitoffset=0.5,mytitoffset=0.1,mxtitsize=1.2,mytitsize=1.2,$
myposition=[0.15,0.18,0.98,0.98]

!p.charsize=char & !x.charsize=char & !y.charsize=char
tick0=replicate(' ',4) & tick1=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n']
xtick=[[tick0],[tick0],[tick1],[tick1],[tick1]]
tick0=replicate(' ',4) & tick1=['10!u-1!n','1','10','10!u2!n']
ytick=[[tick1],[tick0],[tick0],[tick1],[tick0]]


for i=0,nmp-1 do begin
  mpi=mpv[i]
  plot,xh.mev,halofunc0.m.ev.giocoli[*,i],$
  xrange=[5e-5,0.2],yrange=[3.01e-2,2e2],/xlog,/ylog,$
  xtickname=xtick[*,i],ytickname=ytick[*,i]
  for mlr=1,3 do begin
    oplot,xh.mev,halofunc.m.ev[*,ld,mlr,bs,mpi],linestyle=mlr
    ;print,halofunc.m.ev[*,ld,mlr,bs,mpi]
  endfor
  xyouts,1e-4,7e-2,mhostname[i]
  multiplot
endfor
multiplot,/default
labeling,0.75,0.3,0.07,0.05,['A='+['1.','3.5','10.'],'SIM'],$
linestyle=[1,2,3,0],thick=3,/lineation

;============================================================
;============================================================
  mlr=2
  device,filename='model2/mfunc.ps',$
  /color,/encapsul,xsize=10,ysize=8
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  plot,xh.mun,halofunc0.m.un.giocoli,$              ;evolved, data of Giocoli 2007
  xrange=[5e-5,0.5],yrange=[2e-2,4e2],/xlog,/ylog,$
  xtickname=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'],$
  ytickname=['10!u-1!n','1','10','10!u2!n'],$
  xtitle='m/M',position=[0.2,0.2,0.97,0.97];,ytitle='dn/dln(m/M)'
  xyouts,0.06,0.58,'dN/dln(m/M)',/normal,orient=90,align=0.5,charsize=1.4
  ;for i=1,nmp-1 do oplot,xh.mun,halofunc.m.un[*,i],thick=1     ;unevolved merger tree.
  for i=0,nmp-1 do begin
    mpi=mpv[i]
    y=halofunc.m.ev[*,ld,mlr,bs,mpi] & dy=halofunc.m.err[*,ld,mlr,bs,mpi]
    ;dye=exp(dy/y)
    oplot,xh.mev,y,color=!myct.c5[i],linestyle=i+1,thick=2
  endfor
  labeling,0.01,0.08,0.1,0.08,mhostname,linestyle=indgen(5)+1,$
  thick=2,ct=!myct.c5,/lineation,/bottom2top
;  xyouts,0.05,0.1,ms,/normal,color=!myct.blue
;=========================================================
;==============================================================

  rrange=[0.05,1] & nrange=[0.003,1.1] & xtit='r/R!dvir!n' & ytit='N(<r)/N(<R!dvir!n)'
  ;===============================================================;
  mc=0
  device,filename='model2/rfunc.ps',$
  /encapsul,/color,xsize=10,ysize=8 ;,xoffset=1.5,yoffset=10
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
  xrange=rrange,yrange=nrange,xtitle=xtit,ytitle=ytit,$
  position=[0.21,0.17,0.96,0.97]
  ;for i=1,nmp-1 do oplot,xh.r,halofunc.r.i[*,i],color=!myct.c5[i-1],thick=2
  for i=0,nmp-1 do begin
    mpi=mpv[i]
    oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,mc,mpi],linestyle=i+1,$
    color=!myct.c5[i],thick=2
  endfor
  labeling,0.6,0.4,0.1,0.07,mhostname,linestyle=indgen(5)+1,$
  thick=2,ct=!myct.c5,/lineation
  ;xyouts,0.05,0.1,ms,/normal,color=!myct.blue
;=========================================================

end
