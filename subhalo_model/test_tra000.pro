pro tradata000
!except=2
!key.hostz=0 & !m.z=0
mhost=1.e12 ;& a=;strtrim(string(mhost),2)
mh_name=num2str(mhost,format='(e8.2)')
;mh_name=strmid(a,0,4)+'e'+strmid(a,strlen(a)-2,2)
mr=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] 
Mname=num2str(mr,format='(f7.5)')
msub=mr*mhost
ti=0. & chost=8.5  & dti=0.1 & dtmin=0. & dtmax=0.2 
csub=chalo(mr,chost,/normalize)
eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
yita=1. & epson=!epsonv ;& print,'yita=',yita
nm=n_elements(mr) & ny=n_elements(yita) & nu=n_elements(epson)
halosetting,1
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
;----halt condition define the merger time
;0:do NOT stop, 1:mass, 2:time, 3:angular momentum, 4:mass or l,
;5:l or time, 6:mass or l or time, 7:rt lt rs_sub or l
halt=2 & mf=0.
dir='tradata/tra000/'
for k=0,nm-1 do begin
  openw,lun0,dir+'tra_'+mh_name+'_'+Mname[k]+'_info',/get_lun
  openw,lun1,dir+'tra_'+mh_name+'_'+Mname[k],/get_lun
  printf,lun0,mr[k],mhost,csub[k],chost,ny,nu,format='(f9.6,e14.6,2f6.2,2i4)'
  for j=0,ny-1 do begin
    print,'yita=',yita[j]
    for i=0,nu-1 do begin
      print,'es=',epson[i]
      if mr[k] ge 0.05 then tf=total(20.+5.*where(!epsonv eq epson[i])) else tf=14.
      dti=0.1
      tra=trajectory(msub[k],mhost,mz,csub[k],chost,yita[j],epson[i],$
	                   mf,mbaryon,ti,tf,dti,dtmin,dtmax,eps,$
			   halt=halt,df=0,td=0,th=0,bs=0)
      n=n_elements(tra)
      tt=max(tra.t)-ti & lf=min(tra.l)/tra[0].l
      printf,lun0,yita[j],epson[i],n,tt,lf,min(tra.apo),tra[n-1].ID,$
      format='(2f5.1,i10,3e15.6e3,f5.1)'
      writeu,lun1,tra     
    endfor
  endfor
  free_lun,lun0,lun1
endfor


end
;=================================================
;===================================================
pro plot_tra000

mhost=1.e12 & !key.hostz=0
mr=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & nm=n_elements(mr)   ;i
Mname=strmid(strtrim(string(mr),2),0,5)           ;i
epsonname=strmid(strtrim(string(!epsonv),2),0,3)  ;j
yitaname=strmid(strtrim(string(!yitav),2),0,3)    ;k
dir='tradata/tra000/' & k=2

for j=0,5 do begin
  device,/color,filename='orbits/tra000/tra_'+epsonname[j]+'.ps',$
  /encapsul,xsize=18,ysize=18
  multiplot,[3,3],mxtitle='r/kpc',mytitle='r/kpc'
  for i=0,nm-1 do begin
    ;print,mr[i],!epsonv[j],!yitav[k]
    tra_info=read_tra_info(dir,mr[i],mhost,mrk)
    tra=read_tra(dir,mrk,tra_info,!epsonv[j],!yitav[k])
    rmax=max(tra.r)
    n=n_elements(tra)
    tt=tra[n-1].t & mf=tra[n-1].m
;      t=findgen(3*n)*max(tra.t)/3/n
;      r=spline(tra.t,tra.r,t) & theta=spline(tra.t,tra.theta,t)
    plot,tra.r,tra.theta,/polar,/iso,xrange=[-1,1]*rmax,yrange=[-1,1]*rmax	 
    circle,0.,0.,1.,color=30
    xyouts,0.2*rmax,-0.7*rmax,$
    strmid(strtrim(string(tt),2),0,4)+'Gyr!c'+num2str(mf,format='(e7.1)')+'M!dsun!n'
    multiplot
  endfor
  multiplot,/default
endfor










end



