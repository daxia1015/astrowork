pro test_mhostz
mp=0
treedir=!wdir.mp[mp]
tree_series,treedir,z_lev,nhalos,n_levs
readtree,treedir,nhalos,n_levs,nplev=nplev,itree=1

device,filename='thesis/dynamics/potential_t.eps',/color,/encapsul,$
xsize=18,ysize=15
multiplot,[2,2],xgap=0.05,ygap=0.05,/doxaxis,/doyaxis,$
myposition=[0.04,0.04,1,1]
xtit=['redshift (z)','r/kpc','r/kpc','r/kpc']
ytit=['M(z)/M(z=0)','M(<r,z)/M(z=0)',$
      '!mr!x(r,z)/10!u6!n','-!mf!x(r,z)/10!u10!n']

psi=0
plot,nplev.z,nplev.mhost/nplev[0].mhost,xrange=[0,5],yrange=[0.01,1],$
xtitle=xtit[psi],ytitle=ytit[psi]
multiplot,/doxaxis,/doyaxis

charv=replicate('',6)
for psi=1,3 do begin
  plot,findgen(230),findgen(230)/230+0.01,/nodata,$
  xtitle=xtit[psi],ytitle=ytit[psi]
  li=0
  for i=0,15,3 do begin
    if psi eq 1 then begin
      print,nplev[i].mhost,nplev[i].z
      charv[li]=string(nplev[i].z,format='(f4.2)')
    endif

    rvir=r_vir(nplev[i].mhost,nplev[i].z)
    r=findgen(100)*rvir/100+0.1
    c=chalo(nplev[i].mhost,nplev[i].z)
    rs=r_s(c,nplev[i].mhost,nplev[i].z)  
    mr=mhalo_r(r,rs,c,nplev[i].mhost)
    fg=f_g(r,rs,c,nplev[i].mhost)
    ros=ros_nfw(nplev[i].z,c)
    ror=ro_r(r,rs,c,ros)
;    coeff=1/(2*mr/r^3-4*!pi*ror)
    fai=mr/r
    ci=!myct.c7[li]
    case psi of
      1: begin
         oplot,r,mr/nplev[0].mhost,color=ci,linestyle=li
;	 fgmax=2e3
	 romax=1e6
;	 coeffmax=1e2
	 faimax=1e10
	 end
      2: oplot,r,ror/romax,color=ci,linestyle=li
      3: oplot,r,fai/faimax,color=ci,linestyle=li
      else: message,'out of range'
    endcase
    li++
  endfor
  multiplot,/doxaxis,/doyaxis
endfor
multiplot,/default


labeling,0.15,0.35,0.07,0.04,'z='+charv,/lineation,$
linestyle=indgen(6),ct=!myct.c7
end
