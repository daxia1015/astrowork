;==================================================================
;==================================================================
pro test_halofunc
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=0,major=1
mp=0 & major=1
treedir=!wdir.mp[mp] & mpname=strmid(treedir,9,15)
hdir='halofunc/evolving/bs0x40.00_ld1Mtmtfit_3.5mlr/'
halon=read_halon(hdir,mpname,ns,major=major,rmax=1.,/alive,/alltrees)

half=0.5 & !key.hostz=1
rpvir=r_vir(mphalo,0.)



end


;pro test_halofunc
;read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=0,major=1
;help,halofunc,halofunc0,/struct
;rrange=[0.05,1] & nrange=[0.02,200] & xtit='r/R!dvir!n' & ytit='N(<r)'

;mlr=2  & char=1.2 & vlcolor=!myct.ltblue 
;civ=[[2,4,5],[3,4,5],[1,2,2]]
;psname=['m(z=0)/M(z=0)','m(z!dacc!n)/M(z=0)','accretion time']
;device,filename='halofunc/rfunc_segregation_n.ps',/encapsul,/color,$
;xsize=18,ysize=11.5
;multiplot,[3,2],mxtitle=xtit,mytitle=ytit,$
;mxtitsize=char,mytitsize=char,mxtitoffset=0.5,mytitoffset=1.5
;!p.charsize=char & !x.charsize=char & !y.charsize=char 
;for bs=2,5,3 do begin
;  bsi=bs mod 2 & ld=bs+1
;  for psi=0,2 do begin
;    plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
;    xrange=rrange,yrange=nrange;,xtickname=['0.1','1'],ytickname=['0.01','0.1','1']
;    for si=0,2 do begin
;      if(si eq 2)and(psi eq 2)then continue
;      ci=civ[si,psi]
;      case psi of
;        0:begin 
;	  y=halofunc.r.mcf[*,ld,mlr,bs,ci]*halofunc.r.mcf_ntot[ld,mlr,bs,ci]
	  ;if ci lt 5 then $
	  ;oplot,xh.r,halofunc0.r.diemand[*,ci],linestyle=si,color=vlcolor
;          end
;	1:y=halofunc.r.mci[*,ld,mlr,bs,ci]*halofunc.r.mci_ntot[ld,mlr,bs,ci]
;	2:y=halofunc.r.zc[*,ld,mlr,bs,ci]*halofunc.r.zc_ntot[ld,mlr,bs,ci]
;	else:message,'out of range'
;      endcase
;      if(bs eq 2)and(psi eq 0)and(si eq 0) then print,xh.r,y
;      oplot,xh.r,y/100,linestyle=si ;,color=!myct.blue
;    endfor
;    if bsi then begin
;      xyouts,(!p.position[0]+!p.position[2])/2,0.96,psname[psi],align=0.5,/normal
;      case psi of
;        0:begin
;	  charv=['[10!u-5!n,10!u-4!n]','[10!u-3!n,10!u-2!n]','[10!u-2!n,1]']
;	  lstyle=[0,1,2]
;	  ct=0
;	  end
;	1:begin
;	  charv=['[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]','[10!u-2!n,1]']
;	  lstyle=[0,1,2]
;	  ct=0
;	  end
;	2:begin
;	  charv=['z<6.','z<1.']
;	  lstyle=[0,1]
;	  ct=0
;	  end
;	else:message,'out of range'
;      endcase
;      labeling,0.4,0.35,0.13,0.12,charv,/lineation,linestyle=lstyle,ct=ct
;    endif else begin
;      if psi eq 0 then begin
;        ;labeling,0.4,0.25,0.13,0.12,['[10!u-5!n,10!u-4!n]','[10!u-3!n,10!u-2!n]'],$
	;/lineation,linestyle=[0,1],ct=[vlcolor,vlcolor]
	;xyouts,0.94,0.34,'Via Lactea',charsize=1.1,align=1,color=vlcolor
;      endif
;    endelse
;    multiplot
;  endfor
;  xyouts,0.14,0.88-bsi*0.41,'M'+string(bsi+1,format='(i1)'),/normal
;endfor
;multiplot,/default 
;
