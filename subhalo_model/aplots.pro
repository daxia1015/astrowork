pro aplot
!except=2
!p.font=0
entry_device=!d.name
set_plot,'ps'
;device,isolatin1=1
colors_kc
char=1.2 & ply=4 & len=0.04 
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char & !x.ticklen=len
!y.thick=ply & !y.style=1 & !y.charsize=char & !y.ticklen=len

;jmr
;plot_bk_j
;plot_halofunc
;plot_halofunc1
;plot_halofunc2
;plot_tmerge
;plot_tmerge1
;plot_tmerge_raa
;plot_tra_raa
;heating_ps
;plot_all_tra
;plot_orbit
;plot_tra000
;plot_tree
;plot_mhaloz
;count_halofunc
;time_series
;tzentner
;ldzentner
;mfunc_zentner
;test_mhostz
;test_halofunc
;plot_tra_mpv
;plot_mpv
;plot_tw
;plot_thesis_intro
;plot_thesis_dft
;plot_thesis_dft_tra
;plot_thesis_bk_j
;plot_thesis_tmerge
;plot_thesis_jmr
;plot_thesis_halo
test_rp

print,textoidl('\times \rho \epsilon \oplus _{\odot} \varepsilon')
print,textoidl('\Lambda \eta \phi \Phi \psi \Psi')
print,textoidl('\xi \mu \alpha \ge \geq \geqslant \geqq')
print,textoidl('{\cal f} \mathcal{f} \vec{f}')
print,textoidl('\CYRYA \cyrya \quad \,')
print,sunsymbol()
;print,textoidl('\rho')
;test_rbnd


multiplot,/default
device,/close_file
set_plot,entry_device
end
