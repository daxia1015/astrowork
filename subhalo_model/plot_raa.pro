pro plot_tmerge_raa
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=!epsonv & yita=1.
mrr=findgen(26)*0.01+0.05 & print,mrr
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
;print,mratio
tfit=tmerge_pre(mrr,epson,yita,z,mhost,chost)
;print,tfit.taffoni

nmlr=10 & nld=15 & nbs=15
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=1. & lf=0.01 & get_ID=0
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
bsv=[1,6,7,8,9]
for bs=2,10,4 do begin 
  !m.irs=float(strmid(!wdir.bs[bs],5,4))
  for ld=10,10 do begin
    for mlr=1,6 do begin
      if (mlr eq 2)or(mlr eq 3) then continue
      tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                       get_ID=get_ID,rmax=rmax,lf=lf,$
				       extrapolate=!m.irs eq 0)
    endfor
  endfor
endfor
help,tsam
char=1 & k=ny-1
;=====================================================
;tfit from BK08, J08, T03, LC93
ld=10 & mlrv=[4,1,5,6] & bs=6
;tick0=replicate(' ',6) & tick1=[' ','0.1',' ','0.2',' ','0.3']
;xtick=[[tick0],[tick0],[tick0],[tick1],[tick1],[tick1]]
xtit='R!dm!n!u-1!n=M!ds!n(0)/M!dh!n(0)' & ytit='T!ddf!n /Gyr'
psname='t'+['prior','mlr','mlr00','mlr3','mlr_th01']+'.ps'
for fig=0,4 do begin
  device,filename='raa/'+psname[fig],/encapsul,/color,xsize=16,ysize=11 ;,xoffset=1.5,yoffset=10
  multiplot,[3,2],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.5,$
  mytitle=ytit,mytitsize=1.1,mytitoffset=-2.3,$
  xgap=0.03,ygap=0.04,/doxaxis,/doyaxis,$
  myposition=[0.05,0.1,1,1]
  !p.charsize=char & !x.charsize=char & !y.charsize=char
  for j=0,5 do begin  ;epson
    t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
    plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
    xtickname=[' ','0.1',' ','0.2',' ','0.3'],/nodata
    case fig of 
      0:begin
        oplot,mrr,tfit.lc[*,j,k],linestyle=1
        oplot,mrr,tfit.taffoni[*,j,k],linestyle=4
        oplot,mrr,tfit.jiang[*,j,k],linestyle=3
	oplot,mrr,t,linestyle=2
	charv=['LC93','T03','J08','BK08'] & lstyle=[1,4,3,2] & thickv=4
      end
      1:begin
      	for ii=1,3 do begin
          mlr=mlrv[ii]
	  yt=spline(mratio,tsam[*,j,k,ld,mlr,bs],mrr)
          ;oplot,mratio,tsam[*,j,k,ld,mlr,bs],thick=ii*3
	  oplot,mrr,yt,thick=ii*3
	  charv='!ma!x='+['1','2','3'] & lstyle=0 & thickv=[3,6,9]
        endfor
      end
      2:begin
        oplot,mrr,tfit.lc[*,j,k],linestyle=1
      	oplot,mratio,tsam[*,j,k,ld,4,bs] 
	charv=['!ma!x=0','LC93'] & lstyle=[0,1] & thickv=4
      end
      3:begin
        oplot,mrr,tfit.jiang[*,j,k],linestyle=3
	oplot,mrr,t,linestyle=2
      	oplot,mratio,tsam[*,j,k,ld,6,bs]       
	charv=['J08','BK08','!ma!x=3'] & lstyle=[3,2,0] & thickv=4
      end
      4:begin
      	for ii=1,3 do begin
          mlr=mlrv[ii]
          oplot,mratio,tsam[*,j,k,ld,mlr,bs],thick=ii*3
          oplot,mratio,tsam[*,j,k,ld,mlr,10],thick=ii*3,linestyle=1
	  charv='!ma!x='+['1','2','3'] & lstyle=0 & thickv=[3,6,9]
        endfor
      end
      else:print,'out of range'
    endcase
    xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
    multiplot,/doxaxis,/doyaxis
  endfor
  multiplot,/default
  labeling,0.85,0.38,0.05,0.05,charv,/lineation,linestyle=lstyle,thickv=thickv
endfor
;==========================================================
;==========================================================
device,filename='raa/tmlr0.ps',/encapsul,/color,xsize=16,ysize=5.8 ;,xoffset=1.5,yoffset=10
multiplot,[3,1],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.,$
mytitle=ytit,mytitsize=1.1,mytitoffset=-1.5,$
xgap=0.03,myposition=[0.06,0.22,1,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
jv=[0,2,5]
for jj=0,2 do begin  ;epson
  j=jv[jj]
  t=tfit.lc[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mrr,t,yrange=[0.1*tmin,2*tmax],linestyle=1,$
  xtickname=[' ','0.1',' ','0.2',' ','0.3']
  for bs=2,10,8 do begin
    yt=spline(mratio,tsam[*,j,k,ld,4,bs],mrr)
    oplot,mrr,yt,linestyle=bs/5;,color=!myct.c3[bs/5]
  endfor
  xyouts,0.28,1.7*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.8,0.85,0.05,0.12,['LC93','!ma!x=0,NFW','!ma!x=0,ISO'],$
/lineation,linestyle=[1,0,2];,ct=[0,!myct.red,!myct.blue]
;==========================================================
;==========================================================
xtit=['R!dm!n!u-1!n=M!ds!n(0)/M!dh!n(0)','!me!x (circularity)']
ytit=['T!ddf!n / T!ddf!n(R!dm!n=20)','T!ddf!n / T!ddf!n(!me!x=1)']
ld=10 & bs=10 & mlr=4
device,filename='raa/tdep0.ps',/encapsul,/color,xsize=16,ysize=7
multiplot,[2,1],xgap=0.05,myposition=[0.05,0.17,1,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
k=ny-1
for col=0,1 do begin
  if col eq 0 then begin
    x=mrr & i=fix(total(where(mratio eq 0.05)))
    j=fix(total(where(epson eq 0.9)))
    y1=tfit.lc[*,j,k]/tfit.lc[i,j,k]
    bs=2
    y2=tsam[*,j,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    y2=spline(mratio,y2,mrr)
    bs=10
    y10=tsam[*,j,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    y10=spline(mratio,y10,mrr)
    tick=[' ','0.1',' ','0.2',' ','0.3']
  endif else begin
    x=epson
    j=fix(total(where(epson eq 1)))
    y1=tfit.lc[i,*,k]/tfit.lc[i,j,k]
    bs=2 & i=fix(total(where(mratio eq 0.05))) 
    y2=tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    bs=10 & i=fix(total(where(mratio eq 0.1))) 
    y10=tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs]
    tick=['0.2','0.4','0.6','0.8','1.0']
  endelse
  plot,x,y1,linestyle=1,xtitle=xtit[col],ytitle=ytit[col],$
  yrange=[0.1,1],xtickname=tick
  oplot,x,y2;,color=!myct.c3[0]
  oplot,x,y10,linestyle=2;,color=!myct.c3[2]
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.2,0.95,0.05,0.1,['LC93','!ma!x=0,NFW','!ma!x=0,ISO'],$
/lineation,linestyle=[1,0,2];,ct=[0,!myct.red,!myct.blue]

;========================================================
;========================================================
ld=10 & bs=6 & mlr=1 & j=nu-1 & k=ny-1
device,filename='raa/tdep1.ps',/encapsul,/color,xsize=11,ysize=9
!p.charsize=char & !x.charsize=char & !y.charsize=char
imv=[0.05,0.3] & lstyle=[5,4,3,2]
plot,epson,epson,xrange=[0.05,1],yrange=[0.1,1],/nodata,$
xtitle=xtit[1],ytitle=ytit[1],position=[0.14,0.13,0.96,0.98];,linestyle=lstyle[im]
for im=0,1 do begin
  i=fix(total(where(mratio eq imv[im])))
  oplot,epson,tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs],psym=im+5
endfor
oplot,epson,tfit.bk[i,*,k]/tfit.bk[i,j,k],linestyle=2
oplot,epson,epson^0.4/epson[j]^0.4,linestyle=5    ;Colpi99
labeling,0.68,0.3,0.05,0.08,['R!dm!n!u-1!n=0.05','R!dm!n!u-1!n=0.3'],$
/symboling,psym=[5,6]
labeling,0.66,0.13,0.13,0.07,['C99','BK08'],/lineation,linestyle=[5,2]

;==========================================================
;==========================================================
;========================================================
;========================================================


end
;========================================================
;========================================================
pro plot_tra_raa
yita=1. & mhost=1e12 & mr=0.1 & epson=0.5  & bs=6 & ld=10
snc=[1,2] & mlrv=[4,1,5,6,7,8] & char=1
ytit=['M!ds!n(t)/M!ds!n(0)','j(t)/j(0)']
;=========================================================
device,/color,/encapsul,filename='raa/mj.ps',$
xsize=16,ysize=7

multiplot,[2,1],xgap=0.06,myposition=[0.05,0.19,1,0.97] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
for row=0,1 do begin
  plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
  xrange=[0,9.5],yrange=[0.01,1.05],xtitle='t / Gyr',$
  ytitle=ytit[row] ;,ylog=row eq 0
  
  j=snc[row]
  for ii=1,3 do begin
    mlr=mlrv[ii]
    for bs=6,6 do begin
    dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
    tra_info=read_tra_info(dir,mr,mhost,mrk)
    tra=read_tra(dir,mrk,tra_info,epson,yita)
    t=tra.t & y=tra.(j)/tra[0].(j)
    oplot,t,y,thick=ii*3,linestyle=bs/10
    endfor
  endfor
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.8,0.95,0.05,0.1,'!ma!x='+['1','2','3'],/lineation,$
thickv=[3,6,9]
;========================================================
;========================================================
@bk08fit/BK08j.dat
ijv=[2,4,5] & ipv=[14,8,7] ;& gav=[0.1,0.5,1.]
para1='R!dm!n!u-1!n='+['0.05','0.1','0.1']
para2='!me!x='+['0.65','0.65','0.46']

device,filename='raa/bk.ps',/encapsul,/color,xsize=16,ysize=6
multiplot,[3,1],mxtitle='t / Gyr',mxtitsize=1.1,$
mytitle='j(t)/j(0)',mytitsize=1.1,mytitoffset=-1,$
myposition=[0.1,0.2,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for col=0,2 do begin
  ij=ijv[col] & ip=ipv[col]
  plot,tseries,jv[*,ij],linestyle=2,xrange=[0,9.99],yrange=[0.01,1.05]
  for mlr=1,3 do begin 
    mlrs=strmid(strtrim(string(float(mlr)),2),0,3)
    file='tradata/raa/'+mlrs+'mlr_'+strtrim(string(ip),2)
    info=read_tra_any(file,tra=tra)
    oplot,tra.t,tra.l/tra[0].l,thick=mlr*3
  endfor
  xyouts,9.5,0.9,para1[col],align=1
  xyouts,9.2,0.78,para2[col],align=1
  multiplot 
endfor
multiplot,/default
labeling,0,0.4,0.05,0.1,['BK08','!ma!x='+['1','2','3']],$
/lineation,thickv=[4,3,6,9],linestyle=[2,0,0,0]



end
;========================================================
;========================================================

pro tradata_bk
!except=1 
hprofile=2
!key.hostz=0 & !m.z=0
mhost=1.e12 & ti=0. & chost=8.5  & dtmin=0. & dtmax=0.4
rmax=1. & lfc=0.01 & eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
mf=1e4 & tf=45.  
halosetting,1
!key.At=0 &  !key.nex=0

tlist=tlist_bk(np)
sam=replicate({t:0.,t0:0.,ld:0.},np)
!ld.i=6  & !key.bs=1
halt=6-!key.bs 
for mlr=4,5 do begin
  !mlr=float(mlr) & print,'mlr=',!mlr
  for i=0,np-1 do begin
    csub=chalo(tlist[i].mr,chost,/normalize)
    mbaryon=mhost*tlist[i].mr*0.048
    tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
                   tlist[i].yita,tlist[i].es,mf,mbaryon,$
	           ti,tf,0.1,dtmin,dtmax,eps,hprofile=hprofile,$
                   halt=halt,df=1,td=1,th=1,bs=!key.bs)
    nstep=n_elements(tra)
    tt=trefine(tra,rmax=rmax,lfc=lfc,extrapolate=((!key.nex eq 0)or(tlist[i].treal gt 14)))
    print,i,(tt-tlist[i].treal)/tlist[i].treal
    name=strmid(strtrim(string(!mlr),2),0,3)+'mlr_'+strtrim(string(i),2) ;+'_'+string(!m.ga,format='(f3.1)')
    lnlda=alog(1+1/tlist[i].mr)
    openw,lun,'tradata/raa/'+name,/get_lun
      writeu,lun,tt,max(tra.t),lnlda,nstep
      writeu,lun,tra
    free_lun,lun
  endfor
endfor



end
;device,filename='raa/tdep.eps',/encapsul,/color,xsize=18,ysize=8
;multiplot,[2,1],xgap=0.06,myposition=[0.05,0.22,1,0.97]
;!p.charsize=char & !x.charsize=char & !y.charsize=char
;i=0 & j=nu-1 & k=ny-1
;plot,mratio,tfit.bk[*,j,k]/tfit.bk[i,j,k],linestyle=2,$
;xtitle='R!u-1!n=M!ds!n(0)/M!dh!n(0)',ytitle='T!ddf!n / T!ddf!n(R=20)',$
;xtickname=[' ','0.1',' ','0.2',' ','0.3'],yrange=[0.1,1]
;oplot,mratio,tfit.jiang[*,j,k]/tfit.jiang[i,j,k],linestyle=1
;oplot,mratio,tfit.taffoni[*,j,k]/tfit.taffoni[i,j,k],linestyle=3
;oplot,mratio,tfit.lc[*,j,k]/tfit.lc[i,j,k],linestyle=4
;multiplot,/doyaxis
;i=nm-1 & j=nu-1 & k=ny-1
;plot,epson,tfit.bk[i,*,k]/tfit.bk[i,j,k],linestyle=2,$
;xtitle='!me!x (circularity)',ytitle='T!ddf!n / T!ddf!n(!me!x=1)',$
;yrange=[0.1,1]
;oplot,epson,tfit.jiang[i,*,k]/tfit.jiang[i,j,k],linestyle=1
;oplot,epson,tfit.taffoni[i,*,k]/tfit.taffoni[i,j,k],linestyle=3
;oplot,epson,tfit.lc[i,*,k]/tfit.lc[i,j,k],linestyle=4
;multiplot,/default
;labeling,0.24,0.95,0.05,0.1,['J08','BK08','T03','LC93'],$
;/lineation,linestyle=[1,2,3,4],charsize=char
;========================================================
;========================================================
;device,filename='raa/tes.eps',/encapsul,/color,xsize=18,ysize=6
;multiplot,[3,1],mxtitle='!me!x (circularity)',mxtitoffset=0.5,$
;mytitle='T!ddf!n / T!ddf!n(!me!x=1)',$
;myposition=[0.2,0.22,0.97,0.97]
;!p.charsize=char & !x.charsize=char & !y.charsize=char
;im=[2,5,7] & j=nu-1 & k=ny-1
;for col=0,2 do begin
;  i=im[col]
;  plot,epson,tfit.bk[i,*,k]/tfit.bk[i,j,k],$
;  yrange=[0.1,1],/nodata
;  for ii=0,2 do begin
;    mlr=mlrv[ii]
;    oplot,epson,tsam[i,*,k,ld,mlr,bs]/tsam[i,j,k,ld,mlr,bs],linestyle=ii
;  endfor
;  multiplot
;endfor
;multiplot,/default
;labeling,0.25,0.95,0.05,0.1,'!ma!x='+['0','1','3'],$
;/lineation,linestyle=[0,1,2]
;========================================================
;========================================================
;ld=10 & mlrv=[4,1,5,6] & bs=1
;device,filename='raa/tmlr.eps',/encapsul,/color,xsize=16,ysize=5.8 ;,xoffset=1.5,yoffset=10
;multiplot,[3,1],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.2,$
;mytitle=ytit,mytitsize=1.1,mytitoffset=-2.5,$
;xgap=0.025,/doyaxis,myposition=[0.05,0.23,1,0.97]
;!p.charsize=char & !x.charsize=char & !y.charsize=char
;for j=1,3 do begin  ;epson
;  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
;  plot,mratio,t,yrange=[0.1*tmin,tmax],$
;  xtickname=[' ','0.1',' ','0.2',' ','0.3'],/nodata
;  for ii=0,3 do begin
;    mlr=mlrv[ii]
;    oplot,mratio,tsam[*,j,k,ld,mlr,bs],linestyle=ii
;  endfor
;  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
;  multiplot,/doyaxis
;endfor
;multiplot,/default
;labeling,0.85,0.8,0.05,0.1,'!ma!x='+['0','1','3'],$
;/lineation,linestyle=[0,1,2]
;;========================================================
;;========================================================
;ld=10 & jv=[0,3,5] & bsv=[1,6,7,8,9] & mlrv=[4,1,5,6] 
;device,filename='raa/tbs.eps',/encapsul,/color,xsize=16,ysize=5.8 ;,xoffset=1.5,yoffset=10
;multiplot,[3,1],mxtitle=xtit,mxtitsize=1.1,mxtitoffset=0.2,$
;mytitle=ytit,mytitsize=1.1,mytitoffset=-2.5,$
;xgap=0.025,/doyaxis,myposition=[0.05,0.23,1,0.97]
;!p.charsize=char & !x.charsize=char & !y.charsize=char
;for jj=0,2 do begin  ;epson
;  j=jv[jj]
;  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
;  plot,mratio,t,yrange=[0.1*tmin,tmax],$
;  xtickname=[' ','0.1',' ','0.2',' ','0.3'],/nodata
;  for bb=0,4 do begin
;  bs=bsv[bb]
;  for ii=2,2 do begin
;    mlr=mlrv[ii]
;    oplot,mratio,tsam[*,j,k,ld,mlr,bs],linestyle=bb
;  endfor
;  endfor
;  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.1
;  multiplot,/doyaxis
;endfor
;multiplot,/default
;labeling,0.85,0.8,0.05,0.1,'!ma!x='+['0','1','3'],$
;/lineation,linestyle=[0,1,2]



