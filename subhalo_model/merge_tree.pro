pro tree_series,dir,zlev,nhalos,n_levs
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
zlev=fltarr(nlev)
openr,lun,dir+'time_series',/get_lun
  readf,lun,nlev0,format='(i5)'
  if nlev0 ne nlev then message,'error found'
  readf,lun,zlev,format='(f9.6)'
free_lun,lun

out=lonarr(2,ntree) ; & nhalos=lonarr(ntree) & n_levs=lonarr(ntree)
openr,lun,dir+'trees_series',/get_lun
  readf,lun,out,format='(i7,i5)'
  ;if ~(eof(lun)) then print,'tree_series has NOT been read out!'
free_lun,lun
nhalos=reform(out[0,*],ntree)
n_levs=reform(out[1,*],ntree)
end
;=================================================================
;===============================================================
pro readtree,dir,nhalos,n_levs,nplev=nplev,tree=tree,mhostz=mhostz,$
             itree=i,alltrees=alltrees
openr,lun0,dir+'trees_levels',/get_lun
openr,lun1,dir+'trees_all',/get_lun
nplevi={fp_lev:7L,np_lev:7,z:0.,mhost:0.}
treek={f_p:7L,k_parent:7L,nchild:5,z:0.,mhalo:0.}
if keyword_set(alltrees) then begin
  if arg_present(nplev) then begin
    nplev=replicate(nplevi,total(n_levs,/int))
    readf,lun0,nplev,format='(2i7,2e14.6)'
  endif
  if arg_present(mhostz) then begin
    mhostz=fltarr(total(n_levs,/int))
    readf,lun0,mhostz,format='(28x,e14.6)'
  endif
  if arg_present(tree) then begin
    tree=replicate(treek,total(nhalos,/int))
    readf,lun1,tree,format='(2i7,i5,2e14.6)'
  endif
endif else begin
  n_lev=n_levs[i]
  skip_lev=0LL
  if i ge 1 then skip_lev=total(n_levs[0:i-1],/int) 
  skip_lun,lun0,skip_lev,/lines
  if arg_present(nplev) then begin
    nplev=replicate(nplevi,n_lev)
    readf,lun0,nplev,format='(2i7,2e14.6)'
  endif
  if arg_present(mhostz) then begin
    mhostz=fltarr(n_lev)
    readf,lun0,mhostz,format='(28x,e14.6)'
  endif
  if arg_present(tree) then begin
    nhalo=nhalos[i]
    skip_np=0LL
    if i ge 1 then skip_np=total(nhalos[0:i-1],/int)
    tree=replicate(treek,nhalo)
    skip_lun,lun1,skip_np,/lines
    readf,lun1,tree,format='(2i7,i5,2e14.6)'
  endif
endelse
if eof(lun0) then print,'end of trees_levels'
if eof(lun1) then print,'end of trees_all'
free_lun,lun0,lun1
end

;=========================================================
;-----generate tree net---------------
function treenet,z_lev,nhalo,n_lev,nplev,tree,major=major 
treek={sub:0L,host:0L,ilev:0,z:0.,msub:0.,mhost:0.} 
if major then begin
  treen=treek
  for ilev=n_lev-1,0,-1 do begin
    host=nplev[ilev].fp_lev
    treei=tree[host-1]  
    if(treei.nchild gt 1)then begin
      for ichild=1,treei.nchild-1 do begin
        treek.sub=treei.f_p+ichild & treek.host=host
	treek.ilev=ilev
	treen=[treen,treek]
      endfor
    endif  
  endfor
  nhalo=n_elements(treen)-1
  treen=treen[1:nhalo]
  treen.z=z_lev[treen.ilev]
endif else begin
  kf=nhalo
  while tree[tree[kf-1].k_parent-1].nchild le 1 do kf=kf-1
  treek.sub=kf & treek.host=tree[kf-1].k_parent
  treek.z=tree[tree[kf-1].k_parent-1].z
  treen=treek
  for k=kf-1,2,-1 do begin
    halo=tree[k-1]
    parent=tree[halo.k_parent-1]
    if (parent.nchild gt 1)and(parent.f_p ne k) then begin    
      treek.sub=k
      treek.host=halo.k_parent
      treek.z=parent.z
      treen=[treen,treek]
    endif
  endfor
  
  nhalo=n_elements(treen) & id=intarr(nhalo)
  for ilev=n_lev-1,0,-1 do begin
    host=nplev[ilev].fp_lev
    sn=where((treen.host gt host)and(id eq 0),count)
    if count ne 0 then treen[sn].host=tree[treen[sn].host-1].k_parent
    sn=where((treen.host eq host)and(id eq 0),count)
    if count ne 0 then id[sn]=1
    sn=where(abs(treen.z-z_lev[ilev]) lt 1e-3,count)
    if count ne 0 then treen[sn].ilev=ilev
  endfor
endelse
treen.msub=tree[treen.sub-1].mhalo
treen.mhost=tree[treen.host-1].mhalo
n_lev_p=where(abs(z_lev-max(treen.z)) lt 1e-3,count)+1 
n_lev=n_lev_p[0]
if count gt 1 then message,'error found'
return,treen
end
;===============================================================
;===============================================================
pro deal_trees,treedir
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
print,'mphalo=',mphalo
print,'mres=',mres
tree_series,treedir,z_lev,nhalos,n_levs
;help,nhalos,n_levs
print,total(nhalos,/int),total(n_levs,/int)
treek={ilev:0,z:0.,msub:0.,mhost:0.,yita:0.,es:0.}
openw,lun0,treedir+'treenet_series',/get_lun
openw,lun1,treedir+'treenet',/get_lun
openw,lun2,treedir+'treenet_series_major',/get_lun
openw,lun3,treedir+'treenet_major',/get_lun
luns=[lun0,lun2] & lunt=[lun1,lun3]
for i=0,ntree-1 do begin
  if i mod 10 eq 0 then print,'dealing TEN trees',i
  readtree,treedir,nhalos,n_levs,nplev=nplev,tree=tree,itree=i
  for major=0,1 do begin  
    nhalo=nhalos[i] & n_lev=n_levs[i]
    treen=treenet(z_lev,nhalo,n_lev,nplev,tree,major=major) ;major=0 should be executed firstly!!!
    out=replicate(treek,nhalo)
    struct_assign,treen,out,/nozero
    ini_orbits,i+1,nhalo,out,hprofile=2   ;NFW
    printf,luns[major],nhalo,n_lev,format='(i7,i5)'
    writeu,lunt[major],out
  endfor
endfor
free_lun,lun0,lun1,lun2,lun3
end
;===========================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro treen_series,dir,nhalos,n_levs,major=major
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
out=lonarr(2,ntree)
openr,lun,dir+'treenet_series'+!wdir.major[major],/get_lun
  readf,lun,out,format='(i7,i5)'
free_lun,lun
nhalos=reform(out[0,*],ntree)
n_levs=reform(out[1,*],ntree)
end
;=============================================================
;==============================================================
function readtreen,dir,nhalos,itree=i,major=major,alltrees=alltrees
treek={ilev:0,z:0.,msub:0.,mhost:0.,yita:0.,es:0.}	 
openr,lun,dir+'treenet'+!wdir.major[major],/get_lun
if keyword_set(alltrees) then begin
  treen=replicate(treek,total(nhalos,/int))
  readu,lun,treen
endif else begin
  nhalo=nhalos[i]
  point_b=0LL
  if i ge 1 then point_b=total(nhalos[0:i-1],/int)*22
  treen=replicate(treek,nhalo)
  point_lun,lun,point_b
  readu,lun,treen;,format='(i4,3e14.6)'
endelse
if eof(lun) then print,'end of treenet'
free_lun,lun
return,treen
end
;==============================================================
;==============================================================
function mfunc_tree,treedir,ml,mh,dbin,nbin,xun,major=major
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
treen_series,treedir,nhalos,n_levs,major=major
treen=readtreen(treedir,nhalos,major=major,/alltrees)
mvun=treen.msub
mvun=alog10(temporary(mvun)/mphalo)
h=histo(mvun,ml,mh,nbin,locations=lgxun)
xun=10^lgxun
mfun=h/ntree/dbin/alog(10)  ;dN(<m)/dln(m/M)
return,mfun
end
;=======================================================
;========================================================

pro test_tree
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
treedir=!wdir.mp[0]
tree_series,treedir,zlev,nhalos0,n_levs0
readtree,treedir,nhalos0,n_levs0,nplev=nplev,itree=0

cz=chalo(nplev.mhost,nplev.z)
plot,nplev.mhost,cz,/xlog,xstyle=1

end

;ml={mun:-4.,     mev:-4., t:0., r:alog10(0.01), ri:alog10(0.01), rm:alog10(0.01), e:0.01}
;mh={mun:-0.35, mev:-1., t:1., r:alog10(1.),   ri:alog10(1.),   rm:alog10(1.) ,  e: 1.}
;nbin={mun:11,  mev:11,  t:10, r:        10,   ri:        10,   rm:    10,       e: 10}
;dbin=ml
;for i=0,n_tags(ml)-1 do dbin.(i)=(mh.(i)-ml.(i))/(nbin.(i)-1);

;mp=0
;treedir='treedata'+!wdir.d+!wdir.mp[mp]+!wdir.d
;device,filename='halofunc/mfun.ps',/color,/encapsul
;multiplot,[1,1],mxtitle='lg(m/M)',mytitle='lg[dN/dln(m/M)]',$
;mxtitsize=1.5,mytitsize=1.5
;!p.thick=4
;plot,findgen(10),findgen(10),/nodata,$              ;unevolved, fitted by Giocoli 2007
;xrange=[-4.3,-0.3],yrange=[-1,3] ;,charsize=1;

;for major=0,1 do begin
;  mfun=mfunc_tree(treedir,ml.mun,mh.mun,dbin.mun,nbin.mun,xun,major=major)
;  oplot,xun,mfun,color=!myct.c2[major]
;endfor
;multiplot,/default


;function mhostz,dir,major=major
;common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
;tree_series,dir,z_lev,n_levs,nhalos
;z=z_lev(where(z_lev lt 5.))
;nstep=n_elements(z)
;t=tGy(z)
;m=dblarr(nstep)
;out=dblarr(3,nstep)
;out[0,*]=z
;out[1,*]=t
;for i=0,ntree-1 do begin
;  tree=readtree(dir,i,n_levs,nhalos,nplev,major=major)
;  m=m+nplev[0:nstep-1].mhost/mphalo
;endfor
;m=m/ntree
;out[2,*]=m
;plot,t,m
;out=reverse(out,2)
;return,out
;end

