pro plot_halofunc2
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=0,major=1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
char=1.1

device,filename='halofunc/subpop.ps',/color,/encapsul,$
xsize=18,ysize=14
multiplot,[3,2],myposition=[0.14,0.05,0.97,0.99],ygap=0.06
!p.charsize=char & !x.charsize=char & !y.charsize=char
tick1=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n']
xtick=[[tick1],[tick1],[tick1]]
tick0=replicate(' ',5) & tick1=['10!u-2!n','10!u-1!n','1','10','10!u2!n']
ytick=[[tick1],[tick0],[tick0]]
help,xtick,ytick
rrange=[0.05,1] & nrange=[0.003,1.1] & mc=0
xtit=['m/M','r/R!dvir!n'] & ytit=['dN/dln(m/M)','N(<r)/N(<R!dvir!n)']
abc=[['a','b','c'],['d','e','f']]
for row=0,1 do begin
  xyouts,0.04,(!p.position[1]+!p.position[3])/2,ytit[row],$
  /normal,orientation=90,alignment=0.5  
  for col=0,2 do begin
    case row of 
      0:begin
      	plot,xh.mev,halofunc0.m.ev.giocoli[*,1],/nodata,$              ;evolved, data of Giocoli 2007
        xrange=[5e-5,0.5],yrange=[1.001e-2,4e2],/xlog,/ylog,$
        xtickname=xtick[*,col],ytickname=ytick[*,col]
	oplot,xh.mev,halofunc0.m.ev.giocoli[*,1],linestyle=5
      end
      1:plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
        xrange=rrange,yrange=nrange
      endcase
    case col of
      0:begin
        ld=6 & mlr=2 & bs=5
        case row of 
	  0:begin
            oplot,xh.mun,halofunc0.m.un.giocoli,linestyle=4
            oplot,xh.mun,halofunc.m.un,linestyle=1     ;unevolved merger tree.
            oplot,xh.mev,halofunc.m.ev[*,ld,mlr,bs];,thick=mlr*4
            charv=['M2','evolved,SIM','unevolved,SIM','unevolved,EPS']
            lstyle=[0,5,4,1]
            labeling,0.04,0.34,0.15,0.08,charv,/lineation,linestyle=lstyle
      	  end
	  1:begin
	    oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,mc]
            oplot,xh.r,halofunc0.r.MW,psym=6
            shadowing,xh.r,halofunc0.r.diemand[*,1],halofunc0.r.einasto50,density=8,thick=3
            labeling,0.72,0.25,0.05,0.08,'MW',/symboling,psym=6
            labeling,0.65,0.17,0.12,0.08,'M2',/lineation
            shadowing,findgen(12)/100+0.65,fltarr(12)+0.11,fltarr(12)+0.05,density=0.6,thick=3
            xyouts,0.77,0.05,'SIM'
	  end
      	endcase
      end
      1:begin
        ld=3 & bs=2
        for mlr=1,3 do begin  
	  case row of
    	    0:begin
	      x0=xh.mev & y0=halofunc.m.ev[*,ld,mlr,bs]
	    end  
      	    1:begin
	      x0=xh.r & y0=halofunc.r.mcf[*,ld,mlr,bs,mc]
	    end
	  endcase   
          oplot,x0,y0,linestyle=mlr
        endfor
        xyouts,(!p.position[0]+!p.position[2])/2,0.49-0.47*row,$
	xtit[row],align=0.5,/normal
        charv='A='+['1','3.5','10']
      end
      2:begin
        mlr=2 & bs=2
        for ld=0,2 do begin
	  case row of
    	    0:begin
	      x0=xh.mev & y0=halofunc.m.ev[*,ld,mlr,bs]
	    end  
      	    1:begin
	      x0=xh.r & y0=halofunc.r.mcf[*,ld,mlr,bs,mc]
	    end
	  endcase   
    	  oplot,x0,y0,linestyle=ld+1
        endfor
        charv='ln!mL!x='+['3','4','5']
      end
    endcase
    if col ge 1 then begin
      case row of
        0:labeling,0.05,0.08,0.22,0.08,'evolved,SIM',/lineation,linestyle=5
      	1:labeling,0.5,0.28,0.15,0.08,charv,/lineation,linestyle=[1,2,3]
      endcase
    endif
    xyouts,0.85-0.75*row,0.85,abc[col,row]
    multiplot
  endfor
endfor
multiplot,/default
;==============================================================
;==============================================================
;----segregation of subhalos radial distribution-------
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mlr=2  & char=1.1 & vlcolor=!myct.grey
civ=[[2,3,4],[2,3,4],[1,2,3]] 
psname=['m(z=0)/M(z=0)','m(z!dacc!n)/M(z=0)','accretion time(z)']
for mi=0,1 do begin 
  bs=2+mi*3 & ld=3+mi*3
  Mname='M'+string(mi+1,format='(i1)')
  device,filename='halofunc/rfunc_mczc_'+Mname+'.ps',$
  /encapsul,/color,xsize=18,ysize=7
  multiplot,[3,1],mxtitle='r/R!dvir!n',mytitle='N(<r)/N(<R!dvir!n)',$
  mxtitsize=char,mytitsize=char,mytitoffset=0.5,$
  myposition=[0.11,0.18,0.97,0.9]
  !p.charsize=char & !x.charsize=char & !y.charsize=char 
  for psi=0,2 do begin
    plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$  
    xrange=rrange,yrange=nrange;,xtickname=['0.1','1'],ytickname=['0.01','0.1','1']
    for si=0,2 do begin
;    if(si eq 2)and(psi eq 2)then continue
      ci=civ[si,psi]
      case psi of
        0:begin 
            y=halofunc.r.mcf[*,ld,mlr,bs,ci]
    	    oplot,xh.r,halofunc0.r.diemand[*,ci],linestyle=si,color=vlcolor
          end
        1:y=halofunc.r.mci[*,ld,mlr,bs,ci]
        2:y=halofunc.r.zc[*,ld,mlr,bs,ci]
        else:message,'out of range'
      endcase
      oplot,xh.r,y,linestyle=si ;,color=!myct.blue
;      print,halofunc.r.mcf_ntot[ld,mlr,bs,*]/halofunc.r.mcf_ntot[ld,mlr,bs,0],format='(6f8.3)'
    endfor
    xyouts,(!p.position[0]+!p.position[2])/2,0.93,psname[psi],align=0.5,/normal
    case psi of
      0:begin
        charv=['[10!u-5!n,10!u-4!n]','[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]']
        ;lstyle=[0,1,2]
      end
      1:begin
        charv=['[10!u-5!n,10!u-4!n]','[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]']
        ;lstyle=[0,1,2]
      end
      2:begin
        ;charv=['z<6.','z<1.']
        charv=['[2,6]','[0.5,2]','[0,0.5]']
        ;lstyle=[0,1,2]
      end
      else:message,'out of range'
    endcase
    labeling,0.4,0.35,0.13,0.12,charv,/lineation,linestyle=[0,1,2]
    if psi eq 0 then xyouts,0.93,0.55,'Via Lactea',charsize=1,align=1,color=vlcolor
    multiplot
  endfor
  multiplot,/default
;  xyouts,0.13,0.81,Mname,/normal
endfor
;==============================================================
;==============================================================
;==============================================================
;==============================================================
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=[0,5],major=1

;sun='!9' + string(110B) + '!X'
ld=6 & mlr=2 & bs=5
mpv=10^(findgen(5)+11)

device,filename='halofunc/hod.ps',/encapsul,/color,$
xsize=10,ysize=8

plot,mpv,findgen(5)*0.4,/xlog,/nodata,yrange=[0.5,1.3],$
xtitle='M/( h!u-1!nM'+sunsymbol()+' )',ytitle='!ma!x',$
position=[0.18,0.17,0.94,0.96]
for mc=2,4 do begin
  oplot,mpv,halofunc.nfunc[1,mc,ld,mlr,bs,1:5],linestyle=mc-2
    
endfor
str='lg(m/M) '+textoidl('\geq')+' -'
labeling,0.05,0.3,0.11,0.1,str+['5','4','3'],$
/lineation,linestyle=[0,1,2]

;==============================================================
;==============================================================
device,filename='halofunc/hod1.ps',/encapsul,/color,$
xsize=18,ysize=15
multiplot,[5,3],xgap=0.01,ygap=0.03,mxtitle='N',mytitle='P(N)',$
/doxaxis,myposition=[0.1,0.19,1,0.98]
!p.charsize=char & !x.charsize=char & !y.charsize=char 
for mc=2,4 do begin
;  y=halofunc.pfunc[*,mc,ld,mlr,bs,0]
;  plot,halofunc.npx[*,mc,ld,mlr,bs,0],y/total(y),yrange=[0,0.39]
  for mpi=1,5 do begin
    x=halofunc.npx[*,mc,ld,mlr,bs,mpi]
    y=halofunc.pfunc[*,mc,ld,mlr,bs,mpi] ;& print,y
    plot,x,y/total(y),yrange=[0,0.39] ;,psym=5;,$ linestyle=mpi,color=!myct.c6[mpi]
    mu=halofunc.nfunc[0,mc,ld,mlr,bs,mpi]
    ;mu=mean(x)
    ;oplot,x,exp(-mu)*mu^x/factorial(x),linestyle=2

    multiplot,/doxaxis
  
  endfor

endfor
multiplot,/default

end




;device,filename='halofunc/rfunci_mci.ps',/encapsul,/color,$
;xsize=10,ysize=8
;plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$  
;xrange=rrange,yrange=nrange,xtitle='r/R!dvir!n',$
;ytitle='N(<r)/N(<R!dvir!n)',position=[0.21,0.17,0.96,0.98];,xtickname=['0.1','1'],ytickname=['0.01','0.1','1']
;for mc=2,5 do begin
;  oplot,xh.ri,halofunc.r.i[*,mc],linestyle=mc-2
;endfor
;charv=['[10!u-5!n,10!u-4!n]','[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]','[10!u-2!n,1]']
;labeling,0.5,0.4,0.12,0.11,charv,/lineation,linestyle=indgen(4)
;==============================================================
;==============================================================
