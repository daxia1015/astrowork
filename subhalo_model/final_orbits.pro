pro orbit_shape,tra,mhost,rs_host,chost,rpvir,circularity=es,$
                eccentricity=eccentricity,rc_equiv=rcf,$
		rapo=rapo,rperi=rperi,hprofile=hprofile
common tree_parameters ;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
n=1000
rc=10^(findgen(n+1)*alog10(rpvir*10.)/n)-0.9
ec=energy_r(rc,mhost,rs_host,chost,rpvir,/circular,hprofile=hprofile)
;print,max(ec)
;sn=where(tra.r gt 2.14*rpvir,count1)
;ef=tra.energy
;sn=where((ef gt max(ec))and(tra.r lt rpvir*2.14),count)
;print,count,min(tra[sn].r),max(tra[sn].r),min(tra[sn].v),max(tra[sn].v)
;ef=ef > min(ec)
rcf=interpol(rc,ec,tra.energy)
m_rcf=mhalo_r(rcf,rs_host,chost,mhost,hprofile=hprofile)
es=tra.l/sqrt(gaussc*m_rcf*rcf)
;sn=where((es lt 0.05),count)
es=temporary(es) < 1. ;orbital circularity
;rapo=rcf*(1+sqrt(1-es^2))
;sn=where(rapo lt 5.,count)
;print,count
eccentricity=sqrt(1-es^2)  ;orbital eccentricity
rapo=rcf*(1+eccentricity)  ;apocenter
rperi=rcf*(1-eccentricity)  ;pericenter

end

;=====================================================
function tra_s2l,trav,mhost,rs_host,chost,rvir_h,n,hprofile=hprofile;,tree=tree
;n=n_elements(trav)
;trai={t:0.,m:0.,l:0.,energy:0.,vr:0.,vth:0.,v:0.,r:0.,theta:0.,k:0.,es:0.,apo:0.,ID:0.}
tra=replicate(!halo.tral,n)
struct_assign,trav,tra,/nozero,/verbose

tra.vth=tra.l/tra.r
tra.v=sqrt(tra.vr^2+tra.vth^2)
tra.energy=energy_r(tra.r,mhost,rs_host,chost,rvir_h,tra.v,hprofile=hprofile)
fg=f_g(tra.r,rs_host,chost,mhost,hprofile=hprofile)
tra.k=tra.vth*fg/tra.v^3  ;& print,'k=',tra_i.k          ;trajectory curvature
if n_elements(mhost) gt 1 then begin
  sn=uniq(rvir_h,sort(rvir_h)) & nuniq=n_elements(sn)
  for i=0,nuniq-1 do begin
    j=sn[i]
    sni=where(rvir_h eq rvir_h[j],count)
    orbit_shape,tra[sni],mhost[j],rs_host[j],chost[j],rvir_h[j],$
    circularity=es,rapo=rapo,hprofile=hprofile
    tra[sni].es=es & tra[sni].apo=rapo
  endfor
endif else begin
  orbit_shape,tra,mhost,rs_host,chost,rvir_h,circularity=es,rapo=rapo,hprofile=hprofile
  tra.es=es
  tra.apo=rapo 
endelse
return,tra
end

