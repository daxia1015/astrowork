function read_halon,hdir,mpname,nhalos,major=major,rmax=rmax,$
                    rpvir=rpvir,mf=mf,$
                    itree=itree,alive=alive,alltrees=alltrees
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
nhalos=lonarr(ntree) 
openr,lun,hdir+'halonet.'+mpname+!wdir.major[major],/get_lun
readu,lun,nhalos
if keyword_set(alltrees) then begin
  halon=replicate(!halo.out,total(nhalos,/int))
  readu,lun,halon
  if keyword_set(alive) then begin
    if keyword_set(rmax) then begin
      sn=where(halon.tra.apo lt rmax,count)
      if count ne 0 then halon[sn].tra.ID=0.1     
    endif
    if keyword_set(rpvir) then begin
      sn=where(halon.tra.r gt rpvir,count)
      if count ne 0 then halon[sn].tra.ID=0.8
    endif
    if keyword_set(mf) then begin
      sn=where(halon.tra.m lt mf,count)
      if count ne 0 then halon[sn].tra.ID=0.4
    endif
    ns=nhalos & out=!halo.out & k=0L
    for i=0,ntree-1 do begin
      halo=halon[k:k+nhalos[i]-1]
      sn=where(halo.tra.ID ge 1,count)
      ns[i]=count
      out=[temporary(out),halo[sn]]
      k+=nhalos[i]
    endfor
    nhalos=ns & halon=out[1:n_elements(out)-1]
  endif
endif else begin
  halon=replicate(!halo.out,nhalos[itree])
  point_b=0LL
  out_b=(n_tags(!halo.out)-1+n_tags(!halo.tral))*4
  if itree gt 0 then point_b=total(nhalos[0:itree-1],/int)*out_b
  point_lun,lun,ntree*4+point_b
  readu,lun,halon
endelse
;if eof(lun) then print,'end of '+hdir+'halonet.'+mpname+!wdir.major[major]
free_lun,lun
return,halon
end
;=============================================================
;=============================================================
;=============================================================
function read_halon_track,hdir,mpname,nhalos,nstepv,major=major,$
                          itree=itree,matrix=matrix,ihalo=ihalo,$
			  alltrees=alltrees
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
nhalos=lonarr(ntree)
openr,lun,hdir+'halonet.'+mpname+!wdir.major[major]+'_track',/get_lun
readu,lun,nhalos              ;file pointer is here
nstept=lonarr(total(nhalos,/int))
readu,lun,nstept              ;;file pointer is here
;if keyword_set(
nstepv=lonarr(max(nhalos),ntree) & ii=0L
for i=0,ntree-1 do begin
  nstepv[0:nhalos[i]-1,i]=nstept[ii:ii+nhalos[i]-1]
  ii+=nhalos[i]
endfor

if keyword_set(alltrees) then begin
  tralt=replicate(!halo.tral,total(nstept,/int))
  readu,lun,tralt                     ;file pointer is here
  if keyword_set(matrix) then begin
    tralv=replicate(!halo.tral,max(nhalos),max(nstepv),ntree)
    kk=0LL
    for i=0,ntree-1 do begin
      for k=0,nhalos[i]-1 do begin
        tralv[k,0:nstepv[k,i]-1,i]=tralt[kk:kk+nstepv[k,i]-1]
	kk+=nstepv[k,i]
      endfor
    endfor
  endif else begin
    nstepv=nstept
    tralv=tralt
  endelse
endif else begin
  nhalo=nhalos[itree]
  nstept=nstepv[0:nhalo-1,itree]
  tralt=replicate(!halo.tral,total(nstept,/int))
  point_b=0LL
  if itree gt 0 then point_b=total(nstepv[*,0:itree-1],/int)*n_tags(!halo.tral)*4
  point_lun,lun,ntree*4+total(nhalos,/int)*4+point_b
  readu,lun,tralt 
                          ;file pointer is here
  nstepv=nstept
  if n_elements(ihalo) ne 0 then begin
    kk=0LL
    if ihalo gt 0 then kk=total(nstepv[0:ihalo-1],/int)
    tralv=tralt[kk:kk+nstepv[ihalo]-1]    
  endif else begin
    if keyword_set(matrix) then begin
      tralv=replicate(!halo.tral,nhalo,max(nstepv))
      kk=0LL
      for k=0,nhalo-1 do begin
        tralv[k,0:nstepv[k]-1]=tralt[kk:kk+nstepv[k]-1]
        kk+=nstepv[k]
      endfor
    endif else tralv=tralt
  endelse
endelse
;if eof(lun) then print,'end of file'
free_lun,lun

return,tralv
end
