pro write_tra,mhost
!except=2
!key.hostz=0 & !m.z=0
mr=!mrv[where((!mrv gt 0.3)and(!mrv le 0.5))] 
if n_elements(mhost) eq 0 then begin
  mhost=1.e12 ;& a=;strtrim(string(mhost),2)
  chost=8.5
  csub=chalo(mr,chost,/normalize)
endif else begin
  chost=chalo(mhost,!m.z)
  csub=chalo(mr*mhost,!m.z)
endelse
msub=mr*mhost
mh_name=num2str(mhost,format='(e8.2)')
;mh_name=strmid(a,0,4)+'e'+strmid(a,strlen(a)-2,2)
Mname=num2str(mr,format='(f7.5)')
ti=0. & dti=0.1 & dtmin=0. & dtmax=0.2
eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
yita=1. & epson=!epsonv ;& print,'yita=',yita
nm=n_elements(mr) & ny=n_elements(yita) & nu=n_elements(epson)
halosetting,1
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
;----halt condition define the merger time
;0:do NOT stop, 1:mass, 2:time, 3:angular momentum, 4:mass or l,
;5:l or time, 6:mass or l or time, 7:rt lt rs_sub or l
!key.At=0 
for bs=2,2 do begin
  !key.bs=fix(strmid(!wdir.bs[bs],2,1)) & print,'bs=',!wdir.bs[bs],bs
  halt=6-!key.bs
  if strpos(!wdir.bs[bs],'iso') gt 0 then hprofile=1 else hprofile=2
  if strpos(!wdir.bs[bs],'th0') gt 0 then heating0=0 else heating0=1
  px=strpos(!wdir.bs[bs],'x')
  if px gt 0 then !key.nex=fix(strmid(!wdir.bs[bs],px+1,1)) else !key.nex=0
  pc=strpos(!wdir.bs[bs],'c')
  if pc gt 0 then contraction=float(strmid(!wdir.bs[bs],pc+1,1)) else contraction=1.
  ;print,contraction
  !m.irs=float(strmid(!wdir.bs[bs],5,4))
  for ld=10,10 do begin
    !ld.i=fix(strmid(!wdir.ld[ld],2,1)) & print,'lnlda=',!wdir.ld[ld]
    for mlr=1,3 do begin
      !mlr=float(strmid(!wdir.mlr[mlr],0,3)) & print,'mlr=',!mlr
      if !mlr eq 0 then heating=0 else heating=heating0
      if(strpos(!wdir.ld[ld],'fit') gt 0)and $
        ((strpos(!wdir.bs[bs],'x4') eq -1)or $
        (strpos(!wdir.mlr[mlr],'3.5') eq -1))then continue      
      dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      for k=0,nm-1 do begin
        print,'mr=',Mname[k]
        if strpos(!wdir.bs[bs],'rs') gt 0 then mf=msub[k]*gx(!m.irs)/gx(csub[k])
	if strpos(!wdir.bs[bs],'gs') gt 0 then mbaryon=msub[k]*!m.irs*omegab/omega0
        if (!key.bs eq 1)or(!m.irs eq 0) then mf=1e4
        openw,lun0,dir+'tra_'+mh_name+'_'+Mname[k]+'_info',/get_lun
        openw,lun1,dir+'tra_'+mh_name+'_'+Mname[k],/get_lun
        printf,lun0,mr[k],mhost,csub[k],chost,ny,nu,format='(f9.6,e14.6,2f6.2,2i4)'
        for j=0,ny-1 do begin
	  print,'yita=',yita[j]
          for i=0,nu-1 do begin
	    if !ld.i le 1 then begin
	      if strpos(!wdir.ld[ld],'fit') gt 0 then begin
	        curveld,reform([mr[k],epson[i],yita[j]],1,3),c,fldx,pder,/cknown
	        !ld.x=fldx ;
	      endif else !ld.x=float(strmid(!wdir.ld[ld],3+!ld.i*4,3))
	    endif
            print,'es=',epson[i]
            if mr[k] ge 0.05 then tf=total(20.+5.*where(!epsonv eq epson[i])) else tf=14.
            dti=0.1
;	    print,!key.th,!key.bs,!key.nex,mf,ti,tf,dti
            tra=trajectory(msub[k],mhost,mz,csub[k]*contraction,chost,yita[j],epson[i],$
	                   mf,mbaryon,ti,tf,dti,dtmin,dtmax,eps,$
			   hprofile=hprofile,$
			   halt=halt,df=1,td=1,th=heating,bs=!key.bs)
            n=n_elements(tra)
	    tt=max(tra.t)-ti & lf=min(tra.l)/tra[0].l
            printf,lun0,yita[j],epson[i],n,tt,lf,min(tra.apo),tra[n-1].ID,$
	    format='(2f5.1,i10,3e15.6e3,f5.1)'
            ;printf,lun1,tra,format='(10e15.6e3,2x,f3.1)' 
	    writeu,lun1,tra     
          endfor
        endfor
        free_lun,lun0,lun1
      endfor
    endfor
  endfor
endfor
end

;============================================================
function read_tra_info,dir,mr,mhost,mrk
mh_name=num2str(mhost,format='(e8.2)')
;Mname=strmid(strtrim(string(mr),2),0,6)
Mname=num2str(mr,format='(f7.5)')
mrk={mr:9.6,mhost:12.6,csub:6.2,chost:6.2,ny:4,nu:4}
openr,lun,dir+'tra_'+mh_name+'_'+Mname+'_info',/get_lun
  readf,lun,mrk,format='(f9.6,e14.6,2f6.2,2i4)'
;  print,double(mrk.mr)-double( mr) 
  if (mrk.mr ne mr)or(mrk.mhost ne mhost) then message,'Sorry! NO mass ratio or host mass MATCHED.'
  info=replicate({yita:0.,epson:0.,nstep:0L,tt:0.,lf:0.,rapo:0.,ID:1.},mrk.ny*mrk.nu)
  readf,lun,info,format='(2f5.1,i10,3e15.6e3,f5.1)'
free_lun,lun
;print,info,format='(f4.1,2x,f4.1,2x,i10,2x,e12.6)'
return,info
end
;====================================================================
;==================================================================
function read_tra,dir,mrk,tra_info,epson,yita
mh_name=num2str(mrk.mhost,format='(e8.2)')
;Mname=strmid(strtrim(string(mrk.mr),2),0,6)
Mname=num2str(mrk.mr,format='(f7.5)')
if mrk.ny eq 1 then i=0 else i=where(!yitav eq yita) 
j=where(!epsonv eq epson)
if(i eq -1)or(j eq -1)then message,'Sorry! No orbital parameters MATCHED.'
kk=long(total(i*mrk.nu+j)) ;& print,nn
skip_n=0LL

if kk ge 1 then $
;for k=0,kk-1 do skip_n+=tra_info[k].nstep
skip_n=total(tra_info[0:kk-1].nstep,/int)
n=tra_info[kk].nstep
;print,n
;trai={t:0.,m:0.,l:0.,energy:0.,vr:0.,vth:0.,v:0.,r:0.,theta:0.,k:0.,es:0.,apo:0.,ID:1.}
openr,lun,dir+'tra_'+mh_name+'_'+Mname,/get_lun
  tra=replicate(!halo.tral,n)
  ;skip_lun,lun,skip_n,/lines
  point_lun,lun,skip_n*n_tags(!halo.tral)*4
  ;readf,lun,tra,format='(10e15.6e3,2x,f3.1)'
  readu,lun,tra
free_lun,lun
return,tra
end
;===============================================
;===============================================
function read_tra_any,filename,tra=tra
info={t:0.,t0:0.,ld:0.,nstep:0L}
openr,lun,filename,/get_lun ;==including directory
readu,lun,info
if arg_present(tra) then begin
  tra=replicate(!halo.tral,info.nstep)
  readu,lun,tra
endif
free_lun,lun
return,info
end
;=====================================================================
;=====================================================================
function custom_tra,bsv,ldiv,ldxv,mlrv,mrv,yita,epson
!except=2 
mhost=1e12 & !key.hostz=0 & !m.z=0
ti=0. & chost=8.5  & dtmin=0. & dtmax=0.4
msub=mrv*mhost & csub=chalo(mrv,chost,/normalize)

eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
nbs=n_elements(bsv) & nldi=n_elements(ldv) & nldx=n_elements(ldxv) 
nmlr=n_elements(mlrv)
nm=n_elements(mrv) & ny=n_elements(yita) & nu=n_elements(epson)
halosetting,1
ntra=nbs*nld*nmlr*nm*ny*nu
tag='tra'+strtrim(sindgen(ntra),2)
ktra=0

for bsi=0,nbs-1 do begin
  bs=bsv[bsi]
  !key.bs=fix(strmid(!wdir.bs[bs],2,1)) & print,'bs=',!key.bs
  halt=6-!key.bs
  if strpos(!wdir.bs[bs],'th0') gt 0 then !key.th=0 else !key.th=1
  !m.irs=float(strmid(!wdir.bs[bs],5,strlen(!wdir.bs[bs])-5))
  for ldi=0,nldi-1 do begin
    !ld.i=ldiv[ldi]
    for ldx=0,nldx-1 do begin
      !ld.x=ldxv[ldx]
      for mlr=0,nmlr-1 do begin
        !mlr=mlrv[mlr] & print,'mlr=',!mlr
        for k=0,nm-1 do begin
          if strpos(!wdir.bs[bs],'rs') gt 0 then mf=msub[k]*gx(!m.irs)/gx(csub[k])
	  if strpos(!wdir.bs[bs],'gs') gt 0 then mf=msub[k]*!m.irs*omegab/omega0
          if !m.irs eq 0 then mf=1e4
          for j=0,ny-1 do begin
            for i=0,nu-1 do begin
              tf=total(20.+5.*where(!epsonv eq epson[i]))
              tra=trajectory(msub[k],mhost,mz,csub[k],chost,yita[j],epson[i],$
	                     mf,ti,tf,0.1,dtmin,dtmax,eps,$
		             halt=halt,df=1,td=1,th=!key.th,bs=!key.bs)
              if ktra eq 0 then trav={tra0:tra} else trav=create_struct(trav,tag[ktra],tra)
              ktra++
            endfor
          endfor
        endfor
      endfor
    endfor
  endfor
endfor
help,ktra
help,trav,/struc
return,trav
end
