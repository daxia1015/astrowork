pro plot_mhaloz
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
major=1 & mp=0 & !key.hostz=1
dir=!wdir.mp[mp]+!wdir.d
tree_series,dir,z_lev,nhalos,n_levs

nbin=10
sn=indgen(nbin)*4+2 ;& print,sn
zs=z_lev[sn] 
mhost=fltarr(nbin,ntree) & y_ave=fltarr(nbin) & dy=fltarr(nbin)
char=1.1
!p.charsize=char & !x.charsize=char & !y.charsize=char
device,filename='treedata/image/mhaloz.ps',/color,/encapsul,$
xsize=10,ysize=9
plot,findgen(10)+1,findgen(10)*0.1+1e-2,/nodata,/xlog,/ylog,$
xrange=[1,12],yrange=[0.009,1.1],position=[0.2,0.14,0.98,0.98],$
xtitle='1+z',ytitle='M(z)/M(z=0)'
iv=fix(randomu(100,10)*100)
iv=iv[sort(iv)] ;& print,iv
k=0
for i=0,ntree-1 do begin
  readtree,dir,nhalos,n_levs,nplev=nplev,itree=i
  mhost[*,i]=nplev[sn].mhost
;  print,nplev[sn].mhost
  if (k le 9) then begin
    if (i eq iv[k]) then begin
      oplot,1+nplev.z,nplev.mhost/mphalo,thick=2
      ;print,k,i
      k++
    endif
  endif
endfor
mhost=mhost/mphalo
for ibin=0,nbin-1 do begin
  y_ave[ibin]=mean(mhost[ibin,*])
  aa=moment(mhost[ibin,*],sdev=dev)
  dy[ibin]=dev
endfor
;print,mhost,format='(10f8.4)'
ply=6
oplot,1+zs,y_ave,thick=ply  
dye=exp(dy/y_ave)
errplot,1+zs,y_ave/dye,y_ave*dye,width=0.02,thick=ply
end
;==========================================================
;==========================================================
pro plot_itree,iname,z_lev,n_lev,nhalo,nplev,tree,major=major
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
cc=intarr(nhalo)+1  ;color of circle
cl=intarr(nhalo)+1  ;color of line
x0v=fltarr(nhalo) & y0v=fltarr(nhalo)
zname=string(nplev.z,format='(f4.2)') ;strmid(strtrim(round(nplev.z*100.)/100.,2),0,4)
treen=treenet(z_lev,nhalo,n_lev,nplev,tree,major=major)
if major eq 0 then begin
  cc-- & cl--
  cc[treen.sub-1]=1 & cc[treen.host-1]=1
  cl[treen.sub-1]=1
endif

device,filename='treedata/image/tree_'+iname+'.ps',$
/color,/encapsul,xsize=19,ysize=27
plot,findgen(40),findgen(40),/nodata,/iso,$
xrange=[0,40],yrange=[0,60],xstyle=5,ystyle=5,$
title='tree '+iname

y0=1. & r0=0.25 & dc=0.4
for i=0,n_lev-1 do begin
  x0=0.
  for j=0,nplev(i).np_lev-1 do begin
    k=nplev[i].fp_lev+j-1
    treek=tree[k]
    R=alog10(1.1*treek.mhalo/mres)*r0
    x0=x0+R+dc             ;center of next circle
    circle,x0,y0,R,color=!myct.dkred*cc[k] ;,oc=1           ;plot halo as circle 
    ;==start here to connect correlative halo
    x0v[k]=x0 & y0v[k]=y0    ;store center of every circle
    kf=treek.k_parent-1
    if(k ge 1) then oplot,[x0,x0v[kf]],[y0,y0v[kf]],$
    color=!myct.blue*cl[k],thick=cl[k]  ;connect correlative halo, begin to connect halo from the second halo
    ;==end here to connect halo
    x0=x0+R                ;nearby center of next circle
    if(j eq 0)then R_y=R    ;distance between this time level and next time level
  endfor
  xyouts,-2.2,y0-0.2,zname[i]
  R_y=R_y>r0
  y0=y0+2*R_y+0.3       ;center of next time level  
endfor
xyouts,-2.5,-1,'redshift(z)' ;,charsize=1.2

end


pro plot_tree
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
!except=1
char=1 & ply=2 & len=0.04 
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char & !x.ticklen=len
!y.thick=ply & !y.style=1 & !y.charsize=char & !y.ticklen=len
treedir='treedata/1.77e12_5.00e09/' 
mpname=strmid(treedir,9,15)
parameters,treedir
tree_series,treedir,z_lev,nhalos,n_levs

for itree=0,ntree-1,5 do begin
  iname=string(itree,format='(i3.3)')
  print,'itree: ',iname
  readtree,treedir,nhalos,n_levs,nplev=nplev,tree=tree,itree=itree

  n_lev=n_levs[itree] & nhalo=nhalos[itree]
  
  for major=0,0 do plot_itree,iname,z_lev,n_lev,nhalo,nplev,tree,major=major

endfor
end
