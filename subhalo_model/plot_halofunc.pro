pro plot_halofunc
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=0,major=1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
char=1.1

device,filename='halofunc/mfunc_any.ps',/color,/encapsul,$
xsize=16,ysize=13.5
multiplot,[2,2],mxtitle='m/M',mytitle='dN/dln(m/M)',mxtitoffset=0.5,$
mytitoffset=-0.5,mxtitsize=1.2,mytitsize=1.2,myposition=[0.11,0.15,0.99,0.98]
!p.charsize=char & !x.charsize=char & !y.charsize=char
bs=2
tick0=replicate(' ',4) & tick1=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n']
xtick=[[tick0],[tick0],[tick1],[tick1]]
tick0=replicate(' ',5) & tick1=['10!u-2!n','10!u-1!n','1','10','10!u2!n']
ytick=[[tick1],[tick0],[tick1],[tick0]]
help,xtick,ytick
for ld=0,3 do begin
  plot,xh.mev,halofunc0.m.ev.giocoli[*,1],linestyle=2,$              ;evolved, data of Giocoli 2007
  xrange=[5e-5,0.5],yrange=[1.001e-2,4e2],/xlog,/ylog,$
  xtickname=xtick[*,ld],ytickname=ytick[*,ld] 
  oplot,xh.mun,halofunc0.m.un.giocoli,linestyle=5
;  print,xh.mun,halofunc0.m.un.giocoli
  oplot,xh.mun,halofunc.m.un,linestyle=1     ;unevolved merger tree.
  for mlr=1,3 do begin
    y=halofunc.m.ev[*,ld,mlr,bs]
;    dy=halofunc.m.err[*,ld,mlr,bs]
;    dye=exp(dy/y)
    oplot,xh.mev,y,color=!myct.blue,thick=mlr*4
;    if(mlr eq 2)then errplot,xh.mev,y/dye,y*dye,color=!myct.blue,width=0.02
  endfor
  xyouts,8e-5,2.5e-2,'ln!mL!x='+!ld.form[ld]
  if ld eq 0 then labeling,0.05,0.35,0.1,0.08,'A='+['1.','3.5','10.'],$
  /lineation,thickv=[4,8,12],color=!myct.blue
  multiplot
endfor
multiplot,/default
xyouts,0.45,0.92,'M1',/normal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
device,filename='halofunc/mfunc_ok.ps',/color,/encapsul,$
xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.mev,halofunc0.m.ev.giocoli[*,1],linestyle=2,$              ;evolved, data of Giocoli 2007
xrange=[5e-5,0.5],yrange=[1.001e-2,4e2],/xlog,/ylog,$
xtickname=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'],$
ytickname=['10!u-2!n','10!u-1!n','1','10','10!u2!n'],$
xtitle='m/M',position=[0.18,0.16,0.97,0.97];,ytitle='dn/dln(m/M)'
xyouts,0.06,0.58,'dN/dln(m/M)',/normal,orient=90,align=0.5,charsize=1.2
;oplot,xh.mun,halofunc.m.un,linestyle=1     ;unevolved merger tree.
for ld=6,6 do begin
  y=halofunc.m.ev[*,ld,2,5] & dy=halofunc.m.err[*,ld,2,5]
  dye=exp(dy/y)
  oplot,xh.mev,y,linestyle=0 ;,thick=mlr*4
;  errplot,xh.mev,y/dye,y*dye,color=!myct.blue,width=0.02
endfor
xyouts,8e-5,0.1,'M2!cA=3.5!cln!mL!x=ln[M(t)/m(t)]+C'
;labeling,0.01,0.2,0.15,0.1,ldform,ct=!myct.c2,linestyle=[3,4],/lineation ;,charsize=1
;===============================================================;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
rrange=[0.05,1] & nrange=[0.003,1.1] & xtit='r/R!dvir!n' & ytit='N(<r)/N(<R!dvir!n)'
;===============================================================;
;------M2----------------------
bs=5 & ld=6 & mlr=2 & mc=1  & char=1.1
device,filename='halofunc/rfunc1.ps',/encapsul,/color,xsize=10,ysize=8 ;,xoffset=1.5,yoffset=10
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
xrange=rrange,yrange=nrange,xtitle=xtit,ytitle=ytit,$
position=[0.21,0.17,0.96,0.97]
oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,0]
oplot,xh.r,halofunc0.r.MW,psym=6
shadowing,xh.r,halofunc0.r.diemand[*,mc],halofunc0.r.einasto50,density=8,thick=3

labeling,0.77,0.33,0.05,0.08,'MW',/symboling,psym=6
labeling,0.7,0.25,0.12,0.08,'M2',/lineation
shadowing,findgen(12)/100+0.7,fltarr(12)+0.11,fltarr(12)+0.05,density=0.8,thick=3
xyouts,0.82,0.05,'SIM'
;======================================================; 


end
