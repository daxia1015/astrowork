pro plot_halofunc1
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=0,major=1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
char=1.1
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<,.>>>>>>>>>>>>>>>>>>>>>>>>>>>>
;radidal distribution of subhalo mass
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;`;;;;;;;;;;;;;;;;;;;;;;;;;;
bs=5 & ld=6 & mlr=2 & mc=1 &  char=1.1
device,filename='halofunc/rmfunc_M2.ps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.rm,halofunc0.rm.diemand,/xlog,/ylog,/nodata,$ 
xrange=[0.03,1],yrange=[5e-3,0.3],xtitle='r/R!dvir!n',$
ytitle='m(r)/M(r)',$
position=[0.21,0.17,0.96,0.97]
oplot,xh.rm,halofunc.rm[*,ld,mlr,bs]
oplot,xh.rm,halofunc0.rm.springel,linestyle=1
oplot,xh.rm,halofunc0.rm.diemand,linestyle=2
labeling,0.6,0.22,0.1,0.08,['M2','Aqurius','via Lactea'],$
/lineation,linestyle=[0,1,2]
;print,halofunc.rm[*,ld,mlr,bs],' ',halofunc.rm[*,3,mlr,bs]
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<,.>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
;radidal distribution with baryon
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ld=6 & mlr=2 & char=1.1
device,filename='halofunc/rfunc_bs1.ps',/encapsul,/color,xsize=10,ysize=8 ;,xoffset=1.5,yoffset=10
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
xrange=rrange,yrange=nrange,xtitle=xtit,ytitle=ytit,$
position=[0.21,0.17,0.96,0.97]
;oplot,xr,halofunc0.r.einasto200,linestyle=2,thick=ply+1
;oplot,xr,halofunc0.r.diemand[*,mc],linestyle=2
;oplot,xr,halofunc0.r.einasto50,linestyle=3
;oplot,xh.r,halofunc0.r.DM,linestyle=5
oplot,xh.r,halofunc0.r.MW,psym=6
shadowing,xh.r,halofunc0.r.diemand[*,1],halofunc0.r.einasto50,density=8,thick=3
for bs=3,4 do $
oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,1],thick=(5-bs)*4
labeling,0.77,0.32,0.05,0.08,'MW',/symboling,psym=6
labeling,0.7,0.24,0.12,0.08,['5%','30%'],/lineation,$
ct=[198,198],thickv=[4,8]
shadowing,findgen(12)/100+0.7,fltarr(12)+0.11,fltarr(12)+0.05,density=0.8,thick=3
xyouts,0.82,0.05,'SIM'
;================================================================
;================================================================
rrange=[0.05,1] & nrange=[0.003,1.1] & xtit='r/R!dvir!n' & ytit='N(<r)/N(<R!dvir!n)'
bs=2 & mc=1
device,filename='halofunc/rfunc_any.ps',/encapsul,/color,xsize=16,ysize=13.5
multiplot,[2,2],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=0.5,$
myposition=[0.13,0.16,0.97,0.98]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for ld=0,3 do begin
  plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
  xrange=rrange,yrange=nrange
;  oplot,xh.r,halofunc0.r.MW,psym=6
  for mlr=1,3 do begin
    oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,mc],$
    linestyle=mlr
  endfor
  xyouts,0.06,0.004,'ln!mL!x='+!ld.form[ld]
  if ld eq 0 then begin
    labeling,0.5,0.3,0.12,0.1,'A='+['1.','3.5','10.'],$
    /lineation,linestyle=[1,2,3]
  endif
  multiplot
endfor
multiplot,/default
xyouts,0.15,0.92,'M1',/normal
;=============================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;----segregation of subhalos radial distribution
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mlr=2  & char=1.1 & vlcolor=!myct.cyan
civ=[[2,3,4],[3,4,5],[1,2,3],[3,4,5]] 
bs=5 & ld=6 & mlr=2
psname=['m(z=0)/M(z=0)','m(z!dacc!n)/M(z=0)',$
        'accretion time(z)','m(z!dacc!n)/M(z!dacc!n)']
for mi=0,1 do begin
  Mname='M'+string(mi+1,format='(i1)')
  bs=2+mi*3 & ld=3+mi*3
  device,filename='halofunc/rfunc_mczc1_'+Mname+'.ps',$
  /encapsul,/color,xsize=16,ysize=14
  multiplot,[2,2],mxtitle=xtit,mytitle=ytit,$
  mxtitsize=char,mytitsize=char,myposition=[0.11,0.18,0.97,0.9],$
  mtitle=Mname,mtitoffset=-0.5
  !p.charsize=char & !x.charsize=char & !y.charsize=char 
  for psi=0,3 do begin
    plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$  
    xrange=rrange,yrange=nrange;,xtickname=['0.1','1'],ytickname=['0.01','0.1','1']
    for si=0,2 do begin
  ;    if(si eq 2)and(psi eq 2)then continue
      ci=civ[si,psi]
      case psi of
        0:begin 
            y=halofunc.r.mcf[*,ld,mlr,bs,ci]
	    oplot,xh.r,halofunc0.r.diemand[*,ci],linestyle=si,color=vlcolor
          end
        1:y=halofunc.r.mci[*,ld,mlr,bs,ci]
        2:begin
	    y=halofunc.r.zc[*,ld,mlr,bs,ci]
;	    print,mi,si,halofunc.r.zc_ntot[ld,mlr,bs,ci]
	  end
        3:y=halofunc.r.mcii[*,ld,mlr,bs,ci]
        else:message,'out of range'
      endcase
      oplot,xh.r,y,linestyle=si ;,color=!myct.blue      
    endfor
    xyouts,0.2,0.04,psname[psi]
    case psi of
      0:begin
        charv=['[10!u-5!n,10!u-4!n]','[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]']
        ;lstyle=[0,1,2]
      end
      1:begin
        charv=['[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]','[10!u-2!n,1]']
        ;lstyle=[0,1,2]
        end
      2:begin
        ;charv=['z<6.','z<1.']
        charv=['[2,6]','[0.5,2]','[0,0.5]']
	;lstyle=[0,1,2]
        end
      3:begin
        charv=['[10!u-4!n,10!u-3!n]','[10!u-3!n,10!u-2!n]','[10!u-2!n,1]']
        end
      else:message,'out of range'
    endcase
    labeling,0.5,0.35,0.13,0.12,charv,/lineation,linestyle=[0,1,2]
    if psi eq 0 then xyouts,0.93,0.6,'Via Lactea',charsize=1,align=1,color=vlcolor
    multiplot
  endfor
  multiplot,/default
endfor
;-------------------M1--------------------------
bs=2 & ld=3 & mlr=2 & mc=1 & zc=1 & char=1.1
device,filename='halofunc/rfunc_M1.ps',/encapsul,/color,xsize=10,ysize=8 ;,xoffset=1.5,yoffset=10
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$                       ;unevolved, fitted by Giocoli 2007
xrange=rrange,yrange=nrange,xtitle=xtit,ytitle=ytit,$
position=[0.21,0.17,0.96,0.97]
oplot,xh.r,halofunc.r.mcf[*,ld,mlr,bs,mc]
oplot,xh.r,halofunc0.r.MW,psym=6
shadowing,xh.r,halofunc0.r.diemand[*,mc],halofunc0.r.einasto50,density=8,thick=3

labeling,0.77,0.25,0.05,0.08,'MW',/symboling,psym=6
labeling,0.7,0.17,0.12,0.08,'M1',/lineation
shadowing,findgen(12)/100+0.7,fltarr(12)+0.11,fltarr(12)+0.05,density=0.8,thick=3
xyouts,0.82,0.05,'SIM'
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;=======================================================
;==============================================================
mlr=2
device,filename='halofunc/orbit.ps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
xy=findgen(100)*0.01+1e-3
plot,xy,xy,/nodata,xrange=[0.1,1],yrange=[1.1e-3,1.1],$
xtitle='j(z=0)/j(z!dacc!n)',ytitle='fraction',/ylog,$
position=[0.2,0.18,0.95,0.96]
for mi=0,1 do begin
  oplot,xh.j,halofunc.j[*,3+mi*3,mlr,2+mi*3],psym=10,$
  linestyle=1-mi,color=!myct.c2[mi]
  ; M1: ld=3, bs=2; M2: ld=6, bs=5
endfor
labeling,0.1,0.9,0.1,0.1,['M1','M2'],linestyle=[1,0],ct=!myct.c2,/lineation
;==============================================================
  
end


;mlr=2
;device,filename='halofunc/rfunc_zc.ps',/encapsul,/color,xsize=16,ysize=10;;;;

;  multiplot,[2,1],mxtitle=xtit,mytitle=ytit,$
;  mxtitsize=char,mytitsize=char,myposition=[0.11,0.18,0.97,0.9]
;  !p.charsize=char & !x.charsize=char & !y.charsize=char 
;  for mi=0,1 do begin
;    Mname='M'+string(mi+1,format='(i1)')
;    bs=2+mi*3 & ld=3+mi*3
;    plot,xh.r,halofunc0.r.DM,/xlog,/ylog,/nodata,$  
;    xrange=rrange,yrange=nrange,title=Manme
;    for ci=1,7 do begin
;      y=halofunc.r.zc[*,ld,mlr,bs,ci]
;      print,mi,ci,halofunc.r.zc_ntot[ld,mlr,bs,ci]
;      oplot,xh.r,y,linestyle=ci-1,color=!myct.c7[ci-1]      
;    endfor
;    multiplot
;  endfor
;  multiplot,/default

