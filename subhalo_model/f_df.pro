function vcir,x,c,mvir,rvir
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
v_vir=sqrt(gaussc*mvir/rvir)
vc=v_vir*sqrt(c*gx(x)/x/gx(c))  ;x=r/rs
return,vc
end


function f_df,msub,mhost_r,ro_h_r,v_orb,r_orb,imset,rx,hprofile=hprofile
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun

Rbnd=rx*imset.rs_sub
case !ld.i of 
  0:lnlda=imset.ld
  1:lnlda=alog(imset.mhost/msub)+imset.ld
  2:lnlda=alog(imset.mhost/imset.msubi) 
  3:begin
;     Rbnd=rx*imset.rs_sub  ;& if x_sub lt 0 then print,Rbnd,x_sub
     Ixsub=0.10947*rx^3.989/(1+0.90055*rx^1.099+0.03568*rx^1.189+0.06403*rx^1.989)
     lnlda=alog(r_orb/Rbnd)+Ixsub/gx(rx)^2 ; & print,lnlda    
    end
  4:lnlda=2.5+alog(imset.msubi/msub)
  5:begin
     bmin=gaussc*msub/v_orb^2  ;) >Rbnd
     lnlda=alog(1+(r_orb/bmin)^2)/2.
    end
  6:lnlda=alog(1+imset.mhost/imset.msubi)
  7:lnlda=alog(1+imset.mhost/imset.msub)
else:print,'Coulomb logarithm should be defined!'
endcase

case hprofile of 
  1:X=v_orb/imset.Vvir_host
  2:begin
    xh=r_orb/imset.rs_host
    sigma=imset.vcmax_host*1.4393*xh^0.354/(1+1.1756*xh^0.725)
    X=v_orb/sigma/sqrt(2.) ;& if(X gt 10.)then print,X
  end
  else:message,'halo profile should be defined!!!'
endcase
;X=v_orb/sqrt(gaussc*mhost_r/r_orb)
X=temporary(X) < 4.01
IX=erf(X)-2*X*exp(-X^2)/sqrt(!pi)
f=4*!pi*lnlda*gaussc^2*msub*ro_h_r*IX/v_orb^2   ;M_{sun}kpc Gyr^{-2}
;if(f le 0)then print,'f_df=',f,lnlda                        ;kpc Gyr^{-2}
return,f
end
