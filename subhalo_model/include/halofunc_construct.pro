@include/halofunc_ccv.pro

if n_elements(mp) eq 1 then nmp=1 else nmp=15

nmlr=10 & nld=10 & nbs=10
mfun=dblarr(nbin.mun,nmp)
mfev=dblarr(nbin.mev,nld,nmlr,nbs,nmp) & mfev_err=mfev
tfunc=dblarr(nbin.t,nld,nmlr,nbs,nmp)
drfunc=dblarr(nbin.r,nld,nmlr,nbs,nmp)
rfunc_zc=dblarr(nbin.r,nld,nmlr,nbs,nzc,nmp) & rfunc_zc_ntot=dblarr(nld,nmlr,nbs,nzc,nmp)
rfunc_mci=dblarr(nbin.r,nld,nmlr,nbs,nmc,nmp) & rfunc_mci_ntot=dblarr(nld,nmlr,nbs,nmc,nmp) 
rfunc_mcii=rfunc_mci & rfunc_mcii_ntot=rfunc_mci_ntot
rfunc_mcf=rfunc_mci & rfunc_mcf_ntot=rfunc_mci_ntot
rmfunc=dblarr(nbin.rm,nld,nmlr,nbs,nmp)
efunc=dblarr(nbin.e,nld,nmlr,nbs,nmp) ;& efunci=efunc
jfunc=dblarr(nbin.j,nld,nmlr,nbs,nmp)
rffunc=dblarr(nbin.rf,nld,nmlr,nbs,nmp)
rfunci=dblarr(nbin.ri,nmc,nmp) & rfunci_ntot=dblarr(nmc,nmp)
nfunc=dblarr(3,nmc-1,nld,nmlr,nbs,nmp)
nfunci=dblarr(3,nmc-1,nmp)
pfunc=dblarr(pnbin,nmc-1,nld,nmlr,nbs,nmp)
npx=dblarr(pnbin,nmc-1,nld,nmlr,nbs,nmp)
