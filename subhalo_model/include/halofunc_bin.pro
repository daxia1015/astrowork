ml={mun:-4.,     mev:-4., t:0., r:alog10(0.01), ri:alog10(0.01), rm:alog10(0.01), e:0.01, j:0.1,  rf:0.1 }
mh={mun:-0.35,   mev:-1., t:1., r:alog10(1.),   ri:alog10(1.),   rm:alog10(1.) ,  e: 1.,  j:1. ,  rf:1.9 }
nbin={mun:11,    mev:11,  t:10, r:        10,   ri:        10,   rm:    10,       e: 10,  j:20,   rf:10  }

dbin=ml

for i=0,n_tags(ml)-1 do dbin.(i)=(mh.(i)-ml.(i))/(nbin.(i)-1)

dir0='halofunc/plotdata/'


