mfunc={un:mfun,ev:mfev,err:mfev_err}
rfunc={i:rfunci,zc:rfunc_zc,mci:rfunc_mci,mcii:rfunc_mcii,mcf:rfunc_mcf,$
       i_ntot:rfunci_ntot,zc_ntot:rfunc_zc_ntot,mci_ntot:rfunc_mci_ntot,$
       mcii_ntot:rfunc_mcii_ntot,mcf_ntot:rfunc_mcf_ntot,d:drfunc}

halofunc={m:mfunc,t:tfunc,r:rfunc,rm:rmfunc,e:efunc,j:jfunc,$
    	  rf:rffunc,nfunc:nfunc,nfunci:nfunci,pfunc:pfunc,npx:npx}
