;===========================================================
;---subhalos mass distribution, density profile
function sub_profile,rv,mv,ml,mh,nbin,dbin,mphalo,rpvir,chost,$
                     locations=xr,rlg10=rlg10,nfw=nfw
d2=dbin/2
if ~ keyword_set(locations) then xr=findgen(nbin)*dbin+ml
h=dblarr(nbin)
for i=0,nbin-1 do begin
  sn=where((rv ge xr[i]-d2)and(rv lt xr[i]+d2),count)
  if count ne 0 then h[i]=total(mv[sn])
endfor
if keyword_set(nfw) then begin
  if n_params() le 6 then message,'mphalo, rpvir, chost should be set!'
  rxx=[xr-d2,xr[nbin-1]+d2]
  if keyword_set(rlg10) then rxx=10^rxx
  rxx=rxx*rpvir
;  print,rxx,rpvir,chost,mphalo
  m_r=mhalo_r(rxx,rpvir/chost,chost,mphalo,hprofile=!hprofile)
  y5=m_r[1:nbin]-m_r[0:nbin-1]
  h=h/y5
endif
;print,y5
return,h
end
;============================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;-----Simulation------=======================================
function halofunc_sim,nbin,ml,mh,dbin,xh
common astronomy;,ro_c,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
!key.hostz=1
xun=double(xh.mun) & xev=double(xh.mev) 
xr=double(xh.r) & xrm=double(xh.rm)
xp=xun/0.8
y1=0.21/(xp^0.8*exp(6.283*xp^3))  ;dN(<m)/dln(m/M), unevolved, fitted by Giocoli 2007
mfun={giocoli:y1}

y2=0.0064/xev ;/alog(10)^2   ;-1 ;evolved, fitted by Diemand 2007a
y3=0.869/xev^0.869/10^1.869   ;evolved, fited by Gao 2004???,  -0.869
;y6=alog10(3.26e-5*2.52e7^1.9)-0.9*(xev+alog10(mphalo))  ;evolved, fitted by Springel 2008, -0.9
y6=0.02596/xev^0.9  ;evolved, fitted by Springel 2008, -0.9
y7=10^(-2.05)/xev^0.9/exp(xev^2/0.16^2)         ;evolved, Angulo et al. arxiv:0810.2177, -0.9 
;---begin evolved mass function, Giocoli 2008
out=fltarr(6,47)
openr,lun,'halofunc/plotdata/mfunc_sim_Giocoli.dat',/get_lun
  skip_lun,lun,9,/lines
  readf,lun,out,format='(6a12)'
  if eof(lun) then print,'mfunc_sim_Giocoli.dat read out!'
free_lun,lun
n=n_elements(xev) & xp=alog10(xev)
y4=dblarr(n,5)  ;M_host=10^(11,12,13,14,15) for row=0,1,2,3,4
for i=0,4 do y4[*,i]=interpol(out[5-i,*],out[0,*],xp)
y4=10^y4
;----end evolved mass function, Giocoli  2008

;----evolevd mass function, Ludlow 2008, with unorthodox orbits---
x_l=[-5.3,-5,-4.5,-4,-3.5,-3,-2.5,-2.25]
y_l=[3.06,2.87,2.48,1.97,1.52,1.05,0.43,0.1]  ;8 elements
y5=double(-deriv(x_l,y_l)*10^y_l)
;===all mass function of simulation is here:::
mfev={diemand:y2,gao:y3,giocoli:y4,ludlow:y5,springel:y6,angulo:y7}
mfunc={un:mfun,ev:mfev}
;===========================================================
;-==--begin subhalos radial distribution of simulation----==
mcv=[0.,1e-6,1e-5,1e-4,1e-3,1] & nmc=n_elements(mcv)-1
y1=dblarr(nbin.r,nmc)
out=read_ascii('halofunc/plotdata/vltwosubs.txt',comment_symbol='#',count=k)
out=out.field01  ;& help,out
;out=reform(out,15,k) ;& print,out
snr=where(out[1,*] gt 0)
lgrv_vl=alog10(out[1,snr]/389.) & mvev=out[5,snr]/1.77e12
for mc=0,nmc-1 do begin
  sn=where(mvev ge mcv[mc],count)
  if mc ge 2 then sn=where((mvev ge mcv[mc])and(mvev lt mcv[mc+1]),count)
;  print,mc,count
  rv=lgrv_vl[sn]
  h=histo(rv,ml.r,mh.r,nbin.r,/cumulative)
  y1[*,mc]=h/h[nbin.r-1]     ;Diemand 2006
endfor
y5=12.*xr^3/(1+11.*xr^2)
if !key.hostz then !m.chost=10. else !m.chost=8.5
y2=(1+0.244*!m.chost)*xr^2.75/(1+0.244*!m.chost*xr^2) ; Gao 2004
a=0.678 & r2=0.81 & r200=245.76 & r50=433.48 
y3=igamma(3/a,2*(xr/r2)^a/a)/igamma(3/a,2*(1/r2)^a/a)
r2=r2*r200/r50
y4=igamma(3/a,2*(xr/r2)^a/a)/igamma(3/a,2*(1/r2)^a/a)
y6=gx(xr*!m.chost)/gx(!m.chost)
;=======Milky-Way satellites data
rpvir=r_vir(mphalo,0.) & print,'rpvir=',rpvir,mphalo
out1=read_satellite('halofunc/plotdata/MW_satellite.dat',count=nmw)
rv=alog10(out1.dgc/rpvir)
rfuncmw=histo(rv,ml.r,mh.r,nbin.r,/cumulative)
y7=rfuncmw/rfuncmw[nbin.r-1]
;print,rv,ml.r,mh.r,nbin.r,y7
rfunc={diemand:y1,gao:y2,einasto200:y3,einasto50:y4,$
       madau:y5,DM:y6,MW:y7}
;====end subhalos radial distribution of simulation==============
;=====begin subhalos' mass radial distribution of simulation-------
y3=sub_profile(lgrv_vl,out[5,snr],ml.rm,mh.rm,nbin.rm,dbin.rm,$
    	       1.77e12,389.,!m.chost,/nfw,/rlg10)
lnxr=alog(xrm)
y4=exp(-1.31+0.87*lnxr-0.18*lnxr^2)    ;springel 2008
rmfunc={diemand:y3,springel:y4}
;=====end subhalos mass radial distribution of simulation-------
;=======all halofunc of simulation is here==========
halofunc={m:mfunc,r:rfunc,rm:rmfunc}
return,halofunc
end
;=========================================================
;==========================================================
pro halofunc_one,hdir,mpname,nbin,ml,mh,dbin,rpvir,outx,outh,$
                 rmax=rmax,major=major,enfit=enfit,rxle001=rxle001,$
		 mf=mf
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
hs=dblarr(nbin.mev,ntree) & h_ave=dblarr(nbin.mev) & dh=h_ave 

@include/halofunc_ccv.pro

rfunc_zc=dblarr(nbin.r,nzc) & rfunc_zc_ntot=dblarr(nzc)
rfunc_mci=dblarr(nbin.r,nmc) & rfunc_mci_ntot=dblarr(nmc)
rfunc_mcii=rfunc_mci & rfunc_mcii_ntot=rfunc_mci_ntot
rfunc_mcf=rfunc_mci & rfunc_mcf_ntot=rfunc_mci_ntot
nfunc=dblarr(3,nmc-1) & nsv=dblarr(ntree)
pfunc=dblarr(pnbin,nmc-1) & npx=dblarr(pnbin,nmc-1)

rpvir=r_vir(mphalo,0.) 
if !key.hostz then !m.chost=chalo(mphalo,0.) else !m.chost=8.5
!m.rs_host=rpvir/!m.chost
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  halon=read_halon(hdir,mpname,ns,major=major,rmax=rmax,$
                   rpvir=rpvir,mf=mf,/alive,/alltrees)
  ;mfunc is lower if the subhalos outside rpvir are ignored.
;=========begin mass function===============================
  msub=halon.tra.m & msubi=halon.msubi
  if !key.bs then begin
    msub=msub*(1+!m.irs*omegab/omega0)
    msubi=msubi*(1+!m.irs*omegab/omega0)
  endif
      
  mvuns=alog10(msubi/mphalo) & mvununs=alog10(msubi/halon.mhosti)
  mvevs=alog10(msub/mphalo)  & kk=0LL
  for itree=0,ntree-1 do begin
    mvev=mvevs[kk:kk+ns[itree]-1] 
    hs[*,itree]=histo(mvev,ml.mev,mh.mev,nbin.mev,locations=lgxev)
    kk=kk+ns[itree]
  endfor
  for ibin=0,nbin.mev-1 do begin
    h_ave[ibin]=mean(hs[ibin,*])
    aa=moment(hs[ibin,*],mdev=dev1,sdev=dev2)
    dh[ibin]=dev2
  endfor
;      h=histo(mvevs,ml.mev,mh.mev,nbin.mev,locations=lgxev)
;      h_ave=h/ntree & dh=sqrt(h_ave/ntree)
  xev=10^lgxev
  mfev=h_ave/dbin.mev/alog(10)  ;dN(<m)/dln(m/M)
  mfev_err=dh/dbin.mev/alog(10) ;/h_ave/alog(10.)      
;========end mass function=============== ============================
;========begin distribution of running time============================
;      tfunc=ran_time(ml.t,mh.t,nbin.t,ld,mlr,halon,x_t)
       tfunc=dblarr(nbin.t) & xt=fltarr(nbin.t)
;========end distribution of running time===============================
  lgrv=alog10(halon.tra.r/rpvir)
;=========begin radial distribution====================================
  for zc=0,nzc-1 do begin
    if zc eq 0 then sn=where(halon.zi le zcv[zc],count) else $
      sn=where((halon.zi le zcv[zc])and(halon.zi ge zcv[zc+1]),count)
    if count ne 0 then begin
      rv=lgrv[sn]
      h=histo(rv,ml.r,mh.r,nbin.r,locations=lgxr,/cumulative)
      rfunc_zc[*,zc]=h/h[nbin.r-1]
      rfunc_zc_ntot[zc]=h[nbin.r-1]
    endif
  endfor
  for mc=0,nmc-1 do begin
    for ii=0,2 do begin 
      case ii of 
        0:mvcount=mvuns
	1:mvcount=mvununs
	2:mvcount=mvevs
	else:message,'out of range'
      endcase
      if mc le 1 then  sn=where(mvcount ge mcv[mc],count) else $
        sn=where((mvcount ge mcv[mc])and(mvcount le mcv[mc+1]),count)
      if count ne 0 then begin
        rv=lgrv[sn]
        h=histo(rv,ml.r,mh.r,nbin.r,locations=lgxr,/cumulative)
        case ii of
          0:begin
	    rfunc_mci[*,mc]=h/h[nbin.r-1]
	    rfunc_mci_ntot[mc]=h[nbin.r-1]
	  end
	  1:begin
	    rfunc_mcii[*,mc]=h/h[nbin.r-1]
	    rfunc_mcii_ntot[mc]=h[nbin.r-1]
 	  end
	  2:begin
	    rfunc_mcf[*,mc]=h/h[nbin.r-1]
	    rfunc_mcf_ntot[mc]=h[nbin.r-1]
	  end 
	  else:message,'out of range'
        endcase 
      endif
    endfor
  endfor
  xr=10^lgxr
;=========end radial distribution=================================
;========begin differential radial distribution====================
  h=histo(lgrv,ml.r,mh.r,nbin.r)
  drfunc=h/ntree
;========end differential radial distribution=======================
;=========begin raidal distribution of subhalo mass===========      
  h=sub_profile(lgrv,halon.tra.m,ml.rm,mh.rm,nbin.rm,dbin.rm,$
                mphalo,rpvir,!m.chost,locations=lgxrm,/nfw,/rlg10)
  rmfunc=h/ntree & xrm=10^lgxrm
;========end raidal distribution of subhalo msss====================
;=========begin distribution of circularity=========================
  h=histo(halon.tra.es,ml.e,mh.e,nbin.e,locations=xe)
  efunc=h/(total(h)*dbin.e)
;=========end distribution of circularity=============================
;===begin distribution of angular momentum evolution===========
  jf=halon.tra.l/halon.li
  h=histo(jf,ml.j,mh.j,nbin.j,locations=xj)
  jfunc=h/total(h)
;===end distribution of angular momentum evolution================ 
;=====begin distribution of orbital evolution=================
  rvirz=r_vir(halon.mhosti,halon.zi)
  h=histo(halon.tra.r/rvirz,ml.rf,mh.rf,nbin.rf,locations=xrf)
  rffunc=h/total(h)
;=====end distribution of orbital evolution=================  
;=====begin factorial moment of halo occupation distribution============
  for mc=0,nmc-2 do begin
    kk=0LL
    for itree=0,ntree-1 do begin
      mvev=mvevs[kk:kk+ns[itree]-1] 
      sn=where(mvev ge mcv[mc],count)
      nsv[itree]=count
      kk=kk+ns[itree]
    endfor
    nfunc[0,mc]=mean(nsv)
    nfunc[1,mc]=sqrt(mean(nsv*(nsv-1)))/nfunc[0,mc]       
    nfunc[2,mc]=(mean(nsv*(nsv-1)*(nsv-2)))^(1./3.)/nfunc[0,mc]
    h=histo(nsv,min(nsv),max(nsv),pnbin,locations=px)
    pfunc[*,mc]=h & npx[*,mc]=px
  endfor
;=====end factorial moment of halo occupation distribution===============
;=====begin halo occupation distribution============
;  for mc=3,4 do begin
;    kk=0LL
;    mu=nfunc[0,mc] & print,mu
;    for itree=0,ntree-1 do begin
;      mvev=mvevs[kk:kk+ns[itree]-1] 
;      sn=where(mvev ge mcv[mc],count)
;      npx[itree,mc]=count
;      nfac=factorial(count)
;      pfunc[itree,mc,0]=exp(-mu)*mu^count/nfac
      ;p_b=mu/sigma2[mc] & r_b=mu^2/(sigma2[mc]-mu)
      ;print,mu,nfac,sigma2[mc],p_b,r_b
      ;pfunc[itree,mc,1]=factorial(count+r_b-1)*p_b^r_b*(1-p_b)^count/factorial(r_b-1)/nfac
;      kk=kk+ns[itree]
;    endfor
;  endfor  
;=====end  halo occupation distribution===============

outh={ev:mfev,err:mfev_err,zc:rfunc_zc,mci:rfunc_mci,mcii:rfunc_mcii,$
      mcf:rfunc_mcf,zc_ntot:rfunc_zc_ntot,mci_ntot:rfunc_mci_ntot,$
      mcii_ntot:rfunc_mcii_ntot,mcf_ntot:rfunc_mcf_ntot,$
      t:tfunc,rm:rmfunc,e:efunc,j:jfunc,rf:rffunc,dr:drfunc,$
      nfunc:nfunc,pfunc:pfunc,npx:npx}
outx={mev:xev,t:xt,r:xr,rm:xrm,e:xe,j:xj,rf:xrf,np:npx}


end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;==================================================
;----semi-analytical model halofunc
function halofunc_sam,nbin,ml,mh,dbin,$
         mp,xh,mpname,$
         rmax=rmax,major=major,enfit=enfit,rxle001=rxle001
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt

@include/halofunc_construct.pro

if n_elements(mp) eq 1 then mpv=[0,0] else mpv=mp

for mpi=mpv[0],mpv[1] do begin
  treedir=!wdir.mp[mpi] & mpname=strmid(treedir,9,15) 
  parameters,treedir
  print,mpname,mphalo,mres
  if n_elements(mp) eq 1 then mf=0 else mf=1e-7*mphalo
;    ml.mun=alog10(mres/mphalo)+0.1
  mfun[*,mpi]=mfunc_tree(treedir,ml.mun,mh.mun,dbin.mun,nbin.mun,xun,major=major)
  for bs=0,n_elements(!wdir.bs)-1 do begin
    !key.bs=fix(strmid(!wdir.bs[bs],2,1)) & print,'bs=',bs
    !m.irs=float(strmid(!wdir.bs[bs],5,3))
    for ld=0,n_elements(!wdir.ld)-1 do begin        ;lnlda
      for mlr=0,n_elements(!wdir.mlr)-1 do begin       ;mlr
      	;print,bs,ld,mlr
        hdir='halofunc/'+!wdir.fit[enfit]+!wdir.d+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
        halo=file_search(hdir+'halonet.'+mpname+!wdir.major[major],count=count)
        if count eq 0 then begin
      	 ; print,hdir+' no file!'
          continue
        endif
        halofunc_one,hdir,mpname,nbin,ml,mh,dbin,rpvir,outx,outh,$
	             rmax=rmax,major=major,enfit=enfit,rxle001=rxle001,$
		     mf=mf
        mfev[*,ld,mlr,bs,mpi]=outh.ev  ;dN(<m)/dln(m/M)
        mfev_err[*,ld,mlr,bs,mpi]=outh.err
        tfunc[*,ld,mlr,bs,mpi]=outh.t
      	for zc=0,nzc-1 do rfunc_zc[*,ld,mlr,bs,zc,mpi]=outh.zc[*,zc]
        for mc=0,nmc-1 do begin
          rfunc_mci[*,ld,mlr,bs,mc,mpi]=outh.mci[*,mc]
          rfunc_mcii[*,ld,mlr,bs,mc,mpi]=outh.mcii[*,mc]
      	  rfunc_mcf[*,ld,mlr,bs,mc,mpi]=outh.mcf[*,mc]
        endfor
        rfunc_zc_ntot[ld,mlr,bs,*,mpi]=outh.zc_ntot
        rfunc_mci_ntot[ld,mlr,bs,*,mpi]=outh.mci_ntot  
        rfunc_mcii_ntot[ld,mlr,bs,*,mpi]=outh.mcii_ntot  
      	rfunc_mcf_ntot[ld,mlr,bs,*,mpi]=outh.mcf_ntot  
        rmfunc[*,ld,mlr,bs,mpi]=outh.rm
        efunc[*,ld,mlr,bs,mpi]=outh.e
        jfunc[*,ld,mlr,bs,mpi]=outh.j 
      	rffunc[*,ld,mlr,bs,mpi]=outh.rf 
	drfunc[*,ld,mlr,bs,mpi]=outh.dr  
	nfunc[*,*,ld,mlr,bs,mpi]=outh.nfunc
	pfunc[*,*,ld,mlr,bs,mpi]=outh.pfunc
	npx[*,*,ld,mlr,bs,mpi]=outh.npx
      endfor
    endfor
  endfor
  halon=read_halon('halofunc/evolving/bs0rs1.00_ld1Mtmt0.0_1.0mlr/',$
                   mpname,ns,major=major,/alltrees)
  mvuns=alog10(halon.msubi/mphalo)
;=====begin initial radial distribution==============
  rvirz=r_vir(halon.mhosti,halon.zi)
  lgrvi=alog10(rvirz/rpvir)
  for mc=0,nmc-1 do begin
    if mc le 1 then  sn=where(mvuns ge mcv[mc],count) else $
      sn=where((mvuns ge mcv[mc])and(mvuns le mcv[mc+1]),count)
    if count ne 0 then begin
      rv=lgrvi[sn]
      h=histo(rv,ml.ri,mh.ri,nbin.ri,locations=lgxri,/cumulative)
      rfunci[*,mc,mpi]=h/h[nbin.ri-1]
      rfunci_ntot[mc,mpi]=h[nbin.ri-1]
    endif
  endfor
  xri=10^lgxri
;======end initial radial distribution==============
;=====begin halo occupation distribution============
  nsv=dblarr(ntree)
  for mc=0,nmc-2 do begin
    kk=0LL
    for itree=0,ntree-1 do begin
      mvun=mvuns[kk:kk+ns[itree]-1] 
      sn=where(mvun ge mcv[mc],count)
      nsv[itree]=count
      kk=kk+ns[itree]
    endfor
    nfunci[0,mc,mpi]=mean(nsv)
    nfunci[1,mc,mpi]=sqrt(mean(nsv*(nsv-1)))/nfunci[0,mc,mpi]       
    nfunci[2,mc,mpi]=(mean(nsv*(nsv-1)*(nsv-2)))^(1./3.)/nfunci[0,mc,mpi]
  endfor

;=====end halo occupation distribution===============
  
endfor
;print,nfunci
@include/halofunc_collect.pro
xh={mun:xun,mev:outx.mev,t:outx.t,r:outx.r,ri:xri,rm:outx.rm,$
    e:outx.e,j:outx.j,rf:outx.rf}
return,halofunc
end
;=================================================================
;=================================================================
;=================================================================
pro write_halofunc,mp=mp
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
!except=2
!hprofile=2    ;NFW
;  m_un,  m_ev,tfunc,  rfunc,       rmfunc,      efunc
@include/halofunc_bin.pro
enfit=0 & !key.hostz=1-enfit & rmax=5. & rxle001=0 


;bsv=[0,7] & ldv=[0,7] & mlrv=[0,3]
for major=1,1 do begin
  halofunc=halofunc_sam(nbin,ml,mh,dbin,mp,$
                        xh,mpname,$
                        rmax=rmax,major=major,enfit=enfit,$
       	                rxle001=rxle001)

  if n_elements(mp) eq 1 then begin
    filename='halofunc_sam_'+mpname+!wdir.major[major]
    if mp eq 0  then begin
      halofunc0=halofunc_sim(nbin,ml,mh,dbin,xh)
    	;help,halofunc0.m.un,halofunc0.m.ev,/struc
      openw,lun,dir0+'halofunc_sim_'+mpname,/get_lun
        writeu,lun,halofunc0
      free_lun,lun
    endif
  endif else filename='halofunc_sam_mpv'+!wdir.major[major]

  openw,lun,dir0+filename,/get_lun
    writeu,lun,ml,mh,nbin,dbin
    writeu,lun,xh
    writeu,lun,halofunc
  free_lun,lun

endfor
end
;===================================================================
;===================================================================
pro read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,$
                  mp=mp,major=major
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
@include/halofunc_bin.pro

if n_elements(mp) eq 1 then begin
  mpname=strmid(!wdir.mp[mp],9,15)
  filename='halofunc_sam_'+mpname+!wdir.major[major]
endif else filename='halofunc_sam_mpv'+!wdir.major[major]

openr,lun,dir0+filename,/get_lun
  readu,lun,ml,mh,nbin,dbin
  @include/halofunc_construct.pro
  xh={mun:fltarr(nbin.mun),mev:fltarr(nbin.mev),t:fltarr(nbin.t),$
      r:fltarr(nbin.r),   ri:fltarr(nbin.ri), rm:fltarr(nbin.rm),$
      e:fltarr(nbin.e),   j: fltarr(nbin.j),  rf:fltarr(nbin.rf) }
  @include/halofunc_collect.pro
  readu,lun,xh
  readu,lun,halofunc
  if eof(lun) then print,'end of '+filename
free_lun,lun
;=======================================
if arg_present(halofunc0) then begin
  openr,lun,dir0+'halofunc_sim_1.77e12_1.00e08',/get_lun
  mfun={giocoli:dblarr(nbin.mun)}
  yev=dblarr(nbin.mev)
  y5=dblarr(8)
  mfev={diemand:yev,gao:yev,giocoli:dblarr(nbin.mev,5),ludlow:y5,springel:yev,angulo:yev}
  mfunc={un:mfun,ev:mfev}

  mcv=[0.,1e-6,1e-5,1e-4,1e-3,1] & nmc=n_elements(mcv)-1
  y1=dblarr(nbin.r,nmc) & yr=dblarr(nbin.r)
  rfunc={diemand:y1,gao:yr,einasto200:yr,einasto50:yr,$
       madau:yr,DM:yr,MW:yr}

  yrm=dblarr(nbin.rm)
  rmfunc={diemand:yrm,springel:yrm}
;=======all halofunc of simulation is here==========
  halofunc0={m:mfunc,r:rfunc,rm:rmfunc}
;  help,halofunc0.m.un,halofunc0.m.ev,/struc
  readu,lun,halofunc0
  if eof(lun) then print,'end of halofunc_sim_1.77e12_1.00e08'
  free_lun,lun
endif
end

;=====running time in fraction of life time of surviving subhalo=======
;function ran_time,ml,mh,nbin,ld,mlr,halon,t_xh ;,rmax=rmax,major=major
;common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
;trun=tGy(0.)-tGy(halon.zi)
;tradir='tradata/bs0rs_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
;tsam=tmerge_sam(tradir,!mrv,1e12,!epsonv,!yitav)
;tlife=tfit(!mrv,tsam,halon.msubi/halon.mhosti,halon.es,halon.yita)
;tf=trun/tlife
;h=histo(tf,ml,mh,nbin,locations=t_xh)
;t_h=h/total(h)
;return,t_h
;end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;============================================================
;=================================================================
;function halofunc_sam_mpv,hdir,nbin,ml,mh,dbin,mpv,xh,$
;         rmax=rmax,major=major,enfit=enfit,rxle001=rxle001
;common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
;@include/halofunc_construct_mpv.pro
;for i=mpv[0],mpv[1] do begin
;  treedir=!wdir.mp[i] & mpname=strmid(treedir,9,15) & print,mpname
;  parameters,treedir
;  ml.mun=alog10(mres/mphalo)+0.1
;  mfun[*,i]=mfunc_tree(treedir,ml.mun,mh.mun,dbin.mun,nbin.mun,xun,major=major)
; 
;  halofunci,hdir,mpname,nbin,ml,mh,dbin,zcv,mcv,rpvir,outx,outh,$
;	    rmax=rmax,major=major,enfit=enfit,rxle001=rxle001
;  mfev[*,i]=outh.ev  ;dN(<m)/dln(m/M)
;  mfev_err[*,i]=outh.err
;  tfunc[*,i]=outh.t
;  for zc=0,nzc-1 do rfunc_zc[*,i,zc]=outh.zc[*,zc]
;  for mc=0,nmc-1 do begin
;    rfunc_mci[*,i,mc]=outh.mci[*,mc]
;    rfunc_mcii[*,i,mc]=outh.mcii[*,mc]
;    rfunc_mcf[*,i,mc]=outh.mcf[*,mc]
;  endfor
;  rfunc_zc_ntot[i,*]=outh.zc_ntot
;  rfunc_mci_ntot[i,*]=outh.mci_ntot  
;  rfunc_mcii_ntot[i,*]=outh.mcii_ntot  
;  rfunc_mcf_ntot[i,*]=outh.mcf_ntot  
;  rmfunc[*,i]=outh.rm
;  efunc[*,i]=outh.e
;  jfunc[*,i]=outh.j
;  rffunc[*,i]=outh.rf     ;

;  halon=read_halon(hdir,mpname,ns,major=major,/alltrees)
;  rvirz=r_vir(halon.mhosti,halon.zi)
;  lgrvi=alog10(rvirz/rpvir)
;  h=histo(lgrvi,ml.ri,mh.ri,nbin.ri,locations=lgxri,/cumulative)
;  rfunci[*,i]=h/h[nbin.ri-1]
;  rfunci_ntot[i]=h[nbin.ri-1]
;  xri=10^lgxri
;endfor

;@include/halofunc_collect.pro
;xh={mun:xun,mev:outx.mev,t:outx.t,r:outx.r,ri:xri,rm:outx.rm,$
;    e:outx.e,j:outx.j,rf:outx.rf}
;return,halofunc
;end
;
