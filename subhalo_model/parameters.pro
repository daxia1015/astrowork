pro parameters,treedir
common cosmology,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
common tree_parameters,nlev,mphalo,mres,ntree,zmax ;,zhalt,ihalt
common astronomy,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common unit_ex,kpcGyr2kms,kms2kpcGyr
;----read parameters from tree data.
openr,lun,treedir+'parameters',/get_lun
readf,lun,nlev,format='(i5)'
readf,lun,mphalo,format='(e12.6)'
readf,lun,mres,format='(e12.6)'
readf,lun,ntree,format='(i5)'
readf,lun,omega0,format='(f6.3)'
readf,lun,lambda0,format='(f6.3)'
readf,lun,h0,format='(f6.3)'
readf,lun,omegab,format='(f6.3)'
readf,lun,nspec,format='(f6.3)'
readf,lun,sigma8,format='(f6.3)'
readf,lun,ahalo,format='(f6.3)'
readf,lun,zmax,format='(f6.3)'
;readf,lun,zhalt,format='(f6.3)'
;readf,lun,ihalt,format='(i5)'
if(eof(lun))then print,'The tree parameters have been read out!!!'
free_lun,lun
;help,nlev,mphalo,mres,ntree,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo,zmax,zhalt,ihalt

;----the astronomical constant
ro_c0=2.77671e2*h0^2           ;cosmic critic density, M_{sun}kpc^{-3}
dltc0=200.
pc=3.08567802e16              ;1pc=3.08567802e16 m
staryr=3.1558149984e7         ;1 star yr=3.1558149984e7 s
h0_inv=9.78/h0                ;H0^{-1}=9.78/h0  Gyr
msun=1.989e30                 ;msun=1.989e30 kg
gaussc=4.4986582e-6           ;Gaussian constant, GM_sun=1.32712497e20m^3 s^{-2}=4.4986582e-6kpc^3 Gyr^{-2}
rsun=6.9600e8                 ;rsun=6.9600e8 m
lsun=3.827e26                 ;lsun=3.827e26 W, bolometric luminosity
;help,ro_c,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun

;----units exchange
kpcGyr2kms=0.97777532         ;1kpc/Gyr=0.97777532km/s
kms2kpcGyr=1.022729843        ;1km/s=1.022729843kpc/Gyr
;help,kpcGyr_kms,kms_kpcGyr
end
