pro test
;print,SCOPE_LEVEL(),'test'
mi=1e11 & mx=[1e-3,0.01,0.05,0.1,0.3,0.5,0.8,1] & c=8.5 & z=0.
n=n_elements(mx)
rs=r_s(c,mi,z) & !m.rs_sub=rs
!m.ros_sub=ros_nfw(z,c)
r=findgen(100)*c*!m.rs_sub/100+0.1
device,filename='heating/m_r_th.ps',/color
plot,r,findgen(100)*0.01+0.01,/ylog,/nodata
for i=0,n-1 do begin
  !c_th=c_heating(mx[i],!m.rs_sub)
  m_r1=mhalo_r(r,!m.rs_sub,c,mhalo,th=1)
  oplot,r,m_r1/mi
  rt=!c_th[0]
  ft=!c_th[1]
  ros=!m.ros_sub
  
  y1=atan((-1./3.*rt+2./3.*r)/rt*3^(1./2.))
  y2=alog(rt^2-rt*r+r^2)
  y3=-alog(rt^2-rt*r+r^2)+2*alog(r+rt)
  m=1./18.*((-6*rt^4*(y2-alog(rt)+alog(r+rt)-alog(rt^2)+3*alog(rs)-3*alog(rs+r))+2*rt^3*rs*3^(1./2.)*!pi+rs^4*3^(1./2.)*!pi+6*rs^4*3^(1./2)*y1-6*rt^3*rs*y3+3*rs^4*(y2-2*alog(r+rt)-alog(rt^2)+2*alog(rt))-9*rt^2*rs^2*y3-12*rt*rs^3*(y2-alog(rt)+alog(r+rt)-alog(rt^2)+3*alog(rs)-3*alog(rs+r))-18*rt^2*rs^2*3^(1./2)*y1-3*rt^2*rs^2*3^(1./2)*!pi+12*rt^3*rs*3^(1./2)*y1-18*rt^4+18*rs^3*rt)*r+rs^5*3^(1./2)*!pi-3*rs^5*y3+12*rs^4*rt*(alog(rt)+alog(rt^2)-3*alog(rs)-y2-alog(r+rt)+3*alog(rs+r))-9*rt^2*rs^3*y3-6*rt^3*rs^2*y3+6*rs*rt^4*(alog(rt)+alog(rt^2)-3*alog(rs)-y2-alog(r+rt)+3*alog(rs+r))-3*rt^2*rs^3*3^(1./2)*!pi-18*rt^2*rs^3*3^(1./2)*y1+12*rt^3*rs^2*3^(1./2)*y1+2*rt^3*rs^2*3^(1./2)*!pi+6*rs^5*3^(1./2)*y1)*rs^2*rt^2/(rt^4+2*rt^3*rs+3*rs^2*rt^2+2*rs^3*rt+rs^4)/(rs+r)/(rt-rs)^2
  m_r2=4*!pi*ft*ros*!m.rs_sub*m
  oplot,r,m_r2/mi,color=!myct.red,linestyle=2
  
 endfor
;common haloset

;for i=2,3 do begin
;  for j=2,3 do begin
;    test1,i,j
;    print,i,j
;    mset=*ptr
;    print,*ptr .msub
;  endfor
;endfor
;print,mset.msub
;n=10
;z=findgen(n)*0.1+2
;msub=(n-findgen(n))*1e9
;csub=chalo(msub,z)
;!m.ros_sub=ros_nfw(z,csub)
;!m.rs_sub=r_s(csub,msub,z)
;!c_th=[20.,0.8]
;r=fltarr(n)+10
;m_r=mhalo_r(r,!m.rs_sub,csub,msub,th=1)

;print,r,m_r

end
