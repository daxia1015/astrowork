pro custom_sysv
defsysv,'!m',exists=en
if en then return
;----define a set of rewritable systme variable
defsysv,'!m',{msub:0.,mx:1.,mhost:0.,rs_sub:0.,rs_host:0.,csub:0.,chost:0.,$
              msubi:0.,mhosti:0.,irs:0.,rf:0.,lf:0.,ros_sub:0.,z:0.,$
	      ga:0.,c_th:[0.,0.]}
defsysv,'!vcmax',{sub:0.,host:0.}
defsysv,'!key',{df:1,td:1,th:1,bs:0,hostz:0,nex:0,At:0}
;defsysv,'!rt',{c:0.,r:0.,Rbnd:500.,irs:0.} ;internal rigid structure

ldform=['2.5','5.0','2.5+ln[m(0)/m(t)]','ln[M(t)/m(t)]','ZB03']
defsysv,'!ld',{i:10,ld:10,form:ldform,x:0.,c:fltarr(4)}
defsysv,'!mlr',1.
defsysv,'!itcount',0L   
defsysv,'!bsi',0
defsysv,'!enprint',0
defsysv,'!mrv',[1e-5,5e-5,1e-4,5e-4,1e-3,5e-3,0.01,0.025,0.05,0.075,0.1,$
                0.13,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5],1 
defsysv,'!epsonv',[0.1,0.3,0.5,0.7,0.9,1],1
defsysv,'!yitav',[0.8,0.9,1],1
defsysv,'!hprofile',0  ;0:point object, 1:isothermal, 2:NFW, 3:Hernquist


tras={t:0.,m:0.,l:0.,vr:0.,r:0.,theta:0.,ID:1.}
tral={t:0.,m:0.,l:0.,energy:0.,vr:0.,vth:0.,v:0.,r:0.,theta:0.,k:0.,es:0.,apo:0.,ID:0.}
outk={msubi:0.,mhosti:0.,mhost:0.,zi:0.,z:0.,yita:0.,es:0.,li:0.,$
      rf:0.,tra:tral,nstep:0L}
ink= {msubi:0.,mhosti:0.,csub:0.,rs_sub:0.,mf:0.,mhost:0.,zi:0.,$
      z:0.,tf:0.,yita:0.,es:0.,tra:tras,ros_sub:0.,li:0.,lf:0.,$
      dt:0.,ld:0.,mbaryon:0.,ilev:0,iex:0,nstep:0L}
;help,tras,tral,outk,ink
defsysv,'!halo',{tras:tras,tral:tral,out:outk,in:ink},1

delimeter='/'
tree_major=['','_major']
dirv=file_search('treedata/1.*',count=ndir)
mpdir=shift(dirv,1)+delimeter ;& print,mpdir,format='(a30)'
fitdir=['evolving','']
bsdir='bs'+['0rs1.00','0rs0.50','0rs0.00','1gs0.30_x4','1gs0.05_x4',$
            '0x40.00','1gs0.30','1gs0.05','0rs0.50_c2','0rs0.50_c5',$
	    '0rs0.00_iso']
;bsdir=[bsdir,bsdir+'_th0'] ;,'0gs0.3']
lddir=['ld03.0','ld04.0','ld05.0','ld1Mtmt0.0',$
       'ld3zentner','ld0fit','ld1Mtmtfit',$
       'ld5bt','ld02.5','ld42.5t',$
       'ld61M0m0'] ;,'ld71Mtmt']
mlrdir=['0.1','1.0','3.5','10.',$
        '0.0','2.0','3.0']+'mlr'
;mlrdir=['1.1','1.4','1.8','2.2']+'mlr'
defsysv,'!wdir',{d:delimeter,major:tree_major,mp:mpdir,fit:fitdir,$
                 bs:bsdir,ld:lddir,mlr:mlrdir},1

;print,!motion,!key,!rt,!A
end
