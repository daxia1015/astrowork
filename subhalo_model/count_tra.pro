pro count_tra
mhost=1.e12 & !key.hostz=0
mh_name=num2str(mhost,format='(e8.2)')
rpvir=r_vir(mhost,!m.z) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
mr=!mrv ;[where((!mrv ge 0.05)and(!mrv le 0.3))]
msub=mr*mhost & csub=8.5 & chost=8.5
rpvir=r_vir(mhost,0.) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
rs_host=rpvir/chost   ;initial r_s of subhalo and host halo
nm=n_elements(mr) & nu=n_elements(!epsonv) & ny=n_elements(!yitav)
Mname=num2str(mr,format='(f7.5)')
rv=0. & vv=0.

;!p.multi=[0,2,2,0,0]
for bs=0,4 do begin
  !key.bs=fix(strmid(!wdir.bs[bs],2,1))  & print,bs
  for ld=0,2 do begin 
    for mlr=0,3 do begin
      !mlr=float(strmid(!wdir.mlr[mlr],0,3)) 
      if strpos(!wdir.bs[bs],'bs00') ne -1 then mf=!mlr*1e4
      tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      for k=0,nm-1 do begin
        if strpos(!wdir.bs[bs],'rs') gt 0 then begin
          if strpos(!wdir.bs[bs],'rsh') gt 0 then irs=0.5 else irs=1. 
          mf=msub[k]*gx(irs)/gx(csub)
        endif
        info=read_tra_info(tradir,mr[k],mhost,mrk)
       	for j=0,ny-1 do begin
          for i=0,nu-1 do begin
            ;print,bs,ld,mlr,mr[k],!epsonv[i],!yitav[j]
	    tra=read_tra(tradir,mrk,info,!epsonv[i],!yitav[j])
	    n=n_elements(tra)
            sn=where(tra.energy gt 0,count)
	    if count ne 0 then begin
	      print,bs,ld,mlr,mr[k],!yitav[j],!epsonv[i],count,tra[n-1].t
	      rv=[rv,tra[n-1].r] & vv=[vv,tra[n-1].v]
	      tmax=max(tra.t)<40
	      ;plot,tra.t,tra.energy,xrange=[0,tmax]
	     ; plot,tra.t,tra.r,xrange=[0,tmax]
	      ;plot,tra.t,tra.v,xrange=[0,tmax]
	      ;plot,tra.t,tra.l,xrange=[0,tmax]
	      ;wait,1
	    endif  
	  endfor
	endfor
        ;openw,lun0,tradir+'tra_'+mh_name+'_'+Mname[k]+'_info',/get_lun
        ;  printf,lun0,mrk,format='(f9.6,2x,e12.6,2f6.2,2i4)'
        ;  printf,lun0,info,format='(2f5.1,i10,3e15.6e3,f5.1)'
	;free_lun,lun0
       ; openw,lun1,dir+'tra_'+mh_name+'_'+Mname[k],/get_lun
	;writeu,lun1,tra
	;free_lun,lun1
      endfor
    endfor
  endfor  
endfor
;plot,rv,vv,psym=1

end

;rf=rf/rpvir & help,rf
;sn=where(rf ne 0)
;rf1=alog10(rf[sn]) & help,rf1
;print,min(rf1),max(rf1)
;nbin=10
;yh=histo(rf1,-2.9,-0.28,nbin,locations=xh)
;yh=yh/total(yh)

;device,/encapsul,/color,filename='orbits/bs0_rf.ps',xsize=15,ysize=12
;multiplot,[1,1]
;plot,10^xh,yh,/xlog,xtitle='r/R!dvir!n'
;multiplot,/default
;print,xh,yh
;end
