pro test_es
es=findgen(101)*0.01
f=fepson(es,/jiang)
device,filename='orbits/es_jiang.ps',/encapsul,/color,xsize=10,ysize=8
plot,es,f,xtitle='!me!x',ytitle='p(!me!x)',yrange=[0,1.6],$
position=[0.21,0.2,0.95,0.97]


end
