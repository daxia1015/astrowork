pro curveld,xyz,c,f,pder,cknown=cknown
mr=xyz[*,0] & es=xyz[*,1] & yita=xyz[*,2]
mr=mr>0.05
if keyword_set(cknown) then begin
  cv=fltarr(5,2)
  openr,lun,'bk08fit/ld_c_nex4_'+strmid(strtrim(string(!mlr),2),0,3)+'.dat',/get_lun
  readf,lun,cv,format='(5f6.2)'
  free_lun,lun
  c=cv[*,!ld.i]
endif
if !ld.i eq 0 then temp=alog(1+1/mr)*mr^c[1]*exp(c[2]*es)*yita^c[3]
if !ld.i eq 1 then temp=mr^c[1]*exp(c[2]*es)*yita^c[3]
f=c[0]*temp+c[4]
pder=[[temp],[c[0]*temp*alog(mr)],[c[0]*temp*es],[c[0]*temp*alog(yita)],[mr/mr]]
end
;=====================================
;====================================================
pro fitting_ld
tlist=tlist_bk(np) ;& help,tlist,/struc
sn=where(tlist.treal lt 14,np) & tlist=tlist[sn]
xyz=[[tlist.mr],[tlist.es],[tlist.yita]]
mlr='3.5'
sam=replicate({t:0.,t0:0.,ld:0.},np)
openw,lun0,'bk08fit/ld_c_nex4_'+mlr+'.dat',/get_lun
for nex=4,4 do begin
  for ld=0,1 do begin
    !ld.i=ld
    str='nex'+string(nex,format='(i1)')+'_lnlda_'+ $
    string(ld,format='(i1)')+'_'+mlr+'.dat'
    print,str
    openr,lun,'bk08fit/'+str,/get_lun
    readf,lun,sam,format='(3f11.6)'
    free_lun,lun
    ldv=sam[sn].ld 
    
    c=[1.,-0.1,-1.,1.,0.]
    ldfit=curvefit(xyz,ldv,weights,c,sigma,function_name='curveld',$
                   itmax=1000,tol=1e-6,chisq=chisq,iter=iter,$
	           fita=[1.,-0.1,-1.,1.,ld],status=status,yerror=yerror)
    printf,lun0,c,format='(5f6.2)'
    print,iter,c
    print,yerror
    if status ne 0 then message,'curvefit failed!'
  endfor
endfor
free_lun,lun0;for ip=0,np-1 do print,ip+2,(ldfit[ip]-ldv[ip])/ldv[ip]
end
;========================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro ldfortex
tlist=tlist_bk(np) & mlr='3.5';& help,tlist,/struc
out=replicate({ip:0,mr:0.,es:0.,yita:0.,treal:0.,c1:0.,c2:0.},np)
out1=replicate({mr:0.,es:0.,yita:0.,treal:0.,tsam:0.,c2:0.},np)
struct_assign,tlist,out,/nozero
struct_assign,tlist,out1,/nozero
out.ip=indgen(np)+1
sam=replicate({t:0.,t0:0.,ld:0.},np)
for ld=0,1 do begin
  str='nex4_lnlda_'+string(ld,format='(i1)')+'_'+mlr+'.dat'
  print,str
  openr,lun,'bk08fit/'+str,/get_lun
  readf,lun,sam,format='(3f11.6)'
  free_lun,lun
  out.(ld+n_tags(out)-2)=sam.ld
  if ld eq 1 then begin
    out1.tsam=sam.t
    out1.c2=sam.ld
  endif
endfor
form='(i4,"  &",f8.4,3("  &",f6.2),2("  &",f5.1),"  \\")'
openw,lun,'bk08fit/ldconstantfortex.dat',/get_lun
for ip=0,np-1 do begin
  print,out[ip],format=form
  printf,lun,out[ip],format=form
endfor
free_lun,lun
form='(f8.4,4("  &",f6.2),"  &",f5.1,"  \\")'
openw,lun,'bk08fit/ldconstantfortex1.dat',/get_lun
for ip=0,np-1 do begin
  print,out1[ip],format=form
  printf,lun,out1[ip],format=form
endfor
free_lun,lun
end

;device,filename='bk08fit/fitted_ld_'+mlr+'.ps',$
;/color,/encapsul,xsize=18,ysize=12
;index=findgen(np)+1
;sn=sort(ldv)
;plot,index,ldv(sn),psym=1,xrange=[0,np+1],yrange=[min(ldv)*0.5,max(ldv)*1.1]
;oplot,index,ldfit(sn),psym=5

