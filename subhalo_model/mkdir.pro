function tradir
dirv=''
for bs=2,2 do begin
  for ld=10,10 do begin
    for mlr=4,6 do begin
      dir=!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]
      dirv=[dirv,dir]
	;tradir0=!wdir.bs[bs]+!wdir.th[th]+!wdir.d+!wdir.ld[ld]+!wdir.d+!wdir.mlr[mlr]+!wdir.d
	;file_move,tradir0+'tra*',tradir+!wdir.d
    endfor
  endfor
endfor
n=n_elements(dirv)
dirv=dirv[1:n-1]
return,dirv
end


pro mkdir,dir0,tra=tra,tree=tree
custom_sysv
cd,dir0,current=dd
dirv=tradir()
file_mkdir,dirv
;file_delete,dirv
cd,dd
end
