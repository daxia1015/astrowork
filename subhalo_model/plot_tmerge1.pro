pro plot_tmerge1
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=[0.3,0.5,0.7] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
;print,tfit.taffoni

nmlr=10 & nld=10 & nbs=10
tsam=fltarr(nm,nu,ny,nld,nmlr,nbs) & a=fltarr(nu,ny)
a={mr:a,t:a} & ID_point=replicate(a,nld,nmlr,nbs)
rmax=1. & lf=0.01
rpvir=r_vir(mhost,0.)
rs_host=rpvir/chost
for bs=0,4 do begin 
;  !m.irs=float(strmid(!wdir.bs[bs],5,strlen(!wdir.bs[bs])-5))
  get_ID=0; not(fix(strmid(!wdir.bs[bs],2,1)))
  for ld=7,7 do begin
    for mlr=1,3 do begin
      ;print,bs,ld,mlr
      tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      tsam[*,*,*,ld,mlr,bs]=tmerge_sam(tradir,mratio,mhost,epson,yita,IDp,$
                                       get_ID=get_ID,rmax=rmax,lf=lf,$
				       extrapolate=!m.irs eq 0)
      if get_ID then ID_point[ld,mlr,bs]=IDp
    endfor
  endfor
endfor
help,tsam
;===merger time dependence on mass ratio
;==============================================================
k=ny-1 & ld=7 & char=1.1

device,file='tmerge/t_ld5bt.ps',/encapsul,/color,xsize=18,ysize=27
  multiplot,[3,6],mxtitle='m(0)/M(0)',mxtitsize=1.2,mxtitoffset=0.3,$
  mytitle='T!ddf!n /Gyr',mytitsize=1.2,mytitoffset=-2.5,$
  xgap=0.025,/doyaxis,myposition=[0.05,0.22,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for bs=0,5 do begin
  for j=0,nu-1 do begin
    t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
    plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
    xtickname=[' ','0.1',' ','0.2',' ','0.3']
    oplot,mratio,tfit.jiang[*,j,k],linestyle=1
    oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
    for mlr=1,3 do begin
      ;if mlr eq 1 then print,tsam[*,j,k,ld,mlr,bs]
      oplot,mratio,tsam[*,j,k,ld,mlr,bs],color=!myct.c3[mlr-1]
    
    endfor
    multiplot,/doyaxis
  endfor
endfor
multiplot,/default

;=====================================================
;tfit from BK08, J08, T03
device,filename='twxjz/tfit.eps',$
/encapsul,/color,xsize=18,ysize=6 ;,xoffset=1.5,yoffset=10
multiplot,[3,1],mxtitle='m(0)/M(0)',mxtitsize=1.2,mxtitoffset=0.3,$
mytitle='T!ddf!n /Gyr',mytitsize=1.2,mytitoffset=-2.5,$
xgap=0.025,/doyaxis,myposition=[0.05,0.22,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for j=0,nu-1 do begin  ;epson
  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
  xtickname=[' ','0.1',' ','0.2',' ','0.3']
  oplot,mratio,tfit.jiang[*,j,k],linestyle=1
  oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.2
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.85,0.88,0.05,0.12,['J08','BK08','T03'],$
/lineation,linestyle=[1,2,3],charsize=char
 
end
