pro remaining_mass,char=char,ply=ply,len=len,c0=c0,dc=dc
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
treedir='treedata/'+!wdir.mp+!wdir.d
mp=0 & major=1 & mlr=1 & nz=dblarr(nlev,ntree)
tree_series,treedir[mp],z_lev,n_levs,nhalos,major=0
device,/color,filename='halofunc/remaining_mass.ps',$
xsize=18,ysize=28,xoffset=1,yoffset=1;,/landscape
!p.multi=[0,1,3,0,0]

for bs=0,3 do begin
  hdir='halofunc/evolving/'+!wdir.bs[bs]+'_'+!wdir.ld[2]+'_'+!wdir.mlr[mlr]+!wdir.d
  halon=read_halon(hdir,!wdir.mp[mp],ns,major=major)
  ntot=total(ns)

  for j=0,2 do begin
    plot,z_lev,findgen(6)-6,/nodata,/xlog,$
    xrange=[0.9,10],yrange=[-1,0.02],xstyle=1,ystyle=1,$
    xtitle='accretion time: 1+z',ytitle='lg[m(z=0)/m(accreted)]'
  
    k=0L & zv=-1. & mfv=1.
    for i=0,ntree-1 do begin
      tree=readtree(treedir[mp],i,n_levs,nhalos,nplev,major=0)
      halo=halon[k:k+ns[i]-1]
      k=k+ns[i]
      tree_live=tree[halo.sub-1]
      z=z_lev[tree_live.ilev-1-1]
      macc=tree_live.mhalo/mphalo
      if j eq 0 then sn=where(macc ge 0.01,count)
      if j eq 1 then sn=where((macc lt 0.01)and(macc gt 0.001),count)
      if j eq 2 then sn=where(macc le 0.001,count)
      if count ne 0 then begin
        zv=[zv,z[sn]]
        mfv=[mfv,halo[sn].tra.m/tree_live[sn].mhalo]
      endif
    endfor
    oplot,1+zv,alog10(mfv),psym=1;,color=c0+dc*mlr
    xyouts,7,-0.1,'A='+strmid(!wdir.mlr[mlr],0,3)+'!c'+!wdir.bs[bs]
  endfor
  
endfor
!p.multi=0
end



pro final_mass,major=major,char=char,ply=ply,len=len,c0=c0,dc=dc
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
treedir='treedata/'+!wdir.mp+!wdir.d
mp=0
tree_series,treedir[mp],z_lev,n_levs,nhalos,major=major
ntot=total(nhalos) & print,ntot
tree=readtree(treedir[mp],i,n_levs,nhalos,nplev,major=major,/all)
msub=total(tree.msub)
print,msub/mphalo/ntree

nbs=4 & nld=3 & nmlr=3
n_alive=fltarr(nbs,nld,nmlr) & m_alive=n_alive
for bs=0,3 do begin
  for ld=0,2 do begin 
    for mlr=0,2 do begin
      hdir='halofunc/evolving/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      halon=read_halon(hdir,!wdir.mp[mp],ns,major=major)
      mv=alog10(halon.tra.m/mphalo)
      sn=where((mv ge -3.8)and(mv le -1),count)
      nalive=count/ntot & malive=total(halon[sn].tra.m)/count/1e9
      n_alive[bs,ld,mlr]=nalive & m_alive[bs,ld,mlr]=malive
    endfor
  endfor  
endfor
print,m_alive;*n_alive

xsize=17 & ysize=14 & syms=[1,2,4,5,6,7]
device,/color,filename='halofunc/alive.ps',$
/encapsul,xsize=xsize,ysize=ysize ;,xoffset=1,yoffset=1;,/landscape
!p.multi=[0,2,2,0,0]
regions=multi_regions(xsize,ysize)
for bs=0,3 do begin
  n=n_alive[bs,*,*] & m=m_alive[bs,*,*]
  !p.region=regions[*,bs]
  plot,findgen(10)*0.1+0.1,findgen(10)+0.01,/nodata,$
  xrange=[min(n)*0.97,max(n)*1.02],yrange=[min(m)*0.98,max(m)*1.03],$
  xmargin=[3,2],ymargin=[2,1],xcharsize=1,ycharsize=1,charsize=1
  for mlr=0,2 do oplot,n[0,*,mlr],m[0,*,mlr],thick=1+mlr*3
  for ld=0,2 do oplot,n[0,ld,*],m[0,ld,*],color=c0+dc*ld,psym=-6
  xyouts,max(n),min(m),!wdir.bs[bs],alignment=1
endfor
xyouts,0.28,0.02,'number fraction of subhalos alive',/normal
xyouts,0.04,0.16,'average mass of subhalos alive(1e9M!dsun!n)',orientation=90,/normal
!p.multi=0

end
