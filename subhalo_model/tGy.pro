function tGy,z
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
zp1=1.+z
if(lambda0 ne 0.)then $
tGy=h0_inv*2*alog((sqrt(lambda0/zp1^3)+sqrt(lambda0/zp1^3+omega0))/omega0^0.5)/(3*lambda0^0.5) $
else message,'not suitable, cannot get tGy, lambda=0'
return,tGy
end
