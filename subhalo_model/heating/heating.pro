;pro kazantzidis,r,b,ro,pder
;rb=!m.rs_sub*b
;rvir=r_vir(!m.msubi)
;ch=!m.msubi/(4*!pi*rb*(rb-(rb+rvir)*exp(-rvir/rb)))
;ro=ch/r/exp(r/rb)
;if n_params() ge 4 then pder=1./r/exp(r/!m.rs_sub)
;end



pro testrt,n,k,mx
c=[5e-5,1e-5,5e-6,1e-6,5e-7,1e-7,5e-8,1e-8,5e-9,1e-9,5e-10,1e-10]; &  print,'c=',c
nc=n_elements(c)
for ic=0,nc-1 do begin
  !rt.c=c[ic] & print,'c=',!rt.c
  rx=findgen(n+1)*(30.-ic*2.)/n
  ;a=fltarr(n+1)
  ;for i=0,n do a[i]=rt_eq(rx[i])
  oplot,rx,rt_eq(rx),color=k*10+10
  k++

  ;if(finite(rt) eq 0)or(rt le 0.)or(imaginary(rt) ne 0.)then 
  rt=!rt.c^0.5*1e3*!c_th[0];& print,'r',r
  print,rt
  fp=rt_eq_p(rt)
  while fp le 0. do begin
    rt=rt*(2.+alog10(1-fp))
    fp=rt_eq_p(rt)
;    print,rt
  endwhile
  f=rt_eq(rt)
  dr=abs(f)/fp
  while dr gt 0.01 do begin
    if f lt 0. then rt=rt+dr/2 else rt=rt-dr
    fp=rt_eq_p(rt) & f=rt_eq(rt)
    dr=abs(f)/fp
  endwhile
  print,rt,dr,dr/rt,f
endfor  
end


pro heating,char=char,ply=ply,len=len
mi=1e9 & !m.msubi=mi
c=8.5 & !m.csub=c
rs=r_s(c,mi) & !m.rs_sub=rs
;rvir=r_vir(mi)
;!c_th[1]=1.3 & !c_th[2]=1./4
;rb=rs*!c_th[1]
;!c_th[0]=mi/(4*!pi*rb*(rb-(rb+rvir)*exp(-rvir/rb)))

x=10^(findgen(51)*alog10(c/0.1)/50-1)


mv=[0.5,0.3,0.1,0.05,0.01] & c0=25 & dc=50
nm=n_elements(mv)
ro=ro_r(x*rs,rs,c,mi)
device,/color,filename='heating/ro.ps'
plot,x,ro,/xlog,/ylog,charsize=char,thick=ply,$
xstyle=1,ystyle=1,title='density profile after tidal heating',$
xrange=[0.09,9],xtitle='r/r!ds!n',ytitle='!4q!x(r)'
for i=0,nm-1 do begin
  !c_th=c_heating(mv[i],!m.rs_sub)
  for th=1,1 do begin
    ;rst=rs/mv[i]^(2/3.3)
    ;rst=rs*mv[i]^(1/3.)
    roh=ro_r(x*rs,rs,c,mi,mx=mv[i],th=th)
    oplot,x,roh,linestyle=2*th,thick=ply,color=c0+i*dc
  endfor
endfor

m_r=mhalo_r(x*rs,rs,c,mi) ;& print,m_r[50]
device,/color,filename='heating/m_r.ps'
plot,x,m_r,/xlog,/ylog,charsize=char,thick=ply,$
yrange=[1e-3,1]*mi,xrange=[0.09,9],xstyle=1,ystyle=1,$
xtitle='r/r!ds!n',ytitle='m(<r)',$
title='mass profile after tidal heating'
;device,/color,filename='heating/testrt.ps'
;plot,findgen(30)*0.5,(findgen(30)-20)*0.5,/nodata,$
;xrange=[-0.5,13],yrange=[-10,5],xstyle=1,ystyle=1
;oplot,findgen(30)-5,fltarr(30),linestyle=2
k=0L
for i=0,nm-1 do begin
  !c_th=c_heating(mv[i],!m.rs_sub) & print,mv[i]
  for th=1,1 do begin
    ;rst=rs/mv[i]^(2/3.3)
    ;rst=rs*mv[i]^(1/3.)
    m_rh=mhalo_r(x*rs,rs,c,mi,mx=mv[i],th=th) ;& print,m_rh[50]
;    m_r0=mhalo_r(0.,rs,mi,mx=mv[i],th=th)
;    print,m_r0
    oplot,x,m_rh,linestyle=2*th,thick=ply,color=c0+i*dc
     
    ;testrt,500,k,mv[i]
  endfor
endfor
;xyouts,0.1,0.6,'dashed: with heating',charsize=char,charthick=ply

end
