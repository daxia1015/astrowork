;==============================================================
pro plot_bk_j
tlist=tlist_bk(np)
@bk08fit/BK08j.dat
lns=['ln!mL!x=','ln!mL!x=-ln!mm!x+']
ipv=[16,15,14,13,8,7] ;& gav=[0.1,0.5,1.]
xtit='t / Gyr' & ytit='j(t)/j(t!dacc!n)' & trange=[0,9.99] & jrange=[1e-3,1.05]
;===================================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
i=4 & ip=ipv[i] & char=1.1
;plot badj for mlrv =============================
name=['ld1_0.00_1.0mlr_8','ld1_0.00_3.5mlr_8','ld1_0.00_10.mlr_8']
device,filename='bk08fit/badj_mlrv.ps',/encapsul,/color,xsize=10,ysize=9
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange,$
xtitle=xtit,ytitle=ytit,position=[0.18,0.16,0.98,0.97]
for j=0,2 do begin
  info=read_tra_any('bk08fit/tradata/badj/'+name[j],tra=tra)
  oplot,tra.t,tra.l/tra[0].l,thick=3+j*4
;  print,info.ld
endfor
labeling,0.02,0.3,0.1,0.08,['1','3.5','10'],/lineation,$
thickv=[3,7,11]
xyouts,0.05,0.36,'A='
;=============================================================
;plot badj for ldv  ==================================
name=['ld0_3.00_3.5mlr_8','ld0_4.00_3.5mlr_8','ld0_5.00_3.5mlr_8',$
      'ld1_0.00_3.5mlr_8','ld1_1.00_3.5mlr_8','ld1_2.00_3.5mlr_8']
charv=[['3','4','5'],['0','1','2']]
device,filename='bk08fit/badj_ldv.ps',/encapsul,$
/color,xsize=10,ysize=5.8
multiplot,[2,1],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.8,$
myposition=[0.17,0.25,0.98,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for ldi=0,1 do begin
  plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
  for j=0,2 do begin
    kname=j+ldi*3
    info=read_tra_any('bk08fit/tradata/badj/'+name[kname],tra=tra)
    oplot,tra.t,tra.l/tra[0].l,thick=3+j*4
;    print,info.ld
  endfor
  labeling,0.7,0.8,0.13,0.1,charv[*,ldi],/lineation,thickv=[3,7,11]
  xyouts,0.88,0.87,lns[ldi],alignment=1,charsize=1
  multiplot
endfor
multiplot,/default
;==================================================================
;==================================================================
device,filename='bk08fit/goodj1.ps',/encapsul,/color,xsize=10,ysize=9.5
multiplot,[2,2],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.8,$
myposition=[0.16,0.15,0.98,0.98]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for i=4,3,-1 do begin
  ip=ipv[i]
  for ldi=0,1 do begin
    plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
    file='bk08fit/tradata/nex4_best_ld'+string(ldi,format='(i1)') $
         +'/3.5mlr_'+strtrim(string(ip),2)
    info=read_tra_any(file,tra=tra) 
    oplot,tra.t,tra.l/tra[0].l
;    print,info.ld
    xyouts,9.6-(1-ldi)*0.3,0.9,lns[ldi]+string(info.ld,format='(f3.1)'),$
    charsize=1,align=1
    multiplot
  endfor
endfor
multiplot,/default
;===============================================================
;===============================================================
mrv=['0.025','0.05','!mm!x!di!n=0.1'] & esv=['0.65','0.46']
device,filename='bk08fit/goodj2.ps',/encapsul,/color,xsize=10,ysize=13.5
multiplot,[2,3],mxtitle=xtit,mxtitsize=char,mxtitoffset=0.5,$
mytitle=ytit,mytitsize=char,mytitoffset=-0.5,$
myposition=[0.17,0.12,0.9,0.9]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for i=0,5 do begin
  plot,tseries,jv[*,i],linestyle=2,xrange=trange,yrange=jrange
  ip=ipv[i] 
  file='bk08fit/tradata/nex4_fit_ld1/3.5mlr_'+strtrim(string(ip),2)
  info=read_tra_any(file,tra=tra)
  oplot,tra.t,tra.l/tra[0].l
;  print,info.ld
  if i mod 2 eq 1 then xyouts,0.98,(!p.position[1]+!p.position[3])/2,$
    mrv[i/2],align=0.5,orient=90,/normal
  if i le 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,$
    '!me!x='+esv[i],align=0.5,/normal
  xyouts,0.5,0.1,'C='+strtrim(string(info.ld,format='(f5.2)'),2)
  multiplot 
endfor
multiplot,/default

end

;dir=['nex0_best_ld0','nex0_best_ld0_m','nex0_best_ld0_p',$
;     'nex0_best_ld1','nex0_best_ld1_m','nex0_best_ld1_p',$
;     'nex4_best_ld0','nex4_best_ld1',$
;     'nex4_fit_ld0','nex4_fit_ld1']+'/'
;dir0=[0,3,6,8] & dir1=[2,5,7,9]
;if 1 then begin
;figname=['nex0ld0','nex0ld1','nex4','ok']
;for fig=0,3 do begin
;  case (dir1[fig]-dir0[fig]+1) of
;    1:begin
;      ct=!myct.blue
;      charv=''
;    end
;    2:begin
;      ct=!myct.c2
;      charv=strarr(2)
;    end
;    3:begin
;      ct=!myct.c3
;      charv=strarr(3)
;    end
;  else: message,'error!!!'
;  endcase
;  device,filename='bk08fit/'+figname[fig]+'_j_'+mlr+'.ps',$
;  /encapsul,/color,xsize=10,ysize=13
;  multiplot,[2,3],mxtitle=xtit,mytitle=ytit
;  for i=0,5 do begin
;    plot,tseries,jv[*,i],linestyle=5,xrange=trange,yrange=[1e-3,1.1]
;    ip=ipv[i] & j=0
;    for kdir=dir0[fig],dir1[fig] do begin
;      name='bk08fit/tradata/'+dir[kdir]+mlr+'mlr_'+strtrim(string(ip),2)
;      info=read_tra_any(name,tra=tra)
;      oplot,tra.t,tra.l/tra[0].l,linestyle=j,color=ct[j]
;      charv[j]=string(info.ld,format='(f4.1)')
;      j++
;    endfor
;    if i mod 2 eq 1 then xyouts,0.98,(!p.position[1]+!p.position[3])/2,$
;    mrv[i/2],align=0.5,orient=90,/normal
;    labeling,0.4-(i lt 4)*0.42,0.88-(i lt 4)*0.6,0.1,0.1,'C='+charv,ct=ct,$
;    linestyle=indgen(n_elements(charv));,/lineation    
;;    xyouts,0.48-(i lt 4)*0.42,0.88-(i lt 4)*0.6,'C='+charv,color=ct
;    multiplot ;,/doxaxis,/doyaxis
;  endfor
;  multiplot,/default
;;  xyouts,0.05,0.96,'A='+mlr,/normal
;  xyouts,0.25,0.96,'!me!x=0.65',/normal
;  xyouts,0.65,0.96,'!me!x=0.46',/normal
;endfor
;endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;====================================================================
;if 0 then begin
;device,filename='bk08fit/bad_j_'+mlr+'.ps',/encapsul,/color,xsize=11,ysize=6
;multiplot,[2,1],mxtitle='t/Gyr',mytitle='j(t)/j(0)',$
;mytitoffset=-1,myposition=[0.15,0.2,0.98,0.95]
;i=4 & ip=ipv[i] & charv=strarr(3)
;for fig=0,1 do begin
;  plot,tseries,jv[*,i],linestyle=5,xrange=[0,9.99],yrange=[1e-3,1.01]
;  j=0
;  for kdir=dir0[fig],dir1[fig] do begin
;    name='bk08fit/tradata/'+dir[kdir]+mlr+'mlr_'+strtrim(string(ip),2)
;    info=read_tra_any(name,tra=tra)
;    oplot,tra.t,tra.l/tra[0].l,linestyle=j,color=!myct.c3[j]
;    charv[j]=string(info.ld,format='(f5.2)')
;    j++
;  endfor
;  labeling,0.5,0.8,0.18,0.08,charv,ct=!myct.c3,/lineation,linestyle=[0,1,2]
;  xyouts,0.9,0.87,lns[fig],align=1
;  multiplot
;endfor
;multiplot,/default
;endif

;=============================================================
;=============================================================
;pro plot_bk_t
;tlist=tlist_bk(np) & mlr=3.5
;tt2=fltarr(np)
;sam0=replicate({t:0.,t0:0.,ld:0.},np)
;str='lnlda_0_'+strmid(strtrim(string(mlr),2),0,3)+'.dat'
;openr,lun,'bk08fit/'+str,/get_lun
;readf,lun,sam0,format='(3f11.6)'
;free_lun,lun;

;for ip=0,np-1 do begin
;  info=read_tra_bk('bk08fit/tradata/fit_ld/',mlr,ip)
;  tt2[ip]=info.t
;endfor

;device,filename='bk08fit/tfit.ps',/color,/encapsul,xsize=18,ysize=20
;sn=sort(tlist.treal) & index=findgen(np)+1
;plot,index,tlist[sn].treal,xrange=[0,np+1],yrange=[2,40],psym=1,$
;xtitle='index',ytitle='T!ddf!n /Gyr',/ylog
;oplot,index,sam0[sn].t,color=!myct.red
;oplot,index,tt2[sn],color=!myct.blue
;end
;==============================================================

;;;;;;;;;;;
;;;;;;;;;;
;pro plot_bk_ld
;tlist=tlist_bk(np) ;& help,tlist,/struc
;mlrv=[1.,2.5,3.5,6.,10.] & nmlr=n_elements(mlrv)
;sam0=replicate({t:0.,t0:0.,ld:0.},np)
;sam=replicate({t:0.,t0:0.,ld:0.},np,3,nmlr)
;for k=0,2,2 do begin
;  for mlr=0,nmlr-1 do begin
;    !mlr=mlrv[mlr]
;    str='nex0_lnlda_'+string(k,format='(i1)')+'_'+strmid(strtrim(string(!mlr),2),0,3)+'.dat'
;    openr,lun,'bk08fit/'+str,/get_lun
;    readf,lun,sam0,format='(3f11.6)'
;    sam[*,k,mlr]=sam0
;    free_lun,lun
;  endfor
;endfor
;==================================================================
;================================================================
;x=[0.025,0.05,0.0707,0.1,0.2,0.3] & y=x & nd=2 & cvv=fltarr(3,10)
;es=[0.33,0.46,0.65,0.78,1.] & pss=[1,4,5,6,7]
;xv=findgen(101)*0.003+0.01
;range=[[0.9,5.9],[0,0],[-1,2]]
;openw,lun,'bk08fit/yeqa0xa1plusa2.dat',/get_lun
;for k=0,2,2 do begin
;  device,filename='bk08fit/lnlda'+string(k,format='(i1)')+'.ps',$
;  /color,/encapsul,xsize=16,ysize=20
;  multiplot,[2,3],mxtitle='m(0)/M(0)',mytitle='c',xgap=0.03,ygap=0.03,$
;  /doxaxis,/doyaxis
;  for mlr=0,nmlr-1 do begin
;    ldv=sam[*,k,mlr].ld ;& print,ldv
; ;    cv=poly_fit(x,y,nd,yband=yband,yerror=yerr,yfit=yfit)
;    cv=comfit(tlist.mr,ldv,[4.,-0.2,-4.],/geometric,itmax=100,iter=iter)
;    plot,xv,cv[0]*xv^cv[1]+cv[2],xrange=[0.01,0.32],$
;    yrange=[min(ldv)-0.5,max(ldv)+0.5],$
;    xtickname=[' ','0.1',' ','0.2',' ','0.3']
;;    oplot,xv,cv[0]+cv[1]*xv+cv[2]*xv^2,color=!myct.blue
;    printf,lun,cv,format='(3f11.3)'
;    for ip=0,np-1 do begin
;      oplot,[tlist[ip].mr,tlist[ip].mr],[ldv[ip],ldv[ip]],$
;      psym=pss[where(es eq tlist[ip].es)]
;    endfor
;    xyouts,0.3,max(ldv)*0.9,'A='+strmid(strtrim(string(mlrv[mlr]),2),0,3),align=1
;    multiplot,/doxaxis,/doyaxis
;  endfor
;  multiplot,/default
;endfor
;free_lun,lun
;end
;
