pro test_dm
device,filename='halofunc/rfunc_dm.ps',/encapsul,/color,xsize=15,ysize=12 ;,xoffset=1.5,yoffset=10
xr=findgen(100)*0.01+1e-3
plot,xr,xr,title='Dark Matter',$                       ;unevolved, fitted by Giocoli 2007
xrange=[0.05,1.],yrange=[0.007,1.1],/xlog,/ylog,/nodata,$
xtitle='r/R!dvir!n',ytitle='N(<r)/N!dtot!n'
for i=0,6 do begin
  chost=5.+i*2
  y=gx(xr*chost)/gx(chost)
  oplot,xr,y,linestyle=i,color=!myct.c7[i]
endfor
!key.hostz=1
chost=chalo(1.77e12,0.) & print,chost
y=gx(xr*chost)/gx(chost)
oplot,xr,y,thick=1
end
