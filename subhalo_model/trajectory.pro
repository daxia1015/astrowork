;--this function describe dy/dt of differential equations of motion.
;it return the value of dy/dt_(i) when given y[i),t(i)
function motion,t,y
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common haloset;,mset,neq,npar
yp=fltarr(neq,npar)
en=(total(finite(y),1) eq neq)and(y[0,*] ge mset.lf/2) $
    and(y[2,*] ge mset.rf)and(y[4,*] gt 10)
sn=where(en,n0)
sn1=where(en and (mset.td eq 1),n1)
mset.msub=y[4,*]
if n0 ne 0 then begin 
    imset=mset[0,sn] ;& imset=reform(temporary(imset),1,n0)
    lp=fltarr(1,n0) & mp=lp  
    mhost_r=mhalo_r(y[2,sn],imset.rs_host,imset.chost,imset.mhost,hprofile=!hprofile)
    ro_h_r=ro_r(y[2,sn],imset.rs_host,imset.chost,imset.ros_host,hprofile=!hprofile)
    fg=gaussc*mhost_r/y[2,sn]^2
    pp=y[0,sn]^2/y[2,sn]^3-fg
    rp=y[1,sn]
    thetap=y[0,sn]/y[2,sn]^2
;    help,mhost_r,ro_h_r,fg,pp,rp,thetap
    mx=y[4,sn]/imset.msubi < 1.
;    help,y[4,sn],mx
    Rbndx=Rbounding(mx,imset.csub,th=!key.th)
;    help,Rbndx
    sn11=where(imset.td eq 1,n11)
    if (n1 ne 0)and(!mlr ne 0) then begin
      iimset=mset[0,sn1] ;& iimset=reform(temporary(iimset),1,n1)
      if !key.th then begin
        c_th=c_heating(mx[0,sn11]) 
	rmax=iimset.csub*iimset.rs_sub*(1+mx[0,sn11])
;	help,iimset,rmax
      endif else begin
	c_th=fltarr(2,n1)
	rmax=Rbndx[0,sn11]
;	help,rmax
      endelse
      rt=r_tidal(mhost_r[sn11],ro_h_r[sn11],thetap[sn11],y[2,sn1],$
                 c_th,rmax,iimset)
;      help,rt	 
;      m_rt=y[4,sn1] 
;      sn3=where(root ne 3,n3)
;      if n3 ne 0 then m_rt[sn3]=mhalo_r(rt[sn3],iimset[sn3].rs_sub,c,m,$
;                                        th=!key.th,c_th=c_th[*,sn3],$
;					ros_sub=iimset[sn3].ros_sub,hprofile=!hprofile)
      m_rt=mhalo_r(rt,iimset.rs_sub,iimset.csub,iimset.msub,$
                   th=!key.th,c_th=c_th,ros_sub=iimset.ros_sub,$
		   hprofile=!hprofile)
      m_rt=temporary(m_rt) < y[4,sn1]
;      if !key.At then At=!mlr*(mx[sn11])^!m.ga else At=!mlr
      mp[sn11]=-!mlr*(y[4,sn1]-m_rt)*thetap[0,sn11]/(2*!pi)
    ;help,rt,m_rt,mp
    endif
    if !key.df then begin
      v=sqrt(y[1,sn]^2+(y[0,sn]/y[2,sn])^2)
      if !key.bs then ms=y[4,sn]+imset.mbaryon else ms=y[4,sn]
      fdf=f_df(ms,mhost_r,ro_h_r,v,y[2,sn],imset,Rbndx,hprofile=!hprofile)
      ;alpha=atan(y[0,sn]/(y[1,sn]*y[2,sn]))
      ;sn3=where(rp lt 0,n3)
      ;if n3 ne 0 then alpha[sn3]=alpha[sn3]+!pi
      alpha=atan(abs(y[0,sn]/(y[1,sn]*y[2,sn])))
      lp=-y[2,sn]*fdf*sin(alpha)   ;it should be sin(!pi-alpha)
    endif
    sn3=where((y[2,sn] lt imset.rf*5)and(y[0,sn] ge imset.lf),n3)
    if n3 ne 0 then begin
      pp[sn3]=0. & rp[sn3]=0.
    endif
    yp[0,sn]=lp & yp[1,sn]=pp & yp[2,sn]=rp & yp[3,sn]=thetap & yp[4,sn]=mp
  ;endif else yp=fltarr(n)
endif 
;print,'y=',y
;print,'yp=',yp
;if !enprint and (t gt 11.9) then print,'yp=',yp
return,yp
end


;--this function generate the trajectory status of next step by given step
function tra_walk,tra_i,t0,dt,dtmin,dtmax,eps ;,mhost ,r_s_sub,r_s_host;,df=en_df,td=en_td
common haloset
y=[tra_i.l,tra_i.vr,tra_i.r,tra_i.theta,tra_i.m] 
;if !enprint then print,y
dydx=call_function('motion',t0,y)
yscal=abs(y)+abs(dt*dydx)+1.e-30
dt=temporary(dt) > dtmin & dt=temporary(dt) < dtmax
;help,y,yscal,dt,t0
;if !enprint then print,min(dt),max(dt) ;tra_i,format='(10e10.2e3,1x,f2.0)'
;y=rk4(y,dydx,tra_i.t,dt,'motion') ;on_error,2
;if !enprint then print,dt
call_procedure,'rkqs',y,dydx,t0,dt,eps,yscal,dtnext,'motion'

;print,y
tra_i.t=t0[0,*]
tra_i.l=y[0,*] > 0.1
;tra_i.energy=y[5]
tra_i.vr=y[1,*]
tra_i.r=y[2,*] > mset.rf*0.9
;tra_i.vth=tra_i.l/tra_i.r
;tra_i.v=sqrt(tra_i.vr^2+tra_i.vth^2)
tra_i.theta=y[3,*]
tra_i.m=y[4,*] > 10.
;fg=f_g(tra_i.r)
;tra_i.k=tra_i.vth*fg/tra_i.v^3  ;& print,'k=',tra_i.k          ;trajectory curvature
dt=dtnext
;if !key.hostz eq 0 then eps=eps*abs(tra_i.vr)/abs(vri)
;dt=dt*abs(tra_i.vr)/abs(vri)                      ;adaptive stepsize adjustment based on the variation of trajectory curvature
;if !enprint and (tra_i.t gt 11.9) then print,y;tra_i,format='(10e10.2e3,1x,f2.0)'
;if !enprint and (tra_i.t gt 11.9) then print,'dt1=',dt
return,tra_i
end
;=====================================================
;=====================================================
function tra_ID,tra_i,rf,lf,mf
en1=(tra_i.r lt rf)or(~finite(tra_i.r))or(~finite(tra_i.m))
en2=(tra_i.l lt lf)or(~finite(tra_i.l))
en3=(!key.bs eq 0)and(tra_i.m lt mf)
ID=0.1*en1+0.2*en2+0.4*en3
sn=where(ID eq 0,n)
if n ne 0 then ID[sn]=1.
;print,en1,en2,en3,ID
return,ID
end
;===============================================================
;===============================================================
function trajectory,msub,mhost,mz,csub,chost,yita,epson,$
                    mf,mbaryon,ti,tf,dti,dtmin,dtmax,eps,$
		    hprofile=hprofile,$
		    halt=halt,df=df,td=td,th=th,bs=bs
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common haloset
!hprofile=hprofile
!key.df=df & mset.td=td & !key.th=th & !key.bs=bs ;& print,!key
mset.mhost=mhost & mset.chost=chost 
mset.csub=csub & mset.msubi=msub & mset.msub=msub
rvir_sub=r_vir(msub,!m.z) 
r_s_sub=r_s(csub,msub,!m.z) & mset.rs_sub=r_s_sub ;& r_s_host=r_s(chost,mhost)  ;initial r_s of subhalo and host halo
rvir=r_vir(mhost,!m.z) & r_s_host=rvir/chost ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
mset.ros_sub=ros_profile(!m.z,csub,msub,rvir_sub,hprofile=hprofile)
mset.ros_host=ros_profile(!m.z,chost,mhost,rvir,hprofile=hprofile)
mset.rs_host=r_s_host
mset.vcmax_host=vcir(2.16,chost,mhost,rvir)
mset.Vvir_host=sqrt(gaussc*mhost/rvir)
mset.ld=!ld.x
if !key.bs eq 1 then mset.mbaryon=mbaryon

tra_i=ini_tra(msub,mhost,chost,yita,epson,!m.z,ti,hprofile=hprofile)
tra=tra_i & dt=dti
li=tra_i.l ;& print,li
x=mhost/tra_i.m  
lf=50.*alog(1+x)/x+10. < 0.01*li & mset.rf=1e-3*mset.rs_host > 0.1
mset.lf=lf 
iex=0 & vr0=tra_i.vr

repeat begin
  if !key.nex ne 0 then begin
    if vr0*tra_i.vr le 0 then iex++
    if iex ge !key.nex then mset.td=0 else mset.td=1
  endif else begin
    if(tra_i.m lt mf)then begin
      if !key.bs then mset.td=0 else begin
        mset.td=td
        out=1
        break
      endelse
    endif else mset.td=td
  endelse
  vr0=tra_i.vr
  out=(~finite(tra_i.m))or(~finite(tra_i.l))or(~finite(tra_i.r))or(tra_i.r le mset.rf)
  if out then break
  tra_i=tra_walk(tra_i,tra_i.t,dt,dtmin,dtmax,eps)     ;get the trajectory status of next step
  tra=[tra,tra_i]
  case halt of
    0: en=0
    1: en=(tra_i.m le mf)
    2: en=(tra_i.t ge tf)
    3: en=(tra_i.l lt lf)
    4: en=(tra_i.m lt mf)or(tra_i.l lt lf)
    5: en=(tra_i.l lt lf)or(tra_i.t ge tf)
    6: en=(tra_i.m lt mf)or(tra_i.l lt lf)or(tra_i.t ge tf)
;    7: en=(!rt.r lt r_s_sub/2)or(tra_i.l lt lf)
  else:message,'halt condition should be supplied'
  endcase
  ;if tra_i.t gt 10.3 then print,tra_i,format='(10e15.6e3,2x,f3.1)'
endrep until(en)
n=n_elements(tra)
tra[n-1].ID=tra_ID(tra_i,mset.rf,lf,mf)
;print,tra_i ;,mset.rf,lf,mf,tra[n-1].ID
;print,'ID=',ID,format='(a3,1x,f3.1)'
sn=where((finite(tra.m) eq 0)or(tra.m lt 0),count)
if count ne 0 then tra[sn].m=mf
sn=where(finite(tra.l) eq 0,count)
if count ne 0 then tra[sn].l=min(tra.l)
sn=where(tra.l lt 0,count)
if count ne 0 then tra[sn].l=0.1
sn=where(finite(tra.r) eq 0,count)
if count ne 0 then tra[sn].r=min(tra.r)
sn=where(tra.r lt 0,count)
if count ne 0 then tra[sn].r=mset.rf	      	    

traa=tra_s2l(tra,mhost,r_s_host,chost,rvir,n,hprofile=hprofile)
;print,!m
return,traa
end

;  catch,error
;  if check_math() ne 0 then begin
;    print,'MATH ERROR!'
;    message,/reset
;    break
;  endif

;  if strpos(!error_state.name,'MATHERROR') ne -1 then begin
;    tra_i.ID=0.4
;    message,/reset
;    break
;  endif
