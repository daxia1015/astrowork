pro image_data;_write
!except=1
!key.hostz=0
dir='tradata/'
lddir=['lnlda2.5/','lnlda2.5t/','M0mt/'] & ldname=['2.5','2.5+ln[m(0)/m(t)]','ln[1+M(0)/m(t)]']         ;ld
mlrdir=['','2.2mlr/'] & mlrname=['1.0','2.2']                             ;mlr
mhost=1.e12
mr=[0.01,0.025,0.05,0.1]
;Mname=strmid(strtrim(string(mr),2),0,5)          ;i
;epsonname=strmid(strtrim(string(!epsonv),2),0,3)  ;j
;yitaname=strmid(strtrim(string(!yitav),2),0,3)    ;k
Mname=string(mr,format='(f5.3)')
epsonname=string(!epsonv,format='(f3.1)')
yitaname=string(!yitav,format='(f3.1)')

xh=520 & yh=500
IDinfo=['MERGED!','DISRUPTED!','MERGED & DISRUPTED!']
IDname=['0.1','0.2','0.3']

for bs=0,0 do begin
  help,bs
  for ld=2,2 do begin
    help,ld
    for mlr=0,0 do begin
      help,mlr
      tradir=dir+'bs'+strtrim(string(bs),2)+'/'+lddir[ld]+mlrdir[mlr]
      for i=0,0 do begin
        tra_info=read_tra_info(tradir,mr[i],1.e12,mrk)
        for j=0,0 do begin
          for k=2,2 do begin
            tra=read_tra(tradir,mrk,tra_info,!epsonv[j],!yitav[k])
            n=n_elements(tra)
	    if tra[n-1].ID eq 1. then continue
	    
	    IDi=fix(10*tra[n-1].ID-1)
	    nn=round(n*0.4)<200 ;& help,nn
            t=findgen(nn)*tra(n-1).t/(nn+1)
            tname=strmid(strtrim(string(t),2),0,5)
            m=interpol(tra.m,tra.t,t)
            r=interpol(tra.r,tra.t,t)
            theta=interpol(tra.theta,tra.t,t)
            x0=r*cos(theta) & y0=r*sin(theta)
            R_sub=r_vir(m); & print,R_sub
            rmax=max(tra.r)

            window,xsize=xh,ysize=yh,/free
            fname='tra_'+strtrim(string(bs),2)+'_'+strtrim(string(ld),2)+'_'+mlrname[mlr]+'_'+Mname[i]+'_'+epsonname[j]+'_'+yitaname[k]+'_'+IDname[IDi]
            openw,lun,'mpeg/image_data/'+fname,/get_lun
	    writeu,lun,xh,yh,nn
            for ii=1,nn-1 do begin
              plot,r[0:ii],theta[0:ii],/polar,/iso,charsize=1.5,thick=2,$
	      xtitle='r/kpc',ytitle='r/kpc',title='subhalo evolution',$
              xrange=[-1,1]*rmax,yrange=[-1,1]*rmax,xstyle=1,ystyle=1
     	      circle,0.,0.,1.,color='ffffff'xl,thick=2
              circle,x0[ii],y0[ii],R_sub[ii],color='0000ff'xl,thick=2
	      xyouts,0.5*rmax,0.88*rmax,'t='+tname[ii]+'Gyr',charsize=1.5
              xyouts,-0.95*rmax,0.88*rmax,'baryon='+strtrim(string(bs),2)+'!cln!4K!x='+ldname[ld]+'!cm(0)/M(0)='+Mname[i]+'!c!4e!x='+epsonname[j]+'!c!4g!x='+yitaname[k]+'!cA='+mlrname[mlr],$
	      charsize=1.5
	      if(ii ge n-20)then xyouts,0.5,0.5,IDinfo[IDi],alignment=0.5,color='ff00ff'xl,charsize=4,/normal

              image=tvrd(true=1)
              writeu,lun,image
            endfor
            free_lun,lun
	    wdelete
          endfor
       endfor
     endfor
   endfor
  endfor
endfor
end



pro animation
!except=1
dir1='mpeg/image_data/'
dir2='mpeg/'
f_format='(5x,a29)'
xh=0 & yh=0 & n=0L
fname=f_format & fi=0

openr,lun0,dir1+'file',/get_lun
  while(eof(lun0) eq 0)do begin
    readf,lun0,fname,format=f_format
    if(fi ge 2)and(fi le 7)then begin
      print,fname,fi
      openr,lun,dir1+fname,/get_lun
        readu,lun,xh,yh,n & print,xh,yh,n
        xinteranimate,20,set=[xh,yh,n-1],/mpeg_open,mpeg_quality=100,$
	mpeg_bitrate=double(64*2.^20),mpeg_filename=dir2+fname+'.mpg',$
        title='subhalo evolution'
        image=bytarr(3,xh,yh)
        images=assoc(lun,image,8) ;& help,images
        for ii=0,n-2 do xinteranimate,frame=ii,image=images[ii]
        xinteranimate,/mpeg_close,/close
      free_lun,lun
    endif
    fi++
  endwhile
free_lun,lun0

end
