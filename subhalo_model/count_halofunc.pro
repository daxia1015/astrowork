pro count_fate,mp,nbs,nld,nmlr,lun0,halon,ns,IDv,bs,ld,mlr,it=it
if it eq 0 then begin
  IDv=dblarr(3,nbs,nld,nmlr)
  openw,lun0,'statistics/count_fate_'+strmid(!wdir.mp[mp],9,15),/get_lun
  printf,lun0,'# bs  ld  mlr: Merged   Disrupted   Survive'
endif
if it then begin
  id=halon.tra.ID
  nn=double(total(ns)) 
  for i=0,2 do begin
    case i of
      0 : sn=where((id ne 0.4)and(id lt 1),count)
      1 : sn=where(id eq 0.4,count)
      2 : sn=where(id eq 1.,count)
    else: message,'out of range'
    endcase
    IDv[i,bs,ld,mlr]=double(count)/nn
  endfor
  ldform=['\lnlac','\lnlact','\lnMtmt']+'{}'
  printf,lun0,!wdir.bs[bs],ldform[ld],strmid(!wdir.mlr[mlr],0,3),IDv[*,bs,ld,mlr],$
  format='(a9," &",a10," &",a4," &",f6.3," &",f6.3," &",f6.3," \\")'
endif
if it eq 2 then begin
  printf,lun0,'bs*, lnlda=ln[M(t)/m(t)], mlr=2.5 :'
  for bs=0,4 do printf,lun0,!wdir.bs[bs],IDv[*,bs,2,2],$
  format='(a9," &",f6.3," &",f6.3," &",f6.3," \\")'
  free_lun,lun0
endif
end

;==================================================================
;=================================================================

pro count_halofunc
common tree_parameters;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
mp=0 & major=1 & mpname=strmid(!wdir.mp[mp],9,15)
!key.hostz=1
rpvir=r_vir(mphalo,0.) & print,'rpvir=',rpvir
!m.chost=chalo(mphalo,0.)
!m.rs_host=rpvir/!m.chost

nbs=5 & nld=4 & nmlr=4
;count_fate,mp,nbs,nld,nmlr,lun0,halon,ns,IDv,bs,ld,mlr,it=0
idv=[findgen(7)*0.1+0.1,1] & nid=dblarr(8)

nnv=fltarr(4,3) & mfv=fltarr(4,3)
for bs=2,5,3 do begin
  !key.bs=fix(strmid(!wdir.bs[bs],2,1)) ;& print,bs
  for ld=3,3 do begin 
    ld=bs+1
    for mlr=2,2 do begin
      
      dir0=!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
      hdir='halofunc/evolving/'+dir0
      ;for i=0,ntree-1 do begin
      halon=read_halon(hdir,mpname,ns,major=major,/alltree)
      nn=double(total(ns,/int)) ;& print,nn
      for j=0,7 do begin
        sn=where(halon.tra.ID eq idv[j],n)
        nid[j]=double(n)/nn
      endfor
      print,bs,ld,mlr,nid,format='(3i3,8f8.4)'
      print,bs,ld,mlr,total(halon.tra.m)/total(halon.msubi)
;      jf=halon.tra.l/halon.li
;      sn=where(jf lt 0.5,count)
;      print,ld,mlr,count
      ;ri=r_vir(halon.mhosti,halon.zi) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
      ;;rfori=halon.tra.r/ri
     ; print,min(rfori),max(rfori)
      ;h=histo(rfori,0.05,1.5,10,locations=xr)
      ;print,h/nn,format='(10f8.3)'
     ; ;nnv[ld,mlr-1]=nn/4e4 & mfv[ld,mlr-1]=total(mfreal)/nn

;      count_fate,mp,nbs,nld,nmlr,lun0,halon,ns,IDv,bs,ld,mlr,it=1
      ;endfor		  
    endfor
  endfor  
endfor
;print,xr,format='(10f8.3)'
;print,nnv,'       ',mfv
;count_fate,mp,nbs,nld,nmlr,lun0,halon,ns,IDv,bs,ld,mlr,it=2
end

;      openu,lun,hdir+'halonet.'+mpname+!wdir.major[major],/get_lun
;      readu,lun,ns
;      nn=total(ns)  & idv0=fltarr(nn);& help,ns,nn
;      halon=replicate(!halo.out,nn)
;      readu,lun,halon
;      lf=0.05*halon.li
;      lf=lf < 100. & lf=lf > 10.
;      csub=chalo(halon.msubi,halon.zi)
;      if strpos(!wdir.bs[bs],'rs') gt 0 then begin
;        if strpos(!wdir.bs[bs],'rsh') gt 0 then irs=0.5 else irs=1. 
;        mf=halon.msubi*gx(irs)/gx(csub)
;      endif
;      if strpos(!wdir.bs[bs],'bs00') ne -1 then mf=fltarr(nn)+1e4
;      k=0L
;      while k lt nn do begin
;        idv0[k]=tra_ID(halon[k].tra,rf,lf[k],mf[k])
;	k++
;      endwhile
;      halon.tra.ID=idv0
;      point_lun,lun,400
;      writeu,lun,halon
;      free_lun,lun
