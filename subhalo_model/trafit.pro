function tfit,mrv,tsam,mr,epson,yita
x=interpol(findgen(n_elements(mrv)),mrv,mr)
y=interpol(findgen(n_elements(!epsonv)),!epsonv,epson)
z=interpol(findgen(n_elements(!yitav)),!yitav,yita)
t=interpolate(tsam,x,y,z)
return,t
end


function tra_final,mdir,trun
nm=n_elements(!mrv) & nu=n_elements(!epsonv) & ny=n_elements(!yitav)
nlev=n_elements(trun)
trai={m:15.6,r:15.6}
tra_f=replicate(trai,nm,nu,ny,nlev)
;tlev=tGy(z_lev)
;trun=tlev[0]-tlev
;tdyn=sqrt(2/dltcz(z))*h0_inv/(ez(z_lev))^0.5
for i=0,nm-1 do begin
  tra_info=read_tra_info(mdir,!mrv[i],1.e12,mrk)
  for j=0,nu-1 do begin
    for k=0,ny-1 do begin
      tra=read_tra(mdir,mrk,tra_info,!epsonv[j],!yitav[k])
      n=n_elements(tra)
      tmerge=tra[n-1].t
      for ilev=0,nlev-1 do begin
        if trun[ilev] gt tmerge then begin
	  mf=tra[n-1].m
	  rf=tra[n-1].r  
	endif else begin
	  mf=interpol(tra.m,tra.t,trun[ilev])
	  rf=interpol(tra.r,tra.t,trun[ilev])
	endelse   
        tra_f[i,j,k,ilev].m=mf/tra[0].m
	tra_f[i,j,k,ilev].r=rf/tra[0].r
      endfor
    endfor
  endfor
endfor
return,tra_f
end


function trafit,tra_i,mr,epson,yita,tra_ilev_f
x=interpol(findgen(n_elements(!mrv)),!mrv,mr)
y=interpol(findgen(n_elements(!epsonv)),!epsonv,epson)
z=interpol(findgen(n_elements(!yitav)),!yitav,yita)
mf=interpolate(tra_ilev_f.m,x,y,z)
rf=interpolate(tra_ilev_f.r,x,y,z)
tra_i.m*=mf
tra_i.r*=rf
return,tra_i
end
