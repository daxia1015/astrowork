function t_taffoni,mr,epson,yita,nm,nu,z,mhost,chost
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
t=fltarr(nm,nu) & trigid=t & tlive=t;& help,yita
csoch=mr^(-0.13)
rpvir=r_vir(mhost,0.) 
gc=sqrt(rpvir^3*mhost/gaussc)/(mhost*mr)
;==merger time of rigid body===========
fc=1.7265+0.0416*chost
lnlda=alog(1.+1/mr)
trigid1=0.6*gc*fc*yita^1.5/lnlda
alpha=0.475*(1-tanh(10.3*mr^0.33-7.5*yita))
for j=0,nu-1 do trigid[*,j]=trigid1*epson[j]^alpha
;==================================================
;==merger time of live satellite===================
by=-0.0504+0.3355*yita+0.3281*yita^2
cy=2.151-14.176*yita+27.383*yita^2
tlive1=gc*(0.25/csoch^6-0.07*csoch+1.123)*(by*mr^0.12+cy*mr^2)
for j=0,nu-1 do begin 
  if epson[j] eq 1 then tlive[*,j]=tlive1 else begin
    xc=0.8
    qy=0.9+1e8*(12.84+3.04*xc-23.4*xc^2)*(mr-0.0077/(1-1.08*xc)-0.0362)^6
    tlive[*,j]=tlive1*(0.4+(epson[j]-0.2)*qy)
  endelse
endfor
;=====================================================
sn1=where(mr le 0.007,n1)
sn2=where((mr gt 0.007)and(mr le 0.08),n2)
sn3=where((mr gt 0.08)and(mr le 0.1),n3)
sn4=where(mr gt 0.1,n4)
if n1 ne 0 then begin
  trigs=2.1*trigid
  t[sn1,*]=trigs[sn1,*] > tlive[sn1,*]
endif
if n2 ne 0 then t[sn2,*]=tlive[sn2,*]
if n3 ne 0 then begin
  fmr=rebin((mr[sn3]-0.08)/0.02,n3,nu)
  t[sn3,*]=trigid[sn3,*]*fmr+tlive[sn3,*]*(1-fmr)
endif
if n4 ne 0 then t[sn4,*]=trigid[sn4,*]
return,t
end
;===========================================================
;===========================================================
function tmerge_pre,mratio,epson,yita,z,mhost,chost
nm=n_elements(mratio)
nu=n_elements(epson)
ny=n_elements(yita)
t0=fltarr(nm,nu,ny)
t={bk:t0,jiang:t0,lc:t0,taffoni:t0,colpi:t0}
if n_elements(z) eq 0 then z=0.
h0_inv=9.78/0.7  ;Gyr
h_inv=h0_inv/(ez(z))^0.5
tdyn=sqrt(2/dltcz(z))*h_inv
ratio=1./mratio      ;ratio=M/m,  mratio=m/M
for k=0,ny-1 do begin
  t.taffoni[*,*,k]=t_taffoni(mratio,epson,yita[k],nm,nu,z,mhost,chost)
  for j=0,nu-1 do begin
    t.bk[*,j,k]=0.216*(ratio^1.3/alog(1+ratio))*exp(1.9*epson[j])*yita[k]*tdyn   ;Gyr
    fes=(0.9*epson[j]^0.47+0.6)/0.855
    t.jiang[*,j,k]=(ratio/alog(1+ratio))*fes*sqrt(yita[k])*tdyn
    fes=epson[j]^0.78/0.855
    t.lc[*,j,k]=(ratio/alog(1+ratio))*fes*(yita[k])^2*tdyn        
  endfor  
endfor
return,t
end
;=====================================================================
;=====================================================================
;=====================================================================
;=====================================================================
function trefine,tra,rmax=rmax,lfc=lfc,extrapolate=extrapolate
t0=max(tra.t)-tra[0].t
lf=tra.l/tra[0].l
en1=keyword_set(rmax) and (min(tra.apo) lt rmax)
en2=keyword_set(lfc) and (min(lf) lt lfc)
en3=keyword_set(extrapolate) 
;print,extrapolate;and (t0 ge tf)
if en1 then t1=truncation(tra.apo,rmax,tra.t) else t1=t0
if en2 then t2=truncation(lf,lfc,tra.t) else t2=t0
if en3 then begin
  lp=-deriv(tra.t,tra.l)
  lpc=0.1*max(lp) < (0.5*lp[0]) 
  sn1=where(lp le lpc,n1)
  if n1 ne 0 then begin
    sn3=where((lp[sn1] gt 0)and(lf[sn1]-min(lf) lt 0.2),n3)
    if n3 ne 0 then begin
      sn=sn1[sn3[0]]
      lfp0=lf[sn]
      if lfp0 gt lfc then t3=tra[sn].t*(1-lfc)/(1-lfp0) else t3=tra[sn].t   
    endif else begin
      sn2=where(lf le lfc,n2)
      if n2 ne 0 then t3=tra[sn2[0]].t else t3=t2    
    endelse
;    print,lfp0,tra[sn].t,t3
  endif else begin
    sn2=where(lf le lfc,n2)
    if n2 ne 0 then t3=tra[sn2[0]].t else t3=t2
  endelse
endif else t3=t0
tt=min([t0,t1,t2,t3])
return,tt
end
;====================================================================
;====================================================================
function tmerge_sam,dir,mratio,mhost,epson,yita,ID_point,$
         get_ID=get_ID,rmax=rmax,lfc=lfc,extrapolate=extrapolate
if ~keyword_set(rmax) then rmax=0.
if ~keyword_set(lf) then lf=0.
nm=n_elements(mratio)
nu=n_elements(epson)
ny=n_elements(yita)
t=fltarr(nm,nu,ny) & ID=t
for i=0,nm-1 do begin
  info=read_tra_info(dir,mratio[i],mhost,mrk)
  for j=0,nu-1 do begin
    for k=0,ny-1 do begin
      sn=where((info.epson eq epson[j])and(info.yita eq yita[k]))
;      en1=keyword_set(rmax) and (info[sn].rapo lt rmax)
;      en2=keyword_set(lf) and (info[sn].lf lt lf)
;      en3=keyword_set(extrapolate) ;and (info[sn].tt ge 5.+5.*where(!epsonv eq epson[j]))
;      t0=info[sn].tt
      tra=read_tra(dir,mrk,info,epson[j],yita[k])
;      if en1 then t1=truncation(tra.apo,rmax,tra.t) else t1=1000.
;      if en2 then t2=truncation(tra.l/tra[0].l,lf,tra.t) else t2=1000.
;      if en3 then t3=tmerge_ex(tra) else t3=1000.
      tt=trefine(tra,rmax=rmax,lfc=lfc,extrapolate=extrapolate)
      t[i,j,k]=tt
            ;if t[i,j,k] lt 0 then print,mratio[i],epson[j],yita[k],t[i,j,k]
;       if info[sn].rapo gt 5 then print,mratio[i],epson[j],info[sn].ID,info[sn].rapo
      ID[i,j,k]=info[sn].ID
    endfor
  endfor
endfor
;===================================================
if keyword_set(get_ID) then begin
  a=fltarr(nu,ny) & ID_point={mr:a,t:a}
  for j=0,nu-1 do begin
    for k=0,ny-1 do begin
      for i=0,nm-1 do begin
        ;print,'  ',j,k,i
	if ID[i,j,k] eq 1. then message,'There are still subhalos alive.',/continue
        if ID[i,j,k] eq 0.4 then pl=i       ;pleft
        if ID[i,j,k] lt 0.4 then begin
          pr=i                              ;pright
      	  break
        endif
      endfor
      if total(ID[*,j,k]) lt nm then begin
        if pr eq 0 then pl=0
        if pl eq nm-1 then pr=nm-1
        ID_point.mr[j,k]=(mratio[pl]+mratio[pr])/2
        ID_point.t[j,k]=(t[pl,j,k]+t[pr,j,k])/2
      endif
    endfor
  endfor
endif
return,t
end
;=================================================================

pro tmergei
bs=0 & ld=2 & mlr=2
mr=0.3 & mhost=1e12 & epson=0.5 & yita=1.
dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
info=read_tra_info(dir,mr,mhost,mrk)
tra=read_tra(dir,mrk,info,epson,yita)
n=n_elements(tra)
device,/color,/encapsul,filename='tmerge/itmerge.ps',xsize=13,ysize=18
multiplot,[1,2]
plot,tra.t,tra.l,thick=4
multiplot

yp=-deriv(tra.t,tra.l) & print,yp[0],max(yp),min(yp),yp[n-1]
plot,tra.t,yp,/ylog
multiplot,/default

print,max(tra.t)-tra[0].t,tmerge_ex(tra)

end
