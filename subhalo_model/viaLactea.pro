pro vltrack_data,name0,var0,name1,var1,name2,var2,name3,var3
dir='halofunc/plotdata/vltrack/'
n=n_params()/2
if n eq 0 then begin
  message,'nothing to be output!',/continue
  return
endif
switch n-1 of
  3:begin
    out=read_ascii(dir+name3+'.txt',count=k,comment_symbol='#')
    var3=out.field01
    help,var3
  end
  2:begin
    out=read_ascii(dir+name2+'.txt',count=k,comment_symbol='#')
    var2=out.field01
    help,var2
  end
  1:begin
    out=read_ascii(dir+name1+'.txt',count=k,comment_symbol='#')
    var1=out.field01
    help,var1
  end
  0:begin
    out=read_ascii(dir+name0+'.txt',count=k,comment_symbol='#')
    var0=out.field01
    help,var0
  end
endswitch

end
;====================================
;=========================================
pro viaLactea
vltrack_data

vltrack_data,'progIDs',progIDs
	     
vltrack_data,'progGCdistance',progGCdistance,'progMtidal',progMtidal,$
             'progRtidal',progRtidal,'progVmax'






end
