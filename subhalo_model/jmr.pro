;================================================================
pro jmr
yita=1. & mhost=1e12 & mr=0.05 & epson=0.5  & bs=2
Mname=num2str(mr,format='(f4.2)')
epsonname=num2str(epson,format='(f3.1)')
snc=[1,7,2] & char=1.1
mytit=['m(t)/m(t!dacc!n)','r(t)/R!dvir!n','j(t)/j(t!dacc!n)']
tmax=12.999
;=========================================================
lddir=['ld1Mtmt0.0','ld0'+['3.0','4.0','5.0']]
charv=[['1','3.5','10'],['3','4','5']]
charv1=['A=','ln!mL!x=']

ldv=[[0,0],[1,3]] & mlrv=[[1,3],[2,2]]
device,/color,/encapsul,filename='orbits/mr.ps',$
xsize=16,ysize=13
multiplot,[2,2],mxtitle='t / Gyr',mxtitsize=char,$
mxtitoffset=0.5,myposition=[0.15,0.15,0.98,0.98] ;,/square
!p.charsize=char & !x.charsize=char & !y.charsize=char
for row=0,1 do begin
  j=snc[row]
  xyouts,0.04,(!p.position[1]+!p.position[3])/2,mytit[row],$
  align=0.5,/normal,orient=90
  for col=0,1 do begin
    plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
    xrange=[0,9.5],yrange=[0.011,1.1+(row eq 1)*0.35],ylog=j eq 1
    kk=0
    for ld=ldv[0,col],ldv[1,col] do begin
      for mlr=mlrv[0,col],mlrv[1,col] do begin
        dir='tradata/'+!wdir.bs[bs]+'_'+lddir[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
        tra_info=read_tra_info(dir,mr,mhost,mrk)
        tra=read_tra(dir,mrk,tra_info,epson,yita)
        t=tra.t & y=tra.(j)/tra[0].(j)
        if j eq 7 then begin
          n=n_elements(tra) & tf=max(tra.t)<tmax
          t=findgen(n*3)*tf/n/3
          y=spline(tra.t,y,t)
        endif
        oplot,t,y,thick=3+kk*4
	kk++
      endfor
    endfor
;    print,charv[*,col]
    if row eq 0 then begin
      labeling,0.1,0.25,0.1,0.08,charv[*,col],/lineation,thickv=[3,7,11]
      xyouts,0.2,0.31,charv1[col],alignment=1
    endif
    multiplot
  endfor
endfor
multiplot,/default
;charv=[['1','3.5','10'],[strmid(lddir[1:3],3,1)]]
;colv=['A=','ln!mL!x=']
;for col=0,1 do begin
;  labeling,0.25+col*0.67,0.95,0.07,0.06,charv[*,col],/lineation,thickv=[3,7,11]
;  xyouts,0.25+col*0.64,0.99,colv[col]
;  labeling,0+col*0.6,0.7,0.07,0.06,charv[*,col],/lineation,thickv=[3,7,11]
;  xyouts,0+col*0.56,0.74,colv[col]
;endfor
;xyouts,0.05,0.98,'M1'
;======================================================================
;======================================================================
;=-========================================================

end

;  device,/color,/encapsul,filename='orbits/mrj_mlr.ps',$
;  xsize=18,ysize=6.8
;  multiplot,[3,1],mxtitle='t / Gyr',mxtitsize=char,xgap=0.028,$
;  mxtitoffset=0.4,/doyaxis,myposition=[0.04,0.2,1,0.85] ;,/square
;  !p.charsize=char
;  for col=0,2 do begin
;    j=snc[col] & dy=(col eq 1)*0.42 & kk=0
;    plot,findgen(11),findgen(11)*0.1+1e-3,/nodata,$
;    xrange=[0,tmax],yrange=[1e-6,1+dy]
;    for mlr=1,3 do begin
;      dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
;      tra_info=read_tra_info(dir,mr,mhost,mrk)
;      tra=read_tra(dir,mrk,tra_info,epson,yita)
;      t=tra.t & y=tra.(j)/tra[0].(j)
;      if col eq 1 then begin
;        n=n_elements(tra) & tf=max(tra.t)<tmax
;        t=findgen(n*3)*tf/n/3
;        y=spline(tra.t,y,t)
;      endif
;      oplot,t,y,thick=3+(mlr-1)*4,color=!myct.blue
;    endfor
;    xyouts,0.26+col*0.29,0.92,mtit[col],charsize=1.1,align=0.5,/normal
;    xyouts,(!p.position[0]+!p.position[2])/2,0.93,mtit[col],charsize=1.1,align=0.5,/normal
;    multiplot,/doyaxis;,/square
;  endfor
;  multiplot,/default
;  labeling,0.07,0.8,0.05,0.11,'A='+['1','3.5','10'],color=!myct.blue,$
;  /lineation,thickv=[3,7,11]
;  xyouts,0.08,0.88,'M1',color=!myct.blue

;pro jmr_es
;bs=0 & !ld.i=2 & !mlr=float(strmid(!wdir.mlr[2],0,3)) & !key.bs=0
;!key.hostz=1
;major=1 & mpname=!wdir.mp[0]
;hdir='halofunc/evolving/bs00_M0mt_2.5mlr/'
;halon=read_halon(hdir,mpname,ns,major=major,/alltrees)
;nscumu=total(ns,/cumulative);

;sn0=where((halon.tra.ID ge 1)and(halon.zi gt 2),n0) & print,n0
;mvi=halon[sn0].msubi/halon[sn0].mhosti
;sn3=sort(mvi)
;snp=[10,n0/10,n0*99/100,n0-2] & nplot=n_elements(snp);

;!p.thick=3
;device,/color,/encapsul,filename='orbits/es.eps',xsize=16,ysize=14
;multiplot,[2,2],xgap=0.05,ygap=0.05,/doxaxis,/doyaxis,$
;mxtitle='t/Gyr',mytitle='!4e!x(t)'
;oi=10
;for i=0,nplot-1 do begin
;  sn1=sn0[sn3[snp[i]]]       
;  sn2=where(nscumu gt sn1)
;  itree=sn2[0]
;  if itree eq 0 then ihalo=sn1 else ihalo=sn1-nscumu[itree-1]
;  tra=read_halon_track(hdir,mpname,nhalos,nstepv,major=major,$
;                          itree=itree,ihalo=ihalo)
;  plot,tra.t,tra.(oi),xrange=round([min(tra.t),max(tra.t)]),$
;  yrange=[min(tra.(oi))*0.95,max(tra.(oi))*1.05]
;  halo=halon[sn1]
;  str=string(halo.msubi/halo.mhosti,format='(f6.4)')
;  xyouts,8,max(tra.(oi))*1.01,'m/M='+str
  ;msub=mhost*halo.msubi/halo.mhosti
;  !m.z=halo.zi
;  csub=chalo(halo.msubi,halo.zi) & chost=chalo(halo.mhosti,halo.zi)
;  ti=tGy(halo.zi) & tf=halo.tra.t 
;  tra=trajectory(halo.msubi,halo.mhosti,mz,csub,chost,halo.yita,halo.es,$
;                 1e4,ti,tf,0,tf-ti,$
;                 halt=6-!key.bs,df=1,td=1,th=1,bs=!key.bs)
;  oplot,tra.t,tra.(oi),linestyle=1,color=!myct.dkblue,thick=5
;  multiplot,/doxaxis,/doyaxis
;endfor 

;multiplot,/default
;end
;================================================================
;================================================================
