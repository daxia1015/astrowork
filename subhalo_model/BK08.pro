function tlist_bk,n
n=26L
tlist={mr:0.,es:0.,yita:0.,treal:0.,tpre:0.}
tlist=replicate(tlist,n)
openr,lun,'bk08fit/BK08t.dat',/get_lun
skip_lun,lun,1,/lines
readf,lun,tlist,format='(f6.4,2f6.2,2f7.2)'
if eof(lun) then print,'end of BK08t.dat'
free_lun,lun
return,tlist
end
;==========================================================
;==========================================================
;===========================================================
;===========================================================
pro bk08fit
!except=1 
!hprofile=2
!key.hostz=0 & !m.z=0
mhost=1.e12 & ti=0. & chost=8.5  & dtmin=0. & dtmax=0.4

mlrv=[1.,2.5,3.5,6.,10.] & nmlr=n_elements(mlrv)
rmax=1. & lfc=0.01 & eps=[1e-3,1e-4,1e-4,1e-4,1e-3]
mf=1e4 & tf=45. & !key.bs=0 & halt=6-!key.bs 
halosetting,1
tlist=tlist_bk(np)

;writting trajectory data with parameters list BK08======
if 1 then begin
sam=replicate({t:0.,t0:0.,ld:0.},np)
dir=['nex4_best_ld0','nex4_best_ld1',$
     'nex4_fit_ld0','nex4_fit_ld1','nex4_ld1']+'/'
ndir=n_elements(dir) & print,ndir
for idir=0,4 do begin
  print,dir[idir]
  !key.At=0 
  !key.nex=fix(strmid(dir[idir],3,1))
  ldpos=strpos(dir[idir],'ld')
  !ld.i=fix(strmid(dir[idir],ldpos+2,1))
  !ld.x=0.
  for mlr=2,2 do begin
    !mlr=mlrv[mlr] & print,'mlr=',!mlr
    str='nex'+string(!key.nex,format='(i1)')+'_lnlda_'+string(!ld.i,format='(i1)') $
        +'_'+strmid(strtrim(string(!mlr),2),0,3)+'.dat'
    openr,lun,'bk08fit/'+str,/get_lun
    readf,lun,sam,format='(3f11.6)'
    free_lun,lun
    curveld,[[tlist.mr],[tlist.es],[tlist.yita]],c,fldx,pder,/cknown
    for i=0,np-1 do begin
      if strpos(dir[idir],'best') gt 0 then !ld.x=sam[i].ld
      if strpos(dir[idir],'fit') gt 0 then !ld.x=fldx[i]
      if strpos(dir[idir],'_m') gt 0 then !ld.x=!ld.x-0.5
      if strpos(dir[idir],'_p') gt 0 then !ld.x=!ld.x+1.
      csub=chalo(tlist[i].mr,chost,/normalize)
      tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
                     tlist[i].yita,tlist[i].es,mf,mbaryon,$
		     ti,tf,0.1,dtmin,dtmax,eps,hprofile=!hprofile,$
		     halt=halt,df=1,td=1,th=1,bs=!key.bs)
      nstep=n_elements(tra)
      tt=trefine(tra,rmax=rmax,lfc=lfc,extrapolate=((!key.nex eq 0)or(tlist[i].treal gt 14)))
      print,i,(tt-tlist[i].treal)/tlist[i].treal
      name=strmid(strtrim(string(!mlr),2),0,3)+'mlr_'+ $
           strtrim(string(i),2) ;+'_'+string(!m.ga,format='(f3.1)')
      openw,lun,'bk08fit/tradata/'+dir[idir]+name,/get_lun
      writeu,lun,tt,max(tra.t),!ld.x,nstep
      writeu,lun,tra
      free_lun,lun
    endfor
  endfor
endfor
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;writting trajectory data to show bad j ======
if 0 then begin
i=8 & !key.At=0 &  !key.nex=0
ldxv=findgen(7)
for ldi=0,1 do begin
  !ld.i=ldi
  for ld=0,6 do begin
    !ld.x=ldxv[ld]
    for mlr=2,2 do begin
      !mlr=mlrv[mlr]
      csub=chalo(tlist[i].mr,chost,/normalize)
      tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
                     tlist[i].yita,tlist[i].es,mf,mbaryon,ti,tf,$
		     0.1,dtmin,dtmax,eps,hprofile=!hprofile,$
		     halt=halt,df=1,td=1,th=1,bs=!key.bs)
      nstep=n_elements(tra)
      tt=trefine(tra,rmax=rmax,lfc=lfc,extrapolate=((!key.nex eq 0)or(tlist[i].treal gt 14)))
      print,i,(tt-tlist[i].treal)/tlist[i].treal
      name='ld'+strtrim(string(ldi),2)+'_'+string(!ld.x,format='(f4.2)')+ $
           '_'+strmid(strtrim(string(!mlr),2),0,3)+'mlr_'+ $
           strtrim(string(i),2) ;+'_'+string(!m.ga,format='(f3.1)')
      openw,lun,'bk08fit/tradata/badj/'+name,/get_lun
      writeu,lun,tt,max(tra.t),!ld.x,nstep
      writeu,lun,tra
      free_lun,lun
      wait,5
    endfor
  endfor
endfor
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;=========================================================
;fitting lnlda
if 0 then begin
sam={t:0.,t0:0.,ld:0.} & !key.At=0 
for nex=0,4,4 do begin
  !key.nex=nex
  for k=0,1 do begin
    !ld.i=k 
    for mlr=0,3 do begin
      !mlr=mlrv[mlr]
      str='nex'+string(!key.nex,format='(i1)')+'_lnlda_'+string(!ld.i,format='(i1)') $
          +'_'+strmid(strtrim(string(!mlr),2),0,3)+'.dat'
      print,str
      openu,lun,'bk08fit/'+str,/get_lun
      for i=0,np-1 do begin
        print,'treal=',tlist[i].treal,i
        point_lun,lun,i*34
        readf,lun,sam,format='(3f11.6)'
        !ld.x=sam.ld ;round(sam.ld*10.)/10.
        epst=0.05 & dld=1. & sign=0
        while 1 do begin
	  csub=chalo(tlist[i].mr,chost,/normalize)
          tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
                         tlist[i].yita,tlist[i].es,mf,mbaryon,$
			 ti,tf,0.1,dtmin,dtmax,eps,hprofile=!hprofile,$
                         halt=halt,df=1,td=1,th=1,bs=!key.bs)
          sam.t=trefine(tra,rmax=rmax,lfc=lfc,extrapolate=((!key.nex eq 0)or(tlist[i].treal gt 14)))
          dtf=(sam.t-tlist[i].treal)/tlist[i].treal
          print,sam.t,!ld.x,max(tra.t) ;,tra[nstep-1].m,tra[nstep-1].l/tra[0].l
          if abs(dtf) lt epst then begin
	    sam.ld=!ld.x
            sam.t0=max(tra.t)-ti
            point_lun,lun,i*34
            printf,lun,sam,format='(3f11.6)'
	    break
          endif
	  sign0=sign
          if dtf gt 0 then sign=1 else sign=-1
	  if(sign0+sign eq 0)then dld=dld/10.
          if dld le 0.001 then begin
    	    dld=0.1
	    epst=epst+0.01
	    if epst gt 0.2 then message,'numerical effect encountered!'
	  endif  
          !ld.x=!ld.x+dld*sign 
        endwhile
      endfor
      free_lun,lun
    endfor
  endfor
endfor
endif

end

;=========================================================
;fitting gamma
;if 0 then begin
;!ld.i=1 & !ld.x=0. & !mlr=3.5
;epst=0.1 & sam={t:0.,t0:0.,ga:0.}

;openu,lun,'bk08fit/Ateq3.5rtgamma.dat',/get_lun
;for i=0,25 do begin
;  print,'treal=',tlist[i].treal,i
;  point_lun,lun,i*34
;  readf,lun,sam,format='(3f11.6)'
;  !m.ga=0.01
;  dga=0.01 & sign=0
;  while 1 do begin
;    tra=trajectory(mhost*tlist[i].mr,mhost,mz,csub,chost,$
;                   tlist[i].yita,tlist[i].es,mf,ti,tf,0.1,dtmin,dtmax,$
;                   eps,halt=halt,df=1,td=1,th=1,bs=!key.bs)
;    sam.t=trefine(tra,rmax=rmax,lfc=lfc,/extrapolate)
;    sam.t0=max(tra.t)-ti
;    dtf=(sam.t0-tlist[i].treal)/tlist[i].treal
;    print,sam.t,!m.ga,sam.t0  ;,tra[nstep-1].m,tra[nstep-1].l/tra[0].l
;    if abs(dtf) lt epst then begin
;      sam.ga=!m.ga
;      point_lun,lun,i*34
;      printf,lun,sam,format='(3f11.6)'
;      break
;    endif
;    sign0=sign
;    if dtf gt 0 then sign=1 else sign=-1
;    if(sign0+sign eq 0)then dga=dga/10
;    if dga le 0.001 then message,'numerical effect encountered!'
;    !m.ga=!m.ga+dga*sign 
;  endwhile
;endfor
;free_lun,lun
;endif


;pro append_bk08
;common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;mrv=[0.3, 0.3, 0.3, 0.2, 0.1, 0.05,0.3 ,0.2 , 0.05,0.025]
;esv=[0.7, 0.5, 0.3, 0.3, 0.3, 0.3, 0.9, 0.9,  0.1, 0.1  ]
;na=n_elements(mrv) & print,na
;yitav=fltarr(na)+1.

;list={mr:0.,es:0.,yita:0.,treal:0.,tpre:0.}
;alist=replicate(list,na)
;alist.mr=mrv & alist.es=esv & alist.yita=yitav
;!key.hostz=0
;ratio=1/mrv
;z=0.
;h_inv=h0_inv/(ez(z))^0.5
;tdyn=sqrt(2/dltcz(z))*h_inv ;& print,tdyn
;alist.tpre=0.216*(ratio^1.3/alog(1+ratio))*exp(1.9*esv)*yitav*tdyn   ;Gyr
;alist.treal=alist.tpre
;openu,lun,'bk08fit/BK08t.dat',/get_lun
;skip_lun,lun,27,/lines
;printf,lun,alist,format='(f6.4,2f6.2,2f7.2)'
;free_lun,lun
;end

;=====================================================
;=====================================================
;function lnlda_best,ld,mlr,np
;sam=replicate({t:0.,t0:0.,ld:0.},np)
;str='lnlda_'+string(ld,format='(i1)')+'_'+strmid(strtrim(string(mlr),2),0,3)+'.dat'
;openr,lun,'bk08fit/'+str,/get_lun
;readf,lun,sam,format='(3f11.6)'
;free_lun,lun
;return,sam
;end
;==========================================================
;==========================================================
;function lnlda_fitted,ld,mlr,mr
;mlrv=[1.,2.5,3.5,6.,10.]
;cvv=fltarr(3,10)
;openr,lun,'bk08fit/yeqa0xa1plusa2.dat',/get_lun
;readf,lun,cvv,format='(3f11.3)'
;free_lun,lun
;j=where(mlrv eq mlr)+(ld eq 2)*5
;y=cvv[0,j]*mr^cvv[1,j]+cvv[2,j]
;;help,j,cvv[0,j]
;return,y
;end


