pro plot_orbit
mhost=1.e12 & !key.hostz=0
chost=8.5
rvir=r_vir(mhost,0.) & print,rvir
rs_host=rvir/chost
mr=[0.05,0.1,0.2] & nm=n_elements(mr)   ;i
epsonname=strmid(strtrim(string(!epsonv),2),0,3)  ;j
yitaname=strmid(strtrim(string(!yitav),2),0,3)    ;k
name=['epson_','rapo_'] & ytit=['!4e!x(t)','r!dapo!n/kpc']
psi=1 
mlrname=strmid(!wdir.mlr,0,3)                             ;mlr
mlr=2 & ld=2 & k=2
!x.charsize=1 & !y.charsize=1 & !p.charsize=1

for j=5,5 do begin
  device,/color,filename='orbits/'+name[psi]+epsonname[j]+'.ps',$
  /encapsul,xsize=18,ysize=27
  multiplot,[3,5],xgap=0.03,ygap=0.02,/doxaxis,/doyaxis,$
  mxtitle='time/Gyr',mxtitsize=1.5,mytitle=ytit[psi],mytitsize=1.5
  for bs=0,3 do begin
    tradir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
    for i=0,0 do begin
      ;print,mr[i],!epsonv[j],!yitav[k]
      tra_info=read_tra_info(tradir,mr[i],mhost,mrk)
      tra=read_tra(tradir,mrk,tra_info,!epsonv[j],!yitav[k])
      print,bs,i,max(tra.t),min(tra.m)
      if psi then orbit=tra.apo $
      else orbit=tra.es
      plot,tra.t,orbit,yrange=[0.95*min(orbit),max(orbit)*1.01],$
      xrange=[0,1.05*max(tra.t)],xmargin=[6,2],ymargin=[4,1],ylog=psi
      multiplot,/doxaxis,/doyaxis
      if bs then xyouts,0.2+i*0.29,0.97,'m/M='+string(mr[i],format='(f4.2)'),/normal
    endfor
    xyouts,0.96,0.85-bs*0.18,!wdir.bs[bs],/normal,orientation=90
  endfor
  xyouts,0.1,0.97,!wdir.ld[ld]+'!c'+!wdir.mlr[mlr],/normal
  print,orbit[0]
  multiplot,/default 
endfor
end



pro plot_tra,char=char,ply=ply,len=len,c0=c0,dc=dc ;,msub,mhost,c,yita,epson
mhost=1.e12 & !key.hostz=0
mr=[0.05,0.15,0.25] & nm=n_elements(mr)   ;i
Mname=strmid(strtrim(string(mr),2),0,5)           ;i
epsonname=strmid(strtrim(string(!epsonv),2),0,3)  ;j
yitaname=strmid(strtrim(string(!yitav),2),0,3)    ;k
ldname=['2.5','2.5+ln[m(0)/m(t)]','ln[1+M(0)/m(t)]','zentner/']         ;ld
mlrname=strmid(!wdir.mlr,0,3)                             ;mlr
dir='tradata/bs0rsh' & ld=2 & k=2
rmaxv=!yitav[k]*r_vir(mhost,0.)*(1+sqrt(1-!epsonv^2))/2

for j=0,5 do begin
  rmax=rmaxv[j]
  device,/color,filename='orbits/tra_'+epsonname[j]+'.ps',$
  /encapsul,xsize=18,ysize=22.2
  multiplot,[3,4],mxtitle='r/kpc',mytitle='r/kpc'
  for mlr=0,3 do begin
    tradir=dir+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
    for i=0,nm-1 do begin
      ;print,mr[i],!epsonv[j],!yitav[k]
      tra_info=read_tra_info(tradir,mr[i],mhost,mrk)
      tra=read_tra(tradir,mrk,tra_info,!epsonv[j],!yitav[k])
      n=n_elements(tra)
      tt=tra[n-1].t & mf=tra[n-1].m
      t=findgen(3*n)*max(tra.t)/3/n
      r=spline(tra.t,tra.r,t) & theta=spline(tra.t,tra.theta,t)
      plot,r,theta,/polar,/iso,xrange=[-1,1]*rmax,yrange=[-1,1]*rmax	 
      circle,0.,0.,1.,color=30
      xyouts,0.3*rmax,-0.7*rmax,$
      strmid(strtrim(string(tt),2),0,4)+'Gyr!c'+num2str(mf,format='(e7.1)')+'M!dsun!n'
      multiplot
    endfor
    xyouts,0.99,0.8-0.22*mlr,'A='+strmid(!wdir.mlr[mlr],0,3),orientation=90,/normal
  endfor
  for i=0,nm-1 do xyouts,0.2+0.3*i,0.98,Mname[i]
  multiplot,/default
endfor
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro plot_all_tra
mhost=1e12
sny=[1,7,2]
mtit=['m(t)/m(0)','r(t)/R!dvir!n','j(t)/j(0)']

device,/color,filename='orbits/tra_bs1.ps',$
/encapsul,xsize=18,ysize=7
multiplot,[3,1],mxtitle='t/Gyr',mxtitsize=1.2,xgap=0.028

bs=3 & ld=3 & mlr=2 & mr=0.1 & epson=0.5 & yita=1.
dir='tradata/'+!wdir.bs[bs]+'_'+!wdir.ld[ld]+'_'+!wdir.mlr[mlr]+!wdir.d
tra_info=read_tra_info(dir,mr,mhost,mrk)
tra=read_tra(dir,mrk,tra_info,epson,yita)
n=n_elements(tra)
print,tra[n-1].m/tra[0].m
for col=0,2 do begin
  iy=sny[col]
  plot,tra.t,tra.(iy)/tra[0].(iy),$
  xrange=[0,10.9],yrange=[1e-4,1]
  xyouts,(!p.position[0]+!p.position[2])/2,0.93,mtit[col],/normal,align=0.5
  multiplot,/doyaxis
endfor
multiplot,/default
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
