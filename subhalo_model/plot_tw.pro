pro plot_tw
char=1.1 &  nmp=5
read_halofunc,ml,mh,nbin,dbin,xh,halofunc,halofunc0,mp=[0,15],major=1
help,halofunc.m.ev
mpv=indgen(nmp)+2
ld=7 & bs=0 


device,filename='twxjz/mfunc.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.mun,halofunc0.m.un.giocoli,/nodata,$              ;evolved, data of Giocoli 2007
xrange=[5e-5,0.5],yrange=[2e-2,4e2],/xlog,/ylog,$
xtickname=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'],$
ytickname=['10!u-1!n','1','10','10!u2!n'],$
xtitle='m/M',position=[0.18,0.16,0.98,0.97];,ytitle='dn/dln(m/M)'
xyouts,0.06,0.58,'dN/dln(m/M)',/normal,orient=90,align=0.5,charsize=1.2

for i=0,1 do begin
  ihost=1+i*3
  oplot,xh.mun,halofunc.m.un[*,i*5],linestyle=i,color=!myct.grey
  oplot,xh.mev,halofunc0.m.ev.giocoli[*,ihost],linestyle=i;,color=!myct.blue
endfor
labeling,0.02,0.3,0.1,0.08,['Cluster, EPS','MW, EPS','Cluster, SIM','MW, SIM'],$
/lineation,linestyle=[1,0,1,0],charsize=1,$
ct=[!myct.grey,!myct.grey,0,0]
;========================================================
;========================================================
charv=['MW','Cluster']
device,filename='twxjz/mfunc1.eps',/encapsul,/color,xsize=16,ysize=8
multiplot,[2,1],mxtitle='m/M',mytitle='dN/dln(m/M)',$
mxtitoffset=0.5,mytitoffset=-0.5,mxtitsize=1.2,mytitsize=1.2,$
myposition=[0.11,0.16,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
tick0=replicate(' ',4) & tick1=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n']
xtick=[[tick1],[tick1]]
tick0=replicate(' ',4) & tick1=['10!u-1!n','1','10','10!u2!n']
ytick=[[tick1],[tick0]]

for psi=0,1 do begin
  plot,xh.mun,halofunc0.m.un.giocoli,/nodata,$
  xrange=[5e-5,0.2],yrange=[3.01e-2,2e2],/xlog,/ylog,$
  xtickname=xtick[*,psi],ytickname=ytick[*,psi]
  ihost=1+psi*3
  oplot,xh.mev,halofunc0.m.ev.giocoli[*,ihost]
  for mlr=1,3 do begin
    mpi=psi*5
    oplot,xh.mev,halofunc.m.ev[*,ld,mlr,bs,mpi],linestyle=mlr;,$
    ;color=!myct.c3[mlr-1]  ;thick=3+(mlr-1)*4
  endfor
  xyouts,0.1,70,charv[psi],align=1
  multiplot
endfor
multiplot,/default
labeling,0.53,0.3,0.06,0.08,['SIM','A='+['1.','3.5','10.']],/lineation,$
linestyle=[0,1,2,3]  ;thickv=[4,3,7,11],
;=========================================================
;=========================================================
ld=7  & mlr=2 & mpi=0
device,filename='twxjz/rfunc.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char
plot,xh.r,halofunc0.r.DM,/xlog,/ylog,$                       ;unevolved, fitted by Giocoli 2007
xrange=[0.05,1],yrange=[0.003,1.1],$
position=[0.21,0.17,0.96,0.97],linestyle=5,$
xtitle='r/R!dvir!n',ytitle='N(<r)/N(<R!dvir!n)'
oplot,xh.r,halofunc0.r.einasto200
;oplot,xr,halofunc0.r.diemand[*,mc],linestyle=2
;oplot,xr,halofunc0.r.einasto50,linestyle=3
;oplot,xh.r,halofunc0.r.DM,linestyle=5
oplot,xh.r,halofunc0.r.MW,psym=-6
;shadowing,xh.r,halofunc0.r.diemand[*,1],halofunc0.r.einasto50,density=8,thick=3
lstyle=[2,0,1]
for bs=0,2,2 do $
  oplot,xh.r,halofunc.r.mcf[*,7,2,bs,0,mpi],linestyle=lstyle[bs]

labeling,0.6,0.4,0.16,0.08,'MW',/lineation,/symboling,psym=6
labeling,0.6,0.31,0.16,0.08,['DM','Model','Model','SIM'],$
/lineation,linestyle=[5,1,2,0]
;=======================================================
;=======================================================
out=read_satellite('halofunc/plotdata/MW_satellite.dat')
a=0.684 & b=5.667 
mag=findgen(101)*0.2-20
Rlim=(3/(4*!pi*0.194))^(1./3.)*10^((-a*mag-b)/3+3)
device,file='twxjz/sate.eps',/encapsul,/color,xsize=10,ysize=8
!p.charsize=char & !x.charsize=char & !y.charsize=char

plot,out.mag,out.dsun,xrange=[-20,0],yrange=[0,270],$
xtitle='M!dv!n',ytitle='d / kpc',position=[0.2,0.18,0.97,0.97],$
psym=6
;for i=0,1 do begin
;  sn=where(out.ID eq i*2+1998)
;  oplot,out[sn].mag,out[sn].dsun,psym=i+5
;endfor
oplot,mag,Rlim
;labeling,0.05,0.9,0.05,0.1,['Classical','Ultra-faint'],/symboling,psym=[5,6]
end
;========================================================
;=========================================================
pro plot_tmerge_tw
!key.hostz=0
mhost=1.e12 
mratio=!mrv[where((!mrv ge 0.05)and(!mrv le 0.3))] & epson=[0.3,0.5,0.7] & yita=1.
nm=n_elements(mratio) & nu=n_elements(epson) & ny=n_elements(yita)
Mname=strmid(strtrim(string(mratio),2),0,5)
epsonname=string(epson,format='(f3.1)')
yitaname=string(yita,format='(f3.1)')
chost=8.5
tfit=tmerge_pre(mratio,epson,yita,z,mhost,chost)
;print,tfit.taffoni

k=ny-1 & ld=7 & char=1.1
;=====================================================
;tfit from BK08, J08, T03
device,filename='twxjz/tfit.eps',$
/encapsul,/color,xsize=18,ysize=6 ;,xoffset=1.5,yoffset=10
multiplot,[3,1],mxtitle='m(0)/M(0)',mxtitsize=1.2,mxtitoffset=0.3,$
mytitle='T!ddf!n /Gyr',mytitsize=1.2,mytitoffset=-2.5,$
xgap=0.025,/doyaxis,myposition=[0.05,0.22,0.99,0.97]
!p.charsize=char & !x.charsize=char & !y.charsize=char
for j=0,nu-1 do begin  ;epson
  t=tfit.bk[*,j,k] & tmin=min(t) & tmax=max(t)
  plot,mratio,t,yrange=[0.1*tmin,tmax],linestyle=2,$
  xtickname=[' ','0.1',' ','0.2',' ','0.3']
  oplot,mratio,tfit.jiang[*,j,k],linestyle=1
  oplot,mratio,tfit.taffoni[*,j,k],linestyle=3
  xyouts,0.28,0.88*tmax,'!me!x='+epsonname[j],align=1,charsize=1.2
  multiplot,/doyaxis
endfor
multiplot,/default
labeling,0.85,0.88,0.05,0.12,['J08','BK08','T03'],$
/lineation,linestyle=[1,2,3],charsize=char
 
end
