function rx_eq,x,csub,mx
return,gx(x)/gx(csub)-mx
end
;============================================================
function rx_eq_p,x,csub
return,x/(1+x)^2/gx(csub)
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;==============================================================
function Rbounding,mx,csub,th=th
it=0 & itmax=1000 & eps=1e-3 & root=fix(mx) & root[*]=0
rx=mx*csub & f=mx & fp=mx
sn=where(mx eq 0.,n0)
if n0 ne 0 then root[sn]=1

  while 1 do begin
    sn1=where(root eq 0,n1) ;& print,n1
    if n1 ne 0 then begin
      f[sn1]=rx_eq(rx[sn1],csub[sn1],mx[sn1])
      fp[sn1]=rx_eq_p(rx[sn1],csub[sn1])
      dx=abs(f/fp) & dx0=rx*eps > 0.01
      sn2=where((root eq 0)and((dx le dx0)or(abs(f)/mx le eps)),n2)
      if n2 ne 0 then root[sn2]=1
      sn2=where((root eq 0)and(dx gt dx0)and(f lt 0),n2)
      sn3=where((root eq 0)and(dx gt dx0)and(f gt 0),n3)
      if n2 ne 0 then rx[sn2]=(temporary(rx[sn2])+dx[sn2])
      if n3 ne 0 then rx[sn3]=(temporary(rx[sn3])-dx[sn3]/2) > 1e-3
      ;if !enprint then print,rx,dx,format='(2e15.6)'
      sn2=where(root eq 0,n2)
      if n2 ne 0 then begin
        fp[sn2]=rx_eq_p(rx[sn2],csub[sn2])
      endif else break    
    endif else break
    it++
  endwhile
;help,it
if keyword_set(th) then rx=temporary(rx)*2 < csub
;Rbnd=rx*rs_sub
return,rx
end
;============================================================
;=============================================================
;pro test_rbnd
;n=100
;csub=randomu(test,n)*22.1+0.9 & mx=randomu(test,n) ;& print,mx
;rx=Rbounding(mx,csub)
;device,filename='heating/rbnd.ps',/encapsul,/color
;plot,mx,rx/csub,psym=1,xrange=[0,1],yrange=[0,1],$
;xtitle='m(t)/m(0)',ytitle='r!dbound!n/r!dvir!n'
;rx=temporary(rx)*2 < csub
;oplot,mx,rx/csub,color=!myct.red,psym=4
;end





;pro write_rbnd
;cmin=0.9 & cmax=23.
;rsmin=0.5 & rsmax=32.
;mxmin=1e-9 & mxmax=1.
;end
