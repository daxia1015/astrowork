;----probability density profile provided by Zenter
function fepson,es 
a=2.22
f=gamma(2*a)*es^(a-1)*(1-es)^(a-1)/gamma(a)^2
return,f
end

;---probability density profile provided by Jiang
function fepsonj,es  
f=2.77*es^1.19*(1.55-es)^2.99
return,f
end


pro epsondata
func='fepsonj'
C=qromb(func,0.,1.) & print,'C=',C
n=500
epson=findgen(n+1)/n
int_epson=qromb(func,0.,epson)/C
out=fltarr(2,n+1)
out(0,*)=epson
out(1,*)=int_epson
openw,lun,'upsilon.dat',/get_lun
  printf,lun,out,format='(f9.6,2x,f9.6)'
free_lun,lun
nn=20000
int_epson1=randomu(angmomenta,nn)
epson1=interpol(epson,int_epson,int_epson1)
epson2=epson1
;epson1>=0.1 & epson1<=1.
while 1 do begin
  sn=where((epson2 lt 0.1)or(epson2 gt 1.),count)
  if count ne 0 then begin
    int=randomu(angmomenta,count)
    epson2[sn]=interpol(epson,int_epson,int)
  endif else break
endwhile


nbin=10 & dbin=1./nbin
Cbin=nn/nbin
print,'epson:',min(epson1),max(epson1)
h=histogram(epson1,binsize=dbin,min=0.,max=1,locations=x1) ;& nnn=total(h)
;h=double(h)/nn
plot,x1+dbin/2,h,psym=-1;,xrange=[0,1]
print,'histogram of epson:'
print,x1
print,h

print,'F(<epson):',min(int_epson1),max(int_epson1)
h=histogram(int_epson1,binsize=dbin,min=0.,max=1.,locations=x1)
;h=double(h)/nn
oplot,x1+dbin/2,h,color='ff0000'xl  ;blue
print,'histogram of F(<epson):'
print,x1
print,h

oplot,epson,Cbin*fepsonj(epson)/C,linestyle=2,thick=2
oplot,epson,Cbin*int_epson,color='00ff00'xl ;green
end
