function multi_regions,xsize,ysize,title=title,right_title=right_title
n_col=!p.multi[1] & n_row=!p.multi[2] & direction=!p.multi[4]
n=n_col*n_row & region=fltarr(4,n)
if direction then plotij=transpose(indgen(n_row,n_col)) $
else plotij=indgen(n_col,n_row)

large=1.2 & small=0.3 & dl=0.5
left=(large+dl)/xsize & bottom=(large+dl)/ysize
if keyword_set(title) then top=large/ysize else top=small/ysize
if keyword_set(right_title) then right=large/xsize else right=small/xsize
xlen=1-left-right & ylen=1-bottom-top
dx=xlen/n_col & dy=ylen/n_row
xs=left+dx*findgen(n_col+1)
ys=bottom+dy*findgen(n_row+1)

for i=0,n_col-1 do begin
  for j=0,n_row-1 do begin
    region[*,plotij[i,j]]=[xs[i],ys[n_row-1-j],xs[i+1],ys[n_row-j]]
  endfor
endfor
return,region
end
