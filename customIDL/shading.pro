;y1:lower limit, y2:upper limit
pro shading,x,y1,y2,shadeColor=shadeColor,density=density,_extra=_extra
n=n_elements(x)
if(n_elements(y1) ne n)or(n_elements(y2) ne n)then message,'x, y1, y2 should have equal elements'
oplot,x,y1,_extra=_extra
oplot,x,y2,_extra=_extra

ColorTable=shadeColor
if n_elements(density) eq 0 then density=5
dy=(y2-y1)/(density-1)
xc=rebin(x,density*n)
yc=xc
for i=0,n-1 do yc[i*density:(i+1)*density-1]=findgen(density)*dy[i]+y1[i]
zc=fltarr(density*n)
Contour,zc,xc,yc,/Fill,/irregular,C_Colors=shadeColor,NLevels=1,/overplot

end
