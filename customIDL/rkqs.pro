;Fifth order Runge-Kutta step with monitoring of local truncation
;error to ensure accuracy and adjust stepsize. Input are the 
;dependent variable vector y and its derivative dydx at the starting
;value of the independent variable x. Also input are the stepsize to 
;be attempted htry, the required accuracy eps, and the vector yscal 
;against which the error is scaled. y,dydx, and yscal are all of the 
;same length. On output, y and x are replaced by their new values, 
;htry is modified to the stepsize that was actually accomplished, and hnext is 
;the estimated next stepsize. derivs is the user-supplied subroutine
;that computes the right-hand-side derivatives.
;This code was modified from a fortran code 'rkqs.f90'.
;In IDL, this code can be used to calculate a huge number of 
;particles simutaneously by given y,dydx,x,htry,eps etc. in array
;of two dimensions that: [n_equations,n_particles]  
;=======================================================
pro rkqs,y,dydx,x,htry,eps,yscal,hnext,derivs
SAFETY=0.9 & PGROW=-0.2 & PSHRNK=-0.25 & ERRCON=1.89e-4
if htry[0] ge 0 then sign=1. else sign=-1.  ;Here suppose all timesteps have same sign.
neq=n_elements(y[*,0]) & np=n_elements(y[0,*])
while 1 do begin
  ytemp=rkck(y,dydx,x,htry,yerr,derivs)
  errmax=max(abs(yerr/yscal)/eps,dimension=1)
;  if np gt 1 then errmax=rebin(errmax,neq,np)
  if np gt 1 then errmax=rebin(reform(temporary(errmax),1,np),neq,np)
  sn=where(errmax gt 1,count)
  if count eq 0 then break else begin
  
;  if errmax le 1. then break
    htemp=SAFETY*htry[sn]*(errmax[sn]^PSHRNK)
    htry[sn]=(abs(htemp) > abs(0.1*temporary(htry[sn])))*sign ;$
;    else htry=max([htemp,0.1*htry],/absolute)
  endelse
  xnew=x+htry
  sn=where(xnew eq x,count)
  if count ne 0 then begin
    ;print,htry[sn]
    message,'stepsize underflow in rkqs, may be caused by too high accuracy'
  endif
;  if xnew eq x then message,'stepsize underflow in rkqs'
endwhile

hnext=htry
sn=where(errmax gt ERRCON,count)
sn1=where(errmax le ERRCON,count1)
if count ne 0 then hnext[sn]=SAFETY*htry[sn]*(errmax[sn]^PGROW)
if count1 ne 0 then hnext[sn1]=5.*htry[sn1]
;if errmax gt ERRCON then hnext=SAFETY*h*(errmax^PGROW) else hnext=5.0*h
;hdid=htry
x=temporary(x)+htry
y=ytemp
;return,ytemp
end
