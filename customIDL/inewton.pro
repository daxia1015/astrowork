;This function find the root of an equation f(x)=0, of which f(x) is monotonic 
;within the range of possible value. 
;This function can slove the equation with huge number of initial 
;guess value and constant simutaneously.
;xi:  initial value
;fxp: the function name of the derivation of f(x)
;fx:  the function name of f(x)
;tol: expected accurance
function inewton,xi,fx,fxp,Xtol=Xtol,Ytol=Ytol,lower=lower,$
    	 upper=upper
on_error, 2 ;Return to caller if error occurs.
if n_elements(Xtol) eq 0 then Xtol=1e-6
if n_elements(Ytol) eq 0 then Ytol=1e-6

it=0 & itmax=1000 & xroot=xi+0.
f=xroot & fp=xroot & status=fix(xroot) 
;status=fix(temporary(status))
status[*]=0 

sn1=where(status eq 0,n1) ;& print,n1
while n1 ne 0 do begin
  f =call_function(fx, xroot)
  fp=call_function(fxp,xroot)

  sn2=where((status eq 0)and(abs(f)        lt Ytol),n2)
  if n2 ne 0 then status[sn2]=1
  
  dx=f/fp ;& dx0=x*eps ;> eps
  sn2=where((status eq 0)and(abs(dx/xroot) lt Xtol),n2)
  if n2 ne 0 then begin 
    status[sn2]=1
    xroot[sn2]=xroot[sn2]-dx[sn2]
  endif
  it++
;  if n_elements(lower) ne 0 then x=x > lower
;  if n_elements(upper) ne 0 then x=x < upper
  sn1=where(status eq 0,n1)
  if n1 ne 0 then begin
    if (it gt itmax) then message,'failed to find root'
    xroot[sn1]=xroot[sn1]-dx[sn1]
  endif  
endwhile
message,'iterations='+string(it),/continue
return,xroot
end
;  endif else begin
;    if nc ne 2 then message,'n_const must have two elements.'
;    case n_const[0] of
;      0: f[sn1]=call_function(fx,x[sn1])
;      1: f[sn1]=call_function(fx,x[sn1],c1[sn1])
;      2: f[sn1]=call_function(fx,x[sn1],c1[sn1],c2[sn1])
;      3: f[sn1]=call_function(fx,x[sn1],c1[sn1],c2[sn1],c3[sn1])
;      4: f[sn1]=call_function(fx,x[sn1],c1[sn1],c2[sn1],c3[sn1],c4[sn1])
;      5: f[sn1]=call_function(fx,x[sn1],c1[sn1],c2[sn1],c3[sn1],c4[sn1],c5[sn1])
;      else: message,'number of coefficients are not enough!'
;    endcase 
;    case n_const[1] of
;      0: fp[sn1]=call_function(fxp,x[sn1])      
;      1: fp[sn1]=call_function(fxp,x[sn1],c1[sn1])
;      2: fp[sn1]=call_function(fxp,x[sn1],c1[sn1],c2[sn1])
;      3: fp[sn1]=call_function(fxp,x[sn1],c1[sn1],c2[sn1],c3[sn1])
;      4: fp[sn1]=call_function(fxp,x[sn1],c1[sn1],c2[sn1],c3[sn1],c4[sn1])
;      5: fp[sn1]=call_function(fxp,x[sn1],c1[sn1],c2[sn1],c3[sn1],c4[sn1],c5[sn1])
;      else: message,'number of coefficients are not enough!'
;    endcase 
;  endelse
