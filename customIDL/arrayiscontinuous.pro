function ArrayIsContinuous,array,arithmetic=arithmetic,geometric=geometric,$
    	    	    	   equ=equ,verbose=verbose
n=n_elements(array)
if n lt 3 then message,'array must have at least 3 elements'

if keyword_set(arithmetic) then begin
  d=array[1:n-1]-array[0:n-2]
  sn=uniq(d,sort(d))
  if n_elements(sn) eq 1 then begin
    equ=d[0]
    if equ eq 0 then message,'array have same elements'
    mes='array is arithmetic progression and equ='+string(equ)
    if keyword_set(verbose) then message,mes,/continue
    return,1 
  endif else return,0
endif

if keyword_set(geometric) then begin
  d=array[1:n-1]/array[0:n-2]
  sn=uniq(d,sort(d))
  if n_elements(sn) eq 1 then begub
    equ=d[1]
    if equ eq 1 then message,'array have same elements'
    mes='array is geometic progression and equ='+string(equ)
    if keyword_set(verbose) then message,mes,/continue
    return,1 
  endif else return,0
endif

end
