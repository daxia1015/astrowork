;usage: psfigure,'procedure_name'
pro psfigure,to_plot,char=char,ply=ply,len=len
!except=2
entry_device=!d.name
set_plot,'ps'
!p.font=0
colors_kc
if n_elements(char) eq 0 then char=1.2 
if n_elements(ply)  eq 0 then ply=4 
if n_elements(len)  eq 0 then len=0.04 
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char & !x.ticklen=len
!y.thick=ply & !y.style=1 & !y.charsize=char & !y.ticklen=len

call_procedure,to_plot


multiplot_Gan,/default
device,/close_file
set_plot,entry_device
!except=1
end
