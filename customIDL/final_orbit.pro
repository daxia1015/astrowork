function final_orbit,tra_f,mhost,rs_host,chost,rpvir
common tree_parameters ;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
n=500
rc=10^(findgen(n+1)*alog10(rpvir*1.2)/n)+0.1
ec=energyc(rc,mhost,rs_host,chost,rpvir)
ecf=tra_f.energy
ecf=ecf[where(ecf ge min(ec))]
;ecf=ecf > min(ec)
rcf=interpol(rc,ec,ecf)
;print,min(ec),max(ec)
;print,min(ecf),max(ecf)
;sn=where(rcf lt 0,count)
;if count ne 0 then print,rcf[sn],ecf[sn]
m_rcf=mhalo_r(rcf,rs_host,chost,mhost)
es=tra_f.l/sqrt(gaussc*m_rcf*rcf)
return,es
end


;pro test_e,major=major,mp=mp
;common tree_parameters ;,nlev,mphalo,mres,ntree,zmax,zhalt,ihalt
;common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;!except=2
;!key.hostz=1 & z=0.
;chost=chalo(mphalo,z) & rpvir=r_vir(mphalo,z) ;& print,'rvir=',rvir      ;kpc, virial radius of host halo
;rs_host=rpvir/chost   ;initial r_s of subhalo and host halo
;dir='halofunc/evolving/bs1rs_M0mt1_2.2mlr/'
;mpname=!wdir.mp[mp]
;halon=read_halon(dir,mpname,ns,major=major)
;help,halon,/struc
;print,halon.tra
;rc=(findgen(2000)*0.1+0.1)
;ec=energyc(rc)
;plot,rc,ec
;epson=final_orbit(halon.tra,rs_host,chost,rpvir)
;print,epson
;nbin=10 & dbin=1./nbin
;h=histogram(epson,binsize=dbin,min=0.,max=1,locations=x1)
;Cbin=total(h)/nbin
;epson0=findgen(100)*0.01
;f0=fepson(epson0,/jiang)
;plot,x1+dbin/2,h/Cbin,linestyle=2,xrange=[0,1] ;,color='00ff00'xl
;oplot,epson0,f0
;end
