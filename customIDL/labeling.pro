;+
;x0: the horizontal starting position of first line or central
;    position of first symbol
;y0: the vertical starting position of first line or central
;    postion of first symbol
;xlen: the length of line or the horizontal gap between symbols and chars
;ygap: the vertical gap between two lines or symbols
;charv: the array of chars to be labeled
;ct: color tabels for lines, symbols or chars
;linestyle: linestyles for lines
;psym: symbols types for symbols
;/lineation: to label lines
;/symboling: to label symbols
;_extra: extra keywords are accepted by oplot, xyouts
;-
;================================================================== 
pro labeling,x0,y0,xlen,ygap,charv,ct=ct,linestyle=style,psym=symv,$
    thickv=thickv,lineation=lineation,symboling=symboling,$
    chardownsize=chardownsize,bottom2top=bottom2top,_extra=_extra

en1=keyword_set(lineation) & en2=keyword_set(symboling)

nc=n_elements(charv)
if nc ne 0 then begin
  if n_elements(ct) eq 0 then ct=intarr(nc)+!p.color
  if n_elements(ct) eq 1 then ct=intarr(nc)+ct
  if n_elements(style) eq 0 then style=intarr(nc)+!p.linestyle
  if n_elements(style) eq 1 then style=intarr(nc)+style
  if n_elements(symv) eq 0 then symv=intarr(nc)+!p.psym
  if n_elements(symv) eq 1 then symv=intarr(nc)+symv
  if n_elements(thickv) eq 0 then thickv=intarr(nc)+!p.thick
  if n_elements(thickv) eq 1 then thickv=intarr(nc)+thickv
endif
if keyword_set(bottom2top) then sign=1 else sign=-1
;+1: from bottom to top
;-1: from top to bottom

n=100 &   n1=10
plot,findgen(n+1)/n,findgen(n+1)/n,/nodata,xstyle=4,ystyle=4,/noerase	

if en1 then begin
  for i=0,nc-1 do begin
    oplot,findgen(n1)*xlen/n1+x0,fltarr(n1)+y0+sign*i*ygap,$
    color=ct[i],linestyle=style[i],thick=thickv[i],_extra=_extra
  endfor
endif

if en2 then $
 for i=0,nc-1 do $
   oplot,[x0,x0]+en1*xlen*0.49,[y0,y0]+sign*ygap*i,color=ct[i],psym=symv[i],_extra=_extra
;print,float(!d.y_ch_size), float(!d.y_size)
if n_elements(chardownsize) eq 0 then begin
  chardownsize=0.6*float(!d.y_ch_size) / float(!d.y_size)
endif

if nc ne 0 then $
  for i=0,nc-1 do $
    xyouts,x0+xlen,y0-chardownsize+sign*i*ygap,charv[i],$
    color=ct[i],_extra=_extra

end
