;On output nok and nbad are the number
;of good and bad (but retried and
;fixed) steps taken. If save steps is
;set to true in the calling program,
;then intermediate values are stored
;in xp and yp at intervals greater than
;dxsav. kount is the total number of
;saved steps.
;this code was modified from a fortran code 'odeint.f'
;=============================================
;MODULE ode_path
;	USE nrtype
;	INTEGER(I4B) :: nok,nbad,kount
;	LOGICAL(LGT), SAVE :: save_steps=.false.
;	REAL(SP) :: dxsav
;	REAL(SP), DIMENSION(:), POINTER :: xp
;	REAL(SP), DIMENSION(:,:), POINTER :: yp
;END MODULE ode_path
;======================================================
pro save_a_step,kcount,x,y,xv,yv,xsav
kount++
if n_elements(xv) eq 0 then xv=x else xv=[xv,x]
if n_elements(yv) eq 0 then yv=y else yv=[[yv],[y]]
xsav=x
END 
;==========================================================
;Runge-Kutta driver with adaptive stepsize control. Integrate the
;array of starting values ystart from x1 to x2 with accuracy eps, 
;storing intermediate results in the module variables in ode path. 
;h1 should be set as a guessed first stepsize, hmin as the minimum
;allowed stepsize (can be zero). On output ystart is replaced by 
;values at the end of the integration interval. derivs is the 
;user-supplied subroutine for calculating the right-handside
;derivative, while rkqs is the name of the stepper routine to be used.
;======================================================
function odeint,ystart,x1,x2,eps,h1,hmin,derivs,$
                dxsav,save_steps=save_steps
TINY=1.0e-30
MAXSTP=10000L

x=x1
if x2-x1 ge 0 then sign=1. else sign=-1.
h=abs(h1)*sign
nok=0L & nbad=0L & kount=0L & nstp=0L
y=ystart
	
if keyword_set(save_steps) then xsav=x-2.*dxsav

while 1 do begin
  dydx=call_function(derivs,x,y)
  yscal=abs(y)+abs(h*dydx)+TINY
  if keyword_set(save_steps) and (abs(x-xsav) gt abs(dxsav)) then $
  save_a_step,kcount,x,y,xv,yv,xsav
  
  if ((x+h-x2)*(x+h-x1) gt 0.) then h=x2-x
  
  call_procedure,'rkqs',y,dydx,x,h,eps,yscal,hdid,hnext,derivs
  if hdid eq h then nok=nok+1 else nbad=nbad+1

  if ((x-x2)*(x2-x1) ge 0.) then begin
    ystart=y
    if keyword_set(save_steps) then save_a_step,kcount,x,y,xv,yv,xsav
    break
  endif
  if(abs(hnext) lt hmin)then message,'stepsize smaller than minimum in odeint',/continue
  h=hnext
  nstp++
  if nstp gt MAXSTP then message,'too many steps in odeint'
endwhile
root={x:xv,y:yv,n:kcount,ok:nok,bad:nbad}
return,root
end
