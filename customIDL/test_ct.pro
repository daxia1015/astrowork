pro test_ct
entry_device=!d.name
set_plot,'ps'
!except=2
char=1.5 & ply=3 & len=0.04 & c0=25 & dc=80
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char
!y.thick=ply & !y.style=1 & !y.charsize=char
colors_kc
;help,!myct,/struct

device,/color,filename='ct.ps',xsize=16,ysize=20
!p.multi=[0,2,3,0,0]
x=findgen(100)*0.04
for j=0,5 do begin
  ct=!myct.(j)
  n=n_elements(ct)
  plot,x,x,yrange=[min(ct)-10,max(ct)+10],/nodata
  for i=0,n-1 do oplot,x,fltarr(100)+ct[i],color=ct[i],thick=10 ;,linestyle=i
endfor
!p.multi=0
device,/close_file
set_plot,entry_device
end
