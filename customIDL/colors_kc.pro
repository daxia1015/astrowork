;!!!available for idl postscript plotting
;usage: color=!myct.* or color=value 
;
pro colors_kc,reload=reload


defsysv,'!myct',exists=en
if en then return

;===load color table==============
loadct, 0,  ncolors=144, bottom=0
loadct, 13, ncolors=48,  bottom=144 ;rainbow
loadct, 12, ncolors=16,  bottom=192 ;16level
loadct, 6,  ncolors=16,  bottom=208 ;prism
loadct, 38, ncolors=32,  bottom=224 ;rainbow18

a={black:0,dkgrey:37,grey:72,ltgrey:108,white:143,$
   ltblue:160,dkyellow:183,ltorange:186,dkorange:188,$
   green:194,cyan:197,blue:198,magenta:200,pink:201,ltred:202,red:204,$
   rufous:210,dkred:211,ltgreen:216,dkblue:221,$
   dkpurple:225,purple:226,dkgreen:234,yellow:244,orange:250} 
c2=[a.blue,a.dkred]   
c3=[a.dkred,a.dkgreen,a.blue]
c4=[a.red,a.blue,a.dkgreen,a.dkpurple]
c5=[a.pink,a.rufous,a.dkgreen,a.ltblue,a.dkorange]
c6=[a.red,a.dkorange,a.blue,a.dkgreen,a.dkpurple,a.ltblue]
c7=[a.pink,a.rufous,a.dkorange,a.dkgreen,a.ltblue,a.blue,a.dkpurple]
c8=[a.pink,a.rufous,a.dkorange,a.dkgreen,a.ltblue,a.blue,a.dkpurple,a.black]
c9=[a.dkgrey,a.pink,a.rufous,a.dkorange,a.dkgreen,a.ltblue,a.blue,a.dkpurple,a.cyan]
ct=create_struct(['c2','c3','c4','c5','c6','c7','c8','c9'],$
    	    	   c2,c3,c4,c5,c6,c7,c8,c9,a)

defsysv,'!myct',ct,1
end





