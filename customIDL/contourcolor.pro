pro contourcolor,nlevel,ct,

LoadCT, 33, NColors=12, Bottom=3

    ; Draw the first plot. Let IDL select contour intervals by
    ; using the NLEVELS keyword.
    Window, 0, Title='IDL Selected Contour Intervals', XSize=300, YSize=400
    SetDecomposedState, 0, CurrentState=state
    Contour, data, x, y, /Fill, C_Colors=Indgen(levels)+3, Background=cgColor('white'), $
       NLevels=levels, Position=[0.1, 0.1, 0.9, 0.80], Color=cgColor('black')
    Contour, data, x, y, /Overplot, NLevels=levels, /Follow, Color=cgColor('black')
    SetDecomposedState, state
    cgColorBar, NColors=12, Bottom=3, Divisions=6, $
       Range=[Min(data), Max(data)], Format='(I4)', $
       Position = [0.1, 0.9, 0.9, 0.95], AnnotateColor='black'
