function histo2d,xx0,yy0,nbinx=nbinx,nbiny=nbiny,xmin=xmin0,xmax=xmax0,$
    	    	 ymin=ymin0,ymax=ymax0,dbinx=dbinx,dbiny=dbiny,locationx=hx,$
		 locationy=hy,xlog=xlog,ylog=ylog

if n_elements(nbinx) eq 0 then nbinx=11
if n_elements(nbiny) eq 0 then nbiny=11
sn=uniq(xx0,sort(xx0)) & n=n_elements(sn)
if n_elements(xmin0) eq 0 then xmin0=xx0[sn[1]]*1.1
if n_elements(xmax0) eq 0 then xmax0=xx0[sn[n-2]]*0.95
sn=uniq(yy0,sort(yy0)) & n=n_elements(sn)
if n_elements(ymin0) eq 0 then ymin0=yy0[sn[1]]*1.1
if n_elements(ymax0) eq 0 then ymax0=yy0[sn[n-2]]*0.95

xmax=xmax0+0. & xmin=xmin0+0. & xx=xx0+0. ;use another variable for protecting the original data
ymax=ymax0+0. & ymin=ymin0+0. & yy=yy0+0.

if keyword_set(xlog) then begin
  xmax=alog10(xmax)
  xmin=alog10(xmin)
  sn=where(xx gt 0,n1)
  if n1 eq 0 then message,'no valid data found in array xx'  
  xx=alog10(xx[sn])
endif
if keyword_set(ylog) then begin
  ymax=alog10(ymax)
  ymin=alog10(ymin)
  sn=where(yy gt 0,n1)
  if n1 eq 0 then message,'no valid data found in array yy'  
  yy=alog10(yy[sn])
endif

dbinx=(xmax-xmin)/(nbinx-1)
dbiny=(ymax-ymin)/(nbiny-1)
d2x=dbinx/2. & d2y=dbiny/2.
hx=findgen(nbinx)*dbinx+xmin
hy=findgen(nbiny)*dbiny+ymin
h=dblarr(nbinx,nbiny)

for ix=0,nbinx-1 do begin
  for iy=0,nbiny-1 do begin
    sn=where((xx ge hx[ix]-d2x)and(xx lt hx[ix]+d2x)and $
    	     (yy ge hy[iy]-d2y)and(yy lt hy[iy]+d2y),count)  
    h[ix,iy]=count  
  endfor
endfor
if keyword_set(xlog) then hx=10^hx
if keyword_set(ylog) then hy=10^hy
return,h
end
