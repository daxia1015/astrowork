function histo_moment,h0,nbin,get_unit=get_unit,log10=log10
mi={average:0.0d,deviation:0.0d,up:0.0d,low:0.0d}
if keyword_set(get_unit) then return,mi

sz=size(h0)
if sz[0] ne 2 then message,'The input array must have 2 dimensions.'
if sz[1] ne nbin then message,'The size of first dimension is wrong.'
if sz[2] le 2 then message,'The second dimension is too small.'
mom=replicate(mi,nbin)
if keyword_set(log10) then h=alog10(h0) else h=h0+0.0d
for ibin=0,nbin-1 do begin
  temp=moment(h[ibin,*],sdev=dev)
  mom[ibin].average=temp[0]
  mom[ibin].deviation=dev
endfor
mom.up=mom.average+mom.deviation
mom.low=mom.average-mom.deviation
if keyword_set(log10) then begin
  for i=0,3 do mom.(i)=10^mom.(i)
endif
return,mom
end
