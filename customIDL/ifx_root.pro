;$Id: //depot/idl/IDL_70/idldir/lib/fx_root.pro#1 $
;
; Copyright (c) 1994-2007, ITT Visual Information Solutions. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;       FX_ROOT
;
; PURPOSE:
;       This function computes real roots (zeros) of a univariate nonlinear
;       function. It can solve an equation with a set of parameters simutaneously.
;
; CATEGORY:
;       Nonlinear Equations/Root Finding
;
; CALLING SEQUENCE:
;       Result = FX_ROOT(X, Func)
;
; INPUTS:
;       X :      A 3-element initial guess vector of type real or complex.
;                Real initial guesses may result in real or complex roots.
;                Complex initial guesses will result in complex roots.
;
;       Func:    A scalar string specifying the name of a user-supplied IDL
;                function that defines the univariate nonlinear function.
;                This function must accept the vector argument X.
;
; KEYWORD PARAMETERS:
;       DOUBLE:  If set to a non-zero value, computations are done in
;                double precision arithmetic.
;
;       ITMAX:   Set this keyword to specify the maximum number of iterations
;                The default is 100.
;
;       STOP:    Set this keyword to specify the stopping criterion used to
;                judge the accuracy of a computed root, r(k).
;                STOP = 0 implements an absolute error criterion between two
;                successively-computed roots, |r(k) - r(k+1)|.
;                STOP = 1 implements a functional error criterion at the
;                current root, |Func(r(k))|. The default is 0.
;
;       tol:     Set this keyword to specify the stopping error tolerance.
;                If the STOP keyword is set to 0, the algorithm stops when
;                |x(k) - x(k+1)| < tol.
;                If the STOP keyword is set to 1, the algorithm stops when
;                |Func(x(k))| < tol. The default is 1.0e-4.
;
; EXAMPLE:
;       Define an IDL function named FUNC.
;         function FUNC, x
;           return, exp(sin(x)^2 + cos(x)^2 - 1) - 1
;         end
;
;       Define a real 3-element initial guess vector.
;         x = [0.0, -!pi/2, !pi]
;
;       Compute a root of the function using double-precision arithmetic.
;         root = FX_ROOT(x, 'FUNC', /double)
;
;       Check the accuracy of the computed root.
;         print, exp(sin(root)^2 + cos(root)^2 - 1) - 1
;
;       Define a complex 3-element initial guess vector.
;         x = [complex(-!pi/3, 0), complex(0, !pi), complex(0, -!pi/6)]
;
;       Compute a root of the function.
;         root = FX_ROOT(x, 'FUNC')
;
;       Check the accuracy of the computed root.
;         print, exp(sin(root)^2 + cos(root)^2 - 1) - 1
;
; PROCEDURE:
;       FX_ROOT implements an optimal Muller's method using complex
;       arithmetic only when necessary.
;
; REFERENCE:
;       Numerical Recipes, The Art of Scientific Computing (Second Edition)
;       Cambridge University Press
;       ISBN 0-521-43108-5
;
; MODIFICATION HISTORY:
;       Written by:  GGS, RSI, March 1994
;       Modified:    GGS, RSI, September 1994
;                    Added support for double-precision complex inputs.
;-

function ifx_root, xii, func, Xtol = Xtol, Ytol=Ytol,lower=lower,$
    	 upper=upper

  on_error, 2 ;Return to caller if error occurs.

  xi = xii + 0.0 ;Create an internal floating-point variable, x.
  sx = size(xi)
  if sx[0] ne 2 then message,'xi must be a (n,3) initial guess array.'
  if sx[2] ne 3 then message, 'xi must be a 3-element initial guess vector in the second dimension.'
  if (sx[3] ne 4)and(sx[3] ne 5)then message,'solve for real root only!!!'
  ;Initialize keyword parameters.
;  if keyword_set(itmax)  eq 0 then itmax = 100
;  if keyword_set(stop)   eq 0 then stop = 0
  if n_elements(Xtol) eq 0 then Xtol=1e-6
  if n_elements(Ytol) eq 0 then Ytol=1e-6
  
  ;Initialize stopping criterion and iteration count.
  status = intarr(sx[1])  & xroot=fltarr(sx[1]) 
  it = 0  & itmax=1000
  
  ;Begin to iteratively compute a root of the nonlinear function.
  sn1=where(status eq 0,n1)
  while n1 ne 0 do begin
    q = (xi[*,2] - xi[*,1])/(xi[*,1] - xi[*,0])
    pls = (1 + q)
    f0 = call_function(func, xi[*,0])
    f1 = call_function(func, xi[*,1])
    f2 = call_function(func, xi[*,2])
    a = q*f2 - q*pls*f1 + q^2*f0
    b = (2*q+1)*f2 - pls^2*f1 + q^2*f0
    c = pls*f2
    disc = b^2 - 4*a*c
    
    sn2=where(disc lt 0,n2)
    if n2 ne 0 then begin
      print,sn2
      message,'bad initial guess for the above equations'
    endif
    rR0 = b + sqrt(disc)
    rR1 = b - sqrt(disc)
    div=rR0
    sn2=where(abs(rR0) lt abs(rR1),n2)
    if n2 ne 0 then div[sn2]=rR1[sn2]     

    xroot[sn1]= xi[sn1,2] - (xi[sn1,2] - xi[sn1,1]) * (2 * c[sn1]/div[sn1])

    sn2=where((status eq 0)and(abs((xroot-xi[*,2])/xi[*,2]) lt Xtol),n2)
    if n2 ne 0 then status[sn2]=1           ;X error tolerance.
    evalFunc = call_function(func, xroot)
    sn2=where((status eq 0)and(abs(evalFunc) lt Ytol),n2)
    if n2 ne 0 then status[sn2]=1          ;Y error tolerance.
    it++
    sn1=where(status eq 0,n1)
    if n1 ne 0 then begin
      if (it gt itmax) then message,'failed to find root'
      xi[sn1,0]=xi[sn1,1]
      xi[sn1,1]=xi[sn1,2]
      xi[sn1,2]=xroot[sn1]
    endif
endwhile
message,'iterations='+string(it),/continue
return, xroot
end
