pro time_consumed,t_start
t_end=systime(/s)
ts=t_end-t_start
ths=['',''] & tms=ths & tds=ths
switch 1 of
  ts ge 86400 :begin
                td=long(ts/86400.)
		ts=ts mod 86400 ;-double(86400)*td
		tds[1]=strtrim(string(td),2)+'d '
               end
  ts ge 3600  :begin
                th=long(ts/3600.)
	        ts=ts mod 3600 ;-3600.*th
	        ths[1]=strtrim(string(th),2)+'h '
	       end
    ts ge 60  :begin
                tm=long(ts/60.)
	        ts=ts mod 60  ;-60.*tm
                tms[1]=strtrim(string(tm),2)+'m '
	       end
        else  : tss=strtrim(string(ts),2)+'s'
endswitch
id=n_elements(td)
ih=n_elements(th)
im=n_elements(tm)
time=tds[id]+ths[ih]+tms[im]+tss
print,'time consumed: ',time
end
