pro testplot
device,filename='test.ps',/color,/encapsul,xsize=10,ysize=16
multiplot_Gan,[1,3],mxtitle='x',myposition=[0.18,0.1,0.98,0.99]
; this figure is plotted in 1 column, 3 rows, see the comment line in multiplot.pro for more details.
x=findgen(100)*0.05
for i=0,2 do begin
  plot,x,sin(x),ytitle='y'+string(i,format='(i1)')
  oplot,x,sin(x+i+1),color=!myct.c3[i]

  multiplot_Gan                
  ;enter "multiplot ..." after each plot, see the comment line in multiplot.pro for more details.
endfor
multiplot_Gan,/default

end
;============================================================
;============================================================

pro test
psfigure,'testplot'
end
