pro circle,x0,y0,R,color=color,thick=thick
x=findgen(101)*2*R/100.+x0-R
y=sqrt(R^2-(x-x0)^2)
oplot,x,y0+y,color=color,thick=thick
oplot,x,y0-y,color=color,thick=thick
oplot,[x0,x0],[y0,y0]
end

