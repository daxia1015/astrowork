function histo_bin,nbin,xmin,xmax,dbin=dbin,log10=log10

if keyword_set(log10) then begin
  extent=alog10(xmax/xmin)
  dbin=extent/(nbin-1)
  locations=findgen(nbin)*dbin+alog10(xmin)
endif else begin
  extent=xmax-xmin
  dbin=extent/(nbin-1)
  locations=findgen(nbin)*dbin+xmin
endelse

return,locations
end

