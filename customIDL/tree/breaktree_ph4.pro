pro breaktree_ph4,fname,totnhalo,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,$
    tree0=tree0,halosn=halosn,tag=tag,alltree=alltree,verbose=verbose
ntag=n_elements(tag) & nhalo=n_elements(halosn) 
if (nhalo ne 0)and(keyword_set(alltree)) then message, $
   'only one of keyword "halosn" or "alltree" can be set.'
if keyword_set(alltree) then nhalo=0

if n_elements(tree0) ne totnhalo then tree0=readtree_ph(fname,/alltree)
      tag=strupcase(tag) ;& tagsn=intarr(ntag)
      for ii=0,ntag-1 do begin
        tagi=tag[ii]
	sn2=where(!Phoenix.Component eq tagi,n2)
	if n2 ne 1 then message,'tag name not match!!!' 
    	if nhalo ne 0 then array=tree0[halosn].(sn2) else array=tree0.(sn2)
        case ii of 
	  0: v0=array
      	  1: v1=array
    	  2: v2=array
    	  3: v3=array
    	  4: v4=array
    	  5: v5=array
    	  6: v6=array
    	  7: v7=array
    	  8: v8=array
    	  9: v9=array
          else: message,'variables not enough!'
        endcase
      endfor
if keyword_set(verbose) then print,'%breaktree4: ',tag,'  have been read out.'
end
