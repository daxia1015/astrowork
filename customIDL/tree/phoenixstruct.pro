;The properties of the first halo in each tree:
;They are identified at snapnum=71 (a=1, z=0) and all have no Descendant.
;Their FirstProgenitor is eithor 1 or -1.
;Their  NextProgenitor all eq -1.
;They all belong to the group of the first halo (haloID=0) in the whole tree,
;i.e., their FirstHaloInFOFgroup all equal 0.
;==================================================================
;Any pointer of any halo point to -1 or the halo lie in the first tree.
;==================================================================
;All trees are not self-contained except the first tree.
;In the not self-contained tree, the FirstProgenitor pointers either equal
; -1 or lie in the first tree. The FirstHaloInFOFgroup pointers all 
;lie in the first tree.
;==================================================================
;The halos beyond the first tree mainly locate at the outside region
;of the cluster (>3R_200). And they are mainly low mass halo (<0.05M_200).
;==================================================================
;==================================================================
;==================================================================
pro phoenixstruct

defsysv,'!Phoenix',exists=en
if en then return
;----define a set of rewritable systme variable

;defsysv,'!n_snap',72,1
    st = { $
    Descendant           : 0L, $   ;The haloId of the unique descendant of this subhalo in the merger tree. -1 if there is no descendant.
    FirstProgenitor      : 0L, $   ;The ID of the main progenitor of this subhalo. most massive
    NextProgenitor       : 0L, $   ;The ID of the "next progenitor" in the subhalo merger trees (the "next progenitor" of a subhalo S is the next most massive subhalo that has the same descendant as subhalo S).
    FirstHaloInFOFgroup  : 0L, $   ;ID of the dominant subhalo of the friend-of-friends group to which this subhalo belongs. most massive
    NextHaloInFOFgroup   : 0L, $   ;Id of the next most massive halo within the same FOF group.
    Len                  : 0L, $    ;Number of simulation particles in this halo.
    M_Mean200            : 0.0, $   ;10^10M_sun/h, The mass within the radius where the halo has an overdensity 200 times the mean density of the simulation. NB this value is only defined for halos with haloId=firstHaloInFOFgroup.
    M200                 : 0.0, $   ;10^10M_sun/h, The mass within the radius where the halo has an overdensity 200 times the critical density of the simulation. NB this value is only defined for halos with haloId=firstHaloInFOFgroup.
    M_TopHat             : 0.0, $   ;10^10M_sun/h, The mass within the radius where the halo has an overdensity corresponding to the value at virialisation in the top-hat collapse model for this cosmology. NB this value is only defined for halos with haloId=firstHaloInFOFgroup.
    Pos                  : fltarr(3), $   ;Mpc/h, position in comoving coordinates, Cartesian.
    Vel                  : fltarr(3), $   ;km/s, peculiar velocity
    VelDisp              : 0.0, $  ;km/s, The 1-D velocity dispersion of the subhalo, computed from all of the subhalo particles.
    Vmax                 : 0.0, $  ;km/s, Maximum rotational velocity, calculates as the maximum of the expression sqrt(G M(r)/r) where r runs of the particles in the halo. 
    Spin                 : fltarr(3), $   ;(Mpc/h)*(km/s), the spin of the halo. 
    MostBoundID          : 0LL, $  ;The id of the most bound particle of this suhalo  
    SnapNum              : 0L, $     ;The snapshot number where this subhalo was identified. This column corresponds to the snapnum column in the Snapshots table in MField. 
    FileNr               : 0L, $  ;Original file number in which the halo was defined.
    SubhaloIndex         : 0L, $  ;Index of this subhalo in the file identified by fileNr.
    HalfMassRadius       : 0.0 }  ;Mpc/h
;defsysv,'!PhoenixComponent',tag_names(!PhoenixStruct),1
;b1=intarr(9)+4 & b2=intarr(4)+4
;defsysv,'!tagsize',[b1,12,12,4,4,12,8,b2],1  ;help,!tagsize  & print,total(!tagsize)		    
datapath=getenv('HOME')+'/data/phoenix_tree/'

fname=file_search(datapath+'ph?-?',count=nsim)
fname=fname+'/trees_sf1_071.0'

p1=strpos(fname[0],'pha')
aa=strmid(fname,p1,5)
remchar,aa,'-'
;tab=PhoenixTable(datapath)
iv2=[1,indgen(8)*2+4]
iv4=indgen(9)*2+3
isimv=[[iv2],[iv4]]

defsysv,'!Phoenix',{Path    	: datapath,$
    	    	    n_snap      : 72,$
		    nsim    	: nsim,$
		    isimv	: isimv,$
    	    	    Struct      : st,$
		    Component   : tag_names(st),$
		    TreeFile    : fname,$
		    TreeName    : aa,$
		    unitM	: 1e10},1

end
