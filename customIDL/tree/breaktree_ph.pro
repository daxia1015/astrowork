;Be sure to 
;close,/all
;after calling this procedure.
pro breaktree_ph,fname,tree,totnhalo,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,$
    tree0=tree0,halosn=halosn,tag=tag,alltree=alltree,verbose=verbose
p1=strpos(fname,'/trees_sf1_071.0')		     
if strmid(fname,p1-1,1) eq '4' then begin
  breaktree_ph4,fname,totnhalo,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,tree0=tree0,$
    	     halosn=halosn,tag=tag,alltree=alltree,verbose=verbose
  return
endif
nhalo=n_elements(halosn) 	     
nhalo0=nhalo
if (nhalo ne 0)and(keyword_set(alltree)) then message, $
   'only one of keyword "halosn" or "alltree" can be set.'
if (nhalo eq 0)or(keyword_set(alltree)) then nhalo=totnhalo


      ntag=n_elements(tag) 
      tag=strupcase(tag) & tagsn=intarr(ntag)
      for ii=0,ntag-1 do begin
        tagi=tag[ii]
	sn2=where(!Phoenix.Component eq tagi,n2)
	if n2 ne 1 then message,'tag name not match!!!' 
	tagsn[ii]=sn2       
        n3=n_elements(!Phoenix.Struct.(sn2))
        if n3 eq 1 then array=replicate(!Phoenix.Struct.(sn2),nhalo) else array=rebin(!Phoenix.Struct.(sn2),n3,nhalo)
        case ii of 
	  0: v0=array
      	  1: v1=array
    	  2: v2=array
    	  3: v3=array
    	  4: v4=array
    	  5: v5=array
    	  6: v6=array
    	  7: v7=array
    	  8: v8=array
    	  9: v9=array
          else: message,'variables not enough!'
        endcase
      endfor

for k=0L,nhalo-1 do begin
  if  nhalo0 eq 0 then i=k else i=halosn[k]
  haloi=tree[i]
  for ii=0,ntag-1 do begin
    itag=tagsn[ii]
    n3=n_elements(!Phoenix.Struct.(itag))
    case ii of
      0: if n3 eq 1 then v0[k]=haloi.(itag) else v0[*,k]=haloi.(itag)
      1: if n3 eq 1 then v1[k]=haloi.(itag) else v1[*,k]=haloi.(itag)
      2: if n3 eq 1 then v2[k]=haloi.(itag) else v2[*,k]=haloi.(itag)
      3: if n3 eq 1 then v3[k]=haloi.(itag) else v3[*,k]=haloi.(itag)
      4: if n3 eq 1 then v4[k]=haloi.(itag) else v4[*,k]=haloi.(itag)
      5: if n3 eq 1 then v5[k]=haloi.(itag) else v5[*,k]=haloi.(itag)
      6: if n3 eq 1 then v6[k]=haloi.(itag) else v6[*,k]=haloi.(itag)
      7: if n3 eq 1 then v7[k]=haloi.(itag) else v7[*,k]=haloi.(itag)
      8: if n3 eq 1 then v8[k]=haloi.(itag) else v8[*,k]=haloi.(itag)
      9: if n3 eq 1 then v9[k]=haloi.(itag) else v9[*,k]=haloi.(itag)
    endcase
  endfor
endfor
if keyword_set(verbose) then print,'%breaktree: ',tag,'  have been read out.'
end


