function luminosityPosition,pos0,SnapNum,zlist
pos=pos0
for i=0,!Phoenix.n_snap-2 do begin
  sn=where(SnapNum eq i,n)
  if n eq 0 then continue
  pos[*,sn]=pos0[*,sn]/(1+zlist[i])
endfor
return,pos
end
