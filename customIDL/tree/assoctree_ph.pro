function assoctree_ph,fname,lunx,ntree,totnhalo,nhalos
@readhead_ph.pro
head=8L+4L*ntree
tree=assoc(lunx,!Phoenix.Struct,head)
return,tree
end
