function snaplist,redshift=redshift
fname=!Phoenix.Path+'outputlist.txt'
n_snap=!Phoenix.n_snap
alist=dblarr(n_snap)
openr,lun,fname,/get_lun
readf,lun,alist,format='(d16)'
free_lun,lun
;if not(keyword_set(double)) then alist=float(alist)
;print,alist
if keyword_set(redshift) then return, 1.0/alist-1
return,alist
end
