;The properties of the first halo in each tree:
;They are identified at snapnum=71 (a=1, z=0) and all have no Descendant.
;Their FirstProgenitor is eithor 1 or -1.
;Their  NextProgenitor all eq -1.
;They all belong to the group of the first halo (haloID=0) in the whole tree,
;i.e., their FirstHaloInFOFgroup all equal 0.
;==================================================================
;Any pointer of any halo point to -1 or the halo lie in the first tree.
;==================================================================
;All trees are not self-contained except the first tree.
;In the not self-contained tree, the FirstProgenitor pointers either equal
; -1 or lie in the first tree. The FirstHaloInFOFgroup pointers all 
;lie in the first tree.

;==================================================================
function readtree_ph,fname,ntree,totnhalo,nhalos,alltree=alltree,itree=itree,$
    	    	     nline=nline,skip_nhalo=skip_nhalo,readhead=readhead,$
		     verbose=verbose
@readhead_ph.pro
if keyword_set(readhead) then begin
  free_lun,lunx
  return,0
endif

if keyword_set(alltree) then begin
  tree=replicate(!Phoenix.Struct,totnhalo)
  readu,lunx,tree
endif else begin
  if n_elements(nline) ne 0 then begin
    if n_elements(skip_nhalo) eq 0 then message,'error'
    Tree=replicate(!Phoenix.Struct, nline);every halo's formation in the tree
  endif

  if n_elements(itree) ne 0 then begin
    if itree eq 0 then skip_nhalo=0 else skip_nhalo=total(nhalos[0:itree-1],/int)
    Tree=replicate(!Phoenix.Struct, Nhalos[itree] );every halo's formation in the tree
  endif

  skip=8L+4L*ntree+104L*skip_nhalo
  point_lun,lunx,skip  
  readu,lunx,Tree
endelse

if eof(lunx) and keyword_set(verbose) then message,'end of '+fname,/continue
free_lun,lunx
return,tree
end
