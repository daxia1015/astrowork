;read out the table data in Gao et al. (2012) 2012MNRAS.425.2169G.pdf
pro PhoenixTable
defsysv,'!PhoenixTable',exists=en
if en then return

datapath=!Phoenix.Path
readcol,datapath+'Gao2012_Table1.txt',name,mp,M200,R200,$
    	comment='#',format='a,f,f,f'
readcol,datapath+'Gao2012_Table2.txt',zf,cE,c,alpha,$
    	comment='#',format='x,x,x,f,f,f,x,x,f'

remchar,name,'-'
table={name    : name,$
       mp      : mp,$
       M200    : m200,$
       R200    : R200,$
       zf      : zf,$
       cE      : cE,$
       cNFW    : c,$
       alpha   : alpha}
defsysv,'!PhoenixTable',Table,1

end
