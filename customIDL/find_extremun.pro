pro get_extremun,yps,tm,ym,tmin,tmax,ymin,ymax
if yps lt 0 then begin
  tmax=[tmax,tm]
  ymax=[ymax,ym]
endif else begin
  tmin=[tmin,tm]
  ymin=[ymin,ym]
endelse
end
;--==============================================
;================================================
pro find_extremun,t,y,yp,tmin=tmin,tmax=tmax,$
                  ymin=ymin,ymax=ymax
n=n_elements(t) ;& index=0
tmin=0. & tmax=0. & ymin=0. & ymax=0.

sn=where(yp eq 0,n0)
if n0 ne 0 then begin
  for j=0,n0-1 do begin
    i2=sn[j]
    tm=t[i2]
    ym=t[i2]
    get_extremun,yp[i2+1],tm,ym,tmin,tmax,ymin,ymax  
  endfor
endif

i2=0L
while i2 lt n-1 do begin
  sn2=where(sn eq i2,n2)
  if n2 ne 0 then begin
    i2++
    continue
  endif
  pm=yp[i2]*yp[i2+1:n-1]
  sn1=where(pm lt 0,n1)
  if n1 ne 0 then begin 
    i2=sn1[0]
    i1=i2-1
    tm=(t[i2]+t[i1])/2
    ym=(t[i2]+t[i1])/2
    get_extremun,yp[i2],tm,ym,tmin,tmax,ymin,ymax
  endif else break
endwhile
n0=n_elements(tmin)
if n0 gt 1 then begin
  tmin=tmin[1:n0-1]
  ymin=ymin[1:n0-1]
  sn=sort(tmin)
  tmin=tmin[sn]
  ymin=ymin[sn]
endif else print,'no minimum found'
n0=n_elements(tmax)
if n0 gt 1 then begin
  tmax=tmax[1:n0-1]
  ymax=ymax[1:n0-1]
  sn=sort(tmax)
  tmax=tmax[sn]
  ymax=ymax[sn]
endif else print,'no maximum found'
end

