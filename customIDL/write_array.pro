pro write_array,array,fname,format=fmt
openw,lun1,fname,/get_lun
  if n_elements(fmt) eq 0 then begin
    writeu,lun1,array
  endif else printf,lun1,array,format=fmt
free_lun,lun1
end
