pro multitick,pmulti,xtick0,ytick0,xtick,ytick,rowmajor=rowmajor

case n_elements(pmulti) of
  2:begin
    n_col=pmulti[0]
    n_row=pmulti[1]
    colmajor = not keyword_set(rowmajor) 
  end
  5:begin
    n_col=pmulti[1]
    n_row=pmulti[2]
    if keyword_set(rowmajor) then begin
      colmajor = 0 
    endif else begin
      colmajor = pmulti[4] eq 0
    endelse
  end
  else: message,'pmulti can only have 2 or 5 elements.'
endcase

nbox=n_col*n_row
 
nx=n_elements(xtick0)
ny=n_elements(ytick0)


if colmajor then begin
  xtick1=replicate(' ',nx*(nbox-n_col))
  xtick2=xtick0
  for i=1,n_col-1 do xtick2=[xtick2,xtick0]
  xtick2=[xtick1,xtick2]

  ytick1=[ytick0,replicate(' ',ny*(n_col-1))]
  ytick2=ytick1
  for i=1,n_row-1 do ytick2=[ytick2,ytick1]
endif else begin
  xtick1=[replicate(' ',nx*(n_row-1)),xtick0]
  xtick2=xtick1
  for i=1,n_col-1 do xtick2=[xtick2,xtick1]
  
  ytick1=ytick0
  for i=1,n_row-1 do ytick1=[ytick1,ytick0]
  ytick2=replicate(' ',ny*(nbox-n_row))
  ytick2=[ytick1,ytick2]
endelse
xtick=reform(xtick2,nx,nbox)
ytick=reform(ytick2,ny,nbox)

end
