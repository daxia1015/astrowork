function root_eq,x
common root_block,c
return,x^2-c
end
;====================================================================
;====================================================================
function root_eq_p,x
return,2*x
end
;====================================================================
;====================================================================
pro test_root
common root_block,c
!except=2
n=100
c=findgen(n)+1
xi=fltarr(n)+1
x1=inewton(xi,'root_eq','root_eq_p')
xi=findgen(n,3)
x2=ifx_root(xi,'root_eq')

for i=0,n-1 do print,c[i],x1[i],x2[i]






end

