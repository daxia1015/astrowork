;available for idl windows plotting
;example of usage:
; 
; colors_tv
; x=findgen(100)*0.05
; plot,x,sin(x),color=!tvct.black,background=!tvct.white
;---------------------------------------------------------
;
pro colors_tv
device,decomposed=0

defsysv,'!tvct',EXISTS = en
if en then return

defsysv,'!tvct',{black     : 0, $
                 white     : 1, $
                 red       : 2, $
                 green     : 3, $
                 blue      : 4, $
                 yellow    : 5, $
                 magenta   : 6, $
                 cyan      : 7, $
                 orange    : 8, $
                 ltgreen   : 9, $
                 violet    : 10,$
                 turquoise : 11,$
                 ltblue    : 12,$
                 ltred     : 13 $
                }
;        0   1    2   3   4   5   6   7   8   9  10  11  12  13
tvlct,[  0,255, 255,  0,  0,255,255,  0,255,125,125,  0,  0,255],$ ;red
      [  0,255,   0,255,  0,255,  0,255,125,255,  0,255,125,  0],$ ;green
      [  0,255,   0,  0,255,  0,255,255,  0,  0,255,125,255,125]   ;blue
end
