pro shadowing,x,y1,y2,density=density,char=char,pos=pos,$
    shadeColor=shadeColor,_extra=_extra
n=n_elements(x)
if(n_elements(y1) ne n)or(n_elements(y2) ne n)then $
  message,'x, y1, y2 should have equal elements'
oplot,x,y1,_extra=_extra
oplot,x,y2,_extra=_extra

xp=x & y1p=y1 & y2p=y2
if n_elements(density) eq 0 then density=1 
nline=(n-1)*density+1
if nline ne n then begin
    nexpand=findgen(nline)/density
    xp=interpolate(x,nexpand)
    y1p=interpolate(y1,nexpand)
    y2p=interpolate(y2,nexpand)
endif
;help,x,y1,y2,nline
for i=0,nline-1 do oplot,[xp[i],xp[i]],[y1p[i],y2p[i]],_extra=_extra

if n_elements(shadeColor) ne 0 then begin
  if n_elements(density) eq 0 then density=5
  dy=(y2-y1)/(density-1)
  xc=rebin(x,density*n)
  yc=xc
  for i=0,n-1 do yc[i*density:(i+1)*density-1]=findgen(density)*dy[i]+y1[i]
  zc=fltarr(density*n)
  Contour,zc,xc,yc,/Fill,/irregular,C_Colors=shadeColor,NLevels=1,/overplot
endif

if (n_elements(char) ne 0)and(n_elements(pos) eq 2) then begin
  xyouts,pos[0],pos[1],char,_extra=_extra
endif

end
