function create_double_struct,inner,outter,func,_extra=_extra
n_in=n_elements(inner)
n_out=n_elements(outter)
for i=0,n_out-1 do begin
  for j=0,n_in do begin
    stij=call_function(func,i,j,_extra=_extra)
    if n_in eq 1 then sti=stij else begin
      if j eq 0 then sti=create_struct(inner[j],stij) else $
        sti=create_struct(sti,inner[j],stij)
    endelse
  endfor
  if n_out eq 1 then st=sti else begin
    if i eq 0 then st=create_struct(outter[i],sti) else $
      st=create_struct(st,outter[i],sti)
  endelse
endfor
return,st
end
