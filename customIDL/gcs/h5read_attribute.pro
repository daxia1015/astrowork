;This function read out the attributes of id.
;id: the identifier of a group or dataset;
;name: a return variable contains the name of the attributes
;n: a return variable contains the number of the attributes
;attrs: the results is an idl structure, in which the tag name is the 
;   	attribute name and the tag value is the attribute value

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
   	

function h5read_attribute,id,name,n
n=h5a_get_num_attrs(id)
if n eq 0 then message,'No attribute in this group/dataset.'
name=strarr(n)
for i=0,n-1 do begin
  aid=h5a_open_idx(id,i)
  name[i]=h5a_get_name(aid)
  value=h5a_read(aid)
  tag=tagname(name[i])
  if i eq 0 then attrs=create_struct(tag,value) else $
    attrs=create_struct(attrs,tag,value)
  h5a_close,aid
endfor
return,attrs
end
