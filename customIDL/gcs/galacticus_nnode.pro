;This function read out the number of nodes at each output in a Galacticus output file.
;file: the file name (include the path) or idenfier of a Galacticus output file;
;outputName:  the output group to be read; if not present, all group will be read
;n_node: the result is an idl array

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012



function Galacticus_nNode,file,outputName=outputName
;if n_elements(file) ne 1 then message,'error'
type=size(file,/type)
if type eq 7 then fid=h5f_open(file) else fid=file
nout=n_elements(outputName)
if nout eq 0 then OutputName=h5read_MemberName(fid,'Outputs',nout,/all)

Name='Outputs/'+outputName+'/nodeData/nodeMass'
n_node=lonarr(nout) 
for iout=0,nout-1 do begin
  did=h5d_open(fid,name[iout])
  sid=h5d_get_space(did)
  n_node[iout]=H5S_GET_SIMPLE_EXTENT_NPOINTS(sid)
  h5s_close,sid
  h5d_close,did
endfor
if type eq 7 then h5f_close,fid
return,n_node
end
;====================================================================
;====================================================================
