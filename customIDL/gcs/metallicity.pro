;This function return the metallicity of a consider object.
;metalMass: the mass of the metal
;baseMass: the mass of the stellar or gas
;FeoverH: if set, return [Fe/H]
;OoverH:  if set, return [O/H]
;p12OoverH: if set, return 12+[O/H]


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
 
   	
function metallicity,metalMass,baseMass,FeoverH=FeoverH,OoverH=OoverH,$
    	    	     p12OoverH=p12OoverH
solarMetalAbundance  = 0.0188;
solarOxygenAbundance = 4.8977e-4;

metalAbundance=metalMass/baseMass
lgFeoverH=alog10(metalAbundance/solarMetalAbundance)
if keyword_set(FeoverH)   then return,lgFeoverH
if keyword_set(OoverH)    then return,lgFeoverH+alog10(solarOxygenAbundance)
if keyword_set(p12OoverH) then return,12+lgFeoverH+alog10(solarOxygenAbundance)
end
