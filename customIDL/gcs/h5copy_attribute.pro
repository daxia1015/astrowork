;This procedure copy the attributes from id1 to id0.
;id0: an integer pointing to the destination of dataset or group
;id1: an integer pointing to the source of dataset or group

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


pro h5copy_attribute,id0,id1
n=h5a_get_num_attrs(id1)
;help,n
for i=0,n-1 do begin
  aid1=h5a_open_idx(id1,i)
  name=h5a_get_name(aid1)
  value=h5a_read(aid1)
;  help,name,value
  h5write_attribute,id0,name,value
  h5a_close,aid1
endfor
end
