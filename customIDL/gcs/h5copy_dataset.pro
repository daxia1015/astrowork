;This procedure copy the specific dataset and its attributes from fid1 to fid0.
;fid0: an integer pointing to the destination hdf5 file
;fid0: an integer pointing to the source hdf5 file
;name: the name of dataset to be copied
;GroupName: the path to the dataset in the hdf5 file. if not present, 
;   	    it is the root path.

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


pro h5copy_dataset,fid0,fid1,Name,GroupName=GroupName

did1=H5D_OPEN(fid1,GroupName+'/'+Name) 
data=h5d_read(did1)

h5write_dataset,fid0,data,Name,GroupName=GroupName

did0=H5D_OPEN(fid0,GroupName+'/'+Name) 
h5copy_attribute,did0,did1


end


