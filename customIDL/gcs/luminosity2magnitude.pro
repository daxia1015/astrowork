;This function transfer the luminosity to the magnitude.
;lumi: the luminosity in the AB-Magnitude system;
;mag: the result

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
   	


function Luminosity2Magnitude,lumi;,filter
mag=lumi
mag[*]=1000.0
sn=where(lumi gt 0)
mag[sn]=-2.5*alog10(lumi[sn])
return,mag
end
