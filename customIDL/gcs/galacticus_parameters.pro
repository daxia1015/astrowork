;This function read out the attributes in the group "Parameters" in a Galacticus output file.
;fname: the file name (include the path) of the Galacticus output file;
;paraName: a return array containning the name of the parameters
;npara:  the number of the parameters
;para: the results is an idl structure, in which the tag name is the 
;      parameter name and the tag value is the parameter value

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


function Galacticus_Parameters,fname,paraName,npara
GroupRoot='Parameters'
fid=h5f_open(fname)
gid=h5g_open(fid,GroupRoot)
para=h5read_attribute(gid,paraName,npara)
h5g_close,gid
h5f_close,fid
return,para
end
