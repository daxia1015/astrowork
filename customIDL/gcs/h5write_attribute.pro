;This procedure write an attirbute to loc_id.
;loc_id: the identifier of a group or dataset
;name: the attribute name
;data: the attribute value 

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
;Modifications:  Jianling Gan, 07.Aug.2013


pro h5write_attribute,loc_id,data,name
n=n_elements(name)
type=size(data,/type)
if type eq 8 then begin
  n1=n_tags(data)
  if n1 ne n then message,'The number of name and data do NOT match.'
  for i=0,n-1 do begin
    datatype_id=h5t_idl_create(data.(i))
    dataspace_id=h5s_create_scalar()
    attr_id=h5a_create(loc_id,name[i],datatype_id,dataspace_id)
    h5a_write,attr_id,data.(i)
    h5t_close,datatype_id
    h5s_close,dataspace_id
    H5A_CLOSE,attr_id 
  endfor
endif else begin
  n1=n_elements(data)
  if n1 ne n then message,'The number of name and data do NOT match.'
  for i=0,n-1 do begin
    datatype_id=h5t_idl_create(data[i])
    dataspace_id=h5s_create_scalar()
    attr_id=h5a_create(loc_id,name[i],datatype_id,dataspace_id)
    h5a_write,attr_id,data[i]
    h5t_close,datatype_id
    h5s_close,dataspace_id
    H5A_CLOSE,attr_id 
  endfor
endelse

end
