;This function read out the data in the group "Outputs/Output*/nodeData/" in a Galacticus output file.
;fid: the identifier of a Galacticus output file;
;outputName: the name of the output group to be read
;dataName: the name list of the data to be read
;dataIndex: the index list of the data to be read, only dataIndex or dataName is required.
;allData: if set, read out all the node data in the group specified by outputName
;readpos: if set, convert the result to a (3,n_node) array, and in this case,
;    	  dataName must have 3 elements
;sum:     if set, find the sum of the node data and the result is a (n_node) array.
;data:  the result is an idl structure, in which the tag name is the data name 
;   	and the tag value is the node data.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 30.Jun.2012




function NodeData_Read,fid,outputName,dataname=dataname,$
         dataIndex=dataIndex,allData=allData,readpos=readpos,$
	 sum=sum
GroupName='Outputs/'+outputName+'/nodeData'
nIndex=n_elements(dataIndex)
if nIndex ne 0 then dataname=h5read_MemberName(fid,GroupName,index=dataIndex)
if keyword_set(allData) then dataname=h5read_MemberName(fid,GroupName,/all)
;print,dataName
data=h5read_dataset(fid,dataName,GroupName=GroupName)
if keyword_set(readpos) then begin
  if n_elements(dataname) ne 3 then message,'if /readpos set, dataName must have 3 elements.'
;  s0=size(data.(0))
;  if n_elements(s0) ne 4 then message,'error'
  pos=make_array(3,n_elements(data.(0)),type=size(data.(0),/type))
  for i=0,2 do pos[i,*]=data.(i)
  data=pos
endif

ndata=n_elements(dataName)
if keyword_set(sum)and(ndata gt 1) then begin
  data1=0.
  for i=0,ndata-1 do data1=data1+data.(i)
;  help,data1,data.(0)
  data=data1
endif
return,data
end
;====================================================================
;====================================================================
