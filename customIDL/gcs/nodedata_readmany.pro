;==================================================================
;==================================================================
function nodeData_ReadMany,fid,outputName,Name0,Mstar=Mstar,SFR=SFR,$
    	 pos=pos,velocity=velocity,MagName=MagName,ColorName=ColorName,$
	 metalName=metalName,lumiName=lumiName,_extra=_extra

nodeData=NodeData_Read(fid,outputName,dataname=Name0)
if n_elements(Name0) eq 1 then data=create_struct(Name0,nodeData) else data=nodeData

if keyword_set(Mstar) then begin
  Name=['diskStellarMass','spheroidStellarMass']
  nodedata=NodeData_Read(fid,outputName,dataname=Name,/sum)
  data=create_struct('Mstar',nodedata,data) 
endif
if keyword_set(SFR) then begin
  Name=['diskStarFormationRate','spheroidStarFormationRate']
  nodedata=NodeData_Read(fid,outputName,dataname=Name,/sum)
  data=create_struct('SFR',nodedata,data) 
endif
if keyword_set(pos) then begin
  Name=['positionX','positionY','positionZ']
  nodeData=NodeData_Read(fid,outputName,dataname=Name,/readpos)
  data=create_struct('pos',nodeData,data)    
endif
if keyword_set(velocity) then begin
  Name=['velocityX','velocityY','velocityZ']
  nodeData=NodeData_Read(fid,outputName,dataname=Name,/readpos)
  data=create_struct('vel',nodeData,data)     
endif
if n_elements(MagName) ne 0 then begin
  nodeData=nodeData_Magnitude(fid,outputName,MagName)
  data=create_struct('Mag',nodeData,data)     
endif
if n_elements(ColorName) ne 0 then begin
  nodeData=nodeData_Color(fid,outputName,colorName)
  data=create_struct('ct',nodeData,data)     
endif
if n_elements(metalName) ne 0 then begin
  nodeData=nodeData_Metal(fid,outputName,metalName,_extra=_extra)
  data=create_struct('metal',nodeData,data)     
endif  
if n_elements(lumiName) ne 0 then begin
  nodeData=nodeData_Luminosity(fid,outputName,lumiName)
  data=create_struct('lumi',nodeData,data)     
  
endif
return,data
end
;==================================================================
;==================================================================
