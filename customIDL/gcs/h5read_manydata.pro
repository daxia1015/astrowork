pro h5read_manydata,file,dataName,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,$
    	    	    groupName=groupName
type=size(file,/type)
if type eq 7 then fid=h5f_open(file) else fid=file
if n_elements(groupName) ne 0 then gid=h5g_open(fid,groupName) else gid=fid

nd=n_elements(dataName)
for i=0,nd-1 do begin
  case i of
    0 :v0=h5read_dataset(gid,dataName[i])
    1 :v1=h5read_dataset(gid,dataName[i])
    2 :v2=h5read_dataset(gid,dataName[i])
    3 :v3=h5read_dataset(gid,dataName[i])
    4 :v4=h5read_dataset(gid,dataName[i])
    5 :v5=h5read_dataset(gid,dataName[i])
    6 :v6=h5read_dataset(gid,dataName[i])
    7 :v7=h5read_dataset(gid,dataName[i])
    8 :v8=h5read_dataset(gid,dataName[i])
    9 :v9=h5read_dataset(gid,dataName[i])
    else:message,'variable is NOT enough!'
  endcase
endfor

if n_elements(groupName) ne 0 then h5g_close,gid
if type eq 7 then h5f_close,fid

end
