;This function read out a set of data in fileid
;fileid: the identifier of a hdf5 file
;DataSetname: an array contains the name of the dataset to be read
;GroupName: the path to the dataset in the hdf5 file. if not present, 
;   	    it is the root path.

;out: the results is an idl structure, in which the tag name is the 
;   	data name and the tag value is the data value

;Author: Jianling Gan
;Modifications: Jianling Gan, 19.Jun.2012
   	

function h5read_dataset,fileid,DataSetName,GroupName=GroupName
nset=n_elements(DataSetName)
if nset eq 0 then message,'DataSetName must have one or more elements.'
if n_elements(GroupName) ne 0 then name=GroupName+'/'+DataSetName else name=DataSetName
;print,'nset=',nset
;print,name
for iset=0,nset-1 do begin
;  print,'iset=',iset
  DataSetID=H5D_OPEN(fileid,name[iset]) 
  a=H5D_READ(DataSetID) 
  dataTag=tagName(DataSetName[iset])
  if nset eq 1 then out=a else $
     if iset eq 0 then out=create_struct(dataTag,a) else $
        out=create_struct(out,dataTag,a)
  h5d_close,DataSetID
endfor
return,out
end
