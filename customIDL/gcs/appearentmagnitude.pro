;This function transfer the absolute magnitude to the appearent magnitude.
;mag0: the absolute magnitude
;d: the distance of the considered object, in unit of Mpc
;z: the redshift of the considered object
;mag: the result


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
   	

function AppearentMagnitude,mag0,d,z
offset=-2.5*alog10(1.+z)
;mag=mag0+5*alog10(d)-5+offset           ;d: pc
;mag=mag0+5*alog10(d)+10+offset     	 ;d: kpc
mag=mag0+5*alog10(d)+25+offset           ;d: Mpc
return,mag
end
