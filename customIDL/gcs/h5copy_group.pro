;This procedure copy the specific group and its attributes from fid1 to fid0.
;fid0: an integer pointing to the destination hdf5 file
;fid0: an integer pointing to the source hdf5 file
;name: the name of group to be copied
;GroupName: the path to the group in the hdf5 file. if not present, 
;   	    it is the root path.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012



pro h5copy_group,fid0,fid1,Name,GroupName=GroupName
if n_elements(GroupName) ne 0 then begin
  id0=h5g_open(fid0,GroupName)
  id1=h5g_open(fid1,GroupName)
endif else begin
  id0=fid0
  id1=fid1
endelse

gid0=h5g_create(id0,Name)
gid1=h5g_open  (id1,Name)

h5copy_attribute,gid0,gid1

if n_elements(GroupName) ne 0 then begin
  h5g_close,id0
  h5g_close,id1
endif
end
