
;====================================================================
;====================================================================
function nodeData_Magnitude,fid,outputName,MagName
nM=n_elements(MagName)

for iM=0,nM-1 do begin
  dataname=['diskStellarLuminosity:'+MagName[iM]+':rest:z0.0000',$
    	   'spheroidStellarLuminosity:'+MagName[iM]+':rest:z0.0000']
  L1=NodeData_Read(fid,outputName,dataName=dataName,/sum)

;  L1=nodeData.(iM)+nodeData.(iM+nM)
  magi=luminosity2Magnitude(L1)
  if nM eq 1 then Mag=Magi else begin
    tag=tagName(MagName[im])
    if iM eq 0 then Mag=create_struct(tag,Magi) else $
    	Mag=create_struct(Mag,tag,Magi)
  endelse
endfor
return,Mag
end
