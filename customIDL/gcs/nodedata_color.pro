;====================================================================
;====================================================================
function nodeData_Color,fid,outputName,ColorName
nc=n_elements(colorName)
;if nc ne 1 then message,'only read one color for one time'
pm=strpos(colorName,'-') & len=strlen(colorName)
M1=strmid(ColorName,0,pm) & M2=strmid(ColorName,pm+1,len-pm-1)
for ic=0,nc-1 do begin
  Mag1=nodeData_Magnitude(fid,outputName,M1[ic])
  Mag2=nodeData_Magnitude(fid,outputName,M2[ic])
  cti=Mag1-Mag2
  sn=where((Mag1 eq 1000)or(Mag2 eq 1000))
  cti[sn]=1000
  if nc eq 1 then ct=cti else begin
    tag=tagName(colorName[ic])
    if ic eq 0 then ct=create_struct(tag,cti) else $
    	ct=create_struct(ct,tag,cti)
  endelse
endfor
return,ct
end
;==================================================================
;==================================================================
