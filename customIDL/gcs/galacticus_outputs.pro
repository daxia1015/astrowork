;This procedure read out the data in the group "Outputs" in a Galacticus output file.
;fname: the file name (include the path) of a Galacticus output file;
;outIndex: the index of the output group to be read (beginning with 0)
;outputName: the name of the output group to be read, only outIndex or outputName is required
;info: if present, read out the merger tree info; the result is an idl structure,
;      in which the tag name is the merger tree info name and the tag value
;      is the merger tree info

;nodeData: if present, read out the node data; the result is an idl structure,
;   	   in which the tag name is the data name and the tag value is the node data.

;dataName: the name list of the node data to be read
;dataIndex: the index list of the node data to be read, only dataIndex or dataName is required.
;allData: if set, read out all the node data in the specified group
;readpos: if set, convert the node data to a (3,n_node) array, and in this case,
;    	  dataName must have 3 elements
;sum:     if set, find the sum of the node data and the result is a (n_node) array.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 30.Jun.2012



pro Galacticus_Outputs,fname,outIndex=outIndex,outputName=outputName,$
    info=info,nodeData=nodeData,dataname=dataname,dataIndex=dataIndex,$
    allData=allData,readpos=readpos,sum=sum
GroupRoot='Outputs'
fid=h5f_open(fname)
nIndex=n_elements(outIndex)
nName=n_elements(outputName)
if(nIndex ne 0)and(nName ne 0)then message,'only outIndex or outputName is required!'
if nIndex ne 0 then outputName=h5read_MemberName(fid,GroupRoot,n_out,index=outIndex) 
if nName ne 0 then n_out=nName
if(nIndex eq 0)and(nName eq 0) then outputName=h5read_MemberName(fid,GroupRoot,n_out,index=0)

for i=0,n_out-1 do begin
  if arg_present(info) then begin
    info1=MergerTreeInfo(fid,outputName[i])
    if n_out eq 1 then info=info1 else begin
      if i eq 0 then info=create_struct(outputName[i],info1) else $
        info=create_struct(info,outputName[i],info1)
    endelse
  endif
  
  if arg_present(nodeData) then begin
    data1=NodeData_Read(fid,outputName[i],dataname=dataname,dataIndex=dataIndex,$
    	    	    	allData=allData,readpos=readpos,sum=sum)
    if n_out eq 1 then nodeData=data1 else begin
      if i eq 0 then nodeData=create_struct(outputName[i],data1) $
        else nodeData=create_struct(nodeData,outputName[i],data1)
    endelse 
  endif
endfor
h5f_close,fid
end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================

