;This procedure create a set of recursive groups in fid.
;fid: the identifier of a hdf5 file or group
;v0,v1,v2,v3,v4,v5: the array contains the name of the recursive group,
;    	    	    if no array is present, nothing to be done.
;GroupName: the path to the group in fid, if not present, it is the root path.
;gzip: the compression level, the default value is 9.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


pro h5write_group,fid,v0,v1,v2,v3,v4,v5,GroupName=GroupName
if n_elements(GroupName) ne 0 then loc_id=h5g_open(fid,GroupName) else loc_id=fid 
    
n0=n_elements(v0)
n1=n_elements(v1)
n2=n_elements(v2)
n3=n_elements(v3)
n4=n_elements(v4)
n5=n_elements(v5)

for i0=0,n0-1 do begin
  gid0=h5g_create(loc_id,v0[i0])
  h5g_close,gid0
  for i1=0,n1-1 do begin
    gid0=h5g_open(loc_id,v0[i0])
    gid1=h5g_create(gid0,v1[i1])
    h5g_close,gid0
    h5g_close,gid1
    for i2=0,n2-1 do begin
      gid1=h5g_open(loc_id,v0[i0]+'/'+v1[i1])
      gid2=h5g_create(gid1,v2[i2])
      h5g_close,gid1
      h5g_close,gid2
      for i3=0,n3-1 do begin
        gid2=h5g_open(loc_id,v0[i0]+'/'+v1[i1]+'/'+v2[i2])
        gid3=h5g_create(gid2,v3[i3])
        h5g_close,gid2
        h5g_close,gid3	
    	for i4=0,n4-1 do begin
          gid3=h5g_open(loc_id,v0[i0]+'/'+v1[i1]+'/'+v2[i2]+'/'+v3[i3])
    	  gid4=h5g_create(gid3,v4[i4])
	  h5g_close,gid3
	  h5g_close,gid4
    	  for i5=0,n5-1 do begin
            gid4=h5g_open(loc_id,v0[i0]+'/'+v1[i1]+'/'+v2[i2]+'/'+v3[i3]+'/'+v4[i4])
    	    gid5=h5g_create(gid4,v5[i5])
	    h5g_close,gid4
	    h5g_close,gid5
    	  endfor
    	endfor
      endfor
    endfor
  endfor
endfor
if n_elements(GroupName) ne 0 then h5g_close,loc_id
end
