;This procedure write a dataset to fid.
;fid: the identifier of a hdf5 file
;data: the data to be written
;dataname: the name of the data to be written
;GroupName: the path to the dataset in the hdf5 file. if not present, 
;   	    it is the root path.
;gzip: the compression level, the default value is 9.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


pro h5write_dataset,fid,data,dataName,GroupName=GroupName,$
    member_names=member_names,chunk_dimensions=chunk_dimensions,$
    gzip=gzip
;checking the input arguments and keywords...
if n_elements(GroupName) ne 0 then loc_id=h5g_open(fid,GroupName) else loc_id=fid 

n_dim=size(data,/n_dimension)
dims=size(data,/dimension)
if n_elements(chunk_dimensions) eq 0 then begin
  if n_dim eq 0 then chunk_dimensions=1 else chunk_dimensions=long(sqrt(dims))
endif
if n_elements(gzip) eq 0 then gzip=9

; create dataset in the output file 
datatype_id = H5T_IDL_CREATE(data,member_names=member_names)
if (size(data,/type) eq 7)and(n_dim ne 0) then begin
  len=strlen(data)
  lenmax=max(len,index)
  datatype_id=H5T_IDL_CREATE(data[index])
endif
if n_dim eq 0 then dataspace_id=H5S_CREATE_SCALAR() else $
    dataspace_id=H5S_CREATE_SIMPLE(dims)  
dataset_id = H5D_CREATE(loc_id,dataName,datatype_id,dataspace_id,$
    	     chunk_dimensions=chunk_dimensions,gzip=gzip) 

; write data to dataset 
H5D_WRITE,dataset_id,data  

; close all open identifiers 
H5D_CLOSE,dataset_id   
H5S_CLOSE,dataspace_id 
H5T_CLOSE,datatype_id 
if n_elements(GroupName) ne 0 then h5g_close,loc_id

end
