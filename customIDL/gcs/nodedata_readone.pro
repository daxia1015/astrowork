function nodeData_ReadOne,fid,outputName,dataName,color=color,$
    	 magnitude=magnitude,metal=metal,original=original,$
	 _extra=_extra

if keyword_set(color) then begin
  data=nodeData_color(fid,outputName,dataName)
endif

if keyword_set(magnitude) then begin
  data=nodeData_magnitude(fid,outputName,dataName)
endif

if keyword_set(metal) then begin
  data=nodeData_metal(fid,outputName,dataName,_extra=_extra)
endif

if keyword_set(original) then begin
  data=NodeData_Read(fid,outputName,dataName=dataName,_extra=_extra)
;  if keyword_set(Rhalf) then data=HalfMassRadius(data,_extra=_extra)
endif

return,data
end
;==================================================================
;==================================================================
