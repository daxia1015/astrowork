function nodeData_Luminosity,fid,outputName,lumiName
nL=n_elements(lumiName)

for iL=0,nL-1 do begin
  dataname=['diskStellarLuminosity:'+lumiName[iL]+':rest:z0.0000',$
    	   'spheroidStellarLuminosity:'+lumiName[iL]+':rest:z0.0000']
  L1=NodeData_Read(fid,outputName,dataName=dataName,/sum)

  if nL eq 1 then lumi=L1 else begin
    tag=tagName(lumiName[iL])
    if iL eq 0 then lumi=create_struct(tag,L1) else $
    	lumi=create_struct(lumi,tag,L1)
  endelse
endfor
return,lumi
end
