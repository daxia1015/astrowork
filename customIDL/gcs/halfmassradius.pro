;This function transfer the scale length of disk or spheroid to their half mass radius.
;rs: the scale length
;ExponentialDisk: if set, return the half mass radius of an Exponential Disk
;HernquistSpheroid:  if set, return the half mass radius of a Hernquist Spheroid


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
   	
function HalfMassRadius,rs,ExponentialDisk=ExponentialDisk,$
    	    	    	HernquistSpheroid=HernquistSpheroid
if keyword_set(ExponentialDisk)then return,1.67834699*rs
if keyword_set(HernquistSpheroid) then return,(1+sqrt(2.0))*rs
end
