;This function read out the member name in a specific group of fileid.
;fileid: the identifier of a hdf5 file
;Groupname: the name of the group to be read
;n_members: a return variable contains the number of the members
;index: the index of the members to be read
;allMembers: if present, all members will be read out
;   	     only one of "index" or "allMembers" is required
;name: the results is an idl array


;Author: Jianling Gan
;Modifications: Jianling Gan, 19.Jun.2012
   	

function h5read_MemberName,fileid,GroupName,n_members,index=index,allMembers=allMembers
n_members=n_elements(index)
;if n_members ne 0 then name=strarr(n_members) 
if keyword_set(allMembers) then begin
  n_members=h5g_get_Nmembers(fileid,GroupName)
  if n_members eq 0 then message,'no member in this group: '+GroupName
  index=lindgen(n_members)
endif

if n_members eq 1 then return,h5g_get_member_name(fileid,GroupName,index)

name=strarr(n_members)
for k=0L,n_members-1 do begin
  i=index[k]
  name[k]=h5g_get_member_name(fileid,GroupName,i)
endfor
return,name
end
