;This function read out the data in the group "globalHistory" in a Galacticus output file.
;fname: the file name (include the path) of the Galacticus output file;
;nhistory:  a return variable contains the number of the output history
;Out: the results is an idl structure, in which the tag name is the 
;     history name and the tag value is the history data



;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


function GlobalHistory,fname,nhistory
GroupRoot='globalHistory'
fid=h5f_open(fname)
name=h5read_MemberName(fid,GroupRoot,nhistory,/all)
out=h5read_dataset(fid,name,GroupName=GroupRoot)
h5f_close,fid
return,out
end
;====================================================================
