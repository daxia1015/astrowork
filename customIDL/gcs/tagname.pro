;This function return a legal tag name for idl structure.
;(1) The idl reserved words such as "and", "or", "then" ... are not allowed;
;(2) The special symbols are not allowed to be present;
;(3) The beginning character can not be a number.

;name0: a tag name to be refined


;Author: Jianling Gan
;Modifications:  Jianling Gan, 1.May.2012


function tagname,name0
;48:'0', 57:'9', 65:'A', 90 :'Z'
;print,byte('09AZaz')
n0=n_elements(name0)
if n0 ne 1 then message,'processing only one word for one time'

reserved_words=['AND','BEGIN','BREAK','CASE','COMMON','COMPILE_OPT', $
                'CONTINUE','DO','ELSE','END','ENDCASE','ENDELSE','ENDFOR', $
                'ENDIF','ENDREP','ENDSWITCH','ENDWHILE','EQ','FOR', $
                'FORWARD_FUNCTION','FUNCTION','GE','GOTO','GT','IF', $
                'INHERITS','LE','LT','MOD','NE','NOT','OF','ON_IOERROR', $
                'OR','PRO','REPEAT','SWITCH','THEN','UNTIL','WHILE','XOR']
name=strupcase(name0)
sn=where(reserved_words eq name,n)
if n ne 0 then message,name0+'is a reserved word.'

b1=byte(name)
;print,b0
  sn=where(((b1 ge 48)and(b1 le 57))or((b1 ge 65)and(b1 le 90)),n1)
  if n1 eq 0 then message,'No valid tagname found.'
  b1=b1[sn] ;&  print,b1
  if b1[0] lt 65 then b1=[byte(65),b1] ;& print,b1
  name=string(b1)
return,name
end


;pro test
;name0=['23DE$sf03./','then','rQ5-=`','fd43-&%*(>,/EDe','or']
;name=tagname(name0)
;print,name0,' ',name

;end
