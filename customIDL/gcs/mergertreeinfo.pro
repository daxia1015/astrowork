;This function read out the merger tree info from the Galacticus output file.
;fid: the identifier of a Galacticus output file
;outputName: the name of the output group to be read
;info: The result include the mergerTreeCount, mergerTreeIndex, mergerTreeStartIndex,
;mergerTreeWeight. They are incorporated into an IDL structure.
;
;


;Author: Jianling Gan
;Modifications:  Jianling Gan, 2.Jul.2012
   	


function MergerTreeInfo,fid,outputName
GroupName='Outputs/'+outputName
;n0=h5g_get_Nmembers(fid,GroupName)
;ntree=n0-5
InfoName=strarr(4)
;print,GroupName,n0,ntree
for i=0,3 do begin
;  i=ntree+k
  InfoName[i]=h5g_get_member_name(fid,GroupName,i)
endfor
info=h5read_dataset(fid,InfoName,GroupName=GroupName)
return,info
end
;====================================================================
;====================================================================
