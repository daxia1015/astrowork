;This procedure read out the output list in a Galacticus output file.
;fname: the file name (include the path) of the Galacticus output file;
;nout:  if present, return the number of the outputs
;OutputName: if present, return the name of the output group
;alist:  if present, return the list of the output expansion factor
;tlist:  if present, return the list of the output time
;zlist:  if present, return the list of the output redshift

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012


pro OutputList,fname,nout,OutputName=OutputName,$
    alist=alist,tlist=tlist,zlist=zlist
GroupRoot='Outputs'
fid=h5f_open(fname)
OutputName=h5read_MemberName(fid,GroupRoot,nout,/all)
alist=dblarr(nout)
tlist=dblarr(nout)
;help,nstep
for i=0,nout-1 do begin
  gid=h5g_open(fid,GroupRoot+'/'+OutputName[i])
  time=h5read_attribute(gid)
  alist[i]=time.outputExpansionFactor
  tlist[i]=time.outputTime
  h5g_close,gid
endfor
sn=sort(alist)
alist=alist[sn]
tlist=tlist[sn]
OutputName=OutputName[sn]
if arg_present(zlist) then begin
  gid=h5g_open(fid,'Parameters')
  aid=h5a_open_name(gid,'outputRedshifts')
  zlist=h5a_read(aid)
  zlist=zlist[reverse(sort(zlist))]
  h5a_close,aid
  h5g_close,gid
endif
h5f_close,fid
end
;====================================================================
;====================================================================
