
;dataName: even item: the dataName of metal mass component; 
;odd itme: the dataName of base mass component to be computed.
;==================================================================
;==================================================================
function nodeData_Metal,fid,outputName,dataName,_extra=_extra
n2=n_elements(dataName)
if (n2 mod 2) ne 0 then message,'dataName must have 2, 4, 6 ... elements.'

metalMass=NodeData_Read(fid,outputName,dataName=dataName[0:n2/2-1],/sum)
baseMass=NodeData_Read(fid,outputName,dataName=dataName[n2/2:n2-1],/sum)

metal=metalMass
metal[*]=1000
sn=where((metalMass gt 0)and(baseMass gt 0))
metal[sn]=metallicity(metalMass[sn],baseMass[sn],_extra=_extra)
return,metal
end
;==================================================================
;==================================================================
