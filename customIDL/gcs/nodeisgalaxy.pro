;This function identify if a node harbors stellar mass in the Galacticus output file.
;fid: the identifier of a Galacticus output file
;outputName: the name of the output group to be identified
;sn: 1: is galaxy, 0: not galaxy


;Author: Jianling Gan
;Modifications:  Jianling Gan, 30.Jul.2012


function NodeIsGalaxy,fid,outputName
stellar=NodeData_Read(fid,outputName,dataname=['diskStellarMass','spheroidStellarMass'],/sum)
sn=stellar gt 0
return,sn
end
;====================================================================
;====================================================================
