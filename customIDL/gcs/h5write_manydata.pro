pro h5write_manydata,file,dataName,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,$
    	    	    groupName=groupName
type=size(file,/type)
if type eq 7 then fid=h5f_create(file) else fid=file
if n_elements(groupName) ne 0 then gid=h5g_create(fid,groupName) else gid=fid

nd=n_elements(dataName)
for i=0,nd-1 do begin
  case i of
    0 :h5write_dataset,gid,v0,dataName[i]
    1 :h5write_dataset,gid,v1,dataName[i]
    2 :h5write_dataset,gid,v2,dataName[i]
    3 :h5write_dataset,gid,v3,dataName[i]
    4 :h5write_dataset,gid,v4,dataName[i]
    5 :h5write_dataset,gid,v5,dataName[i]
    6 :h5write_dataset,gid,v6,dataName[i]
    7 :h5write_dataset,gid,v7,dataName[i]
    8 :h5write_dataset,gid,v8,dataName[i]
    9 :h5write_dataset,gid,v9,dataName[i]
    else:message,'variable is NOT enough!'
  endcase
endfor
if n_elements(groupName) ne 0 then h5g_close,gid
if type eq 7 then h5f_close,fid

end
