;This function read out the data in the group "massAccretionHistories" in a Galacticus output file.
;fname: the file name (include the path) of the Galacticus output file;
;index: the index of the tree to be read
;treeName: the name of the tree to be read; only index or treeName is required;
;   	   if both them are not set, it is default to read the first tree, 
;   	   which is usually the main branch of merger tree.


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012




function Galacticus_MAH,fname,index=index,treeName=treeName
GroupRoot='massAccretionHistories'
fid=h5f_open(fname)
nIndex=n_elements(index)
nName=n_elements(treeName)
if(nIndex ne 0)and(nName ne 0)then message,'only index or treeName is required!'
if nIndex ne 0 then treeName=h5read_MemberName(fid,GroupRoot,ntree,index=index)
if nName ne 0 then ntree=nName
;if keyword_set(allBranch) then name=h5read_MemberName(fid,GroupRoot,ntree,/all)
if(nIndex eq 0)and(nName eq 0) then treeName=h5read_MemberName(fid,GroupRoot,ntree,index=0)

name=GroupRoot+'/'+treeName
;print,name
for itree=0,ntree-1 do begin
  dataname=h5read_MemberName(fid,name[itree],/all)
  MAH0=h5read_dataset(fid,dataname,GroupName=name[itree])
  if ntree eq 1 then MAH=MAH0 else begin
    if itree eq 0 then MAH=create_struct(treeName[i],MAH0) else $
      MAH=create_struct(MAH,treeName[i],MAH0)
  endelse
endfor
h5f_close,fid
return,MAH
end
