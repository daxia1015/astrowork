pro cumulate_data,data,temp
sn=size(data,/dimensions) eq size(temp,/dimensions)
sn1=where(sn eq 0,n1)
if n1 ne 0 then message,'The dimensions of data do not match.' 
data=data+temp
end
