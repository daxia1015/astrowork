pro append_data,data,temp
if size(data,/type) ne size(temp,/type) then $
    message,'The types of data do not match.'
data=[data,temp]
end
