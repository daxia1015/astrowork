;This function find the root of an equation f(x)=0, of which f(x) is monotonic within [a,b]. 
;This equation must have one and only root within [a,b], that says, f(a)*f(b)<0.
;x:  initial value
;ab: the value range of x
;fxp: the function name of the derivation of f(x)
;fx:  the function name of f(x)
;fp1: the sign of fxp, 1:positive; -1:negative.
;fpp1: the sign of the derivation of fxp, positive: f(x) is concave; negative: f(x) is convex. 
;tolx: tolerance of x

function inewton,x,fxp,fx,fpp1=fpp1,ab=ab,tolx=tolx
if ~keyword_set(tolx) then tolx=1e-3
fp=call_function(fxp,x) & f=call_function(fx,x)
dx=abs(f/fp) & dx2=dx/2

if fp ge 0 then fp1=1. else fp1=-1.

while dx gt tolx do begin
  if fpp1 gt 0 then $
    if f lt 0. then x=x+dx2*fp1 else x=x-dx*fp1 $
  else if f lt 0. then x=x+dx*fp1 else x=x-dx2*fp1
  if n_elements(ab) eq 2 then begin
    x=x> ab[0]
    x=x< ab[1]
  endif
  fp=call_function(fxp,x) & f=call_function(fx,x)
  dx=abs(f/fp) & dx2=dx/2
endwhile
return,x
end


;pro testr
;parameters,'treedata/'
;!except=2
;miv=2.5e10 & nj=n_elements(miv)
;;mv=[1.,1e-1,1e-2,1e-3,1e-4,1e-5,1e-6,5e-7] & ni=n_elements(mv)
;mv=0.004+findgen(10)*0.001 & ni=n_elements(mv)
;csub=8.5 & !m.csub=csub
;r=[0.,0.]
;for j=0,nj-1 do begin
;  mi=miv[j] & !m.msubi=mi & print,mi
;  rs_sub=r_s(csub,mi) 
;  !m.rs_sub=rs_sub 
;  for i=0,ni-1 do begin
;    mx=mv[i] & !m.mx=mx & print,'mx',mx
;    msub=mx*mi
;    ;if msub lt 1000 then continue
;    for th=0,1 do r[th]=r_bond(msub,th=th)
;    print,msub,r[0],r[1],r[1]/r[0]
;  endfor
;endfor
;end
