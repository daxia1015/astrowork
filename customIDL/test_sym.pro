pro test_sym
entry_device=!d.name
set_plot,'ps'
!except=2
char=1.3 & ply=2 & len=0.04 & c0=25 & dc=80
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char
!y.thick=ply & !y.style=1 & !y.charsize=char
colors_kc
value=[string(181B) + 'm',STRING(197B),string(177B),string(176B),$
       '!9' + string(110B) + '!X','!9l!X','!9'+string(98B)+'!X' ,$
       '!9X!X','!9'+string(63B)+'!X','!9'+string(65B)+'!X'      ,$
       '!20'+string(83B)+'!X','!7'+string(85B)+'!X','!7'+string(87B)+'!X',$
       '!7'+string(88B)+'!X','!7a!X','!7b!X','!7d!X','!7'+string(107B)+'!X',$
       '!7'+string(109B)+'!X','!7p!X',string(181B) + 'm',STRING(197B),$
       string(177B),'!9'+string(176B)+'!X','!9'+string(163B)+'!X' ,$
       '!9'+string(179B)+'!X','!9'+string(180B)+'!X','!9'+string(181B)+'!X',$
       '!9'+string(187B)+'!X','!9'+string(197B)+'!X','!9'+string(70B)+'!X' ,$
       '!9'+string(89B)+'!X','!9'+string(87B)+'!X','!9'+string(97B)+'!X' ,$
       '!9'+string(98B)+'!X','!9'+string(100B)+'!X','!9'+string(108B)+'!X',$
       '!9'+string(110B)+'!X','!9'+string(112B)+'!X' ]
xsize=8 & ysize=20
device,filename='sym_table.eps',/color,xsize=xsize,ysize=ysize,/encapsul
n=100. &   n1=10.
plot,findgen(n+1)/n,findgen(n+1)/n,/nodata,xstyle=4,ystyle=4,/noerase
k=0 & name="" & ygap=1./ysize & y0=(ysize-0.5)/ysize
openr,lun,'symbols_kc.pro',/get_lun
while eof(lun) ne 1 do begin
  readf,lun,name
  print,name
  pe=strpos(name,'=') & len=strlen(name)
  if len ne 0 then begin
    c0=!myct.c2[k mod 2]
    xyouts,0,y0-k*ygap,strmid(name,0,pe),color=c0 
    xyouts,6./xsize,y0-k*ygap,value[k],color=c0
;    xyouts,0.4,y0-k*ygap,strmid(name,pe+1,len-pe-1)
    k++
  endif
endwhile
free_lun,lun
print,k

device,/close_file
set_plot,entry_device
end
