;---subhalos distribution, density profile
function sub_profile,rv,mv,xr,mphalo,rpvir,chost,lg=lg,dm=dm
d2=abs(xr[0]-xr[1])/2
nbin=n_elements(xr)
h=fltarr(nbin)
for i=0,nbin-1 do begin
  sn=where((rv ge xr[i]-d2)and(rv lt xr[i]+d2),count)
  if count ne 0 then h[i]=total(mv[sn])
endfor
if keyword_set(dm) then begin
  rxx=[xr-d2,xr[nbin-1]+d2]
  if keyword_set(lg) then rxx=10^rxx
  rxx=rxx*rpvir
  m_r=mhalo_r(rxx,rpvir/chost,chost,mphalo)
  y5=m_r[1:nbin]-m_r[0:nbin-1]
  h=h/y5
endif
;print,y5
return,h
end
