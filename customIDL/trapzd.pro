;This routine computes the nth stage of refinement of an
;extended trapezoidal rule. func is input as the name of 
;the function to be integrated between limits a and b, 
;also input. When called with n=1, the routine returns as 
;s the crudest estimate of b a f(x)dx. Subsequent calls 
;with n=2,3,... (in that sequential order) will improve 
;the accuracy of s by adding 2n-2 additional interior points. 
;s should not be modified between sequential calls.


pro trapzd,func,a,b,s,n
;	USE nrtype; USE nrutil, ONLY : arth
;	IMPLICIT NONE
;	REAL(SP), INTENT(IN) :: a,b
;	REAL(SP), INTENT(INOUT) :: s
;	INTEGER(I4B), INTENT(IN) :: n
;	INTERFACE
;		FUNCTION func(x)
;		USE nrtype
;		REAL(SP), DIMENSION(:), INTENT(IN) :: x
;		REAL(SP), DIMENSION(size(x)) :: func
;		END FUNCTION func
;	END INTERFACE
;	REAL(SP) :: del,fsum
;	INTEGER(I4B) :: it
if (n eq 1) then s=0.5*(b-a)*sum(func( (/ a,b /) )) else begin
  it=2^(n-2)
  del=(b-a)/it
  fsum=sum(func(arth(a+0.5_sp*del,del,it)))
  s=0.5_sp*(s+del*fsum)
endlse

END
