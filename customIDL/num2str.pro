function num2str,num,format=form
a=string(num,format=form)
if strpos(form,'e') ne -1 then begin
  si=strpos(a,'+')
  n=strlen(a)
  str=strmid(a,0,si)+strmid(a,si+1,n-si-1)
endif else str=a
return,str
end
