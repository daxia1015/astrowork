function histo_weight,nweight,count,sn,weight,ndata
case nweight of 
      0: hi=count
      1: hi=count*weight
  ndata: if count ne 0 then hi=total(weight[sn]) else hi=0.
   else: message,'Weight must have 1 element or same number of elements as data.'
endcase
return,hi
end
;====================================================================
;====================================================================
function histo,array0,nbin=nbin,xmin=xmin0,xmax=xmax0,dbin=dbin,$
    	       locations=hx,weight=weight,log10=log10,$
	       cumulative=cumulative,inverse=inverse,$
	       get_locations=get_locations
ndata=n_elements(array0)
nweight=n_elements(weight)
if n_elements(nbin) eq 0 then nbin=11

if (n_elements(xmin0) eq 0)or(n_elements(xmax0) eq 0) then begin
  sn=uniq(array0,sort(array0)) & n=n_elements(sn)
  if n_elements(xmin0) eq 0 then xmin0=array0[sn[1]]*1.1
  if n_elements(xmax0) eq 0 then xmax0=array0[sn[n-2]]*0.95
endif

xmax=xmax0+0. & xmin=xmin0+0. & array=array0+0.

if keyword_set(log10) then begin
  sn=where(array gt 0,n1)
  if n1 eq 0 then message,'no valid data found'
  xmax=alog10(xmax)
  xmin=alog10(xmin)
  array=alog10(array[sn])
  if nweight eq ndata then begin
    weight=weight[sn]
    nweight=n1
    ndata=n1
  endif
endif

dbin=(xmax-xmin)/(nbin-1)
hx=findgen(nbin)*dbin+xmin
if keyword_set(get_locations) then return,hx

d2=dbin/2.0
h=dblarr(nbin)
if keyword_set(cumulative) then begin
  if keyword_set(inverse)then begin
    for i=0,nbin-1 do begin
      sn=where(array ge hx[i],count)
      h[i]=histo_weight(nweight,count,sn,weight,ndata)
    endfor
  endif else begin
    for i=0,nbin-1 do begin
      sn=where(array le hx[i],count)
      h[i]=histo_weight(nweight,count,sn,weight,ndata)
    endfor
  endelse  
endif else begin
  for i=0,nbin-1 do begin
    sn=where((array ge hx[i]-d2)and(array lt hx[i]+d2),count)  
    h[i]=histo_weight(nweight,count,sn,weight,ndata)
  endfor
endelse

return,h
end
