;Given values for N variables y and their derivatives dydx known 
;at x, use the fifth order Cash-Karp Runge-Kutta method to advance
;the solution over an interval h and return
;the incremented variables as yout. Also return an estimate of 
;the local truncation error in yout using the embedded fourth 
;order method. The user supplies the subroutine derivs(x,y,dydx),
;which returns derivatives dydx at x.
;This code was modified from a fortran code 'rkck.f90'.
;In IDL, this code can be used to calculate a huge number of 
;particles simutaneously by given y,dydx,x,h etc. in array of two
;dimensions that: [n_equations,n_particles]  
;==========================================================
function rkck,y,dydx,x,h,yerr,derivs
A2=0.2 & A3=0.3 & A4=0.6 & A5=1.0 & A6=0.875
B21=0.2 & B31=3./40. & B32=9./40. & B41=0.3 & B42=-0.9 & B43=1.2 
B51=-11./54. & B52=2.5 & B53=-70./27. & B54=35./27. 
B61=1631./55296. & B62=175./512. & B63=575./13824. & B64=44275./110592. & B65=253./4096.
C1=37.0/378. & C3=250./621. & C4=125./594. & C6=512./1771.
DC1=C1-2825./27648. & DC3=C3-18575./48384. & DC4=C4-13525./55296. & DC5=-277./14336. & DC6=C6-0.25


ytemp=y+B21*h*dydx
ak2=call_function(derivs,x+A2*h,ytemp)
ytemp=y+h*(B31*dydx+B32*ak2)
ak3=call_function(derivs,x+A3*h,ytemp)
ytemp=y+h*(B41*dydx+B42*ak2+B43*ak3)
ak4=call_function(derivs,x+A4*h,ytemp)
ytemp=y+h*(B51*dydx+B52*ak2+B53*ak3+B54*ak4)
ak5=call_function(derivs,x+A5*h,ytemp)
ytemp=y+h*(B61*dydx+B62*ak2+B63*ak3+B64*ak4+B65*ak5)
ak6=call_function(derivs,x+A6*h,ytemp)
yout=y+h*(C1*dydx+C3*ak3+C4*ak4+C6*ak6)
yerr=h*(DC1*dydx+DC3*ak3+DC4*ak4+DC5*ak5+DC6*ak6)
;help,yout,yerr
return,yout
end
