;+
;xv is degressive in generally, but may be aggressive during some
;part or in the end
;-
function truncation,xv,xc,yv,index=index,xaggressive=xaggressive
;x_min=min(xv,sn1)
;sn2=where(xv ge xc,n2)
;sn3=where(sn2 lt sn1[0],n3)
;sn4=sn2[sn3[n3-1]]
if keyword_set(xaggressive) then begin
  sn1=where(xv ge xc,n1)
  if n1 eq 0 then x_m=max(xv,sn1)
endif else begin  
  sn1=where(xv le xc,n1)
  if n1 eq 0 then x_m=min(xv,sn1)
endelse
sn4=sn1[0]
if keyword_set(index) then return,sn4

if n_params() lt 3 then message,'array yv should be supplied!'
yc=yv[sn4]
return,yc
end
