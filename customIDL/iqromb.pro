;Returns the integral of the function func from a to b.
;Integration is performed by Romberg's method of order 2K,
;where, e.g., K=2 is Simpson's rule. Parameters: EPS is the
;fractional accuracy desired, as determined by the
;extrapolation error estimate; JMAX limits the total number
;of steps; K is the number of points used in the extrapolation.
;
;=======================================================
FUNCTION iqromb,func,a,b,jmax=jmax,eps=eps,k=k
;	USE nrtype; USE nrutil, ONLY : nrerror
;	USE nr, ONLY : polint,trapzd
;	IMPLICIT NONE
;	REAL(SP), INTENT(IN) :: a,b
;	REAL(SP) :: qromb
;	INTERFACE
;		FUNCTION func(x)
;		USE nrtype
;		REAL(SP), DIMENSION(:), INTENT(IN) :: x
	;	REAL(SP), DIMENSION(size(x)) :: func
		;END FUNCTION func
;	END INTERFACE
if not keyword_set(jmax) then jmax=20
jmaxp=jmax+1
if not keyword_set(k) then k=5
km=k-1
if not keyword_set(eps) then eps=1e-6
h=fltarr(jmaxp) & s=fltarr(jmaxp)

;	INTEGER(I4B), PARAMETER :: JMAX=20,JMAXP=JMAX+1,K=5,KM=K-1
	;REAL(SP), PARAMETER :: EPS=1.0e-6_sp
;	REAL(SP), DIMENSION(JMAXP) :: h,s
;	REAL(SP) :: dqromb
;	INTEGER(I4B) :: j
h[0]=1.0
for j=1,JMAX do begin
  call_procedure,'trapzd',func,a,b,s(j),j
  if (j ge K) then begin
    call polint(h(j-KM:j),s(j-KM:j),0.0_sp,qromb,dqromb)
			if (abs(dqromb) <= EPS*abs(qromb)) RETURN
  endif
  s(j+1)=s(j)
  h(j+1)=0.25_sp*h(j)
endfor
message,'qromb: too many steps'

END
