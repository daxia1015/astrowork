function lnLambda,msub,r_orb,v_orb
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
bmin=gaussc*msub/v_orb^2  ;) >Rbnd                ;bmin=Gm(t)/v_orb^2, ln[1+r_orb/bmin], Binney 1987
lnlda=alog(1+(r_orb/bmin)^2)/2.
return,lnlda
end
;==================================================================
;==================================================================


function f_DynaFric,msub,ro_h_r,v_orb,r_orb,rs_host,vmax_host,lnlda,hprofile=hprofile
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
case hprofile of 
  1:X=v_orb/Vvir_host                      ;Taylor 2001, isothermal
  2:begin
    xh=r_orb/rs_host
    sigma=Vmax_host*1.4393*xh^0.354/(1+1.1756*xh^0.725)
    X=v_orb/sigma/sqrt(2.) ;& if(X gt 10.)then print,X             ;NFW, Zentner 2003
  end
  else:message,'halo profile should be defined!!!'
endcase
;X=v_orb/sqrt(gaussc*mhost_r/r_orb)
X=temporary(X) < 4.01
IX=erf(X)-2*X*exp(-X^2)/sqrt(!pi)
f=4*!pi*lnlda*gaussc^2*msub*ro_h_r*IX/v_orb^2   ;M_{sun}kpc Gyr^{-2}
;if(f le 0)then print,'f_df=',f,lnlda                        ;kpc Gyr^{-2}
return,f
end
