;==================================================================
;==================================================================
function galaxyData_histogram,fid,dataName,baseName,nbin=nbin,$
    	    	   GroupName=Group,_extra=_extra
if n_elements(nbin) eq 0 then nbin=30
nstage=!galaxy.nstage
hi=dblarr(nbin)
out=replicate({xh:hi,h:hi},nstage)

for istage=0,2 do begin
  data=h5read_dataset(fid,dataName,GroupName=Group+'/'+!galaxy.stage[istage])
  if n_elements(baseName) ne 0 then begin
    base=h5read_dataset(fid,baseName,GroupName=Group+'/'+!galaxy.stage[istage])
    sn=where(base gt 0)
    data=data[sn]/base[sn]
  endif
  h=histo(data,nbin=nbin,locations=xh,_extra=_extra)
  out[istage].xh=xh
  out[istage].h=h
endfor
return,out
end
;==================================================================
;==================================================================
function galaxyData_contour,fid,Xname,Yname,Xbase=Xbase,Ybase=Ybase,$
    	    	    nbin=nbin,GroupName=GroupName,_extra=_extra

if n_elements(nbin) eq 0 then nbin=30
hi=dblarr(nbin)
out={xh:hi,yh:hi,h:dblarr(nbin,nbin)}

Xdata=h5read_dataset(fid,XName,GroupName=GroupName)
if n_elements(Xbase) ne 0 then begin
  base=h5read_dataset(fid,Xbase,GroupName=GroupName)
  sn=where(base gt 0)
  Xdata=Xdata[sn]/base[sn]
endif
Ydata=h5read_dataset(fid,YName,GroupName=GroupName)
if n_elements(Ybase) ne 0 then begin
  base=h5read_dataset(fid,Ybase,GroupName=GroupName)
  sn=where(base gt 0)
  Ydata=Ydata[sn]/base[sn]
endif

h=histo2d(Xdata,Ydata,nbinx=nbin,nbiny=nbin,locationX=hx,locationY=hy,_extra=_extra)

out.xh=hx
out.yh=hy
out.h=h
return,out
end
;==================================================================
;==================================================================
