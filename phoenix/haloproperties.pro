function ez,z
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
if !halokey.hostz then begin
  zp1=1.+z
  ez2=omega0*zp1^3+(1-omega0-lambda0)*zp1^2+lambda0
endif else begin
  ez2=z
  ez2[*]=1. 
endelse
return,ez2
end
;==================================================================
;==================================================================
function dltcz,z
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8,ahalo
common astronomy;,ro_c,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if !halokey.densityz then begin
  ldaz=lambda0/ez(z) ; only for LCDM
  dltc=18*!pi^2-82.*ldaz-39.*ldaz^2
endif else begin
  dltc=z
  dltc[*]=dltc0
endelse
return,dltc
end
;==================================================================
;==================================================================
;        rho_c=rho_c0*(omega_m+omega_l/(1+zz(num))^3)
;        r200 = (3.0*m200/4.0/!pi/200.0/rho_c)^0.3333333
function r_virial,m_vir,z   
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if n_params() eq 1 then z=0.
r=(m_vir/(dltcz(z)*ro_c0*ez(z)*4.*!pi/3.))^(1./3)          ;kpc
return,r                                    
end
;==================================================================
;==================================================================
function v_circular,x,c,mvir,rvir
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
v_vir=sqrt(gaussc*mvir/rvir)
vc=v_vir*sqrt(c*gx(x)/x/gx(c))  ;x=r/rs ;with NFW profile
;when x=2.16258, vc=vcmax
;vcmax^2=0.216217*v_vir^2*c/g(c)
;vcmax=0.464991*v_vir*sqrt(c/g(c))
return,vc
end
;==================================================================
;==================================================================
function HaloConcen,m,z
  ;alpha=0.13 & mstar=1e13 & A=9.6  ;Bullock 2001
;  alpha=0.11 & mstar=1e14/0.73 & A=4.67  ;Neto 2007
;  mu=m/mstar
;  c=A/mu^alpha/(1.+z)
;==================================================================
;Gao 2008
A=-0.14*exp(-((0.05-alog10(1.+z))/0.35)^2)
B=2.646*exp(-(alog10(1.+z)/0.5)^2)
c=10^(A*alog10(m)+B)
c=temporary(c) > 4.
return,c
end
;==================================================================
;==================================================================
function halospin,c,mhalo,rvir,ang,specific=specific
common astronomy;,ro_c,dltc,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if keyword_set(specific) then n=1. else n=3.
c=2./3.+(c/21.5)^0.7
spin=ang*sqrt(c/(2.*gaussc*mhalo^n*rvir))
return,spin
end
;==================================================================
;==================================================================
function r_scale,m,z
;if n_params() eq 2 then z=0.
c=HaloConcen(m,z)
rvir=r_virial(m,z)                ;kpc
r_s=rvir/c
return,r_s
end
;==============================================================
function ros_profile,z,c,Mvir,Rvir,hprofile=hprofile
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
case hprofile of 
  1:return,Mvir/(4*!pi*Rvir)                  ;isothermal
  2:return,ro_c0*ez(z)*dltcz(z)*c^3/(3.*gx(c))  ;NFW
  else:message,'halo profile should be defined!!!'
endcase
end
;================================================================
function c_heating,mx
x=alog10(mx) > (-1.8)       ;(-1.86486)
c0=10^(1.02+1.38*x+0.37*x^2)
ft=10^(-0.007+0.35*x+0.39*x^2+0.23*x^3) ;> 0.45
c=[1/c0,ft]
;help,mx,c
;c=[rte,ft]
;if c[0] gt 1e5 then print,mx,rs
return,c
end
;=============================================================
function ro_r,r,rs,c,ros,th=th,c_th=c_th,hprofile=hprofile
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if not keyword_set(hprofile) then hprofile=2
case hprofile of
  1:return,ros/r^2        ;isothermal
  2:begin       ;NFW
    if n_elements(ros) eq 0 then ros=ros_profile(!m.z,c,/nfw)
    x=r/rs
    ro=ros/x/(1+x)^2      ;M_{sun}kpc^{-3} 
    if keyword_set(th) then begin
      b=rs & ft=rs
      b[*]=c_th[0,*] & ft[*]=c_th[1,*]
      ro=temporary(ro)*ft/(1+(b*x)^3)
    endif ;else begin
;     sn2=where(x gt c,n2)
;      if n2 ne 0 then ro[sn2]=0.    
;    endelse
    return,ro
  end
  else:message,'halo profile should be defined!!!'
endcase
end
;==============================================================
function ror2,x
ror2=x/(1+x)^2
ror2=ror2/(1+(!m.c_th[0]*x)^3)
return,ror2
end
;=============================================================
;=============================================================
function mhalo_r,r,rs,c,mhalo,th=th,c_th=c_th,ros_sub=ros_sub,$
    	    	 hprofile=hprofile
if not keyword_set(hprofile) then hprofile=2
case hprofile of 
  0:begin    ;point object
    m=r
    m[*]=mhalo
    return,m
  end
  1:return,mhalo*r/(rs*c)	 ;isothermal
  2:begin                 ; NFW
    x=r/rs ;& print,r
    if keyword_set(th) then begin
      b=rs & ft=rs
      b[*]=c_th[0,*] & ft[*]=c_th[1,*]
;    !c_th=c_th
;    m=qromb('ror2',0.,x,eps=1e-3); & print,'m',m
;    m=4*!pi*ft*ros_sub*rs^3*m
      y0=1-b^3 
      y1=3*b^2*y0^2
      y2=(2-3*b+b^3)*sqrt(3)*b^3
      y3=-(2+3*b+b^3)*b^3
      y4=2*b^3+1
      m=-x/y0/(1+x)+y2*atan((2*b*x-1)/sqrt(3))/y1+y4*alog(1+x)/y0^2 $
        +y3*alog(1+b*x)/y1-y3*alog(1-b*x+b^2*x^2)/y1/2 $
      	-b^3*y4*alog(1+b^3*x^3)/y1/b+!pi*y2/y1/6
      m=4*!pi*ft*ros_sub*rs^3*temporary(m)
    endif else begin
      m=mhalo*gx(x)/gx(c)             ;M_sun    
;      m=temporary(m)<mhalo
    endelse
    return,m                ;M_sun
  end
  else:message,'halo profile should be defined!!!'
endcase

end

    ;print,rs,ros_sub
;    y1=atan((-1./3.*rt+2./3.*r)/rt*3^(1./2.))
;    y2=alog(rt^2-rt*r+r^2)
;    y3=-alog(rt^2-rt*r+r^2)+2*alog(r+rt)
;    m=1./18.*((-6*rt^4*(y2-alog(rt)+alog(r+rt)-alog(rt^2)+3*alog(rs) $
;      -3*alog(rs+r))+2*rt^3*rs*3^(1./2.)*!pi+rs^4*3^(1./2.)*!pi $
;      +6*rs^4*3^(1./2)*y1-6*rt^3*rs*y3+3*rs^4*(y2-2*alog(r+rt) $
;      -alog(rt^2)+2*alog(rt))-9*rt^2*rs^2*y3-12*rt*rs^3*(y2-alog(rt) $
;      +alog(r+rt)-alog(rt^2)+3*alog(rs)-3*alog(rs+r))-18*rt^2*rs^2*3^(1./2)*y1 $
;      -3*rt^2*rs^2*3^(1./2)*!pi+12*rt^3*rs*3^(1./2)*y1-18*rt^4+18*rs^3*rt)*r $
;      +rs^5*3^(1./2)*!pi-3*rs^5*y3+12*rs^4*rt*(alog(rt)+alog(rt^2) $
;      -3*alog(rs)-y2-alog(r+rt)+3*alog(rs+r))-9*rt^2*rs^3*y3 $
;      -6*rt^3*rs^2*y3+6*rs*rt^4*(alog(rt)+alog(rt^2)-3*alog(rs)-y2 $
;      -alog(r+rt)+3*alog(rs+r))-3*rt^2*rs^3*3^(1./2)*!pi $
;      -18*rt^2*rs^3*3^(1./2)*y1+12*rt^3*rs^2*3^(1./2)*y1 $
;      +2*rt^3*rs^2*3^(1./2)*!pi+6*rs^5*3^(1./2)*y1)*rs^2*rt^2 $
;      /(rt^4+2*rt^3*rs+3*rs^2*rt^2+2*rs^3*rt+rs^4)/(rs+r)/(rt-rs)^2
;    m=4*!pi*ft*ros_sub*rs*temporary(m)

;y1=atan(1/3*(-rt+2*r)*3^(1/2)/rt)
;y2=alog(rt^2-rt*r+r^2)
;y3=alog(r+rt)-3*alog(rs+r)-alog(rt^2)+3*alog(rs)-alog(rt)+alog(rt^2-rt*r+r^2)
;m=1/18*rs^2*rt^2*((6*rt^4*(log(rt)-3*log(rs)+log(rt^2)-log(r+rt)+3*log(rs+r)-y(2))+2*rt^3*rs*3^(1/2)*pi+12*rt^3*rs*3^(1/2)*y(1)+18*rs^3*rt-3*rt^2*rs^2*3^(1/2)*pi-18*rt^2*rs^2*3^(1/2)*y(1)+6*rs^4*3^(1/2)*y(1)-18*rt^4-9*rt^2*rs^2*(log(rt^2)+2*log(r+rt)-2*log(rt)-y(2))-12*rt*rs^3*y(3)-6*rt^3*rs*(log(rt^2)+2*log(r+rt)-2*log(rt)-y(2))+rs^4*3^(1/2)*pi+(3*y(2)-6*log(r+rt)-3*log(rt^2)+6*log(rt))*rs^4)*r+rs^5*3^(1/2)*pi+(6*y(2)-12*log(r+rt)-6*log(rt^2)+12*log(rt))*rs^2*rt^3+2*rt^3*rs^2*3^(1/2)*pi-3*rt^2*rs^3*3^(1/2)*pi+(9*y(2)-18*log(r+rt)-9*log(rt^2)+18*log(rt))*rs^3*rt^2-6*rs*rt^4*y(3)-12*rs^4*rt*y(3)+6*rs^5*3^(1/2)*y(1)+(3*y(2)-6*log(r+rt)-3*log(rt^2)+6*log(rt))*rs^5+12*rt^3*rs^2*3^(1/2)*y(1)-18*rt^2*rs^3*3^(1/2)*y(1))/(rt^2+rs*rt+rs^2)^2/(-2*rs*rt+rs^2+rt^2)/(rs+r)
 
