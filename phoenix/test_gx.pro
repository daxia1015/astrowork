;==============================================================
function yita_eqt,x
f=yitag(x)-1.91431
return,f
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;==============================================================
pro test_gx
xi=[1.,2.,3.]
root=fx_root(xi,'yita_eqt',tol=1e-6)
print,root,yitag(root)
;print,sqrt(gx(root)/root)
x=findgen(9000)*0.01-0.1
;plot,x,gx(x)/x/0.216217,/xlog,/ylog
;plot,x,gxox_p(x)
plot,x,yitag(x);,/ylog

end
