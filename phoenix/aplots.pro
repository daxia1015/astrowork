pro aplot
!except=2
!p.font=0
entry_device=!d.name
set_plot,'ps'
colors_kc
char=1 & ply=3 & len=0.03 
!p.thick=ply & !p.charsize=char & !p.charthick=ply & !p.ticklen=len
!x.thick=ply & !x.style=1 & !x.charsize=char & !x.ticklen=len
!y.thick=ply & !y.style=1 & !y.charsize=char & !y.ticklen=len

;!smf.region[3]='[3R!d200!n,'+textoidl('\infty')+')'

;plot_type
;plot_SMFobs
;plot_SMFobsType
;plot_SMFcompare
;plot_SMFev
;plot_SMFsq
plot_SMFtype
;plot_SMFCSR
;plot_SMFCS
;plot_SMFdm
;tree01
;treeDensity
;plot_fE
;plot_sSFR
;plot_bod
;plot_cm_contour
;plot_cm_point
;plot_color
;plot_mol

;plot_sfr
;plot_magZf
;,filename='font.ps',xsize=16,ysize=12
;DEVICE,/helvetica,/bold,FONT_INDEX = 20
;showfont,20,'font'
print,textoidl('\rho \epsilon \oplus _{\odot} \varepsilon \infty')
print,textoidl('$\ast \simeq \approx \Phi \psi \Psi \star \Delta$')
;print,textoidl('\xi \mu \alpha \ge \geq \geqslant \geqq')
;print,textoidl('{\cal f} \mathcal{f} \vec{f}')
;print,textoidl('\CYRYA \cyrya \quad \,')
;print,sunsymbol()



multiplot_Gan,/default
device,/close_file
set_plot,entry_device
end
