function tra_sim2sam,isim,track,totnstep,len,m200,pos,velocity,Vmax,snapnum,$
    	    	    	    	 hprofile=hprofile
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common simulation  ;,particlemass,unit
;tree=assoctree(!treefile[isim],ntree,totnhalo)
;breaktree,tree,totnhalo,len,m200,pos,velocity,Vmax,snapnum,$
;    	       tag=['len','m200','pos','vel','vmax','snapnum']
zlist=snaplist(/redshift)
tra=replicate(!twobody.tra,totnstep)
sub=track.sub & host=track.host
tra.sub=sub & tra.host=host
zv=zlist[snapnum[track.host]]
vecd=(pos[*,sub]-pos[*,host])*unit.d
vecvel=(velocity[*,sub]-velocity[*,host])*unit.v
vecangl=crosspn(vecd,vecvel)                    ;specific angular momentum, vector
vmax_host=Vmax[host]*unit.v

tra.t=tGy(zv)
tra.mhost=m200[host]*unit.m & tra.msub=m200[sub]*unit.m
sn1=where(tra.msub eq 0)
tra[sn1].msub=len[sub[sn1]]*particlemass[isim]
sn1=where(tra.mhost eq 0,n1) ;& help,n1
tra[sn1].mhost=len[host[sn1]]*particlemass[isim]

tra.d=sqrt(total(vecd^2,1))                       ;radial position
tra.Vorb=sqrt(total(vecvel^2,1))     	    	  ;orbital velocity
tra.angl=sqrt(total(vecangl^2,1))                           ;angular mementum
tra.chost=chalo(tra.mhost,zv)                  ;concentration of host halo
Vvir2=vmax_host^2*gx(tra.chost)/tra.chost/0.216217             ;NFW profile
tra.Vvir=sqrt(Vvir2)	    	    	    	     ;virial velocity of host halo
tra.Rhost=gaussc*tra.mhost/Vvir2	    	    	    ;virial radius of host halo

rs_host=tra.Rhost/tra.chost
ros_host=ros_profile(zv,tra.chost,hprofile=hprofile)
mhost_r=mhalo_r(tra.d,rs_host,tra.chost,tra.mhost,hprofile=hprofile)  ;host halo mass within tra.d
ro_host_r=ro_r(tra.d,rs_host,tra.chost,ros_host,hprofile=hprofile)

tra.energy=energy_r(tra.d,tra.mhost,rs_host,tra.chost,tra.Vorb,hprofile=hprofile)
tra.f_g=gaussc*mhost_r/tra.d^2                          ;gravity at radius tra.d
tra.lnlda=lnLambda(tra.msub,tra.Vorb,tra.d)       ;Coulomb logarithm
tra.f_df=f_DynaFric(tra.msub,ro_host_r,tra.Vorb,tra.d,rs_host,vmax_host,$
    	    	    	      tra.lnlda,hprofile=hprofile)     ;dynamical friction

orbit_parameters,tra.mhost,rs_host,tra.chost,tra.energy,$
    	    	    	  tra.angl,tra.Vvir,yita,epson,sn=boundsn 
tra[boundsn].yita=yita
tra[boundsn].epson=epson
close,/all
return,tra
end
;==================================================================
;==================================================================
pro tra_sim_write
!key.hprofile=2
!except=1

for isim=1,19 do begin
  track=twobody_read(!twobody.fnametrack[isim],ngroup,totnstep,nsteps,IDs)
  ;help,track
  tra=tra_sim2sam(isim,track,totnstep,hprofile=!key.hprofile)
  ;help,tra
  openw,lun,!twobody.fnametra[isim],/get_lun
    writeu,lun,ngroup,totnstep
    writeu,lun,nsteps,IDs
    writeu,lun,tra
  free_lun,lun

endfor
end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
;function tra_sim_read,fname,ngroup,totnstep,nsteps,IDs
;ngroup=0L & totnstep=0L
;
;openr,lun,fname,/get_lun
;  readu,lun,ngroup,totnstep
;  nsteps=intarr(ngroup) & IDs=fltarr(ngroup)
;  tra=replicate(!twobody.tra,totnstep)
;  readu,lun,nsteps,IDs
;  readu,lun,tra
;  if eof(lun) then print,'end of ',fname
;free_lun,lun
;return,tra
;end
