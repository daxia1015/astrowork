pro plot_histogram_catamorph,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.group
ngroup=n_elements(group) & nstage=!galaxy.nstage
hi=dblarr(nbin)
hv=replicate({xh:hi,yh:hi},ngroup,nstage,2)
;xrv=[[2e6,5e10],[2e4,5e10],[2e6,5e11]]
xrv=[[9e-2,2.1],[1.1e-2,20]]
xtitv=['R!dspheroid!n / R!ddisk!n','M!dspheroid!n / M!ddisk!n']

icol=0
hv[*,*,icol]=galaxyData_histogram(fid,group,'spheroidHalfMassRadius','diskHalfMassRadius',/lg,$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
icol++		     
hv[*,*,icol]=galaxyData_histogram(fid,group,'spheroidStellarMass','diskStellarMass',/lg,$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])


device,filename=psdir+'cata_morph.eps',/color,/encapsul,xsize=12,ysize=15
multiplot,[2,3],mytitle=ytit,mytitoffset=-1,myposition=[0.15,0.17,0.99,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.02,!p.position[3]-0.04,!galaxy.stage[istage],/normal	 
  for icol=0,1 do begin
    if istage eq 2 then xyouts,(!p.position[0]+!p.position[2])/2,$
    	    	   !p.position[1]-0.07,xtitv[icol],/normal,alignment=0.5
    plot_histogram_panel,hv[*,istage,icol],/xlog,xrange=xrv[*,icol],yrange=yr,/cumulative
    multiplot
  endfor
endfor
multiplot,/default
labeling,0.01,0.96,0.09,0.04,group,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)
end
;==================================================================
;==================================================================
pro plot_cata_mass_size,psdir,fid
nbin=30 & xlog=1 & ylog=1
Xtit='M!dspheroid!n / M!ddisk!n'
Ytit='R!dspheroid!n / R!ddisk!n'
xr=[[[0.2,40],[9e-3,0.9],[4e-3,1]],$
    [[1e-2,40],[2e-2,50],[2e-2,30]],$
    [[8e-3,30],[7e-3,40],[8e-3,30]]] 
yr=[[[0.1,3],[0.1,3],[0.1,3]],$
    [[0.1,3],[0.1,3],[0.1,3]],$
    [[0.1,3],[0.1,3],[0.1,3]]]
@include/contour_blue2red.pro

device,filename=psdir+'cata_mass_size.eps',/color,/encapsul,xsize=17,ysize=16
multiplot,[3,3],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,myposition=[0.1,0.15,0.99,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.01,!p.position[3]-0.04,!galaxy.stage[istage],/normal
  for igroup=0,2 do begin
    GroupName=!galaxy.group[igroup]+'/'+!galaxy.stage[istage]+'/'
    hist=galaxyData_contour(fid,GroupName+'spheroidStellarMass',$
        	GroupName+'spheroidHalfMassRadius',$
		XbaseName=GroupName+'diskStellarMass',$
		YbaseName=GroupName+'diskHalfMassRadius',$
		nbin=nbin,xlog=xlog,ylog=ylog,$
		xmin=xr[0,istage,igroup],xmax=xr[1,istage,igroup],$
		ymin=yr[0,istage,igroup],ymax=yr[1,istage,igroup])
    contour,hist.h/total(hist.h),hist.xh,hist.yh,xlog=xlog,ylog=ylog,$
    	    xrange=[min(xr[0,*,*])*0.95,max(xr[1,*,*])*1.05],$
	    yrange=[min(yr[0,*,*])*0.95,max(yr[1,*,*])*1.05],$
    	    /fill,nlevels=nlevel,c_colors=colortable
    multiplot
  endfor
endfor
multiplot,/default
end
;==================================================================
;==================================================================
pro plot_sateGroup_mass_size,psdir,fid
nbin=30 & xlog=1 & ylog=1
Xtit='M!dspheroid!n / M!ddisk!n'
Ytit='R!dspheroid!n / R!ddisk!n'
xr=[[8e-3,30],[6e-3,40],[7e-3,50],[8e-3,60]] 
yr=[[0.1,2.5],[0.1,2.5],[0.1,2.5],[0.1,2.5]]
;dataName=['timeDuration','hotHaloMass','starFormationRate']    
@include/contour_blue2red.pro

device,filename=psdir+'sateGroup_mass_size.eps',/color,/encapsul,xsize=17,ysize=5.8
multiplot,[4,1],mxtitle=xtit,mytitle=ytit,mytitoffset=-1.5,$
    	  myposition=[0.1,0.21,0.99,0.98]
for igroup=0,3 do begin
  xyouts,!p.position[0]+0.01,!p.position[3]-0.1,$
    	    	      !galaxy.sateName[igroup],/normal
  GroupName=!galaxy.sategroup[igroup]+'/'+!galaxy.stage[2]+'/'
  hist=galaxyData_contour(fid,GroupName+'spheroidStellarMass',$
        	GroupName+'spheroidHalfMassRadius',$
		XbaseName=GroupName+'diskStellarMass',$
		YbaseName=GroupName+'diskHalfMassRadius',$
		nbin=nbin,xlog=xlog,ylog=ylog,$
		xmin=xr[0,igroup],xmax=xr[1,igroup],ymin=yr[0,igroup],$
		ymax=yr[1,igroup])
  contour,hist.h/total(hist.h),hist.xh,hist.yh,xlog=xlog,ylog=ylog,$
    	  xrange=[min(xr[0,*])*0.99,max(xr[1,*])*1.05],$
	  yrange=[min(yr[0,*])*0.95,max(yr[1,*])*1.05],$
    	  /fill,nlevels=nlevel,c_colors=colortable,xtickname=['10!u-2!n','10!u-1!n','1','10']
  multiplot
endfor
;endfor
multiplot,/default
end
;==================================================================
;==================================================================
pro plot_cata_spheroid_disk,psdir,fid
nbin=30 & xlog=1 & ylog=1
Xtit='R!dspheroid!n / R!dvir!n'
Ytit='R!ddisk!n / R!dvir!n'
xr=[[[9e-3,8e-2],[1e-2,2e-1],[1e-2,2e-1]],$
    [[9e-3,2e-1],[3e-3,2e-1],[2e-3,3e-1]],$
    [[1e-2,2e-1],[1e-2,2e-1],[1e-2,2e-1]]] 
yr=[[[9e-3,2e-1],[1e-2,2e-1],[1e-2,3e-1]],$
    [[9e-3,2e-1],[2e-3,3e-1],[2e-3,5e-1]],$
    [[1e-2,4e-1],[2e-2,5e-1],[2e-2,5e-1]]]
@include/contour_blue2red.pro

device,filename=psdir+'cata_spheroid_disk.eps',/color,/encapsul,xsize=17,ysize=16
multiplot,[3,3],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,myposition=[0.1,0.15,0.99,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.01,!p.position[3]-0.04,!galaxy.stage[istage],/normal
  for igroup=0,2 do begin
    GroupName=!galaxy.group[igroup]+'/'+!galaxy.stage[istage]+'/'
    hist=galaxyData_contour(fid,GroupName+'spheroidHalfMassRadius',$
        	GroupName+'diskHalfMassRadius',$
		XbaseName=GroupName+'nodeVirialRadius',$
		YbaseName=GroupName+'nodeVirialRadius',$
		nbin=nbin,xlog=xlog,ylog=ylog,$
		xmin=xr[0,istage,igroup],xmax=xr[1,istage,igroup],$
		ymin=yr[0,istage,igroup],ymax=yr[1,istage,igroup])
    contour,hist.h/total(hist.h),hist.xh,hist.yh,xlog=xlog,ylog=ylog,$
    	    xrange=[min(xr[0,*,*])*0.95,max(xr[1,*,*])*1.05],$
	    yrange=[min(yr[0,*,*])*0.95,max(yr[1,*,*])*1.05],$
    	    /fill,nlevels=nlevel,c_colors=colortable
    multiplot
  endfor
endfor
multiplot,/default
end
;==================================================================
;==================================================================
pro plot_sateGroup_spheroid_disk,psdir,fid
nbin=30 & xlog=1 & ylog=1
Xtit='R!dspheroid!n / R!dvir!n'
Ytit='R!ddisk!n / R!dvir!n'
xr=[[1e-2,2e-1],[1e-2,3e-1],[1e-2,3e-1],[8e-3,3e-1]] 
yr=[[2e-2,5e-1],[1e-2,5e-1],[2e-2,5e-1],[9e-3,5e-1]]
;dataName=['timeDuration','hotHaloMass','starFormationRate']    
@include/contour_blue2red.pro

device,filename=psdir+'sateGroup_spheroid_disk.eps',/color,/encapsul,xsize=17,ysize=5.8
multiplot,[4,1],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,$
    	  myposition=[0.1,0.21,0.99,0.98]
for igroup=0,3 do begin
  xyouts,!p.position[0]+0.01,!p.position[3]-0.1,$
    	    	      !galaxy.sateName[igroup],/normal
  GroupName=!galaxy.sategroup[igroup]+'/'+!galaxy.stage[2]+'/'
  hist=galaxyData_contour(fid,GroupName+'spheroidHalfMassRadius',$
        	GroupName+'diskHalfMassRadius',$
		XbaseName=GroupName+'nodeVirialRadius',$
		YbaseName=GroupName+'nodeVirialRadius',$
		nbin=nbin,xlog=xlog,ylog=ylog,$
		xmin=xr[0,igroup],xmax=xr[1,igroup],ymin=yr[0,igroup],$
		ymax=yr[1,igroup])
  contour,hist.h/total(hist.h),hist.xh,hist.yh,xlog=xlog,ylog=ylog,$
    	  xrange=[min(xr[0,*])*0.99,max(xr[1,*])*1.05],$
	  yrange=[min(yr[0,*])*0.95,max(yr[1,*])*1.05],$
    	  /fill,nlevels=nlevel,c_colors=colortable
  multiplot
endfor
;endfor
multiplot,/default
end
;==================================================================
;==================================================================
