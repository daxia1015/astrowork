function nodeDataCombine,isimv,outputName,Weight0
nsim=n_elements(isimv)
Name0=['nodeVirialRadius','nodeIsIsolated']
datai={nodeVirialRadius:0.0d,nodeIsIsolated:-1L,mstar:0.0d,$
       pos:dblarr(3),ct:0.0d,weight:0.0}
out=datai
for i=0,nsim-1 do begin
  isim=isimv[i]
  fid=h5f_open(!galacticus.treeEv[isim])
  data=nodeData_ReadMany(fid,outputName[iout],Name0,/Mstar,/pos,$
        	    	   ColorName='SDSS_g-SDSS_r')
  sn=where((data.mstar gt 1e7)and(data.ct ne 1000),ngal)
  outi=replicate(datai,ngal)
  outi.nodeVirialRadius=data.nodeVirialRadius[sn]
  outi.nodeIsIsolated=data.nodeIsIsolated[sn]
  outi.mstar=data.mstar[sn]
  outi.pos=data.pos[*,sn]
  outi.ct=data.ct[sn]
  outi.weight=fltarr(ngal)+weight0[3,isim]
  out=[out,outi]
endfor
n=n_elements(out)-1
out=out[1:n]
return,out
end
