;bining for the stellar mass function for lg M_star between [8,12]

m_low=1.0e8
m_high=1e12
;nbinPerDecade=4
nbin=17
log10=1
extent=alog10(m_high/m_low)
;nbin=nbinPerDecade*extent+1
dbin=extent/(nbin-1)
;d2=dbin/2
lgm=findgen(nbin)*dbin+alog10(m_low)

dir0='SMF/'
