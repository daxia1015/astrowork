    out=replicate(outi,nhalo)
    out.treeID=itree
    out.NodeID=halosn
    out.HostID=FirstHaloInFOFgroup
    out.Descendant=Descendant
    out.NodeMass=m200 
    out.Redshift=zlist[snapnum]
    out.HalfMassRadius=HalfMassRadius
    pos=transpose(pos) & velocity=transpose(velocity) & spin=transpose(spin) 
    out.PX=pos[*,0] & out.PY=pos[*,1] & out.PZ=pos[*,2]
    out.VX=velocity[*,0] & out.VY=velocity[*,1] & out.VZ=velocity[*,2] 
    out.SX=spin[*,0] & out.SY=spin[*,1] & out.SZ=spin[*,2] 
    if itree eq 0 then begin
      openw,lun1,outname,/get_lun
    endif else begin
      openu,lun1,outname,/get_lun
      skip_lun,lun1,/eof
    endelse;& help,out,/struct
    printf,lun1,out,format='(4i10,12e15.6e3)' 
    free_lun,lun1
