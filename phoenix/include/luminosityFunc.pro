;bining for the stellar mass function for lg M_star between [8,12]

L_low=1.0e8
L_high=1.0e11
nbin=13
;nbinPerDecade=4
log10=1
extent=alog10(L_high/L_low)
;nbin=nbinPerDecade*extent+1
dbin=extent/(nbin-1)
;d2=dbin/
lgL=findgen(nbin)*dbin+alog10(L_low)

;dir0='SMF/'
