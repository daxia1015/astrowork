nsim=n_elements(!treefile)
nhost=1
nlev=n_elements(ilev_v)

@include/halofunc_bin.pro
@include/halofunc_ccv.pro

mfunc=dblarr(nbin.m,nlev,nhost,nsim)
vfunc=dblarr(nbin.v,nlev,nhost,nsim) 
rfunc=dblarr(nbin.r,nmc-1,nlev,nhost,nsim)
cmfunc=mfunc & cvfunc=vfunc & crfunc=rfunc

nvalid=intarr(nhost,nsim)
hostv=lonarr(nlev,nhost,nsim) & Rvirv=fltarr(nlev,nhost,nsim)
