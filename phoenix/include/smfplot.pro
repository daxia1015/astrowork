Mstar='M'+starsymbol()
Msun='M'+sunsymbol()
;Mstar='M!d!4'+string(42b)+'!x!n'
xtit='log['+Mstar+' / (h!u-2!n'+Msun+')]'
ytit='!mf!x('+Mstar+') / (h!u3!nMpc!u-3!nlog!u-1!n'+Mstar+')'
xr=[8.01,12.2]
yr={field:[1.01e-9,9e-1],cluster:[1.01e-7,9.9e2],mid:[1.01e-8,9.9e1],low:[1.01e-9,9.9e-1],all:[1.01e-9,9.9e2]}
zname='z~'+!smf.zname
lts=[0,2,1]

li=SMF_li2009()

fname=!galacticus.path+'stellarMassFunction.hdf5'

