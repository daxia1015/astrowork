  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow

  phi    =SchechterFunction(lgm,phis[i],    	   alpha[i],        	lgms[i]+2*alog10(h))
  phi_up =SchechterFunction(lgm,phis[i]+phisErr[i],alpha[i]-alphaErr[i],lgms[i]+lgmsErr[i]+2*alog10(h))
  phi_low=SchechterFunction(lgm,phis[i]-phisErr[i],alpha[i]+alphaErr[i],lgms[i]+lgmsErr[i]-2*alog10(h))
  smf={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
