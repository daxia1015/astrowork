@include/halofunc_ccv.pro

nmp=6
mfun=dblarr(nbin.mun,nmp)
mfev=dblarr(nbin.mev,nmp) & mfev_err=mfev
tfunc=dblarr(nbin.t,nmp)
rfunc_zc=dblarr(nbin.r,nmp,nzc) & rfunc_zc_ntot=dblarr(nmp,nzc)
rfunc_mci=dblarr(nbin.r,nmp,nmc) & rfunc_mci_ntot=dblarr(nmp,nmc) 
rfunc_mcii=rfunc_mci & rfunc_mcii_ntot=rfunc_mci_ntot
rfunc_mcf=rfunc_mci & rfunc_mcf_ntot=rfunc_mci_ntot
rmfunc=dblarr(nbin.rm,nmp)
efunc=dblarr(nbin.e,nmp) ;& efunci=efunc
jfunc=dblarr(nbin.j,nmp)
rfunci=dblarr(nbin.ri,nmp) & rfunci_ntot=dblarr(nmp)

