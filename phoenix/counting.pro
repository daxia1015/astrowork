function count_nhalo,level=level
;if(level ne '2')or(level ne '4')then message,'only level 2 or 4 is allowed.'
nhalo=replicate({name:'',n0:0L,nc0:0L,ntot:0L,nctot:0L},10)

ii=1 & i0=0L & i1=0L
for isim=0,19 do begin
  if strmid(!Phoenix.TreeName[isim],3,1) ne level then continue
  tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,/readhead)
;  print,!Phoenix.TreeName[isim],nhalos[0],nhalos[0]+i0,totnhalo,totnhalo+i1
  nhalo[ii].name=!Phoenix.TreeName[isim]
  nhalo[ii].n0=nhalos[0]
  nhalo[ii].nc0=nhalos[0]+i0
  nhalo[ii].ntot=totnhalo
  nhalo[ii].nctot=totnhalo+i1
  i0=i0+nhalos[0]
  i1=i1+totnhalo
  ii++
endfor
return,nhalo
end
;==================================================================
;==================================================================
pro count_halo,nn,write=write
nfile=n_elements(!Phoenix.TreeFile)
nn=lonarr(!Phoenix.n_snap,nfile)
fname='counting/halonumber'
if keyword_set(write) then begin
  for ifile=0,nfile-1 do begin
    breaktree,!Phoenix.TreeFile[ifile],snapnum,tag=['snapnum']
    for i=0,!Phoenix.n_snap-1 do begin
      sn=where((snapnum eq i),n)
      nn[i,ifile]=n
    endfor
  endfor
  openw,lun,fname,/get_lun
  writeu,lun,nn
  free_lun,lun
endif else begin
  openr,lun,fname,/get_lun
  readu,lun,nn
  free_lun,lun
endelse
end
;==================================================================
;==================================================================

pro plot_nhaloz
alist=snaplist(n_snap)
count_halo,nn
device,filename='counting/nhaloz.ps',/color,/encapsul,xsize=18,ysize=15
multiplot,[3,3],mxtitle='a=1/(1+z)',mytitle='n(z)/n(z=0)'
for i=0,8 do begin
  plot,findgen(100)*0.01,findgen(100)*0.02,/nodata,$
  xrange=[0.01,1],yrange=[0.01,2]
  for j=0,1 do begin
    if i eq 0 then isim=2*j+1 else isim=j+i*2+2	  
    oplot,alist,nn[*,isim]/double(nn[n_snap-1,isim]),linestyle=j
  endfor
  multiplot
endfor
multiplot,/default
end

