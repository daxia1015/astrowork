function stellarHalo,isim
;print,'computing stellar-halo relation...'
;gid=h5g_create(fidr,'stellarHaloRelation')

;lgM=massBinning(1e10,1e14,10,nbin)
Name0=['nodeIsIsolated','nodeMass']
;h5write_dataset,gid,10^lgm,'HaloMass'  ;M_sun
;stellar=fltarr(nbin,3,!Phoenix.nsim)
;for isim=1,19 do begin
  fid=h5f_open(!galacticus.treeEv[isim])
  data=nodeData_ReadMany(fid,'Output55',Name0,/mstar)
  
;  for i=0,2 do begin
;    if i eq 2 then sn1=where(data.mstar gt 0) else begin
;      sn1=where((data.nodeIsIsolated eq i)and(data.mstar gt 0))
;    endelse
;    lgMhalo=alog10(data.nodeMass[sn1])
;    lgMstar=alog10(data.Mstar[sn1]) 
;    lgMstar=interpol(lgMstar,lgMhalo,lgM)
;    print,lgMstar
;    stellar[*,i,isim]=10^lgMstar
;  endfor  
;endfor
;h5write_dataset,gid,stellar,'StellarMass'  ;M_sun

;h5g_close,gid
return,data
end
