;====================================================================
;====================================================================
pro smflf_write,fidr
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
print,'computing stellar mass function...'
gidSMF=h5g_create(fidr,'stellarMassFunction')
gidSate=h5g_create(fidr,'SatelliteStatistics')
phiAll=dblarr(!smf.all.nbin,!Phoenix.nsim,3,3)
;first 3: all, red, blue
;second 3: all, central, satellite
phiR200=dblarr(!smf.R200.nbin,!Phoenix.nsim,3)
;first 3:  all, red, blue
phiR206=dblarr(!smf.R200.nbin,!Phoenix.nsim,3)
;first 3:  all, red, blue
gidLF=h5g_create(fidr,'luminosityFunction')
LphiAll =dblarr(!lf.nbin,!Phoenix.nsim,3,3)
;first 3: all, red, blue
;second 3: all, central, satellite
LphiR200=dblarr(!lf.nbin,!Phoenix.nsim,3)
;first 3:  all, red, blue

fidVW=h5f_open(!galacticus.path+'SelfContainedTree.hdf5')
W0=h5read_dataset(fidVW,'VolumeWeight')
h5f_close,fidVW
h5write_dataset,gidSMF,W0,'VolumeWeight'
h5write_dataset,gidLF, W0,'VolumeWeight'

Name0=['nodeVirialRadius','nodeIsIsolated']
for isim=1,19 do begin
  fid=h5f_open(!galacticus.treeEv[isim])
  data=nodeData_ReadMany(fid,'Output25',Name0,/Mstar,/pos,$
       lumiName='SDSS_r',ColorName=['SDSS_g-SDSS_r'])
;  data.nodeMass=alog10(data.nodeMass*h0)  ;h^-1 M_sun
  data.Mstar=data.Mstar*h0^2   ; h^-2 M_sun
  data.nodeVirialRadius=data.nodeVirialRadius*h0  ;h^-1 Mpc
  data.pos=data.pos*h0  ;h^-1 Mpc
  mag=Luminosity2Magnitude(data.lumi)
  data.lumi=data.lumi*h0^2/Lsun    ;h^-2 L_sun
  csn=GalaxyIsBlue(data.ct,mag,h0)
  data=create_struct('IsBlue',csn,data)
  W=W0[3,isim]
  ;==========================================
  smflf_all,isim,data,W,phiAll,LphiAll  
  ;==========================================
  smflf_R200,isim,data,phiR200,phiR206,LphiR200
  ;==========================================
  h5f_close,fid
endfor

lgm=histo(findgen(100)+1,nbin=!smf.nbin,xmin=!smf.low,$
    	  xmax=!smf.high,/log10,/get_locations)
h5write_dataset,gidSMF,lgm,'lgm'
lgL=histo(findgen(100)+1,nbin=!lf.nbin,xmin=!lf.low,$
    	  xmax=!lf.high,/log10,/get_locations)
h5write_dataset,gidLF,lgL,'lgL'
;print,lgm
h5write_dataset,gidSMF,phiAll,'phiAll'
h5write_dataset,gidSMF,phiR200,'phiR200'
h5write_dataset,gidSMF,phiR206,'phiR206'
h5write_dataset,gidLF,LphiAll,'LphiAll'
h5write_dataset,gidLF,LphiR200,'LphiR200'

smf_moment_write,gidSMF,gidLF,phiAll,phiR200,phiR206,LphiAll,LphiR200

;smf_Catalog,gidSMF,gidSate

h5g_close,gidSMF
h5g_close,gidLF
h5g_close,gidSate

end
;====================================================================
;====================================================================
