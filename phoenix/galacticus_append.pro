pro Galacticus_append_pos
fname=!galacticus.treeEv[3]
OutputList,fname,nout,OutputName=OutputName,tlist=tlist,zlist=zlist
dataname=['positionX','positionY','positionZ','velocityX','velocityY',$
    	  'velocityZ','diskStellarMass','spheroidStellarMass','nodeMass',$
	  'nodeVirialRadius']
for isim=5,19,2 do begin
  fname=!galacticus.treeEv[isim]
  print,fname
;  LocalDensity_write,isim,nout,outputName
  galaxy_pair_write,isim,nout,outputName,zlist,tlist
endfor

end
