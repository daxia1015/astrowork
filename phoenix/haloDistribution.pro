pro haloMF_moment,gid,nbin,nhv,phiHalo
mi=histo_moment(/get_unit)
mom  =replicate(mi,nbin,2,!smf.nz,!smf.nr)
momlg=replicate(mi,nbin,2,!smf.nz,!smf.nr)
;nbin    :number of bin
;nsim	 :number of resolution levels
;first  4: z~0.0, 0.3, 1.0, 3.0
;Second 4: all, (0,R200], [R200,3R200], [R200,infinite)
lgmV=fltarr(nbin,!smf.nz,!smf.nr)
cal=[1e-6,1e-4,1e-5,1e-6]
for ilev=0,1 do begin
  iv=!Phoenix.isimv[*,ilev]
  for iz=0,!smf.nz-1 do begin
    for ir=0,!smf.nr-1 do begin
      lgmV[*,iz,ir]=phiHalo[*,1,iz,ir].lgm
      phi=phiHalo[*,iv,iz,ir].phi
      sn1=where(phi le 0,n1)
      if n1 ne 0 then phi[sn1]=cal[ir]
      mom[*,ilev,iz,ir]=histo_moment(phi,nbin)
      momlg[*,ilev,iz,ir]=histo_moment(phi,nbin,/log10)
    endfor
  endfor
endfor
h5write_manydata,gid,['moment_phiHalo','moment_lg_phiHalo','moment_lgm'],$
    	    	 mom,momlg,lgmV
end
;====================================================================
;==================================================================
pro haloMF_counting,all,gid,weight0,zlist
nbin=19 
;  z~  0.0,   0.3,   1.0
Mmax=[[4e13,  5e13,  6e13],$  ;whole sky
      [1e13,  9e12,  7e12],$  ;<R200
      [1e13,  2e13,  9e12],$  ;[R200,3R200]
      [3e13,  4e13,  3e13]]   ;>3R200 
Mmin=1.5e8
hi={lgm:0.0,phi:0.0d}
phiHalo=replicate(hi,nbin,!Phoenix.nsim,!smf.nz,!smf.nr)
nhv=lonarr(!Phoenix.nsim,!smf.nz,!smf.nr)
;nbin    :number of bin
;nsim	 :number of simulations
;first  3: z~0.0, 0.3, 1.0
;Second 4: all, (0,R200], [R200,3R200], [R200,infinite)
level=strmid(!Phoenix.TreeName,3,1)
for isim=1,19 do begin
  print,'isim=',isim
  fname=!Phoenix.TreeFile[isim]
  if level[isim] eq '4' then WC=min(weight0[*,isim]) else WC=max(weight0[*,isim])
  tree=assoctree_ph(fname,lunx,ntree,totnhalo,nhalos)
  nhalo=nhalos[0] ;& print,'nhalo=',nhalo
  if all then breaktree_ph,fname,tree,totnhalo,snapnum,tag='snapnum',tree0=tree0 else $
	breaktree_ph,fname,tree,totnhalo,snapnum,tag='snapnum',tree0=tree0,halosn=lindgen(nhalo)
  for iz=0,!smf.nz-1 do begin
    isnap=!smf.isnapv[iz]
    sn1=where(snapNum eq isnap)
    breaktree_ph,fname,tree,totnhalo,mhalo,pos,tag=['len','pos'],tree0=tree0,halosn=sn1
    mhalo=temporary(mhalo)*!PhoenixTable.mp[isim]
    M200=max(mhalo,center) 
    znow=zlist[isnap]
    R200=R_virial(M200,znow) ;& print,M200,R200  ;h^-1 Mpc
    V200=4*!pi*R200^3/3.0	    ;h^-3 Mpc^3
    Wr=[WC,1.0/V200,1.0/(26*V200),1.0/(1.0/WC-27*V200)]
    RC=pair_distance(pos[*,center],pos,zlist=znow)/R200  
;    print,min(R),max(R)
    for ir=0,!smf.nr-1 do begin
;     print,iz,ir
      rsn=regionOFgalaxy(RC,!smf.ccR,ir=ir)
      sn=where(rsn,nh)
      nhv[isim,iz,ir]=nh
      if nh lt nbin then continue
      mir=mhalo[sn]
      h=histo(mir,nbin=nbin,xmin=Mmin,xmax=Mmax[iz,ir],dbin=dlgm,$
      	      weight=Wr[ir],/log10,locations=lgm)  
      phi=h/dlgm ;/alog(10)
      phiHalo[*,isim,iz,ir].lgm=lgm
      phiHalo[*,isim,iz,ir].phi=phi
    endfor
  endfor
  free_lun,lunx
endfor
h5write_dataset,gid,phiHalo,'phiHalo'
h5write_dataset,gid,nhv,'haloNumber'
;==================================================
haloMF_moment,gid,nbin,nhv,phiHalo 
end
;====================================================================
;==================================================================
pro haloDistribution,weight0
print,' computing halo mass function
fid=h5f_create(!galacticus.path+'haloMassFunction.hdf5')
help,weight0
h5write_dataset,fid,Weight0,'VolumeWeight'

PhoenixTable
zlist=snaplist(/redshift)
gname=['tree0','alltree']
for all=0,0 do begin
  gid=h5g_create(fid,gname[all])
  haloMF_counting,all,gid,weight0,zlist
  h5g_close,gid
endfor
h5f_close,fid
end
