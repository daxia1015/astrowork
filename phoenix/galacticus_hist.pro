;==================================================================
;==================================================================
function hist_fE,fname,outputName,botc,nbin
name0=['diskStellarMass','spheroidStellarMass','nodeVirialRadius']
fid=h5f_open(fname)
Data=nodeData_ReadMany(fid,outputName,name0,/pos)
h5f_close,fid
R200=max(data.nodeVirialRadius,sn1)
center=data.pos[*,sn1]
R=pair_distance(data.pos,center)/R200
tot=data.diskStellarMass+data.spheroidStellarMass
sn=where((R gt 0)and(tot gt 0))
bot=data.spheroidStellarMass[sn]/tot[sn]
lgR0=alog10(R[sn])

lgRmin=min(lgR0)*1.1 & lgRmax=max(lgR0)*0.9
dbin=(lgRmax-lgRmin)/(nbin-1) & d2=dbin/2
lgR=findgen(nbin)*dbin+lgRmin
fE=fltarr(nbin)
for i=0,nbin-1 do begin
  print,i
  sn=where((lgR0 ge lgR[i]-d2)and(lgR0 lt lgR[i]+d2),n)
  sn1=where(bot[sn] gt 0.5,n1)
  fE[i]=float(n1)/float(n)
endfor
return,{fE:fE,ccR:10^lgR}

end
;==================================================================
;==================================================================
function hist_mdr,fname,OutputName,nbin,R,bot
if n_elements(nbin) eq 0 then nbin=21
name0=['diskStellarMass','spheroidStellarMass','nodeVirialRadius']
fid=h5f_open(fname)
Data=nodeData_ReadMany(fid,outputName,name0,/pos)
R200=max(data.nodeVirialRadius,sn1)
center=data.pos[*,sn1]
R=pair_distance(data.pos,center)/R200
tot=data.diskStellarMass+data.spheroidStellarMass

sn=where((R gt 0)and(tot gt 0))
bot=data.spheroidStellarMass[sn]/tot[sn]
R=R[sn]
print,min(bot),max(bot),min(R),max(R)
h=histo2D(R,bot,xmin=min(R)*1.2,xmax=max(R)*0.9,ymin=0.02,ymax=0.98,$
    	  nbinx=nbin,nbiny=nbin,locationx=xh,locationy=yh,$
	  /xlog)
mdr={h:h,R:xh,bot:yh}

h5f_close,fid
return,mdr
end
;==================================================================
;==================================================================
function hist_color,fname,OutputName,nbin
if n_elements(nbin) eq 0 then nbin=21
;out={h:dblarr(nbin,nbin),Mr:dblarr(nbin),gr:dblarr(nbin)}
fid=h5f_open(fname)
name0='nodeIsIsolated'
Data=nodeData_ReadMany(fid,outputName,name0,magName='SDSS_r',$
     	    	       ColorName='SDSS_g-SDSS_r')
sn=where(data.ct ne 1000)
mr=data.mag[sn] & gr=data.ct[sn]

print,min(mr),max(mr),min(gr),max(gr)

h=histo2D(Mr,gr,xmin=min(mr)+1,xmax=-13,ymin=min(gr)*1.05,$
    	  ymax=max(gr)*0.95,nbinx=nbin,nbiny=nbin,locationx=xh,$
	  locationy=yh)
colorh={h:h/total(h),Mr:xh,gr:yh}

h5f_close,fid
return,colorh
end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
pro Galacticus_test
fname=!galacticus.treeEv[21]
;fname='/home/jlgan/v0.9.1/test091.hdf5'
OutputList,fname,nstep,OutputName=OutputName,alist=alist,tlist=tlist

for isim=21,21 do begin
  fname=!galacticus.treeEv[isim]
  print,fname
  n_node=Galacticus_nNode(fname)
  for iout=0,26 do begin
;    print,iout,n_node[iout]
  ;  Galacticus_specific,fname,outputName,stellar=stellar
    Galacticus_Outputs,fname,outIndex=iout,nodeData=nodeData,$
      	dataname=['diskStellarMass','spheroidStellarMass','blackHoleMass','nodeMass']
;    disk=nodeData.diskStellarMass/nodeData.nodeMass
    spheroid=nodeData.spheroidStellarMass/nodeData.nodeMass
;    BH=nodeData.blackHoleMass/nodeData.nodeMass
    sn1=uniq(spheroid,sort(spheroid))
    n1=n_elements(sn1)
    sn2=sort(nodeData.nodeMass)
    n2=n_elements(sn2)
    print,outputName[iout],nodeData.nodeMass[sn1[n1-5:n1-1]]

  endfor
endfor
end
