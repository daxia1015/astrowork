function groupOFgalaxy,isolate,ig=ig
;ig=0,1,2: all, central, satellite
sn=isolate ne ig-1
return,sn
end
;==================================================================
;==================================================================
function sqOFgalaxy,sfr,mstar,sSFRc,isq=isq
;isq=0,1,2: all, star-forming, quiescent
case isq of
  0:sn=mstar gt 0
  1:sn=SFR gt sSFRc*mstar
  2:sn=SFR le sSFRc*mstar
  else:message,'out of range'
endcase
return,sn
end
;==================================================================
;==================================================================
;This function identify the type of galaxies color.
;The galaxies can be divided into red and blue sequences using a line
;with slope -0.01 that passed through g-r=0.65 at M_r=-21. The line is
;that: g-r=-0.01M_r+0.44
;Lin et al. 2008; Patton et al. 2011
;There may be other methods to identify the galaxy type.

;cgr: the color g-r
;Mr: the absolute magnitude in r band
;results: 1:blue, 0:red, -1:not galaxy

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
 
function colorOFgalaxy,cgr,Mr,ic=ic
;ic=0,1,2: all, blue, red
cgr_crit=-0.01*Mr+0.44
case ic of
  0:sn=(cgr ne 1000)and(Mr ne 1000)
  1:sn=(cgr ne 1000)and(Mr ne 1000)and(cgr lt cgr_crit)
  2:sn=(cgr ne 1000)and(Mr ne 1000)and(cgr ge cgr_crit)
  else:message,'out of range'
endcase
return,sn
end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
;This function identify the type of galaxies morphology (unstable).
;The galaxies is late type if the disk mass is greater than the spheroid mass.
;There may be other methods to identify the galaxy type.
;disk: disk mass
;spheroid: spheroid mass

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
function typeOFgalaxy,disk,spheroid,bodc,it=it
;it=0,1,2,3,4: all, Late, Early, lenticulars(S0), Ellipitcal
case it of
  0:sn=(disk gt 0)or(spheroid gt 0)
  1:sn=bodc*disk gt spheroid
  2:sn=bodc*disk lt spheroid
  3:sn=(disk gt 0)and(spheroid gt 0)and(bodc*disk lt spheroid)
  4:sn=(disk eq 0)and(spheroid gt 0)
  else:message,'out of range'
endcase
return,sn
end
;==================================================================
;==================================================================
function regionOFgalaxy,RC,ccR,ir=ir
;ir=0,1,2,3: all, (0,R200], [R200,3R200], [R200,infinite)
if ir eq 0 then sn=RC ge 0 else $
  sn=(RC ge ccR[ir-1])and(RC le ccR[ir])
return,sn
end

;==================================================================
;==================================================================
;function GalaxyIsStarForming,SFR,Mstar,z
;Moustakas et al. 2013 ApJ 767 50
;isSF=fix(SFR)
;isSF[*]=-1
;sn=where((SFR gt 0)and(Mstar gt 0))
;lgSFRmin= -0.49+0.65*(alog10(Mstar[sn])-10)+1.07*(z-0.1)

;isSF[sn]=alog10(SFR[sn]) ge lgSFRmin
;return,isSF
;end
;==================================================================
;==================================================================
