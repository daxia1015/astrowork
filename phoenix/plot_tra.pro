pro tra_end_plot,isim
level=float(strmid(!treefile[isim],13,1))
tra=twobody_read(!twobody.fnametra[isim],ngroup,totnstep,nsteps,IDs)
@include/twobody_nsteps.pro
mf=tra[step1].msub/tra[step0].msub & print,min(mf),max(mf)
rf=tra[step1].d/tra[step1].Rhost  & print,min(rf),max(rf)
lf=tra[step1].angl/tra[step0].angl  & print,min(lf),max(lf)
idfix=fix(IDs)
;sn2=where(nsteps gt 2,n2) & print,ngroup-n2
;mf=mf[sn2] & rf=rf[sn2] & lf=lf[sn2]
;idfix=idfix[sn2]

nbin=11 & rangex=[[8e-3,20],[5e-2,9],[2e-2,20]]
xtit=['m!df!n/m!di!n','r!df!n/R!dvir!n','j!df!n/j!di!n']
char=1.1
device,filename='twobody/traf.ps',/color,/encapsul,xsize=10,ysize=20
multiplot,[1,3],ygap=0.04,/doxaxis
!p.charsize=char & !x.charsize=char & !y.charsize=char
x=10^(-findgen(100)*0.05) & y=findgen(100)*10+1
for ifig=0,2 do begin
  plot,x,y,/xlog,/ylog,/nodata,xrange=[rangex[0,ifig],rangex[1,ifig]],$
  yrange=[2,6*10^(7-level)],xtitle=xtit[ifig],ytitle='N'
  for id=1,4 do begin
    sn1=where(idfix eq  id)
    case ifig of 
      0:array=mf[sn1]
      1:array=rf[sn1]
      2:array=lf[sn1]
      else:message,'error'
    endcase
    h=histo(array,rangex[0,ifig],rangex[1,ifig],nbin,locations=hx,/lg)
    oplot,hx,h,linestyle=id-1,color=!myct.c4[id-1]
  endfor
  multiplot,/doxaxis
endfor
multiplot,/default
xyouts,0.8,0.95,!treename[isim],/normal
;==================================================================
;==================================================================
device,filename='twobody/jf.ps',/color,/encapsul,xsize=10,ysize=20
multiplot,[1,3],ygap=0.04,/doxaxis
!p.charsize=char & !x.charsize=char & !y.charsize=char
x=10^(-findgen(100)*0.05)
sn1=where(idfix eq  4)

  plot,mf[sn1],lf[sn1],xrange=rangex[*,0],yrange=rangex[*,2],psym=3,$
  /xlog,/ylog,xtitle=xtit[0],ytitle=xtit[2]
  multiplot,/doxaxis
  plot,rf[sn1],lf[sn1],xrange=rangex[*,1],yrange=rangex[*,2],psym=3,$
  /xlog,/ylog,xtitle=xtit[1],ytitle=xtit[2]
  multiplot,/doxaxis
  plot,rf[sn1],mf[sn1],xrange=rangex[*,1],yrange=rangex[*,0],psym=3,$
  /xlog,/ylog,xtitle=xtit[1],ytitle=xtit[0]
multiplot,/default
xyouts,0.8,0.95,!treename[isim],/normal
end
