;find the formation time
close,/all
new  =  {$
               uLen                 :0L, $
               elen                 :0L, $
               dis                  :0.0,$
               zz                   :0.0 $
        }
HaloStruct = {$
               Descendant           : 0L, $
               FirstProgenitor      : 0L, $
               NextProgenitor       : 0L, $
               FirstHaloInFOFgroup  : 0L, $
               NextHaloInFOFgroup   : 0L, $
               Len                  : 0L, $
               M_Mean200            : 0.0,$
               M200                 : 0.0,$
               M_TopHat             : 0.0,$
               Pos                  : fltarr(3), $
               Vel                  : fltarr(3), $
               VelDisp              : 0.0, $
               Vmax                 : 0.0, $
               Spin                 : fltarr(3), $
               MostBoundID          : lon64arr(1), $
               SnapNum              : 0L, $
               FileNr               : 0L, $
               SubhaloIndex         : 0L, $
               SubhalfMass          : 0.0 $
             }
zz=read_ascii('outputlist')
zz=1./zz.(0)-1
num=71
G = 43007.1        ;gravitational constant
For FileNr=0,0 do begin   ;why loop?
fname='treedata/trees_sf1_071.0'
fname=strcompress(fname,/remove_all)
openr,1, fname
Ntrees = 0L
TotNHalos = 0L
readu,1,Ntrees,TotNhalos
TreeNHalos = lonarr(Ntrees)
readu,1,TreeNhalos ; number of trees in one halo
for tr=0L, 0 do begin  ; read different tree
	Tree = replicate(HaloStruct, TreeNhalos(tr));every halo's formation in the tree
	readu,1,Tree

	i67=where(tree.snapnum eq Num,c67)  
	if c67 eq 0 then continue
	ii=where(tree(i67).m200 ge 100 ,c67)  ;?why m200 ge 100
;	ii=where(tree(i67).m200 eq max(tree(i67).m200),c67)	

	if c67 eq 0 then continue
	i67=i67(ii)
    for it=0L,c67-1 do begin   ;more than one host halo
	mainb=lonarr(Num+1)
	mbz=fltarr(Num+1)
	ind=i67(it)
        first=long(ind)   ;first halo?
      
        for i=0,Num-1 do begin ;find the main branch
                istep=tree(first).snapnum
                mainb(istep)=first
                mbz(i)=istep
                first=tree(first).firstprogenitor
                if first eq -1 then begin
                        break
                endif
        endfor ;i
;print,mainb(step1),tree(mainb(step1)).m200
;i5=where(tree.snapnum eq step1,cc)
;if cc eq 0 then print,':('
;ind=max(tree(i5).m200 ,ip)
;print,ind,i5(ip)
;stop
rho_c0=27.75   ;
omega_m=0.25
omega_l=0.75

        xc = tree[ind].pos(0) ; the position of the centre
        yc = tree[ind].pos(1)
        zc = tree[ind].pos(2)
        m200=tree(ind).m200
        rho_c=rho_c0*(omega_m+omega_l/(1+zz(num))^3)
        r200 = (3.0*m200/4.0/!pi/200.0/rho_c)^0.3333333
    	print,'r200',r200,ind
        id = where(tree.firsthaloinfofgroup eq ind and tree.snapnum eq Num ,is)
     	print,is
        rr = (tree(id).pos(0) - xc)^2 + (tree(id).pos(1) - yc)^2 + (tree(id).pos(2) - zc)^2
        rr = sqrt(rr)
        ic = where(rr lt r200 and rr ne 0,is) ; subhalo within r200 in the same FoF group at z=0
	print,is
	
        id = id(ic)
	count=lonarr(is)
;;;;last infall;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        newdata = replicate(new,is) ;is :subhalo within r200
        countnum=0L
        count=lonarr(is)
;>>trace all subhaloes within virial radius in the branch of FirstHaloInFoFgroup
;till 	
       for jj=0L,is-1 do begin
            first = id[jj]
            idir = first
            i=1
            repeat begin
                nb = first
                first=tree[nb].firstprogenitor
                if( first eq -1) then begin
                newdata[jj].ulen = tree[nb].len
                newdata[jj].elen = tree[idir].len
;                newdata[jj].dis = rr[idir]
                newdata[jj].zz = zz[tree(nb).snapnum]

                   break
                endif
                if( first eq tree(first).firsthaloinfofgroup) then begin
                newdata[jj].ulen = tree[first].len
                newdata[jj].elen = tree[idir].len
;                newdata[jj].dis = rr[idir]
                newdata[jj].zz = zz[tree(first).snapnum]
		count(countnum)=jj
		countnum++
                    break       ;
                endif
    	    	i=tree(first).snapnum
          endrep until i eq 1
       endfor;jj
       
       
;trace all subhaloes within virial radius in the branch of FirstProgenitor
;till there is no progenitor
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;maximum mass;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	countnum=0L
	count=lonarr(is)
        newdata = replicate(new,is) ;is : halo withen r200 
        for jj = 0, is-1  do begin
            first = id[jj]
            idir = first
            firstplen=lonarr(Num+1)
            firstpz=fltarr(Num+1)
            progs=lonarr(Num+1)
            firstplen(0)=tree(idir).len
            progs(0)=idir

            repeat begin
                nb = first
                first=tree[nb].firstprogenitor  ;trace back
                if first eq -1 then break
		i=tree(first).snapnum
                firstplen(i)=tree(first).len
                firstpz(i)=zz(i)
                progs(i)=first
            endrep until i eq 0
            newdata(jj).ulen=max(firstplen,ip)
            newdata(jj).elen=tree(idir).len
            newdata(jj).zz=firstpz(ip)
;            newdata(jj).dis=rr(idir)
	    if ip ne i then begin
	    count(countnum)=jj
	    countnum++	
	    endif
        endfor;jj
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    endfor
endfor;tr
;print,m200,ftime,mainb,zz(mbz)
print,':)',fileNr
endfor;FileNr
end
	


