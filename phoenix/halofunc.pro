;-----Simulation------=======================================
function halofunc_pre,nbin,ml,mh,dbin,outx
common astronomy;,ro_c,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
!key.hostz=1
xm=outx.m & xv=outx.v & xr=outx.r 
xp=xm/0.8
y1=0.21/(xp^0.8*exp(6.283*xp^3))  ;dN(<m)/dln(m/M), unevolved, fitted by Giocoli 2007
mfun={giocoli:y1}

y2=0.0064/xm ;/alog(10)^2   ;-1 ;evolved, fitted by Diemand 2007a
y3=0.869/xm^0.869/10^1.869   ;evolved, fited by Gao 2004???,  -0.869
;y6=alog10(3.26e-5*2.52e7^1.9)-0.9*(xev+alog10(mphalo))  ;evolved, fitted by Springel 2008, -0.9
y6=0.02596/xm^0.9  ;evolved, fitted by Springel 2008, -0.9
y7=10^(-2.05)/xm^0.9/exp(xm^2/0.16^2)         ;evolved, Angulo et al. arxiv:0810.2177, -0.9 
;---begin evolved mass function, Giocoli 2008
out=fltarr(6,47)
openr,lun,'halofunc/plotdata/mfunc_sim_Giocoli.dat',/get_lun
  skip_lun,lun,9,/lines
  readf,lun,out,format='(6a12)'
  if eof(lun) then message,'end of mfunc_sim_Giocoli.dat',/continue
free_lun,lun
n=n_elements(xm) & xp=alog10(xm)
y4=dblarr(n,5)  ;M_host=10^(11,12,13,14,15) for row=0,1,2,3,4
;for i=0,4 do y4[*,i]=interpol(out[5-i,*],out[0,*],xp)
for i=0,4 do y4[*,i]=spline(out[0,*],out[5-i,*],xp)
y4=10^y4
;----end evolved mass function, Giocoli  2008

;----evolevd mass function, Ludlow 2008, with unorthodox orbits---
x_l=[-5.3,-5,-4.5,-4,-3.5,-3,-2.5,-2.25]
y_l=[3.06,2.87,2.48,1.97,1.52,1.05,0.43,0.1]  ;8 elements
y5=double(-deriv(x_l,y_l)*10^y_l)
;===all mass function of simulation is here:::
mfev={diemand:y2,gao:y3,giocoli:y4,ludlow:y5,springel:y6,angulo:y7}
mfunc={un:mfun,ev:mfev}
;===========================================================
;-==--begin subhalos radial distribution of simulation----==
mcv=[0.,1e-6,1e-5,1e-4,1e-3,1] & nmc=n_elements(mcv)-1
y1=dblarr(nbin.r,nmc)
out=read_ascii('halofunc/plotdata/vltwosubs.txt',comment_symbol='#',count=k)
out=out.(0)  ;& help,out
;out=reform(out,15,k) ;& print,out
snr=where(out[1,*] gt 0)
lgrv_vl=alog10(out[1,snr]/389.) & mvev=out[5,snr]/1.77e12
for mc=0,nmc-1 do begin
  sn=where(mvev ge mcv[mc],count)
  if mc ge 2 then sn=where((mvev ge mcv[mc])and(mvev lt mcv[mc+1]),count)
;  print,mc,count
  rv=lgrv_vl[sn]
  h=histo(rv,ml.r,mh.r,nbin.r,/cumulative)
  y1[*,mc]=h/h[nbin.r-1]     ;Diemand 2006
endfor
y5=12.*xr^3/(1+11.*xr^2)
if !key.hostz then chost=10. 
y2=(1+0.244*chost)*xr^2.75/(1+0.244*chost*xr^2) ; Gao 2004
a=0.678 & r2=0.81 & r200=245.76 & r50=433.48 
y3=igamma(3/a,2*(xr/r2)^a/a)/igamma(3/a,2*(1/r2)^a/a)
r2=r2*r200/r50
y4=igamma(3/a,2*(xr/r2)^a/a)/igamma(3/a,2*(1/r2)^a/a)
y6=gx(xr*chost)/gx(chost)
;=======Milky-Way satellites data
mphalo=177.  ;10^10M_sun/h
rpvir=r_vir(mphalo,0.)*1e3 & print,'rpvir=',rpvir,mphalo
out1=read_satellite('halofunc/plotdata/MW_satellite.dat',count=nmw)
rv=alog10(out1.dgc/rpvir)
rfuncmw=histo(rv,ml.r,mh.r,nbin.r,/cumulative)
y7=rfuncmw/rfuncmw[nbin.r-1]
;print,rv,ml.r,mh.r,nbin.r,y7
rfunc={diemand:y1,gao:y2,einasto200:y3,einasto50:y4,$
       madau:y5,DM:y6,MW:y7}
;====end subhalos radial distribution of simulation==============
;=====begin subhalos' mass radial distribution of simulation-------
;y3=sub_profile(lgrv_vl,out[5,snr],ml.rm,mh.rm,nbin.rm,dbin.rm,$
;       	       1.77e12,389.,!m.chost,/nfw,/rlg10)
;lnxr=alog(xrm)
;y4=exp(-1.31+0.87*lnxr-0.18*lnxr^2)    ;springel 2008
;rmfunc={diemand:y3,springel:y4}
;=====end subhalos mass radial distribution of simulation-------
;=======all halofunc of simulation is here==========
halofunc={m:mfunc,r:rfunc}
return,halofunc
end
;=========================================================
;==========================================================
function subhalo_z,tree,mp,fp_lev,zlist,ilev_v

;fp_lev=TreeHostBranch(tree,hostID,1)
;print,fp_lev
nlev=n_elements(ilev_v)
tagsn='sn'+strtrim(sindgen(nlev),2)
tagr='r'+strtrim(sindgen(nlev),2)
index={host:lonarr(nlev)-1,Rvir:fltarr(nlev)}
for ii=0,nlev-1 do begin
  
  ilev=ilev_v[ii]
  host=fp_lev[ilev]
  if host eq -1 then continue
  index.host[ii]=host
  sn1=where((tree.snapnum eq ilev),n1)  ;(tree.FirstHaloInFoFgroup eq host)and
  if n1 lt 20 then continue 
  subpos=tree[sn1].pos & hostpos=tree[host].pos
  rx=subpos[0,*]-hostpos[0]
  ry=subpos[1,*]-hostpos[1]
  rz=subpos[2,*]-hostpos[2]
  r_c=sqrt(rx^2+ry^2+rz^2)                  ;Mpc/h
  r200=r_vir(tree[host].m200,zlist[ilev])   ;Mpc/h
  index.Rvir[ii]=r200
 ; help,r200,r_c,sn1
  sn2=where((r_c le r200)and(sn1 ne host),n2)
;  print,ii,ilev,host,n1,tree[host].m200,r200,n2
  if n2 lt 20 then continue 
  sn3=sn1[sn2]
  if n_tags(sn) eq 0 then begin
    sn=create_struct(tagsn[ii],sn3)
    rpos=create_struct(tagr[ii],r_c[sn2])
  endif else begin
    sn=create_struct(sn,tagsn[ii],sn3)
    rpos=create_struct(rpos,tagr[ii],r_c[sn2])
  endelse
endfor

if n_tags(sn) eq 0 then begin
  sn=-1
  rpos=-1
endif
halon=create_struct(index,'sn',sn,'r',rpos)
;help,halon,/struct
return,halon
end
;==============================================================
;==============================================================
function halofunc_v,zlist,ilev_v,outx
@include/halofunc_construct.pro

;mhost=10^(findgen(nhost)+1)
for isim=0,nsim-1 do begin
  tree=readtree(!treefile[isim],/all)
;  sn=where(tree.snapnum eq !n_snap-1)
;  mmax=max(tree[sn].m200,host)
  host=0
  for ihost=0,nhost-1 do begin
;    help,mhost[ihost]
;    if (mhost[ihost]-mmax)/mmax gt 10 then continue    
;    fp_lev=TreeGoodHostBranch(tree,mhost[ihost])
    fp_lev=TreeHostBranch(tree,host,1,/FirstHaloInFoFgroup)
;    print,fp_lev
    halon=subhalo_z(tree,!m_p[isim],fp_lev,zlist,ilev_v)
    
    ntag=n_tags(halon.sn)
    nvalid[ihost,isim]=ntag
    hostv[*,ihost,isim]=halon.host & Rvirv[*,ihost,isim]=halon.Rvir
    if ntag eq 0 then continue
    iiv=fix(strmid(tag_names(halon.sn),2,1))
    for itag=0,ntag-1 do begin
      ii=iiv[itag]
;      help,ii
      hosthalo=tree[halon.host[ii]]
      subhalo=tree[halon.sn.(itag)]
      mv=alog10(double(subhalo.len)/double(hosthalo.len))
      h=histo(mv,ml.m,mh.m,nbin.m,locations=lgxm)
      mfunc[*,ii,ihost,isim]=h ;/dbin.m/alog(10)
      h=histo(mv,ml.m,mh.m,nbin.m,/cumulative,/inverse)
      cmfunc[*,ii,ihost,isim]=h
  
      vv=alog10(subhalo.vmax/hosthalo.vmax)
      h=histo(vv,ml.v,mh.v,nbin.v,locations=lgxv)
      vfunc[*,ii,ihost,isim]=h
      h=histo(vv,ml.v,mh.v,nbin.v,/cumulative,/inverse)
      cvfunc[*,ii,ihost,isim]=h
  
      rv0=alog10(halon.r.(itag)/halon.Rvir[ii])
      for mc=0,nmc-2 do begin
        sn1=where(mv gt mcv[mc],n1)
        if n1 eq 0 then continue
        rv=rv0[sn1]
        h=histo(rv,ml.r,mh.r,nbin.r,locations=lgxr)
        rfunc[*,mc,ii,ihost,isim]=h
        h=histo(rv,ml.r,mh.r,nbin.r,/cumulative)
        crfunc[*,mc,ii,ihost,isim]=h ;/h[nbin.r-1]
      endfor
    endfor
  endfor
endfor
xm=10^lgxm & xv=10^lgxv & xr=10^lgxr

@include/halofunc_collect.pro
return,outh
end
;==============================================================
;==============================================================
pro write_halofunc

zlist=snaplist(/redshift)
outh=halofunc_v(zlist,!ilev_v,outx)
openw,lun,'halofunc/plotdata/phoenix.dat',/get_lun
  writeu,lun,outx
  writeu,lun,outh
free_lun,lun
end



pro read_halofunc,outx,outh,halofunc0

ilev_v=!ilev_v
@include/halofunc_construct.pro
xm=fltarr(nbin.m) & xv=fltarr(nbin.v) & xr=fltarr(nbin.r)
@include/halofunc_collect.pro

openr,lun,'halofunc/plotdata/phoenix.dat',/get_lun
  readu,lun,outx
  readu,lun,outh
free_lun,lun
;print,outx,outh
if arg_present(halofunc0) then halofunc0=halofunc_pre(nbin,ml,mh,dbin,outx)

;help,outx,outh,halofunc0,/struct

end
