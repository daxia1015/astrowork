pro plot_magZf
OutputList,!galacticus.treeEv[3],nout
fname=!galacticus.path+'MagGapEvolution.hdf5'
h5read_manydata,fname,['historyTime','historyRedshift','MagGap',$
    	    	'FirstFourMCGs'],tlist,zlist,dm,MBCG
nrec=n_elements(tlist)
irec0=nout-nrec & print,irec0
irecv=!smf.ioutv-irec0
psymv=['x','+','*']
PhoenixTable
ilev=1
iv=!phoenix.isimv[*,ilev]
m12=dm[0,irecv,iv] & help,m12
zf=!PhoenixTable.zf[iv]
name=strmid(!PhoenixTable.name[iv],2,1)

device,filename='MagGap/magZf.eps',/color,/encapsul,xsize=10,ysize=8
yr=[0,3.99]
down=0.5*(yr[1]-yr[0])*float(!d.y_ch_size)/float(!d.y_size)
plot,findgen(15)*0.1,fltarr(15)+2,linestyle=1,xtitle='z!df!n',ytitle='!mD!xM!d12!n',$
     xrange=[0.05,1.3],yrange=yr,position=[0.13,0.14,0.98,0.98]	
for iz=0,2 do begin
  for i=0,8 do begin
    xyouts,zf[i],m12[0,iz,i]-down,psymv[iz],alignment=0.5,color=!myct.c9[i]
    if iz eq 0 then begin
      xyouts,zf[i],max(m12[0,*,i])+0.2,name[i],alignment=0.5,color=!myct.c9[i]
    endif
    print,name[i],MBCG[*,irecv[iz],iv[i]].RC
  endfor  
endfor
zname='z!m'+string(187b)+'!x'+!smf.zname[0:2]
labeling,0.02,0.98,0.05,0.07,zname,/symboling,psym=[7,1,2]
;====================================================================
device,filename='MagGap/magEV.eps',/color,/encapsul,xsize=10,ysize=9
name=strmid(!PhoenixTable.name[iv],0,3)
plot,tlist,fltarr(nrec)+2,linestyle=1,xtitle='t / Gyr',ytitle='!mD!xM!d12!n',$
     yrange=[0,3.99],xstyle=9,position=[0.13,0.13,0.96,0.89]
for i=0,8 do begin
  oplot,tlist,dm[0,*,iv[i]],color=!myct.c9[i]
endfor
axis,xaxis=1,xrange=[max(zlist),min(zlist)],xtitle='Redshift'
name=strmid(!PhoenixTable.name[iv],0,3)
labeling,0.06,0.9,0.08,0.06,name[0:4],/lineation,ct=!myct.c9[0:4]
labeling,0.35,0.9,0.08,0.06,name[5:8],/lineation,ct=!myct.c9[5:8]

end
;====================================================================
;====================================================================
