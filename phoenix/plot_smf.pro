pro callSMFtoPlot,FuncName,linestyle=line,color=color,psym=psym,$
    noerr=noerr,y=y,_extra=_extra
if n_elements(y) eq 0 then y=call_function(FuncName,_extra=_extra)
oplot,y.lgm,y.phi,linestyle=line,color=color,psym=psym
if not(keyword_set(noerr)) then errplot,y.lgm,y.low,y.up,width=0.02,color=color
end
;====================================================================
;====================================================================
pro plot_SMFobs
@include/smfplot.pro
yr.field=[2e-9,0.2] & yr.cluster=[2e-5,3e2]

device,filename='SMF/SMFobs.eps',/color,/encapsul,xsize=17,ysize=8
multiplot_Gan,[2,1],mxtitle=xtit,mytitle=ytit,xgap=0.03,/doyaxis,$
    	      myposition=[0.08,0.17,1,0.97]

plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.field
Func='SMF_'+['li2009','Baldry2012','Moustakas2013','Bernardi2013','Bernardi2013']
model=['cmodel','SerExp']
for i=0,2 do callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i]
for i=3,4 do callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i],model=model[i-3]

labeling,0.06,0.43,0.12,0.09,['Li2009','Baldry2012',$
    	 'Moustakas2013','Bernardi2010','Bernardi2013'],$
	 /lineation,ct=!myct.c5,linestyle=indgen(5)
xyouts,0.8,0.9,'Field'
multiplot_Gan,/doyaxis
;===============================================
plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.cluster
Func='SMF_'+['Vulcani2014','Calvi2013','Giodini2012','CYang2009','Merluzzi2010']
i=0
callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i]
i++
SMF_Calvi2013,WINGS=y,it=0
callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i],y=y
i++
callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i],ig=1
i++
callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i],ih=3,mlow=8.9,mhigh=11.5,nbin=13  ;z=0.1
i++
callSMFtoPlot,Func[i],linestyle=i,color=!myct.c5[i]  ;z=0.048, Shapley supercluster

labeling,0.06,0.43,0.12,0.09,['Vulcani2014','Cavli2013','Giodini2012',$
    	 'Yang2009','Merluzzi2010'],/lineation,$
	 ct=!myct.c5,linestyle=indgen(5)
xyouts,0.75,0.9,'Cluster'
multiplot_Gan
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFobsType
@include/smfplot.pro

device,filename='SMF/SMFobsType.eps',/color,/encapsul,xsize=17,ysize=15
multiplot_Gan,[2,2],mxtitle=xtit,mytitle=ytit

plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.field
Func='SMF_'+['Yang2009','Moustakas2013']
for isq=1,2 do begin
  i=0
  callSMFtoPlot,Func[i],linestyle=i,color=!myct.c2[isq-1],Fname='SMF',ic=3-isq
  i++
  callSMFtoPlot,Func[i],linestyle=i,color=!myct.c2[isq-1],isq=isq
endfor
labeling,0.06,0.43,0.12,0.09,['Yang2009','Moustakas2013'],$
	 /lineation,linestyle=indgen(2)
xyouts,0.8,0.9,'Field'
multiplot_Gan
;===========================================
plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.field
Func='SMF_'+['Bernardi2013']
itv=[5,6,2,1]
for it=1,4 do begin
  i=0
  callSMFtoPlot,Func[i],linestyle=it-2,color=!myct.c2[it ge 2],it=itv[it-1],/noerr
endfor
labeling,0.06,0.43,0.12,0.09,'Bernardi2013',$
	 /lineation,linestyle=indgen(2)
xyouts,0.8,0.9,'Field'
multiplot_Gan
;====CLUSTER Following=========================
plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.cluster
Func='SMF_'+['Vulcani2013','Vulcani2013','vanderBurg2013']
for isq=1,2 do begin
  i=0
  if isq eq 2 then begin
    SMF_Vulcani2013,ICBS=y,icolor=isq
;    callSMFtoPlot,Func[i],linestyle=i,color=!myct.c2[isq-1],y=y
  endif
  i++
  SMF_Vulcani2013,EDisCs=y,icolor=isq
  callSMFtoPlot,Func[i],linestyle=i,color=!myct.c2[isq-1],y=y
  i++
  callSMFtoPlot,Func[i],linestyle=i,color=!myct.c2[isq-1],isq=isq,/cluster
endfor
labeling,0.06,0.43,0.12,0.09,['ICBS','EDisCs','GCLASS'],$
	 /lineation,linestyle=indgen(3)
xyouts,0.8,0.9,'cluster'
multiplot_Gan
;===============================================
plot,li.lgm,li.phi,/ylog,/nodata,xrange=xr,yrange=yr.cluster
Func='SMF_'+['Calvi2013','Vulcani2011']
for it=1,4 do begin
  i=0
  SMF_Calvi2013,lgm,WINGS=y,it=it
  callSMFtoPlot,Func[i],y=y,linestyle=it-2,color=!myct.c2[it ge 2],/noerr
endfor
labeling,0.06,0.43,0.12,0.09,['Calvi2013'],$
	 /lineation,linestyle=indgen(3)
xyouts,0.8,0.9,'cluster'
multiplot_Gan
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFcompare
@include/smfplot.pro
region=['Whole Sky','Cluster']
h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'
;h5read_ManyData,fname,'moment_lg_phiAll',phi01,groupName='es0.1'

ilev=0 & ig=0 & isq=0 & it=0
sn=indgen(!smf.nbin-1)
;base=['within R!d200!n','whole cluster']
device,filename='SMF/SMFcompare.eps',/color,/encapsul,xsize=18,ysize=11
multiplot_Gan,[3,2],mxtitle=xtit,mytitle=ytit,myposition=[0.16,0.13,0.98,0.9]

for ir=0,1 do begin
  for iz=0,2 do begin
;  print,nGalaxy[*,iz,ig,isq,it,ir]
    plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.(ir)
    oplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].average,color=!myct.c4[ir]
    errplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].low,philg[*,ilev,iz,ig,isq,it,ir].up,$
    	  color=!myct.c4[ir],width=0.02
    if ir eq 0 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,zname[iz],alignment=0.5,/normal
    if(ir eq 0)and(iz eq 0)then begin
      oplot,li.lgm,li.phi
      y1=SMF_Bernardi2013()
      oplot,y1.lgm,y1.phi 
      oplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].average,color=!myct.c4[ir]
      y2=SMF_li2009(y1.lgm)
      for i=33,34-14,-1 do oplot,[y1.lgm[i],y2.lgm[i-1]],[y1.phi[i],y2.phi[i-1]],thick=1
      labeling,0.06,0.3,0.12,0.1,region[ir],/lineation,ct=!myct.c4[ir],thick=4
      shadowing,findgen(7)*0.02+0.06,fltarr(7)+0.2,fltarr(7)+0.07,thick=2
      xyouts,0.2,0.16,'Bernardi2013'
      xyouts,0.2,0.05,'Li2009'
    endif
    if(ir eq 0)and(iz eq 1)then begin  ;z~0.3
      Func='SMF_'+['Giodini2012','Moustakas2013']
      igv=[2,0] & izv=[0,2]
      for i=0,1 do callSMFtoPlot,Func[i],linestyle=i+1,ig=igv[i],iz=izv[i]
      labeling,0.06,0.28,0.12,0.1,[region[ir],'Giodini2012','Moustakas2013'],$
    	    /lineation,ct=[!myct.c4[ir],0,0],linestyle=[0,1,2],thick=4
    endif
    if(ir eq 0)and(iz eq 2) then begin  ;z~1
      Func='SMF_'+['Giodini2012','vanderBurg2013','Santini2012','Mortlock2011','Davidzon2013']
      i=0
      callSMFtoPlot,Func[i],linestyle=i+1,ig=2,iz=3
      i++
      callSMFtoPlot,Func[i],linestyle=i+1,/field
      labeling,0.06,0.28,0.12,0.1,[region[ir],'Giodini2012','vanderBurg2013'],$
    	    /lineation,ct=[!myct.c4[ir],0,0],linestyle=[0,1,2],thick=4
    endif
    if(ir eq 1)and(iz eq 0)then begin
      SMF_Calvi2013,WINGS=y,it=0
      oplot,y.lgm,y.phi,linestyle=1
      errplot,y.lgm,y.low,y.up,width=0.02
      labeling,0.06,0.28,0.12,0.11,[region[ir],'Calvi2013'],$
    	    /lineation,ct=[!myct.c4[ir],0,0],linestyle=[0,1,2],thick=4
    endif
    if(ir eq 1)and(iz eq 1)then begin
      y=SMF_Giodini2012(ig=1,iz=0,isq=0)
      oplot,y.lgm,y.phi,linestyle=1
      errplot,y.lgm,y.low,y.up,width=0.02
      SMF_Vulcani2013,ICBS=y,ir=1,icolor=0
      oplot,y.lgm,y.phi,linestyle=3
      errplot,y.lgm,y.low,y.up,width=0.02
      labeling,0.06,0.28,0.12,0.11,[region[ir],'Giodini2012','Vulcani2013'],$
    	    /lineation,ct=[!myct.c4[ir],0,0],linestyle=[0,1,2],thick=4
    endif
    if(ir eq 1)and(iz eq 2)then begin
      Func='SMF_'+['Giodini2012','vanderBurg2013']
      i=0
      callSMFtoPlot,Func[i],linestyle=i+1,ig=1,iz=3
      i++
      callSMFtoPlot,Func[i],linestyle=i+1,/cluster
      labeling,0.06,0.28,0.12,0.1,[region[ir],'Giodini2012','vanderBurg2013'],$
    	    /lineation,ct=[!myct.c4[ir],0,0],linestyle=[0,1,2],thick=4
    endif
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFev
@include/smfplot.pro
ilev=0 & ig=0 & isq=0 & it=0
sn=indgen(!smf.nbin-1)
;base=['within R!d200!n','whole cluster']
device,filename='SMF/SMFev.eps',/color,/encapsul,xsize=18,ysize=13
multiplot_Gan,[3,2],ygap=0.05,/doxaxis,myposition=[0.1,0.05,0.98,1]

xtit='log[M!dh!n / (h!u-1!n'+Msun+')]'
ytit='!mf!x(M!dh!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!dh!n)'
fname=!galacticus.path+'haloMassFunction.hdf5'
h5Read_manydata,fname,['moment_lgm','moment_phiHalo'],lgm,momHalo,groupName='tree0'
for iz=0,2 do begin
  plot,findgen(10)+1,10^(-findgen(10)),/nodata,/ylog,xrange=[8.1,13.99],yrange=[2e-8,3e4]
  for ir=1,3 do begin 
    oplot,lgm[*,iz,ir],momHalo[*,ilev,iz,ir].average,$
    	  linestyle=lts[ir-1],color=!myct.c4[ir]
    errplot,lgm[*,iz,ir],momHalo[*,ilev,iz,ir].low,$
    	    momHalo[*,ilev,iz,ir].up,color=!myct.c4[ir],width=0.02
  endfor
  xyouts,(!p.position[0]+!p.position[2])/2,0.96,zname[iz],alignment=0.5,/normal
  if iz eq 0 then xyouts,0.04,(!p.position[1]+!p.position[3])/2,ytit,alignment=0.45,/normal,orientation=90
  if iz eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.5,xtit,alignment=0.55,/normal
  multiplot_Gan,/doxaxis
endfor
;=====================================================
@include/smfplot.pro

h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'
for iz=0,2 do begin
  plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.all
  for ir=1,3 do begin 
    oplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].average,color=!myct.c4[ir],linestyle=lts[ir-1]
    errplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].low,philg[*,ilev,iz,ig,isq,it,ir].up,$
    	    color=!myct.c4[ir],width=0.02
    if(iz eq 1)then begin
      SMF_Vulcani2013,ICBS=y,ir=ir
      callSMFtoPlot,'',y=y
    endif
    if(iz eq 2)then begin
      SMF_Vulcani2013,EDisCS=y,ir=ir
      callSMFtoPlot,'',y=y
    endif
  endfor
  if iz eq 0 then xyouts,0.04,(!p.position[1]+!p.position[3])/2,ytit,alignment=0.45,/normal,orientation=90
  if iz eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.02,xtit,alignment=0.55,/normal
  multiplot_Gan,/doxaxis
endfor
multiplot_Gan,/default
labeling,0.002,0.69,0.05,0.05,!smf.region[1:3],thick=4,/lineation,$
    	 chardown=0.01,ct=!myct.c4[1:3],linestyle=lts
labeling,0.002,0.13,0.05,0.05,!smf.region[1:3],thick=4,/lineation,$
    	 chardown=0.01,ct=!myct.c4[1:3],linestyle=lts
labeling,0.33,0.04,0.05,0.1,'Vulcani2013',/lineation,chardown=0.01
labeling,0.69,0.04,0.05,0.1,'Vulcani2013',/lineation,chardown=0.01
end
;====================================================================
;====================================================================
pro plot_SMFsq
@include/smfplot.pro
fname=!galacticus.path+'stellarMassFunction.hdf5'
h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'

ilev=0 & ig=0 & isq=0 & it=0
sn=indgen(!smf.nbin-1)
device,filename='SMF/SMFsq.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit,$
    	      myposition=[0.17,0.13,0.98,0.9]
for ir=1,3 do begin
  for iz=0,2 do begin
    if ir eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,zname[iz],alignment=0.5,/normal
    plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.(ir)
    for isq=1,2 do begin
      oplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].average,color=!myct.c2[isq-1]
      errplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].low,philg[*,ilev,iz,ig,isq,it,ir].up,$
    	      color=!myct.c2[isq-1],width=0.02
;      if isq eq 1 then print,philg[*,ilev,iz,ig,isq,it,ir].average
    endfor
    if iz eq 0 then begin
      labeling,0.05,0.19,0.12,0.1,['Star-forming','Quiescent'],$
      	       ct=!myct.c2,/lineation,thick=4
      xyouts,0.05,0.3,!smf.region[ir]
    endif
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFtype
@include/smfplot.pro
c4=[!myct.blue,!myct.red,!myct.dkgreen,!myct.dkpurple]
lstyle=[0,0,1,2]
fname=!galacticus.path+'stellarMassFunction.hdf5'
h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'

ilev=0 & ig=0 & isq=0 & it=0
device,filename='SMF/SMFtype.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit,$
    	      myposition=[0.17,0.13,0.98,0.9]
;iz=0, consistent with Calvi et al. 2013
;iz=2, consistent with van er Burg et al. 2013
for ir=1,3 do begin
  for iz=0,2 do begin
    if ir eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,'z~'+zname[iz],alignment=0.5,/normal
    plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.(ir)
    for it=1,4 do begin
      oplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].average,color=c4[it-1],linestyle=lstyle[it-1]
      errplot,lgm[*,iz,ir],philg[*,ilev,iz,ig,isq,it,ir].low,philg[*,ilev,iz,ig,isq,it,ir].up,$
    	      color=c4[it-1],width=0.02
;      if isq eq 1 then print,philg[*,ilev,iz,ig,isq,it,ir].average
    endfor
    if iz eq 0 then begin
      labeling,0.05,0.19,0.12,0.1,['Late','Early'],$
      	       ct=!myct.c2,/lineation,thick=4
      xyouts,0.05,0.3,!smf.region[ir]
    endif
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFCSR
@include/smfplot.pro
h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'

ilev=0 & isq=0 & it=0
sn=indgen(!smf.all.nbin-1)
c2=[!myct.black,!myct.red]
device,filename='SMF/SMFcsr.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit,$
    	      myposition=[0.17,0.13,0.98,0.9]
for ir=1,3 do begin
  for iz=0,2 do begin
    if ir eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,'z~'+zname[iz],alignment=0.5,/normal
    plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.(ir)
    for ig=1,2 do begin
      if(ir eq 1)and(iz eq 0)and(ig eq 1)then phi=phi0 else phi=philg 
      oplot,lgm[*,iz,ir],phi[*,ilev,iz,ig,isq,it,ir].average,linestyle=ig-1,color=c2[ig-1]
      errplot,lgm[*,iz,ir],phi[*,ilev,iz,ig,isq,it,ir].low,phi[*,ilev,iz,ig,isq,it,ir].up,$
    	      color=c2[ig-1],width=0.02
;      if(ir eq 1)and(iz eq 0)and(ig eq 1)then print,philg[*,ilev,iz,ig,isq,it,ir].average
    endfor
    if iz eq 1 then begin
      labeling,0.05,0.19,0.12,0.1,['Central','Satellite'],$
      	       linestyle=[0,1],ct=c2,/lineation,thick=4
      xyouts,0.05,0.3,!smf.region[ir]
    endif
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMFcs
@include/smfplot.pro
h5read_ManyData,fname,['moment_lgm','moment_lg_phiAll','moment_phiAll'],$
    	    	lgm,philg,phi0,groupName='preset1_shift0'

ilev=0  & ir=0 & it=0
sn=indgen(!smf.all.nbin-1)
device,filename='SMF/SMFcs.eps',/color,/encapsul,xsize=18,ysize=11
multiplot_Gan,[3,2],mxtitle=xtit,mytitle=ytit,$
    	      myposition=[0.16,0.13,0.98,0.9]

for ig=1,2 do begin
  for iz=0,2 do begin
    if ig eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,'z~'+zname[iz],alignment=0.5,/normal
    plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.field
    for isq=1,2 do begin
      if(ir eq 1)and(ig eq 1)and(isq eq 1)then phi=phi0 else phi=philg 
      oplot,lgm[*,iz,ir],phi[*,ilev,iz,ig,isq,it,ir].average,color=!myct.c2[isq-1]
      errplot,lgm[*,iz,ir],phi[*,ilev,iz,ig,isq,it,ir].low,phi[*,ilev,iz,ig,isq,it,ir].up,$
    	      color=!myct.c2[isq-1],width=0.02
;      if(ir eq 1)and(ig eq 1)and(it eq 1)then print,philg[*,ilev,iz,ig,isq,it,ir].average
    endfor
    if iz eq 0 then begin
      labeling,0.05,0.19,0.12,0.1,['Star-forming','Quiescent'],$
      	       ct=!myct.c2,/lineation,thick=4
      xyouts,0.05,0.3,!smf.group[ig]
    endif
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default

end
;====================================================================
;====================================================================
pro plot_SMFdm
@include/smfplot.pro
h5read_ManyData,fname,['moment_lgm','phiAll','phiAll_Calibrated'],$
    	    	lgm,phi0,phiC,groupName='preset1_shift0'

ilev=0 & ig=0 & isq=0 & it=0 & ir=0
iv=!phoenix.isimv[*,ilev]
PhoenixTable
phName=strmid(!PhoenixTable.name[iv],0,3)
device,filename='SMF/SMFdm.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit,myposition=[0.13,0.13,0.98,0.98]
for i=0,8 do begin
;  if ir eq 1 then xyouts,(!p.position[0]+!p.position[2])/2,0.96,'z~'+zname[iz],alignment=0.5,/normal
  isim=iv[i]
  plot,li.lgm,li.phi,/nodata,/ylog,xrange=xr,yrange=yr.field 
  xyouts,12,0.07,phName[i],alignment=1 
  for iz=0,2 do begin
    oplot,phiC[*,isim,iz,ig,isq,it,ir].lgm,phiC[*,isim,iz,ig,isq,it,ir].phi,$
    	  linestyle=lts[iz],color=!myct.c3[iz]
  endfor
  if(i eq 0)then labeling,0.05,0.3,0.12,0.1,zname[0:2],/lineation,linestyle=lts,ct=!myct.c3
;  if(i eq 3)then labeling,0.05,0.3,0.12,0.1,!smf.region[1:3],/lineation,ct=!myct.c4[1:3]
  multiplot_Gan
endfor
multiplot_Gan,/default
end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
pro plot_SMFsample
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8,12.1] & yr=[2e-9,0.2]
li=SMF_li2009()
fname=!galacticus.path+['Field_e0.1.hdf5','Field_Benson2010.hdf5','Field_e1.0.hdf5']
output=['Output55','Output45','Output35']
device,filename='SMF/FieldSample.eps',/color,/encapsul,xsize=10,ysize=8
plot,li.lgm,li.phi,/ylog,linestyle=1,thick=4,xrange=xr,yrange=yr,$
     xtitle=xtit,ytitle=ytit
for i=0,2 do begin
  print,i
  y=smf_Sample(fname[i],output[i])
  oplot,y.lgm,y.phi,thick=i*3+1
endfor

end
;====================================================================
;====================================================================
pro plot_SMFtest
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8,12.1] & yr=[2e-10,9e2]
li=SMF_li2009()
SMF_Calvi2013,WINGS=wings,it=0
device,filename='SMF/test.eps',/color,/encapsul,xsize=10,ysize=8
plot,li.lgm,li.phi,/ylog,linestyle=1,xrange=xr,yrange=yr,$
     xtitle=xtit,ytitle=ytit
oplot,wings.lgm,wings.phi,linestyle=2
iv=!phoenix.isimv[*,0]
post=['_e0.1.hdf5','.hdf5','_e1.0.hdf5']
fname=!galacticus.path+!Phoenix.treename[iv]+'_first_ev_preset1_shift1'
for i=0,2 do begin
  fname=!galacticus.path+!Phoenix.treename[iv]+'_first_ev_preset1_shift1'+post[i]
  y=smf_test(fname,log10=1)
  oplot,y.lgm,y.all,color=!myct.c9[i]
  oplot,y.lgm,y.r200,color=!myct.c9[i]
endfor

end


;====================================================================
;====================================================================
