pro constraint_moment,gidSMF,phiAll,phiR200
print,'  computing moment of stellar mass function...'
mi=histo_moment(/get_unit)
all=replicate(mi,!smf.all.nbin,4)
R200=replicate(mi,!smf.R200.nbin,4)

for it=0,2 do begin
  phi=phiAll[*,1:9,it]
  help,phi
  All[*,it]=histo_moment(phi,!smf.all.nbin,/log10)
  phi=phiR200[*,1:9,it]
  R200[*,it]=histo_moment(phi,!smf.R200.nbin,/log10);,log10=it eq 0)
endfor
h5write_dataset,gidSMF,All,'moment_phiAll'
h5write_dataset,gidSMF,R200,'moment_phiR200'
end
;====================================================================
;====================================================================
pro constraint_write,fidr
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
print,'computing stellar mass function with different prarmeters...'
gidSMF=h5g_create(fidr,'smfConstraint')
;gidSate=h5g_create(fidr,'SatelliteStatistics')
phiAll=dblarr(!smf.all.nbin,10,4)
;first 9: 9 cluster, level 4
;second 4: mergingTimescaleMultiplier=0.1, 1, 2, 10
phiR200=dblarr(!smf.R200.nbin,10,4)
;first 9: 9 cluster, level 4
;second 4: mergingTimescaleMultiplier=0.1, 1, 2, 10
fidVW=h5f_open(!galacticus.path+'treeProperties.hdf5')
W0=h5read_dataset(fidVW,'VolumeWeight')
h5f_close,fidVW
h5write_dataset,gidSMF,W0,'VolumeWeight'

Name0=['nodeVirialRadius','nodeIsIsolated']
;Lsun=10^(-4.67/2.5)  ; solar luminosity of SDSS_r band in AB magnitude system
level=strmid(!Phoenix.TreeName,3,1)
tName=['0.1','1.0','10']
for it=0,2 do begin
  print,'it=',it
  fname=!galacticus.Path+'constraint/'+!Phoenix.TreeName+'_t'+tName[it]+'.hdf5'
  for isim=1,19,2 do begin
    print,'isim=',isim
    fid=h5f_open(fname[isim])
    data=nodeData_ReadMany(fid,'Output2',Name0,/pos,/mstar)
;            	lumiName='SDSS_r',ColorName='SDSS_g-SDSS_r')

    data.mstar=data.mstar*h0^2   ; h^-2 M_sun
    data.nodeVirialRadius=data.nodeVirialRadius*h0  ;h^-1 Mpc
    data.pos=data.pos*h0  ;h^-1 Mpc
;    data.lumi=data.lumi*h0^2/Lsun    ;h^-2 L_sun
    if level[isim] eq '4' then W=min(W0[*,isim]) else W=max(W0[*,isim])
    ;==========================================
    phi=smf_histo(data.Mstar,!smf.all.nbin,!smf.all.low,!smf.all.high,weight=W,$
    	    	  /log10,/li2009)
    phiAll[*,(isim-1)/2,it]=phi
    ;==========================================
    R200=max(data.nodeVirialRadius,sn1)
    center=data.pos[*,sn1]
    R=pair_distance(data.pos,center)
    sn1=where(R le R200)
    WC=3.0/(4.0*!pi*R200^3) ;/(200/omega0)  
    ; Weight=1/V200/(200[omega0*(1+z)^3+lambda0]/[omega0*(1+z)^3])
    Mstar=data.Mstar[sn1]
    phi=smf_histo(Mstar,!smf.R200.nbin,!smf.R200.low,!smf.R200.high,$
    	    	  weight=WC,/log10,/wings)
    phiR200[*,(isim-1)/2,it]=phi
    ;==========================================
    h5f_close,fid
  endfor
endfor
h5write_dataset,gidSMF,phiAll,'phiAll'
h5write_dataset,gidSMF,phiR200,'phiR200'

lgm=histo(findgen(10)+1,nbin=!smf.all.nbin,xmin=!smf.all.low,$
    	  xmax=!smf.all.high,/log10,/get_locations)
h5write_dataset,gidSMF,lgm,'lgmAll'
lgm=histo(findgen(10)+1,nbin=!smf.R200.nbin,xmin=!smf.R200.low,$
    	  xmax=!smf.R200.high,/log10,/get_locations)
h5write_dataset,gidSMF,lgm,'lgmR200'

constraint_moment,gidSMF,phiAll,phiR200

h5g_close,gidSMF
end
