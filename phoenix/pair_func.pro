function pair_distance,pos1,pos2,zlist=zlist
n1=n_elements(pos1[0,*])
n2=n_elements(pos2[0,*])
if n1 eq n2 then d=sqrt(total((pos1-pos2)^2,1)) else begin
  if n1 eq 1 then begin
    posx=pos1[0]-pos2[0,*]
    posy=pos1[1]-pos2[1,*]
    posz=pos1[2]-pos2[2,*]
  endif
  if n2 eq 1 then begin
    posx=pos1[0,*]-pos2[0]
    posy=pos1[1,*]-pos2[1]
    posz=pos1[2,*]-pos2[2]
  endif  
  d=sqrt(posx^2+posy^2+posz^2)
  d=transpose(d)
endelse
if n_elements(zlist) ne 0 then d=d/(1.+zlist)
;help,posx,posy,posz,d
return,d
end
;==================================================================
;==================================================================
function pair_energy,mhalo,velocity,d,sub,host,zlist=zlist
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
m1m2=mhalo[sub]*mhalo[host]
Relvel=pair_distance(velocity[*,sub],velocity[*,host])
KE=m1m2*Relvel^2/(mhalo[sub]+mhalo[host])/2.
PE=-gaussc*m1m2/d
if n_elements(zlist) ne 0 then begin
  ez2=ez(zlist)
  HEE=0.0125*HC0^2*ez2^2*d^5*!pi*omega0*ro_c0
  PE=temporary(PE)+HEE
endif
;HEE=0.0125*HC0^2*ez2*d^5*!pi*omega0*ro_c0*ez2
Etot=KE+PE                    ;M_sun kpc^2 Gry^{-2}
return,Etot
end
;==================================================================
;==================================================================
function pair_masscenter,mhalo,pos,sub,host
msub=transpose(mhalo[sub])
mhost=transpose(mhalo[host])
msum=msub+mhost
for i=0,2 do begin
  posx=(msub*pos[0,sub]+mhost*pos[0,host])/msum
  posy=(msub*pos[1,sub]+mhost*pos[1,host])/msum
  posz=(msub*pos[2,sub]+mhost*pos[2,host])/msum
endfor
posc=[posx,posy,posz]
;help,posx,posy,posz,posc
return,posc
end
;==================================================================
;==================================================================
pro pair_swap,mhalo,sub,host
msub=mhalo[sub] & mhost=mhalo[host]
sn=where(mhost lt msub,n)
if n ne 0 then begin
  temp=sub[sn]
  sub[sn]=host[sn]
  host[sn]=temp
endif
end
;==================================================================
;==================================================================
function pair_nhalo,sub,host,returnsn=returnsn
sn1=[sub,host]
sn2=uniq(sn1,sort(sn1))
nhaloinpair=n_elements(sn2)
if keyword_set(returnsn) then return,sn1[sn2]
return,nhaloinpair
end
;==================================================================
;==================================================================
pro pair_orbits,data,sub,host,zi,Rorb=Rorb,energy=energy,$
    	 boundsn=boundsn,$
    	 circularity=circularity,eccentricity=eccentricity
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
msub=data.nodeMass[sub]
mhost=data.nodeMass[host]
vecd=data.pos[*,sub]-data.pos[*,host]	    ;Mpc
vecvel=data.vel[*,sub]-data.vel[*,host]  ;km/s
vecangl=crosspn(vecd,vecvel)                    ;specific angular momentum, vector, Mpc km/s
Rorb=sqrt(total(vecd^2,1))*Mpc          	    ;km
Vorb=sqrt(total(vecvel^2,1))     	    	  ;orbital velocity, km/s
angl=sqrt(total(vecangl^2,1))*Mpc                    ;angular mementum ,km km/s
if arg_present(circularity) then begin
  Rhost=data.nodeVirialRadius[host] 
  chost=HaloConcen(mhost,zi)
  rs_host=Rhost/chost
;  Vvir=gaussc*mhost/Rhost
  energy=energy_r(Rorb,mhost,rs_host,chost,Vorb,hprofile=2)
  orbit_circularity,mhost,rs_host,chost,angl,energy,yita,$
    	    	    circularity,boundsn=boundsn 
endif
if arg_present(eccentricity) then begin
  energy=-gaussc*mhost/Rorb+0.5*Vorb^2
  eccentricity=sqrt(1+2*energy*angl^2/gaussc^2/mhost^2)
endif
end
;==================================================================
;==================================================================
;function pair_eccentricity,data,sub,host,Rorb=Rorb,energy=energy
;common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;msub=data.mhalo[sub]
;mhost=data.mhalo[host]
;specificReduceMass=mhost/(mhost+msub)
;vecd=data.pos[*,sub]-data.pos[*,host]
;vecvel=data.velocity[*,sub]-data.velocity[*,host]
;vecangl=crosspn(vecd,vecvel)                    ;specific angular momentum, vector
;Rorb=sqrt(total(vecd^2,1))     ;kpc
;Vorb=sqrt(total(vecvel^2,1))     	    	  ;orbital velocity
;angl=specificReduceMass*sqrt(total(vecangl^2,1))                           ;angular mementum
;energy=-gaussc*mhost/Rorb+0.5*specificReduceMass*Vorb^2
;angl=sqrt(total(vecangl^2,1))                           ;angular mementum
;energy=-gaussc*mhost/Rorb+0.5*Vorb^2
;sn=where(energy lt -0.001)
;eccentricity=sqrt((1+2*energy[sn]*angl[sn]^2)/(gaussc^2*mhost[sn]^2*specificReduceMass[sn]))
;eccentricity=sqrt(1+2*energy*angl^2/gaussc^2/mhost^2)
;return,eccentricity
;end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
;pro pair_read,fname,nlev,npairtot,n_series,pair
;nlev=0L & npairtot=0LL
;openr,lun,fname,/get_lun
;  readu,lun,nlev,npairtot
;  n_series=replicate(!halopair.ni,nlev)
;  pair=replicate(!halopair.pair,npairtot)
;  readu,lun,n_series
;  readu,lun,pair
;  if eof(lun) eq 0 then message,'error'
;free_lun,lun
;help,nlev,npairtot,n_series,pair
;end
