pro plot_galacticus4,psdir,isim,outputName,tlist
;total(SFR)/total(Mstar), total(gas)/total(Mstar), vs. time
;total(CoolingRate)/total(Mstar)

pairfile=!galacticus.data+'pair/'+!treename

pair_group,pairfile[isim],ngroup,n_series,outputName=outputName,$
    	   pairNumber=pairNumber,pairGroup=pairGroup




end
