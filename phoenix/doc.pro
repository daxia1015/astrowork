pro doc
alist=snaplist()
zlist=snaplist(/redshift)
tlist=tGy(zlist)
print,zlist,'  '

sn=71-indgen(30)*2
;print,zlist[sn],format='(26f5.2)'
;sn=21-indgen(10)*2
;print,zlist[sn],format='(10f6.2)'
print,zlist[sn]

isim=3
tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,/readhead)
print,!Phoenix.TreeName[isim],nhalos[0],nhalos[0],totnhalo,totnhalo
i0=nhalos[0] & i1=totnhalo
for isim=5,19,2 do begin
  tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,/readhead)
  print,!Phoenix.TreeName[isim],nhalos[0],nhalos[0]+i0,totnhalo,totnhalo+i1
  i0=i0+nhalos[0]
  i1=i1+totnhalo

endfor

end
