pro tree_ascii_give,tree,lun,nhalo,isim,itree,skip_nhalo,zlist,fmt=fmt
outi={TreeID:0L,NodeID:0L,HostID:0L,Descendant:0L,$
      NodeMass:0.,Redshift:0.,pos:fltarr(3),v:fltarr(3)} 
;      $,ang:fltarr(3),rh:0.}

  out=replicate(outi,nhalo)
  out.treeID=itree
  out.NodeID=lindgen(nhalo)+skip_nhalo
  out.HostID=tree.FirstHaloInFOFgroup
  out.Descendant=tree.Descendant
  out.NodeMass=tree.len*!Phoenix.mp[isim]
  out.Redshift=zlist[tree.snapnum]
  out.pos=tree.pos
  out.v=tree.vel
;  for ix=0,2 do out.ang[ix]=tree.spin[ix]*out.nodeMass
;  out.rh=tree.HalfMassRadius
  printf,lun,out,format=fmt

end
;==================================================================
;==================================================================
pro tree_ascii0,isim,outname,zlist,alltree=alltree
nhalomax=1000000L & skip_nhalo=0L

tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,/readhead)
if keyword_set(alltree) then begin
  last=ntree-1
  fmt='(4i9,8e14.6e3)'
endif else begin
  last=0
  fmt='(i2,3i9,8e14.6e3)'
endelse
mod0=ntree/10
openw,lun,outname,/get_lun
for itree=0L,last do begin
  if itree mod mod0 eq 0 then print,'itree=',itree
  nhalo=nhalos[itree]
  if nhalo gt nhalomax then begin
    nread=nhalo/nhalomax & lastread=nhalo mod nhalomax
    print,'nread,lastread=',nread,lastread
    for iread=0,nread-1 do begin
      tree=readtree_ph(!Phoenix.TreeFile[isim],nline=nhalomax,skip_nhalo=skip_nhalo)
      tree_ascii_give,tree,lun,nhalomax,isim,itree,skip_nhalo,zlist,fmt=fmt
      skip_nhalo=skip_nhalo+nhalomax
    endfor
    if lastread ne 0 then begin
      tree=readtree_ph(!Phoenix.TreeFile[isim],nline=lastread,skip_nhalo=skip_nhalo)
      tree_ascii_give,tree,lun,lastread,isim,itree,skip_nhalo,zlist,fmt=fmt
    endif  
  endif else begin
    tree=readtree_ph(!Phoenix.TreeFile[isim],itree=itree,skip_nhalo=skip_nhalo)
    tree_ascii_give,tree,lun,nhalo,isim,itree,skip_nhalo,zlist,fmt=fmt
  endelse
endfor
free_lun,lun
end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
pro tree_ascii
zlist=snaplist('ph',/redshift)
post=['_first','_full']
for isim=1,19 do begin
  for alltree=0,0 do begin
    fname=!Phoenix.TreeFile[isim] 
    outname='~/data/phoenix_tree/gcs/'+!Phoenix.TreeName[isim]+post[alltree]+'.txt'
    print,fname,isim
    tree_ascii0,isim,outname,zlist,alltree=alltree 
  endfor
endfor
end
;==================================================================
;==================================================================
pro galacticus_xml
post=['_first','_full']
alltree=0
home=getenv('HOME')
hdf5=home+'/data/phoenix_tree/gcs/'+!Phoenix.TreeName+post[alltree]+'.hdf5'
hdf5ev=home+'/data/phoenix_tree/gcs/'+!Phoenix.TreeName+post[alltree]+'_ev_preset1_shift0.hdf5'
;state='/home/jlgan/data/phoenix_tree/gcs/'+[!Phoenix.TreeName,'','ph4']
;print,hdf5,!Phoenix.TreeName,hdf5ev
basefile='~/gsoft/xml/phbase_v0.9.1.xml'
outfile='~/gsoft/xml/'+!Phoenix.TreeName+'.xml'
;outfile=[outfile,'','~/gsoft/xml/ph4_first.xml']
;print,basefile,outfile
ph=strmid(!Phoenix.TreeName,2,1)
for i=1,19 do begin
  file_copy,basefile,outfile[i],/overwrite
  openu,lun,outfile[i],/get_lun,/append
;    if(ph[i] eq 'd')or(ph[i] eq 'f')then density='NFW' else density='Einasto'
    printf,lun,'  <parameter>'
    printf,lun,'    <name>mergerTreeReadFileName</name>'
    printf,lun,'    <value>'+hdf5[i]+'</value>'
    printf,lun,'  </parameter>'
    printf,lun,'  <parameter>'
    printf,lun,'    <name>galacticusOutputFileName</name>' 
    printf,lun,'    <value>'+hdf5ev[i]+'</value>'
    printf,lun,'  </parameter>'
    printf,lun,'</parameters>'
  free_lun,lun
;  print,'./Galacticus.exe '+outfile[i]+' &> '+'~/gsoft/log/'+treename[i]+'_$date.log &'
endfor
end
;==================================================================
;==================================================================
pro ConstraintXML
post='10'
hdf5='/home/jlgan/data/phoenix_tree/gcs/'+!Phoenix.TreeName+'_first.hdf5'
hdf5ev='/home/jlgan/data/phoenix_tree/gcs/constraint/'+!Phoenix.TreeName+'_t'+post+'.hdf5'
basefile='~/gsoft/xml/constraint/phConstraint.xml'
outfile='~/gsoft/xml/constraint/'+!Phoenix.TreeName+'_t'+post+'.xml'
;outfile=[outfile,'','~/gsoft/xml/ph4_first.xml']
;print,basefile,outfile
openw,lun0,'~/gsoft/scripts/constraintRun',/get_lun
printf,lun0,'#!/bin/sh'
printf,lun0,'t0=$(date)'
printf,lun0,'dir0=$PWD'
printf,lun0,'cd ~/v0.9.1'
for i=19,1,-2 do begin
  file_copy,basefile,outfile[i],/overwrite
  openu,lun,outfile[i],/get_lun,/append
    printf,lun,'  <parameter>'
    printf,lun,'    <name>mergingTimescaleMultiplier</name>'
    printf,lun,'    <value>'+post+'</value>'
    printf,lun,'  </parameter>'
    printf,lun,'  <parameter>'
    printf,lun,'    <name>mergerTreeReadFileName</name>'
    printf,lun,'    <value>'+hdf5[i]+'</value>'
    printf,lun,'  </parameter>'
    printf,lun,'  <parameter>'
    printf,lun,'    <name>galacticusOutputFileName</name>' 
    printf,lun,'    <value>'+hdf5ev[i]+'</value>'
    printf,lun,'  </parameter>'
    printf,lun,'</parameters>'
  free_lun,lun
  printf,lun0,'./Galacticus.exe '+outfile[i]
endfor
printf,lun0,'cd $dir0'
printf,lun0,'t1=$(date)'
printf,lun0,"echo 'program begin at' $t0"
printf,lun0,"echo 'program  end  at' $t1"
free_lun,lun0
spawn,'chmod +x ~/gsoft/scripts/constraintRun'
end
;==================================================================
;==================================================================
;help,outi,/struc
;if keyword_set(alltree)then nhalo=totnhalo else begin
;  nhalo=nhalos[0] 
;  halosn=lindgen(nhalo)
;endelse
;breaktree4,fname,Descendant,FirstHaloInFOFgroup,$
;      mhalo,snapnum,pos,velocity,$
;;      tag=['Descendant','FirstHaloInFOFgroup','len',$
;      'snapnum','pos','vel'],halosn=halosn,alltree=alltree
;mhalo=temporary(mhalo)*particlemass[isim]
;out=replicate(outi,nhalo)
;if keyword_set(alltree) then begin
;  for itree=1L,ntree-1 do begin
;    skip_nhalo=total(nhalos[0:itree-1],/int)
;    out[skip_nhalo:skip_nhalo+nhalos[itree]-1].TreeID=itree
;  endfor
;  message,'tree ID constructing completed!',/continue
;  if skip_nhalo+nhalos[ntree-1] ne totnhalo then message,'error'
;endif
;out.NodeID=lindgen(nhalo)
;out.HostID=FirstHaloInFOFgroup
;out.Descendant=Descendant
;out.NodeMass=mhalo
;out.Redshift=zlist[snapnum]
;if keyword_set(modify) then tree_modify,fname,ntree,nhalos,out
;out.HalfMassRadius=HalfMassRadius
;pos=transpose(pos) & velocity=transpose(velocity) & spin=transpose(spin) 
;out.PX=pos[*,0] & out.PY=pos[*,1] & out.PZ=pos[*,2]
;out.VX=velocity[*,0] & out.VY=velocity[*,1] & out.VZ=velocity[*,2] 
;out.SX=spin[*,0] & out.SY=spin[*,1] & out.SZ=spin[*,2] 
;out.pos=pos & out.v=velocity ;& out.spin=spin
;write_array,out,outname,format='(4i9,8e14.6e3)'
;==================================================================
;==================================================================
;pro tree_ascii2,isim,fname,tree,ntree,totnhalo,nhalos,outname,zlist,$
;    alltree=alltree,modify=modify
;common simulation  ;,particlemass,unit
;if keyword_set(alltree)then nhalo=totnhalo else begin
;  nhalo=nhalos[0] 
;  halosn=lindgen(nhalo)
;endelse

;outi={TreeID:0L,NodeID:0L,HostID:0L,Descendant:0L}
;out=replicate(outi,nhalo)
;if keyword_set(alltree)then begin
;  for itree=1L,ntree-1 do begin
;    skip_nhalo=total(nhalos[0:itree-1],/int)
;    out[skip_nhalo:skip_nhalo+nhalos[itree]-1].TreeID=itree
;  endfor
;  message,'tree ID constructing completed!',/continue
;  if skip_nhalo+nhalos[ntree-1] ne totnhalo then message,'error'
;endif
;  breaktree_ph,fname,tree,totnhalo,v1,v2,tag=['FirstHaloInFOFgroup',$
;    	    'Descendant'],$
;    	    halosn=halosn,alltree=alltree
;  out.NodeID=lindgen(nhalo)
;  out.HostID=v1
;  out.Descendant=v2
;  if keyword_set(modify) then tree_modify,fname,ntree,nhalos,out
;  write_array,out,'temp0',format='(4i9)'
  
;  out=fltarr(2,nhalo)
;  breaktree_ph,fname,tree,totnhalo,v1,v2,tag=['len','snapnum'],$
;    	    halosn=halosn,alltree=alltree
;  v1=temporary(v1)*particlemass[isim]
;  out[0,*]=v1
;  out[1,*]=zlist[v2]
;;  out[2,*]=v3
;  write_array,out,'temp1',format='(e13.6e3,e14.6e3)'
  
;  out=0  ; free the memory
;  breaktree_ph,fname,tree,totnhalo,v1,v2,tag=['pos','vel'],$
;    	    halosn=halosn,alltree=alltree
;  write_array,v1,'temp2',format='(e13.6e3,2e14.6e3)'
;  write_array,v2,'temp3',format='(e13.6e3,2e14.6e3)'
;  write_array,v3,'temp4',format='(3e15.6e3)'
;  message,'temporary data writing completed!',/continue
;  v1=0 & v2=0 ;& halosn=0 ;& v3=0 ; free the memory
  
;  spawn,'paste temp0 temp1 temp2 temp3  > '+outname

;end
;==================================================================
;==================================================================
;pro tree_modify,nhalo0,tree,out
;  sn0=where((out.Descendant lt nhalo0)and(out.Descendant ne -1),n0)
;  if n0 ne 0 then begin
;    match2,tree.firstprogenitor,out[sn0].Descendant,sn1,sn2
;    match2,tree.FirstHaloInFOFgroup,tree[sn2].firstprogenitor,sn1,sn3
;    match2,tree.FirstHaloInFOFgroup,out[sn0].Descendant,sn1,sn2  ;some pointers are wrong
;    if n_elements(sn2) ne n0 then message,'error'
;    out[sn0].Descendant=out[sn2].NodeID
;    sn1=where(sn2 eq -1,n1)
;    if n1 ne 0 then out[sn0[sn1]].Descendant=-1
;  endif
;  sn0=where((out.HostID lt nhalo0)and(out.HostID ne -1))
;  out[sn0].HostID=out[sn0].NodeID
;end
;==================================================================
;==================================================================
;pro tree_ascii_combine
;common simulation  ;,particlemass,unit
;outi={TreeID:0,NodeID:0L,HostID:0L,Descendant:0L,$
;      NodeMass:0.,Redshift:0.,pos:fltarr(3),$
;      v:fltarr(3)} ;,rs:0.,ang:0.
;zlist=snaplist('ph',/redshift)
;openw,lun,'~/data/phoenix_tree/gcs/ph4_first.txt',/get_lun
;itree=0 & ihalo=0L
;for isim=3,19,2 do begin
;  print,!Phoenix.TreeFile[isim],isim
;  tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,itree=0)
;  nhalo=nhalos[0]
;  out=replicate(outi,nhalo)
;  out.treeID=itree
;  out.NodeID=lindgen(nhalo)+ihalo
;  out.HostID=tree.FirstHaloInFOFgroup
;  sn=where(out.HostID ne -1)
;  out[sn].HostID=out[sn].HostID+ihalo
;  out.Descendant=tree.Descendant
;  sn=where(out.Descendant ne -1)
;  out[sn].Descendant=out[sn].Descendant+ihalo
;  out.NodeMass=tree.len*!Phoenix.mp[isim]
;  out.Redshift=zlist[tree.snapnum]
;  out.rs=r_scale(out.NodeMass,out.Redshift)/unit.d
;  out.ang=sqrt(total(tree.spin^2,1))*out.NodeMass
;  out.pos=tree.pos & out.v=tree.vel  
;  printf,lun,out,format='(i2,3i9,8e14.6e3)'
;  itree=itree+1
;  ihalo=ihalo+nhalo
;endfor
;free_lun,lun
;end
;==================================================================
;==================================================================
;pro tree_ascii_self,isim,ntree,outname,zlist,alltree=alltree
;common simulation  ;,particlemass,unit
;outi={TreeID:0L,NodeID:0L,HostID:0L,Descendant:0L,$
;      NodeMass:0.,Redshift:0.,pos:fltarr(3),$
;      v:fltarr(3)} ;,rs:0.,ang:0.

;fname=!Phoenix.TreeFile[isim]
;tree=assoctree_ph(fname,lunx,ntree,totnhalo,nhalos)

;treeID=TreeSelfContained_read(isim,treeIndex=treeIndex,/ExistAtPresent) 
;nID=n_elements(treeIndex)
;if not(keyword_set(alltree)) then treeID=treeID[lindgen(nhalos[0])]
 
;openw,lun1,outname,/get_lun
;for i=0L,nID-1 do begin
;  itree=treeIndex[i]
;  if itree mod 100 eq 0 then print,'itree=',itree
;  halosn=where(treeID eq itree,nhalo)
;  breaktree_ph,fname,tree,totnhalo,descendant,firsthaloinfofgroup,len,$
;    	       SnapNum,pos,vel0,tag=['descendant','firsthaloinfofgroup',$
;	       'len','SnapNum','pos','vel'],halosn=halosn,tree0=tree0  

;  out=replicate(outi,nhalo)
;  out.treeID=itree
;  out.NodeID=halosn
;  out.HostID=FirstHaloInFOFgroup
;  out.Descendant=Descendant
;  out.NodeMass=len*!Phoenix.mp[isim]
;  out.Redshift=zlist[snapnum]
;;  out.rs=r_scale(out.NodeMass,out.Redshift)/unit.d
;  out.ang=sqrt(total(tree.spin^2,1))*out.NodeMass
;  out.pos=pos & out.v=vel0
;  if keyword_set(modify)and(itree gt 0) then tree_modify,nhalos[0],tree,out
;  printf,lun1,out,format='(4i9,8e14.6e3)'
;endfor
;free_lun,lun1
;close,/all
;end

