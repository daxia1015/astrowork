pro plot_SFRvsMstar
Name0=['nodeIsIsolated']
device,filename='gcsoutput/SFRvsMstar.eps',/color,/encapsul,xsize=18,ysize=16
multiplot_Gan,[3,3],mxtitle='M!d!m*!x!n / '+!Msun,$
    	      mytitle='SFR / '+!Msun+'yr!u-1!n'
for isim=3,15,2 do begin
  print,'isim=',isim
  fid=h5f_open(!galacticus.path+!Phoenix.treename[isim]+'_first_ev.hdf5')
  Data=nodeData_ReadMany(fid,'Output55',name0,/mstar,/sfr)
  sn=where((data.mstar gt 1)and(data.sfr gt 1))
  mstar=data.mstar[sn]
  sfr=data.sfr[sn]*1e-9  ; Msun/yr
  plot,mstar,sfr,psym=3,xrange=[2e7,2e12],yrange=[2e-3,2e3],/xlog,/ylog
  multiplot_Gan
endfor
multiplot_Gan,/default
end

;==================================================================
;==================================================================
pro plot_fE
nbin=16
level=0
iv=!Phoenix.isimv[*,level]
name0=['diskStellarMass','spheroidStellarMass','nodeVirialRadius']
device,filename='gcsoutput/EarlyFraction.eps',/color,/encapsul,xsize=10,ysize=8
plot,findgen(10)+0.1,findgen(10)*0.1,/nodata,/xlog,$
     xrange=[0.02,30],yrange=[0,0.68],xtitle='R / R!d200!n',$
     ytitle='Early Fraction'

for i=0,8 do begin
  mdr=hist_fE(!galacticus.treeEv[iv[i]],'Output25',nbin)
  oplot,mdr.ccR,mdr.fE,color=!myct.c9[i]
endfor
PhoenixTable
name=!PhoenixTable.name[iv]
labeling,0.4,0.9,0.1,0.08,name[0:3],/lineation,ct=!myct.c9[0:3]
labeling,0.7,0.9,0.1,0.08,name[4:8],/lineation,ct=!myct.c9[4:8]

end
;==================================================================
;==================================================================
pro plot_mdr
nbin=11
@include/contour_blue2red.pro
device,filename='gcsoutput/mdr_contour.eps',/color,/encapsul,xsize=18,ysize=16
multiplot_Gan,[3,3],mxtitle='R / R!d200!n',mytitle='B / T'
for isim=3,15,2 do begin
  print,'isim=',isim
;  plot,findgen(10)+0.1,findgen(10)*0.1,/nodata,xrange=[2e-2,5e2],$
;       yrange=[0.01,0.99],/xlog
  mdr=hist_mdr(!galacticus.path+!Phoenix.treename[isim]+'_first_ev.hdf5','Output55',nbin,R,bot)
;  hmax=max(mdr.h,sn)
;  print,sn
;  contour,mdr.h,mdr.R,mdr.bot,/xlog
  plot,R,bot,psym=3,/xlog,xrange=[2e-2,5e2],yrange=[0.01,0.99]
  multiplot_Gan
endfor
multiplot_Gan,/default
end
;==================================================================
;==================================================================
pro plot_cm_contour
nbin=21
mr1=findgen(15)-25
cgr_crit=-0.01*Mr1+0.44

device,filename='gcsoutput/cm_contour.eps',/color,/encapsul,xsize=18,ysize=16
multiplot_Gan,[3,3],mxtitle='Mr',mytitle='g-r'
for isim=3,19,2 do begin
  print,'isim=',isim
  plot,findgen(10)-20,findgen(10)*0.1,/nodata,xrange=[-24.9,-13.1],$
       yrange=[0.01,0.99]
  oplot,mr1,cgr_crit,linestyle=1
  cm=hist_color(!galacticus.treeEv[isim],'Output25',nbin)
  contour,cm.h,cm.Mr,cm.gr,/overplot;,xrange=[-22.8,-16.5],yrange=[0.05,1.1]
;    oplot,x,y/total(y),color=!myct.c3[ihost],psym=10
  multiplot_Gan
endfor
multiplot_Gan,/default

end
;==================================================================
;==================================================================
pro plot_cm_point
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8
name0='nodeIsIsolated'
mr1=findgen(15)-25
cgr_crit=-0.01*Mr1+0.44
device,filename='gcsoutput/cm_point.eps',/color,/encapsul,xsize=18,ysize=16
multiplot_Gan,[3,3],mxtitle='Mr',mytitle='g-r'
for isim=3,15,2 do begin
  print,'isim=',isim
  fid=h5f_open(!galacticus.path+!Phoenix.treename[isim]+'_first_ev.hdf5')
  Data=nodeData_ReadMany(fid,'Output55',name0,magName='SDSS_r',$
     	    	         ColorName='SDSS_g-SDSS_r')
  sn=where(data.mag lt -13)
  mr=data.mag[sn] & gr=data.ct[sn]

  plot,findgen(10)-20,findgen(10)*0.1,/nodata,xrange=[-24.9,-13.1],$
       yrange=[0.01,0.99]
  oplot,mr,gr,psym=3
  oplot,mr1,cgr_crit,linestyle=1
  multiplot_Gan
  h5f_close,fid
endfor
multiplot_Gan,/default

end
;==================================================================
;==================================================================
pro plot_color
OutputList,!galacticus.treeEv[3],nout,OutputName=OutputName,zlist=zlist
nbin=21
device,filename='gcsoutput/g_r.eps',/color,/encapsul,xsize=11,ysize=8
multiplot_Gan,[2,2],mxtitle='g-r',mytitle='Number Fraction'
ioutv=[54,50,40,30]
nbinv=[20,18,14,12]
for i=0,3 do begin
  iout=ioutv[i]
  plot,findgen(10)*0.1,findgen(10)*0.1,/nodata,xrange=[0.001,0.99],yrange=[0,0.39]
  for j=0,6 do begin
    isim=!Phoenix.isimv[j,0]
    fid=h5f_open(!galacticus.path+!Phoenix.treename[isim]+'_first_ev.hdf5')
    gr=nodeData_color(fid,outputName[iout],'SDSS_g-SDSS_r')
    gr=gr[where(gr ne 1000)]
    print,min(gr),max(gr)
    grmin=min(gr)>0.01
;    nbin=(max(gr)-grmin)/0.05
    h=histo(gr,nbin=nbinv[i],xmin=grmin,xmax=max(gr),locations=xh)
    h=h/total(h)
    if (i eq 3) then begin
      sn=indgen(nbinv[i]-1)+1
      oplot,xh[sn],h[sn],color=!myct.c9[j],psym=10
    endif else oplot,xh,h,color=!myct.c9[j],psym=10
    h5f_close,fid
  endfor
  xyouts,0.6,0.32,'z='+string(zlist[iout],format='(f4.2)')
  multiplot_Gan
endfor
multiplot_Gan,/default
end
;==================================================================
;==================================================================
pro plot_global,psdir
evfile=!galacticus.treeEv[1] & print,evfile
sfrName=['Total','Disk','Spheroid']
a=GlobalHistory(evfile)
histv=[8,0,6]
device,filename=psdir+'global.ps',/color,/encapsul,xsize=10,ysize=8

y=a.(histv[0]) & y2=a.(histv[0]+1)
sn1=where(y2 gt 0)
y[sn1]=y[sn1]/y2[sn1]
plot,a.historyTime,y,xtitle='t / Gyr',ytitle='sSFR / (Gyr!u-1!n)',$
    /ylog,yrange=[2e-3,200],$
    ytickname=['10!u-2!n','10!u-1!n','1','10','10!u2!n']
for isfr=1,2 do begin
  y=a.(histv[isfr]) & y2=a.(histv[isfr]+1)
  sn1=where(y2 gt 0)
  y[sn1]=y[sn1]/y2[sn1]
  oplot,a.historyTime,y,color=!myct.c2[isfr-1],linestyle=isfr
endfor
multiplot,/default
labeling,0.55,0.9,0.1,0.1,sfrName,ct=[!myct.black,!myct.c2],$
    	 linestyle=indgen(3),/lineation
end
;================================================================
;================================================================
pro plot_mol  ;ratio of mass to light
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8
name0='nodeIsIsolated'
Lsun=10^(-4.67/2.5)  ; solar luminosity of SDSS_r band in AB magnitude system
device,filename='gcsoutput/mol.eps',/color,/encapsul,xsize=18,ysize=16
multiplot_Gan,[3,3],mxtitle='g - r',mytitle='M / L!dr!n'
for isim=3,19,2 do begin
  print,'isim=',isim
  fid=h5f_open(!galacticus.treeEv[isim] )
  Data=nodeData_ReadMany(fid,'Output25',name0,/mstar,LumiName='SDSS_r',$
    	    	    	 ColorName='SDSS_g-SDSS_r')
  sn=where(data.ct ne 1000)
  mstar=data.mstar[sn] & Lr=data.lumi[sn]/Lsun
  mol=mstar/Lr & gr=data.ct[sn]
;  print,min(mstar),max(mstar),min(lr),max(lr)
  print,min(mol),max(mol),min(gr),max(gr)
  plot,gr,mol,xrange=[0.01,0.99],yrange=[0.1,4.5],psym=3
  multiplot_Gan
  h5f_close,fid
endfor
multiplot_Gan,/default

end
