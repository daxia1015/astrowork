pro custom_sysv
defsysv,'!galacticus',exists=en
if en then return

datapath=!Phoenix.Path+'gcs/'
aa=datapath+!Phoenix.TreeName+'_first.hdf5'
ab=datapath+!Phoenix.TreeName+'_first_ev_preset1_shift0.hdf5'
;ab[3]=datapath+!Phoenix.TreeName[3]+'_first_ev.hdf5'  
;for i=2,3 do ab[i]=datapath+'constraint/'+!Phoenix.TreeName[i]+'_first_ev_t1.0.hdf5'  
defsysv,'!galacticus',{path:datapath,tree:aa,treeEv:ab,$
    	    	       today:13.578924148012504d,isim:0}

overall={low:1e7,high:1e12,nbin:26}
R200={low:1e8,high:3e11,nbin:15}
lgsSFRc=[-11,-10.5,-10,-9.5]
defsysv,'!smf',{nbin:26,ioutv:[54,43,31,18],isnapv:[71,60,48,35],$
    	    	nz:3,ng:3,nsq:3,nt:5,nr:4,$
    	    	zname:['0.0','0.3','1.0','3.0'],grc:0.65,bodc:1.0,$ ;1/10^0.8
		sSFRc:1e-11,ccR:[0,1,3,10000],$
		group:['All','Central','Satellite'],$
		region:['Whole Sky','<R!d200!n','[R!d200!n,3R!d200!n]',$
		'>3R!d200!n']}
defsysv,'!lf', {low:6e7,high:2e11,nbin:17}

defsysv,'!ioutv',[26,18,10],1  ;z~= 0, 0.56, 2.24

defsysv,'!ilev_v',!Phoenix.n_snap-1-indgen(8)*10 ,1  ;z~= 0, 0.5,  1,  2,3.5,  6 
defsysv,'!halokey',{hostz:1,densityz:0,hprofile:2}
;'!hprofile',  ;0:point object, 1:isothermal, 2:NFW, 3:Hernquist

trai={sub:-1L,host:-1L,t:0.,msub:0.,mhost:0.,d:0.,Vorb:0.,angl:0.,$
    	energy:0.,chost:0.,Vvir:0.,Rhost:0.,f_g:0.,lnlda:0.,f_df:0.,$
        yita:0.,epson:0.}
aa={t:0.,mri:0.,yita:0.,epson:0.}	    	  
defsysv,'!twobody',{track:{host:-1L,sub:-1L},tra:trai,life:aa,$
			     fnametra:'twobody/data/tra_'+!Phoenix.TreeName, $
			     fnamelife:'twobody/data/life_'+!Phoenix.TreeName}

ni={t:0.,Nhalo:0L,ngalaxy:lonarr(3),nsate:lonarr(6)}
gal= {iout   :-1,$  ;the beginning time to be identified
      index  :-1L,$ ;the nodeIndex of this halo in merger tree
    satellite:-1L,$
       parent:-1L,$
      mhalo  :0.0d,$  ;node mass
      mstar  :0.0d,$  ;stellar mass 
     isolated:-1,$    ;nodeIsIsolated, central galaxy
      ccR    :0.0d,$  ;cluster centric radius over virial radius
      bot    :0.0d,$
      IsLate :-1}    ;color classification
rec= {iout   :-1,$  ;the beginning time to be identified
      mhalo  :0.0d,$  ;node mass
      mstar  :0.0d,$  ;stellar mass 
      ccR    :0.0d,$  ;cluster centric radius over virial radius
      bot    :0.0d,$
      IsLate :-1}    ;color classification
Group=['Isolated','PoorCentral','RichCentral','EarlyCentral',$
       'LateCentral','MinorSatellite','MajorSatellite',$
       'EarlySatellite','LateSatellite']
defsysv,'!galaxy',{ngroup:9,nstage:3,ni:ni,$
    	    	   MajorRatio:0.3,data:gal,record:rec,$
		   Group:Group,stage:['Initial','Middle','Final']}


end
