pro treemaker_parameters
common TreeMakerParameters,Omega_Matter,Omega_b,Omega_DE,H_0,sigma_8,$
       powerSpectrumIndex,transferFunction,makeReferences,$
       haloMassesIncludeSubhalos,positionsArePeriodic,treesAreSelfContained,$
       treesHaveSubhalos,velocitiesIncludeHubbleFlow,$
       fileBuiltBy,fileTimestamp,source,lengthHubbleExponent,$
       lengthScaleFactorExponent,lengthUnitsInSI,massHubbleExponent,$
       massScaleFactorExponent,massUnitsInSI,velocityHubbleExponent,$
       velocityScaleFactorExponent,velocityUnitsInSI

;===cosmology===
Omega_Matter=0.25d
Omega_b=0.045d
Omega_DE=0.75d
h_0=0.73d
sigma_8=0.9d
powerSpectrumIndex=1.0d
transferFunction='BBKS'

makeReferences=0L

;===haloTrees===
treesHaveSubhalos = 1L
treesAreSelfContained = 1L
velocitiesIncludeHubbleFlow = 0L
positionsArePeriodic = 0L
haloMassesIncludeSubhalos = 1L

;===provenance===
fileBuiltBy='Jianling Gan using IDL'
fileTimestamp=systime()
source='Phoenix'

;===units===
massUnitsInSI = 1.98892D30         ;h^{-1} M_{\odot}
massHubbleExponent = -1L
massScaleFactorExponent = 0L
lengthUnitsInSI = 3.08567758135D22  ;h^{-1} Mpc /(1+z)
lengthHubbleExponent = -1L
lengthScaleFactorExponent = 1L
velocityUnitsInSI = 1000.0d          ;km/s
velocityHubbleExponent = 0L
velocityScaleFactorExponent = 0L
end
;========================================================
;========================================================
pro treemaker_cosmology,fid
common TreeMakerParameters
print,'making the group cosmology...'
gid=h5g_create(fid,'cosmology')
h5write_attribute,gid,['OmegaMatter','OmegaBaryon','OmegaLambda',$
    	    	  'HubbleParam','sigma_8','powerSpectrumIndex'],$
		  [Omega_Matter,Omega_b, Omega_DE,$
		   H_0,sigma_8,powerSpectrumIndex]
h5write_attribute,gid,'transferFunction',transferFunction
h5g_close,gid
end
;========================================================
;========================================================
pro treemaker_haloTrees,fid,isim,ntree,totnhalo,nhalos,zlist,alltree=alltree
common TreeMakerParameters
print,'making the group haloTrees...'
gid=h5g_create(fid,'haloTrees')
haloTreesName=['nodeIndex','descendentIndex','hostIndex','nodeMass',$
    	       'redshift','position','velocity']
;haloTreesData={nodeIndex:0L,descendentIndex:0L,hostIndex:0L,nodeMass:0.,$
;               redshift:0.,position:fltarr(3),velocity:fltarr(3)}

print,' --writting the attributes...'
h5write_attribute,gid,['treesHaveSubhalos','treesAreSelfContained',$
    	    	  'velocitiesIncludeHubbleFlow','positionsArePeriodic',$
		  'haloMassesIncludeSubhalos'],[treesHaveSubhalos,$
		   treesAreSelfContained,velocitiesIncludeHubbleFlow,$
		   positionsArePeriodic,haloMassesIncludeSubhalos]

print,' --reading the meger tree data...'
if keyword_set(alltree) then begin
  nhalotot=totnhalo
  tree=readtree_ph(!Phoenix.TreeFile[isim],/alltree)
endif else begin
  nhalotot=nhalos[0]
  tree=readtree_ph(!Phoenix.TreeFile[isim],itree=0)
endelse
chunk=long(sqrt(nhalotot))

help,tree,tree.Descendant,tree.pos

print,' --writting the dataset...'
h5write_dataset,gid,l64indgen(nhalotot),	haloTreesName[0],chunk_dimensions=[1,chunk]     ;nodeIndex
h5write_dataset,gid,tree.Descendant,	    	haloTreesName[1],chunk_dimensions=chunk     ;descendentIndex
h5write_dataset,gid,tree.FirstHaloInFOFgroup,	haloTreesName[2],chunk_dimensions=chunk     ;hostIndex
h5write_dataset,gid,tree.len*!Phoenix.mp[isim], haloTreesName[3],chunk_dimensions=chunk     ;nodeMass
h5write_dataset,gid,zlist[tree.snapnum],    	haloTreesName[4],chunk_dimensions=chunk     ;redshift
h5write_dataset,gid,tree.pos,	    	    	haloTreesName[5],chunk_dimensions=[3,chunk] ;position
h5write_dataset,gid,tree.vel,	    	    	haloTreesName[6],chunk_dimensions=[3,chunk] ;velocity

h5g_close,gid
end
;========================================================
;========================================================
pro treemaker_provenance,fid
common TreeMakerParameters
print,'making the group provenance...'
gid=h5g_create(fid,'provenance')
h5write_attribute,gid,[fileBuiltBy,fileTimestamp,source],$
    	    	['fileBuiltBy','fileTimestamp','source']
h5g_close,gid
end
;========================================================
;========================================================
pro treemaker_treeIndex,fid,isim,ntree,totnhalo,nhalos
print,'making the group treeIndex...'
gid=h5g_create(fid,'treeIndex')
treeIndexName=['firstNode','numberOfNodes','treeIndex']
chunk=long(sqrt(ntree))

first=lonarr(ntree)
for itree=1L,ntree-1 do first[itree]=total(nhalos[0:itree-1],/int)

h5write_dataset,gid,first,	    treeIndexName[0],chunk_dimensions=chunk    ;firstNode
h5write_dataset,gid,nhalos,	    treeIndexName[1],chunk_dimensions=chunk    ;numberOfNodes
h5write_dataset,gid,lindgen(ntree), treeIndexName[2],chunk_dimensions=chunk    ;treeIndex

h5g_close,gid
end
;========================================================
;========================================================
pro treemaker_units,fid
common TreeMakerParameters
print,'making the group units...'
gid=h5g_create(fid,'units')
h5write_attribute,gid,massUnitsInSI,'massUnitsInSI'
h5write_attribute,gid,[massHubbleExponent,massScaleFactorExponent],$
    	    	    ['massHubbleExponent','massScaleFactorExponent']
h5write_attribute,gid,lengthUnitsInSI,'lengthUnitsInSI'
h5write_attribute,gid,[lengthHubbleExponent,lengthScaleFactorExponent],$
		      ['lengthHubbleExponent','lengthScaleFactorExponent']
h5write_attribute,gid,velocityUnitsInSI,'velocityUnitsInSI'
h5write_attribute,gid,[velocityHubbleExponent,velocityScaleFactorExponent],$
		      ['velocityHubbleExponent','velocityScaleFactorExponent']

h5g_close,gid

end
;========================================================
;========================================================
pro tree_hdf
treemaker_parameters
zlist=snaplist('ph',/redshift)
home=getenv('HOME')
post=['_first','_full']
for isim=3,3 do begin
  for alltree=0,0 do begin
    tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,/readhead)
    outfile=home+'/data/phoenix_tree/gcs/'+!Phoenix.TreeName[isim]+post[alltree]+'.hdf5'
    print,'making the tree '+outfile
    fid=h5f_create(outfile)
    treemaker_cosmology, fid
    treemaker_haloTrees, fid,isim,ntree,totnhalo,nhalos,zlist,alltree=alltree
    treemaker_provenance,fid
    treemaker_treeIndex, fid,isim,ntree,totnhalo,nhalos
    treemaker_units,     fid
    h5f_close,fid
  endfor
endfor
end


;========================================================
;========================================================
;function treemaker_haloTrees_dataset,gid,nhalotot
;common TreeMakerParameters
;print,' --creating the dataset...'
;ndata=n_elements(haloTreesName)
;didv=lonarr(ndata)
;for i=0,ndata-1 do begin
;  datatype_id=h5t_idl_create(haloTreesData.(i))
;  n0=n_elements(haloTreesData.(i))
;  if n0 eq 1 then begin 
;    dims=nhalotot
;    chunk_dims=nhalotot/10000 
;  endif else begin
;    dims=[n0,nhalotot]
;    chunk_dims=[3,nhalotot/10000] 
;  endelse
;  dataspace_id=h5s_create_simple(dims)     
;  did=h5d_create(gid,haloTreesName[i],datatype_id,dataspace_id,$
;    	    	 chunk_dimensions=chunk_dims,gzip=9)
;  didv[i]=did
;  h5t_close,datatype_id
;  h5s_close,dataspace_id
;endfor  
;return,didv
;end
