;This function identify the type of galaxies color.
;The galaxies can be divided into red and blue sequences using a line
;with slope -0.01 that passed through g-r=0.65 at M_r=-21. The line is
;that: g-r=-0.01M_r+0.44
;Lin et al. 2008; Patton et al. 2011
;There may be other methods to identify the galaxy type.

;cgr: the color g-r
;Mr: the absolute magnitude in r band
;results: 1:blue, 0:red, -1:not galaxy

;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012
 
function GalaxyIsBlue,cgr,Mr,h0
csn=fix(cgr)
csn[*]=-1
sn=where((cgr ne 1000)and(Mr ne 1000))
cgr1=cgr[sn]
Mr1=Mr[sn]
;x=Mr1-5*alog10(h0)+23
;cgr_crit=1.022-0.0651*x-0.00311*x^2
cgr_crit=-0.01*Mr1+0.44

csn[sn]=cgr1 lt cgr_crit
return,csn
end
;==================================================================
;==================================================================
function GalaxyIsStarForming,SFR,Mstar,z
;Moustakas et al. 2013 ApJ 767 50
isSF=fix(SFR)
isSF[*]=-1
sn=where((SFR gt 0)and(Mstar gt 0))
lgSFRmin= -0.49+0.65*alog10(Mstar[sn]/10)+1.07*(z-0.1)

isSF[sn]=alog10(SFR[sn]) ge lgSFRmin
return,isSF
end
;==================================================================
;==================================================================
;This function identify the type of galaxies morphology (unstable).
;The galaxies is late type if the disk mass is greater than the spheroid mass.
;There may be other methods to identify the galaxy type.

;disk: disk mass
;spheroid: spheroid mass
;results: 1:late type, 0:early type


;Author: Jianling Gan
;Modifications:  Jianling Gan, 19.Jun.2012

function GalaxyIsLateType,disk,spheroid
sn=disk gt spheroid
return,sn
end
;==================================================================
;==================================================================
