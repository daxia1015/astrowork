pro MBCGgive,MBCG,data,irec,isim,sn
sn=sn[[0,1,3]]
MBCG[*,irec,isim].mhalo=data.nodeMass[sn]
MBCG[*,irec,isim].mstar=data.mstar[sn]
MBCG[*,irec,isim].mag=data.mag[sn]
MBCG[*,irec,isim].RC=data.RC[sn]
MBCG[*,irec,isim].Isolated=data.nodeIsIsolated[sn]
MBCG[*,irec,isim].Index=data.nodeIndex[sn]
end
;====================================================================
;====================================================================
pro MassiveBright_write  
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
print,'collecting the evolution of most massive cluster galaxies'
print,'   and brightest cluster galaxies...'
fid=h5f_create(!galacticus.path+'MagGapEvolution.hdf5')
n1=3 & irec0=!smf.ioutv[2] 
nrec=nout-irec0 & print,nrec
sn=indgen(nrec)+irec0
h5write_dataset,fid,tlist[sn],'historyTime'
h5write_dataset,fid,zlist[sn],'historyRedshift'

hi={mag:0.,mhalo:0.,mstar:0.,RC:0.,isolated:-1,index:-1L}
MCGh=replicate(hi,n1,nrec,!Phoenix.nsim)  ;1,2,4, halo
MCGs=replicate(hi,n1,nrec,!Phoenix.nsim)  ;1,2,4, star
BCG=replicate(hi,n1,nrec,!Phoenix.nsim)  ;1,2,4
MCGisBCG=intarr(nrec,!Phoenix.nsim)-1
MagGap=fltarr(2,nrec,!Phoenix.nsim)   ;dm12,dm14

Name0=['nodeMass','nodeIndex','nodeIsIsolated','nodeVirialRadius']
for isim=1,19 do begin
  print,'isim=',isim
  fidTree=h5f_open(!galacticus.treeEv[isim])
  for irec=0,nrec-1 do begin
    iout=irec0+irec
    data=nodeData_ReadMany(fidTree,outputName[iout],Name0,/Mstar,/pos,MagName='SDSS_r')
    R200=max(data.nodeVirialRadius,sn1)  ;& print,'R200=',R200
    center=data.pos[*,sn1]
    RC=pair_distance(data.pos,center)/R200
    data=create_struct(temporary(data),'RC',RC)
    ;==================================
    sn=reverse(sort(data.nodeMass))
    MBCGgive,MCGh,data,irec,isim,sn
    ;==================================
    sn=reverse(sort(data.mstar))
    MBCGgive,MCGs,data,irec,isim,sn
    ;==================================
    sn=sort(data.mag)
    MBCGgive,BCG,data,irec,isim,sn
    MagGap[0,irec,isim]=data.mag[sn[1]]-data.mag[sn[0]]
    MagGap[1,irec,isim]=data.mag[sn[2]]-data.mag[sn[0]]
    ;==================================
    if MCGs[0,irec,isim].index eq BCG[0,irec,isim].index then MCGisBCG[irec,isim]=1 else MCGisBCG[isim]=0
  endfor
  h5f_close,fidTree
endfor
h5write_manydata,fid,['FirstFourMCGh','FirstFourMCGs','FirstFourBCG','MCGisBCG','MagGap'],$
    	    	 MCGh,MCGs,BCG,MCGisBCG,MagGap

h5f_close,fid
end
;====================================================================
;==================================================================
