function TreeHostProgenitor,tree,hostID,nstep
progenitor=hostID  
nstep=hostID & nstep[*]=1
while 1 do begin
  first=tree[progenitor].firstprogenitor
  sn=where(first ne -1,n)
  if n eq 0 then break else begin
    progenitor[sn]=first[sn]
    nstep[sn]=nstep[sn]+1
  endelse
endwhile
return,progenitor
end
;==================================================================
;==================================================================
function TreeHostBranch,tree,hostID,nstep

HostBranch=replicate({snap:-1L,host:-1L},!Phoenix.n_snap)
first=hostID ;[ihost] 
while first ne -1 do begin
  haloi=tree[first]
  HostBranch[haloi.snapnum].snap=haloi.snapnum
  HostBranch[haloi.snapnum].host=first
;  print,haloi.snapnum,first,haloi.m200
  first=haloi.firstprogenitor
endwhile
sn=where(HostBranch.snap ne -1,nstep)
hostBranch=hostBranch[sn]
return,HostBranch
end
;==================================================================
;==================================================================
pro TreeBranchNoRepeat,neti,net,np_snap,ilev

    sn1=where(net.host eq neti.host,n1)
    if n1 eq 0 then begin
      net=[temporary(net),neti] 
      np_snap[ilev]=np_snap[ilev]+1
    endif else begin
      sn2=where(net[sn1].sub eq neti.sub,n2)
      if n2 eq 0 then begin
        net=[temporary(net),neti]
	np_snap[ilev]=np_snap[ilev]+1
      endif
    endelse

end
;==================================================================
;==================================================================
pro TreeSubBranch,tree,hostID,np_snap,net,nsub
host=hostID
haloi=tree[host]
first=haloi.firstprogenitor
if first eq -1 then return

ilev=haloi.snapnum
if n_elements(np_snap) eq 0 then begin
  np_snap=lonarr(!n_snap)
  np_snap[ilev]=1
endif

neti= {sub:-1L,host:-1L}
if n_elements(net) eq 0 then begin
  net=neti ;,msub:0.,mhost:0.}
  en1=0
endif else en1=1
;fp_lev=TreeHostBranch(tree,host,1)

while first ne -1 do begin
  haloi=tree[first]
  ilev=haloi.snapnum
  neti.sub=first & neti.host=host ;& neti.a=alist[ilev]
  TreeBranchNoRepeat,neti,net,np_snap,ilev
;  net=[temporary(net),neti]
;  np_snap[ilev].fp=first
;  np_snap[ilev]=np_snap[ilev]+1
  next=haloi.nextprogenitor
  while next ne -1 do begin
;    print,next
    halok=tree[next]
    ilev=halok.snapnum
    neti.sub=next & neti.host=host ;& neti.a=alist[ilev]
    TreeBranchNoRepeat,neti,net,np_snap,ilev
;    net=[temporary(net),neti]
;    np_snap[ilev]=np_snap[ilev]+1
    next=halok.nextprogenitor
  endwhile
  host=first
  first=haloi.FirstProgenitor
endwhile

nsub=n_elements(net)
if en1 eq 0 then begin
  net=net[1:nsub-1]
  nsub=nsub-1
endif

end
;==============================================================
;==============================================================
;==============================================================
pro TreeSubofSubBranch,tree,hostID,np_snap,net,nsub
fp_lev=TreeHostBranch(tree,hostID,1)
TreeSubBranch,tree,hostID,np_snap,net,nsub

;haloi=tree[net[0].sub]
khalo=0L ;& ilev=haloi.snapnum
while khalo lt nsub do begin
  host=net[khalo].sub
  if khalo mod 1000 eq 0 then print,khalo,nsub

  TreeSubBranch,tree,host,np_snap,net,nsub
  khalo++
endwhile

end
;==============================================================
;==============================================================
;==============================================================
function TreeGoodHostBranch,tree,hostmass
sn1=where(tree.snapnum eq !n_snap-1)
dm=abs(tree[sn1].m200-hostmass)
sn2=sort(dm)
;print,sn2[0:9]
n10=intarr(10)
for i=0,9 do begin
  host=sn1[sn2[i]]
  fp_lev=TreeHostBranch(tree,host,1)
;  fpp=fp_lev[20:!n_snap-1]
  sn3=where(fp_lev[20:!n_snap-1] eq -1,n3)
  if n3 lt 10 then return,fp_lev
  n10[i]=n3
endfor


nmin=min(n10,i)
host=sn1[sn2[i]]
fp_lev=TreeHostBranch(tree,host,1)
return,fp_lev
end
;  if ilev ge 0 then begin
;    if fp_lev[ilev] eq -1 then ilev=ilev-1
;    if ilev ge 0 then begin
;      if net[khalo].sub eq fp_lev[ilev] then begin
;        ilev=ilev-1
;	khalo++
;        continue
;      endif
;    endif
;  endif
