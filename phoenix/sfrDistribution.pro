pro sfrDistribution,fidr
gid=h5g_create(fidr,'sfrDistribution')
Name0=['nodeIsIsolated']
nbin=11
hi={h:dblarr(nbin,nbin),mstar:fltarr(nbin),sfr:fltarr(nbin)}
sSFR=replicate(hi,!Phoenix.nsim)
for isim=1,19 do begin
  print,'isim=',isim
  fid=h5f_open(!galacticus.treeEv[isim])
  Data=nodeData_ReadMany(fid,'Output55',name0,/mstar,/sfr)
  sn=where((data.mstar gt 1)and(data.sfr gt 1))
  mstar=data.mstar[sn]
  sfr=data.sfr[sn]*1e-9  ; Msun/yr
  print,min(mstar),max(mstar),min(sfr),max(sfr)
  h=histo2D(mstar,sfr,xmin=1e8,xmax=1e12,ymin=1e-3,$
    	    ymax=5e2,nbinx=nbin,nbiny=nbin,locationx=xh,$
	    locationy=yh,/xlog,/ylog)
  sSFR[isim].h=h
  sSFR[isim].mstar=xh
  sSFR[isim].sfr=yh
  h5f_close,fid
endfor
h5write_dataset,gid,sSFR,'SFRvsMstar'

print,h,xh,yh


end
