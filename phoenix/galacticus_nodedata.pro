;====================================================================
;====================================================================
function Galacticus_color,fid,outputName,ColorName
nc=n_elements(colorName)
c1=strmid(ColorName,0,1) & c2=strmid(ColorName,2,1)


for ic=0,nc-1 do begin
  dataname=['diskStellarLuminosity:SDSS_'+c1[ic]+':rest:z0.0000',$
    	    'spheroidStellarLuminosity:SDSS_'+c1[ic]+':rest:z0.0000']
  L1=NodeData_Read(fid,outputName,dataName=dataName,/sum)
  dataname=['diskStellarLuminosity:SDSS_'+c2[ic]+':rest:z0.0000',$
    	    'spheroidStellarLuminosity:SDSS_'+c2[ic]+':rest:z0.0000']
  L2=NodeData_Read(fid,outputName,dataName=dataName,/sum)
  cti=L1
  cti[*]=1000
  sn=where((L1 gt 0)and(L2 gt 0))
  cti[sn]=Luminosity2Magnitude(L1[sn]/L2[sn]) 
  cName=tagName(ColorName[ic])
  if nc eq 1 then ct=cti else begin
    if ic eq 0 then ct=create_struct(cName,cti) else $
        ct=create_struct(ct,cName,cti)
  endelse
endfor
return,ct
end
;====================================================================
;====================================================================
function Galacticus_Magnitude,fid,outputName,MagName
nM=n_elements(MagName)

for iM=0,nM-1 do begin
  dataname=['diskStellarLuminosity:SDSS_'+MagName[iM]+':rest:z0.0000',$
    	   'spheroidStellarLuminosity:SDSS_'+MagName[iM]+':rest:z0.0000']
  L1=NodeData_Read(fid,outputName,dataName=dataName,/sum)

;  L1=nodeData.(iM)+nodeData.(iM+nM)
  Magi=L1
  Magi[*]=1000
  sn=where(L1 gt 0)
  magi[sn]=luminosity2Magnitude(L1[sn])
  if nM eq 1 then Mag=Magi else begin
      if iM eq 0 then Mag=create_struct(MagName[iM],Magi) else $
        Mag=create_struct(Mag,MagName[iM],Magi)
  endelse
endfor
return,Mag
end
;==================================================================
;==================================================================
function Galacticus_metallicity,fid,outputName,dataName,_extra=_extra
n2=n_elements(dataName)
if (n2 mod 2) ne 0 then message,'dataName must have 2, 4, 6 ... elements.'

metalMass=NodeData_Read(fid,outputName,dataName=dataName[0:n2/2-1],/sum)
baseMass=NodeData_Read(fid,outputName,dataName=dataName[n2/2:n2-1],/sum)

metal=metalMass
metal[*]=1000
sn=where((metalMass gt 0)and(baseMass gt 0))
metal[sn]=metallicity(metalMass[sn],baseMass[sn],_extra=_extra)
return,metal
end
;==================================================================
;==================================================================
function Galacticus_nodeData,fid,outputName,dataName,gcolor=gcolor,$
    	 magnitude=magnitude,metal=metal,original=original,Rhalf=Rhalf,$
	 _extra=_extra

if keyword_set(gcolor) then begin
  data=Galacticus_color(fid,outputName,dataName)
endif

if keyword_set(magnitude) then begin
  data=Galacticus_magnitude(fid,outputName,dataName)
endif

if keyword_set(metal) then begin
  data=Galacticus_metallicity(fid,outputName,dataName,_extra=_extra)
endif

if keyword_set(original) then begin
  data=NodeData_Read(fid,outputName,dataName=dataName,_extra=_extra)
  if keyword_set(Rhalf) then data=HalfMassRadius(data,_extra=_extra)
endif

return,data
end
;==================================================================
;==================================================================
;function NodeData_scale,nodedata,dataTag,scaleTag
;nout=n_elements(outputName)
;ndata=n_elements(dataTag)
;nscale=n_elements(scaleTag)
;if keyword_set(scale) and (nscale eq 0) then message,'error'
;tagname=tag_names(nodeData.(0))
;dataTag=strupcase(dataTag)
;print,tagname
;for iout=0,nout-1 do begin
;if ndata eq 1 then data=nodeData else begin
;  data=0.
;  for i=0,ndata-1 do data=data+nodeData.(dataTag[i])
;endelse
;if nscale ne 0 then begin
;  base=0.
;  for i=0,nscale-1 do base=base+nodeData.(scaleTag[i])
;  sn=where(base gt 0)
;  data[sn]=data[sn]/base[sn]
;endif
;  if iout eq 0 then data=create_struct(outputName[iout],data1) else $
;    data=create_struct(data,outputName[iout],data1)
;endfor 
;return,data
;end

;==================================================================
;==================================================================
;pro LocalDensity_write
;fname=!galacticus.treeEv[3]
;OutputList,fname,nout,OutputName=OutputName,tlist=tlist,zlist=zlist
;dataname=['positionX','positionY','positionZ']
;GroupName='Outputs/'+outputName
;for isim=3,19,2 do begin
;  fname=!galacticus.treeEv[isim]
;  print,fname,isim
;  fid=h5f_open(!galacticus.treeEv[isim])
;  n_node=Galacticus_nNode(fid,outputName)
;  openw,lun,!galacticus.data+'localDensity/'+!treename[isim],/get_lun
;    writeu,lun,n_node
;    for iout=0,nout-1 do begin
;      pos=NodeData_Read(fid,outputName[iout],dataname=dataname,/readpos)
;      pos=float(temporary(pos))
;      ng=n_node[iout]
;      ro5=fltarr(ng)
;      print,iout,ng
;      for ig=0L,ng-1 do begin
;        p0=pos[*,ig]
;        d=sqrt((pos[0,*]-p0[0])^2+(pos[1,*]-p0[1])^2+(pos[2,*]-p0[2])^2)
;        sn=uniq(d,sort(d))
;        d5=d[sn[5]]
;        ro5[ig]=5.*3./(4*!pi*d5^3)
;      endfor
;      writeu,lun,ro5
;    endfor
;  free_lun,lun
;  h5f_close,fid
;endfor
;end
;==================================================================
;==================================================================
;function LocalDensity_read,fname,totnout,outputName
;nout=n_elements(outputName)
;ns=lonarr(totnout)
;openr,lun,fname,/get_lun
;  readu,lun,ns
;  for iout=0,nout-1 do begin
;    len=strlen(outputName[iout])
;    i=fix(strmid(outputName[iout],6,len-6))-1
;    skip=4L*totnout
;    if i gt 0 then skip=skip+total(ns[0:i-1],/int)*8
;    ro5i=dblarr(ns[i])
;    point_lun,lun,skip
;    readu,lun,ro5i
;    if iout eq 0 then out=create_struct(outputName[iout],ro5i) else $
;      out=create_struct(out,outputName[iout],ro5i)
;  endfor
;  if eof(lun) then message,'end of '+fname,/continue
;free_lun,lun
;return,out
;end
;==================================================================
;==================================================================
