;==================================================================
;==================================================================
function x500_eq,x500
c=!PhoenixTable.cNFW
f=2.5*x500^3/c^3-gx(x500)/gx(c)
return,f
end
;==================================================================
;==================================================================
;==================================================================
function x500_eq_p,x500
c=!PhoenixTable.cNFW
f=7.5*x500^2/c^3-gx_p(x500)/gx(c)
return,f
end


;==================================================================
;==================================================================
function PhoenixR500
PhoenixTable
help,!PhoenixTable,!PhoenixTable.cNFW,!PhoenixTable.R200

x500=inewton(2*!PhoenixTable.cNFW,'x500_eq','x500_eq_p')
R500=x500*!PhoenixTable.R200/!PhoenixTable.cNFW

for i=0,19 do print,!PhoenixTable.cNFW[i],x500[i],!PhoenixTable.R200[i],R500[i]
xi=!PhoenixTable.cNFW
xi=[[xi*0.5],[xi],[xi*2]]

x500=ifx_root(xi,'x500_eq')
for i=0,19 do print,!PhoenixTable.cNFW[i],x500[i]
return,R500
end
;==================================================================
;==================================================================
