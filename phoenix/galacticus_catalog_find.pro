function galaxy_read_data,isim
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
Name0=['nodeIndex','nodeIsIsolated','nodeMass','nodeVirialRadius',$
       'satelliteNode','parentNode','diskStellarMass','spheroidStellarMass']
Lsun=10^(-4.67/2.5)  ; solar luminosity of SDSS_r band in AB magnitude system
gal=!galaxy.data
fidData=h5f_open(!galacticus.treeEv[isim])
for iout=0,nout-1 do begin    
  Data=nodeData_ReadMany(fidData,outputName[iout],Name0,/pos)
  sn=where((data.diskStellarMass gt 1)and(data.spheroidStellarMass gt 1),ngal)
  gali=replicate(!galaxy.data,ngal)
  gali.iout=iout
  gali.index=data.nodeIndex[sn]
  gali.satellite=data.satelliteNode[sn]
  gali.parent=data.parentNode[sn]
;  gali.nodeTimeLastIsolated=data.nodeTimeLastIsolated[sn]
  gali.mhalo=data.nodeMass[sn]
  tot=data.diskStellarMass[sn]+data.spheroidStellarMass[sn]
  gali.mstar=tot
  gali.isolated=data.nodeIsIsolated[sn]
  ;===========================
  R200=max(data.nodeVirialRadius,sn1)
  center=data.pos[*,sn1]
  R=pair_distance(data.pos[*,sn],center)
  gali.ccR=R/R200
  ;===========================
  gali.bot=data.spheroidStellarMass[sn]/tot
  gali.IsLate=gali.bot le !smf.botc
  ;===========================
  gal=[gal,gali]
endfor
h5f_close,fidData
n=n_elements(gal)-1
gal=gal[1:n]
return,gal
end
;==================================================================
;==================================================================
pro galaxyExample,isim,fidCata,gal2,nstep,group
EV=replicate(!galaxy.record,nstep)
struct_assign,gal2,EV
ThisIndex=gal2[0].index
name=!Phoenix.TreeName[isim]+strtrim(string(ThisIndex),2)
h5write_dataset,fidCata,EV,name,GroupName='EvolutionExample/'+group
end
;==================================================================
;==================================================================
pro galaxyAssign,gal2,nstep,Initial,Middle,Final
rec=!galaxy.record
struct_assign,gal2[0],rec
initial=[initial,rec]
struct_assign,gal2[nstep/2],rec
middle=[middle,rec]
struct_assign,gal2[nstep-1],rec
final=[final,rec]
end
;==================================================================
;==================================================================
pro galaxyWrite,gidCata,ig,Initial,Middle,Final
ngal=n_elements(final)-1
initial=initial[1:ngal]
middle=middle[1:ngal]
final=final[1:ngal]
gid=h5g_open(gidCata,!galaxy.Group[ig])
h5write_dataset,gid,initial,'Initial'
h5write_dataset,gid,Middle,'Middle'
h5write_dataset,gid,Final,'Final'
h5g_close,gid
end
;==================================================================
;==================================================================
pro galaxy_find_isolated,isim,fidCata,gidCata,gal
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
ig=0
sn=where((gal.isolated eq 1)and(gal.satellite eq -1),ngal)
gal1=gal[sn]
index=gal1.index
index0=index[uniq(index,sort(index))]
nTrace=n_elements(index0)
initial=!galaxy.record & middle=!galaxy.record & final=!galaxy.record  
for iTrace=0,nTrace-1 do begin
  sn1=where(index eq index0[iTrace],nstep)
  if(nstep lt 20)then continue
  gal2=gal1[sn1]
  if max(gal2.iout) ne nout-1 then continue
  gal2=gal2[sort(gal2.iout)]
  if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig]
  galaxyAssign,gal2,nstep,Initial,Middle,Final
endfor
galaxyWrite,gidCata,ig,Initial,Middle,Final
end
;==================================================================
;==================================================================
pro galaxy_find_Central1,isim,fidCata,gidCata,gal
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
ig=1
sn=where((gal.isolated eq 1)and(gal.satellite ne -1),ngal)
gal1=gal[sn]
index=gal1.index
index0=index[uniq(index,sort(index))]
nTrace=n_elements(index0) & help,nTrace
match2,gal1.satellite,gal.index,sn1,sn2
;help,sn,sn1
if n_elements(sn) ne n_elements(sn1) then message,'error'
msub=gal[sn1].mhalo

initial1=!galaxy.record & middle1=!galaxy.record & final1=!galaxy.record  
initial2=!galaxy.record & middle2=!galaxy.record & final2=!galaxy.record  
for iTrace=0,nTrace-1 do begin
  sn1=where(index eq index0[iTrace],nstep)
  if(nstep lt 20)then continue
  gal2=gal1[sn1] 
  if max(gal2.iout) ne nout-1 then continue
  mr=msub[sn1]/gal2.mhalo
  sn2=where(mr ge !galaxy.MajorRatio,nMajor)
  gal2=gal2[sort(gal2.iout)]
  if nMajor eq 0 then begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig]
    galaxyAssign,gal2,nstep,Initial1,Middle1,Final1
  endif else begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig+1]
    galaxyAssign,gal2,nstep,Initial2,Middle2,Final2
  endelse
endfor
galaxyWrite,gidCata,ig,Initial1,Middle1,Final1
galaxyWrite,gidCata,ig+1,Initial2,Middle2,Final2
end
;==================================================================
;==================================================================
pro galaxy_find_Central2,isim,fidCata,gidCata,gal
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
ig=3
sn=where(gal.isolated eq 1,ngal)
gal1=gal[sn]
index=gal1.index
index0=index[uniq(index,sort(index))]
nTrace=n_elements(index0)

initial1=!galaxy.record & middle1=!galaxy.record & final1=!galaxy.record  
initial2=!galaxy.record & middle2=!galaxy.record & final2=!galaxy.record  
for iTrace=0,nTrace-1 do begin
  sn1=where(index eq index0[iTrace],nstep)
  if(nstep lt 20)then continue
  gal2=gal1[sn1] 
  if max(gal2.iout) ne nout-1 then continue
  gal2=gal2[sort(gal2.iout)]
  if gal2[nstep-1].IsLate eq 0 then begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig]
    galaxyAssign,gal2,nstep,Initial1,Middle1,Final1
  endif
  if gal2[nstep-1].IsLate eq 1 then begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig+1]
    galaxyAssign,gal2,nstep,Initial2,Middle2,Final2
  endif
endfor
galaxyWrite,gidCata,ig,Initial1,Middle1,Final1
galaxyWrite,gidCata,ig+1,Initial2,Middle2,Final2
end
;==================================================================
;==================================================================
pro galaxy_find_Satellite,isim,fidCata,gidCata,gal
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
ig=5
sn=where(gal.isolated eq 0,ngal)
gal1=gal[sn]
index=gal1.index
index0=index[uniq(index,sort(index))]
nTrace=n_elements(index0)
match2,gal1.parent,gal.index,sn1,sn2
if n_elements(sn) ne n_elements(sn1) then message,'error'
mhost=gal[sn1].mhalo

initial1=!galaxy.record & middle1=!galaxy.record & final1=!galaxy.record  
initial2=!galaxy.record & middle2=!galaxy.record & final2=!galaxy.record  
initial3=!galaxy.record & middle3=!galaxy.record & final3=!galaxy.record  
initial4=!galaxy.record & middle4=!galaxy.record & final4=!galaxy.record  
for iTrace=0,nTrace-1 do begin
  sn1=where(index eq index0[iTrace],nstep)
  if(nstep lt 10)then continue
  gal2=gal1[sn1] 
  if max(gal2.iout) ne nout-1 then continue
  mr=gal2.mhalo/mhost[sn1]
  sn2=sort(gal2.iout)
  gal2=gal2[sn2] & mr=mr[sn2]
  if mr[0] lt !galaxy.MajorRatio then begin
    if nstep ge nout-2 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig]
    galaxyAssign,gal2,nstep,Initial1,Middle1,Final1
  endif else begin
    if nstep ge nout-10 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig+1]
    galaxyAssign,gal2,nstep,Initial2,Middle2,Final2
  endelse
  if gal2[nstep-1].IsLate eq 0 then begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig+2]
    galaxyAssign,gal2,nstep,Initial3,Middle3,Final3
  endif
  if gal2[nstep-1].IsLate eq 1 then begin
    if nstep ge nout-3 then galaxyExample,isim,fidCata,gal2,nstep,!galaxy.group[ig+3]
    galaxyAssign,gal2,nstep,Initial4,Middle4,Final4
  endif
endfor
galaxyWrite,gidCata,ig,  Initial1,Middle1,Final1
galaxyWrite,gidCata,ig+1,Initial2,Middle2,Final2
galaxyWrite,gidCata,ig+2,Initial3,Middle3,Final3
galaxyWrite,gidCata,ig+3,Initial4,Middle4,Final4
end
;==================================================================
;==================================================================
pro galaxyDataCombine,fidCata,W0
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
print,'combining galaxy data in different group...'
Name0=['nodeVirialRadius','nodeIsIsolated','spheroidStellarMass']
datai={ccR:0.0d,nodeIsIsolated:-1L,mstar:0.0d,bot:0.0d,IsLate:-1L,weight:fltarr(4)}
combine=datai
gid=h5g_create(fidCata,'CombinedData')
for i=0,2 do begin
  iout=!smf.ioutv[i]
  for isim=3,19,2 do begin
    fid=h5f_open(!galacticus.treeEv[isim])
    data=nodeData_ReadMany(fid,outputName[iout],Name0,/pos,/mstar)
    R200=max(data.nodeVirialRadius,sn1)
    center=data.pos[*,sn1]
    ccR=pair_distance(data.pos,center)/R200
    ;================================
    sn=where(data.mstar gt 0,ngal)
    bot=data.spheroidStellarMass[sn]/data.mstar[sn]
    ;===================
    combinei=replicate(datai,ngal)
    combinei.ccR=ccR[sn]
    combinei.nodeIsIsolated=data.nodeIsIsolated[sn]
    combinei.mstar=data.mstar[sn]
    combinei.bot=bot
    combinei.IsLate=bot le !smf.botc
    VC=4*!pi*(R200*h0)^3/3.0	    ;h^-1 Mpc
    combinei.weight[0]=1.0/VC
    combinei.weight[1]=1.0/(26*VC)
    combinei.weight[2]=1.0/(1.0/max(W0[*,isim])-27*VC)
    combinei.weight[3]=max(W0[*,isim])
    combine=[combine,combinei]
    h5f_close,fid
  endfor
  ncom=n_elements(combine)-1
  combine=combine[1:ncom]
  h5write_dataset,gid,combine,outputName[iout]
endfor
h5g_close,gid
end
;==================================================================
;==================================================================
pro galaxy_catalog_find
common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
OutputList,!galacticus.treeEv[3],nout,OutputName=OutputName,tlist=tlist,zlist=zlist
zlist_sim=snaplist(/redshift)
tlist_sim=tGy(zlist_sim)
;print,tlist_sim
fidCata=h5f_create(!galacticus.path+'CatalogInfo.hdf5')
h5write_group,fidCata,'EvolutionExample',!galaxy.Group
h5write_group,fidCata,!Phoenix.Treename,!galaxy.Group
fidTree=h5f_open(!galacticus.path+'treeProperties.hdf5')
W0=h5read_dataset(fidTree,'VolumeWeight')
h5write_dataset,fidCata,W0,'VolumeWeight'
galaxyDataCombine,fidCata,W0
print,'tracing galaxies in different group...'
for isim=3,19,2 do begin 
  print,'isim=',isim
  gidCata=h5g_open(fidCata,!Phoenix.Treename[isim])
  gal=galaxy_read_data(isim)

  galaxy_find_isolated,isim,fidCata,gidCata,gal
;  galaxy_find_Central1,isim,fidCata,gidCata,gal
  galaxy_find_Central2,isim,fidCata,gidCata,gal
  galaxy_find_Satellite,isim,fidCata,gidCata,gal
  h5g_close,gidCata
endfor

h5f_close,fidCata
h5f_close,fidTree
print,'finish galaxy catalog finding.'

end
;==================================================================
;==================================================================
function galaxy_catalog_read,fid,ilev,group,stage
;fid=h5f_open(!galacticus.path+'CatalogInfo.hdf5')
isim=!Phoenix.isimv[0,ilev]
data=h5read_dataset(fid,stage,groupName=!Phoenix.TreeName[isim]+'/'+group)
for i=1,8 do begin
  isim=!Phoenix.isimv[i,ilev]
  data1=h5read_dataset(fid,stage,groupName=!Phoenix.TreeName[isim]+'/'+group)
  data=[data,data1]
endfor
;h5f_close,fid
return,data
end
;==================================================================
;==================================================================

;pro galaxy_find_i,gidCata,fidData,igroup,n_series,galTrace,satellite=satellite
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist
;Name0=['nodeIndex','nodeIsIsolated','parentNode','satelliteNode',$
;       'nodeMass']
;GroupPro='galaxy_find_'+!galaxy.group;

;for iout=0,nout-1 do begin
;  Data=Data_ReadMany(fidData,outputName[iout],Name0,/Mstar,pos=satellite,$
;           velocity=satellite)
;  call_procedure,GroupPro[igroup],gidCata,igroup,iout,Data,n_series,galTrace
;endfor
;h5write_dataset,gidCata,galTrace,!galaxy.group[igroup],GroupName='galaxyRecord'

;end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
;pro galaxy_trace_i,gidCata,fidData,igroup,n_series,satellite=satellite
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist
;galaxy_find_i,gidCata,fidData,igroup,n_series,galTrace,satellite=satellite

;index=galTrace.index
;index0=index[uniq(index,sort(index))]
;nTrace=n_elements(index0)
;print,'nTrace=',nTrace
;gal_initial=replicate(galTrace[0],nTrace/2)
;gal_middle =gal_initial
;gal_final  =gal_initial
;base=nTrace/10
;igal=0L
;if keyword_set(satellite) then nstepMin=[10,23] else nstepMin=[20,25]
;for iTrace=0L,nTrace-1 do begin
;  if iTrace mod base eq 0 then print,'iTrace=',iTrace
;  sn1=where(index eq index0[iTrace],nstep)
;  if(nstep lt nstepMin[0])then continue
;  gal1=galTrace[sn1]
;  if not(keyword_set(satellite)) then begin
;    if max(gal1.iout) ne nout-1 then continue
;  endif
;  gal1=gal1[sort(gal1.iout)]
;  if nstep ge nstepMin[1] then begin
;    name='gal'+strtrim(string(index0[iTrace]),2)
;    h5write_dataset,gidCata,gal1,name,GroupName='galaxyExample/'+!galaxy.group[igroup]
;  endif
;  gal_initial[igal]=gal1[0]
;  gal_middle [igal]=gal1[nstep/2]
;  gal_final  [igal]=gal1[nstep-1]
;  igal++
;endfor   
;gal_initial=gal_initial[0:igal-1]
;gal_middle =gal_middle [0:igal-1]
;gal_final  =gal_final  [0:igal-1]
;if keyword_set(central) then galaxy_MergerFrequency,gal_final

;h5write_dataset,gidCata,gal_initial,!galaxy.group[igroup],GroupName='galaxyThreeTrace/Initial'
;h5write_dataset,gidCata,gal_middle, !galaxy.group[igroup],GroupName='galaxyThreeTrace/Middle'
;h5write_dataset,gidCata,gal_Final,  !galaxy.group[igroup],GroupName='galaxyThreeTrace/Final'

;end
;====================================================================
;====================================================================
;pro galaxy_pair_trace_group,fidCata,pair_initial,pair_middle,pair_final
;mr=pair_initial.msub/pair_initial.mhost
;igroup=1
;for im=0,1 do begin
;  for ie=0,1 do begin
;    sn1=where((pair_initial.ecc ge !galaxy.ecrit[ie])and $
;    	      (pair_initial.ecc lt !galaxy.ecrit[ie+1])and $
;     	      (mr gt !galaxy.mr[im])and(mr le !galaxy.mr[im+1]),n1)
;    if n1 ne 0 then begin
;      pair1=pair_initial[sn1]
;      h5write_dataset,fidCata,pair1,!galaxy.stage[0],GroupName=!galaxy.group[igroup]
;      pair1=pair_middle[sn1]
;      h5write_dataset,fidCata,pair1,!galaxy.stage[1],GroupName=!galaxy.group[igroup]
;      pair1=pair_final[sn1]
;      h5write_dataset,fidCata,pair1,!galaxy.stage[2],GroupName=!galaxy.group[igroup]
;    endif
;    igroup++
;  endfor
;endfor
;end
;==================================================================
;==================================================================
;function galaxy_data_read,iout,satellite=satellite
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist
;common galaxy_catalog_find_block,gidCata,fidData
;common simulation  ;,!Phoenix.mp,unit

;dataname1=['nodeIndex','nodeIsIsolated','parentNode','satelliteNode',$
;    	   'nodeMass','nodeVirialRadius']
;dataname2=['diskStellarMass','spheroidStellarMass']
;dataname3=['positionX','positionY','positionZ']
;dataname4=['velocityX','velocityY','velocityZ']
;
;Data=Data_Read(fidData,outputName[iout],dataname=dataname1)
;data=Data
;data.nodeVirialRadius=data.nodeVirialRadius*PhoenixUnit.d     ;kpc

;Data=Data_Read(fidData,outputName[iout],dataname=dataname3,/readpos)
;data=create_struct(data,'pos',Data*PhoenixUnit.d)    ;kpc

;if keyword_set(satellite) then begin
;  Data=Data_Read(fidData,outputName[iout],dataname=dataname4,/readpos)
;  data=create_struct(data,'velocity',Data*PhoenixUnit.v)    ;kpc/Gyr
;endif

;Data=Data_Read(fidData,outputName[iout],dataname=dataname2,/sum)
;data=create_struct(data,'stellar',Data) 
;return,data
;end
;==================================================================
;==================================================================
;pro galaxy_MergerFrequency,gal_final
;common galaxy_catalog_find_block,gidCata,fidData
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist
;sateTrace=h5read_dataset(fidCata,'galaxyAllTrace/SatelliteGalaxies')
;nhost=n_elements(gal_final)
;for ihost=0L,nhost-1 do begin
;  gal1=gal_final[ihost]
;  sn0=where((sateTrace.hostIndex eq gal1.index)and(sateTrace.iout eq gal1.iout),n0)
;  sn1=where((sateTrace.hostIndex eq gal1.index)and(sateTrace.iout lt gal1.iout),n1)
;  subIndex0=sateTrace[sn0].index
;  subIndex =sateTrace[sn1].index
;  subIndex=subIndex[uniq(subIndex,sort(subIndex))]
 
;endfor
;end
;==================================================================
;==================================================================
;pro galaxy_find_PoorCentral,gidCata,gidTree,Data
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
;print,'finding central galaxies which suffer less merger...'
;IsolatedBeginSnap=h5read_dataset(gidTree,'IsolatedBeginSnap')
;IsolatedProgenitor=h5read_dataset(gidTree,'IsolatedProgenitor')
;match,Data.nodeIndex,IsolatedProgenitor,sn1,sn2,count=n1
;if n1 ne 0 then begin 
;  print,n_elements(IsolatedProgenitor),n1
;  sn3=where((Data.Mstar[sn1] gt 1)and(IsolatedBeginSnap[sn2] le 32),ngal)
;endif else return
;
;if ngal ne 0 then begin
;  print,n_elements(IsolatedProgenitor),n1,ngal
;  index=sn1[sn3]
;  gal=replicate(!galaxy.PoorCentral,ngal)
;  gal.t0=tlist_sim[IsolatedBeginSnap[sn2[sn3]]]
;  gal.sn=index
;  gal.index=Data.nodeIndex[index]
;  gal.mhalo=Data.nodeMass[index]
;  gal.mstar=Data.mstar[index]
;  print,min(Data.mstar[index]),max(Data.mstar[index]),min(Data.nodeMass[index]),max(Data.nodeMass[index])
;  h5write_dataset,gidCata,gal,!galaxy.group[0]
;endif
;end
;==================================================================
;==================================================================
;==================================================================
;==================================================================
;pro galaxy_find_Central,gidCata,gidTree,Data
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
;print,'finding central galaxies which suffer frequent merger...'
;NstepInCentral=h5read_dataset(gidTree,'NstepInCentral')
;CentralProgenitor=h5read_dataset(gidTree,'CentralProgenitor')
;match,Data.nodeIndex,CentralProgenitor,sn1,sn2,count=n1
;if n1 ne 0 then begin 
;  sn3=where((Data.Mstar[sn1] gt 1)and(NstepInCentral[sn2] ge 40),ngal)
;endif else return

;if ngal ne 0 then begin
;  print,n_elements(CentralProgenitor),n1,ngal
;  index=sn1[sn3]
;  gal=replicate(!galaxy.richcentral,ngal)
;  gal.t0=tlist_sim[!Phoenix.n_snap-NstepInCentral[sn2[sn3]]]
;  gal.sn=index
;  gal.index=Data.nodeIndex[index]
;  gal.mhalo=Data.nodeMass[index]
;  gal.mstar=Data.mstar[index]
;  print,min(Data.mstar[index]),max(Data.mstar[index]),min(Data.nodeMass[index]),max(Data.nodeMass[index])
;  h5write_dataset,gidCata,gal,!galaxy.group[1]
;endif
;end
;==================================================================
;==================================================================
;function galaxy_find_Satellite,Data
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
;print,'finding satellite galaxies...'
;sn=where((data.mstar gt 100)and(Data.nodeIsIsolated eq 0)and $
;    	 (data.nodeTimeLastIsolated lt 13.1),ngal)
;                                     ^need modification
;print,ngal
;gal=replicate(!galaxy.satellite,ngal)
;gal.t0=data.nodeTimeLastIsolated[sn]
;gal.index=Data.nodeIndex[sn]
;gal.mhalof=Data.nodeMass[sn]
;gal.mstarf=Data.mstar[sn]
;return,gal
;end
;==================================================================
;==================================================================
;pro galaxy_give_satellite,gal,sn,fidData,iout
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
;Name0=['nodeIndex','parentNode','nodeMass']
;Data=nodeData_ReadMany(fidData,outputName[iout],Name0,/Mstar,/pos,/velocity)
;match2,gal[sn].index,data.nodeIndex,subsn,sn1
;gal[sn].mhaloi=data.nodeMass[subsn]
;gal[sn].mstari=data.mstar[subsn]
  
;match2,Data.nodeIndex,Data.parentNode[subsn],sn1,host
;gal[sn].hostIndex=Data.nodeIndex[host];

;gal[sn].mratio=gal[sn].mhaloi/Data.nodeMass[host]
;pair_orbits,Data,subsn,host,zlist[iout],eccentricity=eccentricity
;gal[sn].ecc=eccentricity
;end
;==================================================================
;==================================================================
;pro galaxy_trace_satellite,gal,fidData,gidCata
;common galaxy_catalog_find_block,nout,outputName,tlist,zlist,tlist_sim
;t0=gal.t0 & epsilon=0.01;

;sn=where(t0 lt min(tlist),n1)
;if n1 ne 0 then galaxy_give_satellite,gal,sn,fidData,0
;;                      ^need to be removed
;for iout=0,nout-2 do begin
;  t1=tlist_sim[iout*2+18] & t2=tlist_sim[iout*2+19]
;  sn=where((abs(t0-t1) lt epsilon)or(abs(t0-t2) lt epsilon),n1)
;;                   ^need modification
;  if n1 ne 0 then begin
;;    print,'iout=',iout,n1
;    galaxy_give_satellite,gal,sn,fidData,iout
;  endif
;endfor

;sn=where(gal.mhaloi eq 0,n1)
;if n1 ne 0 then message,'error'
;h5write_dataset,gidCata,gal,!galaxy.group[2]
;=====================================================
;isate=0
;for im=0,1 do begin
;  for ie=0,1 do begin
;    sn1=where((gal.ecc ge !galaxy.ecrit[ie])and(gal.ecc lt !galaxy.ecrit[ie+1]) $
;  	  and(gal.mratio ge !galaxy.mr[im])and(gal.mratio lt !galaxy.mr[im+1]),n1)
;    if n1 ne 0 then begin
;      gal1=gal[sn1]
;      h5write_dataset,gidCata,gal1,!galaxy.sateGroup[isate]
;    endif
;    isate++
;  endfor  
;endfor
;end
