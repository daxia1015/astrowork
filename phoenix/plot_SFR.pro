pro plot_sfr
fid=h5f_open(!galacticus.path+'globalHistory.hdf5')
tlist=h5read_dataset(fid,'historyTime')
Name=['nodeMass','StellarMass','StarFormationRate','specificStarFormationRate']
h5read_manydata,fid,Name,mhalo,mstar,SFR,sSFR,$
    	    	 groupName='treeVolumeNormalized'
h5f_close,fid
ilev=1 & ig=0 & isq=0 & it=0 & ir=0
iv=!Phoenix.isimv[*,ilev]

Msun='M'+sunsymbol()
ytit=['M!dhalo,tot!n / '+Msun,'M!dstar,tot!n / '+Msun,'SFR!dtot!n / ('+Msun+' yr!u-1!n)']
gName=['Satellite','Central','All']    

device,filename='gcsoutput/global.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle='t / Gyr' ;,myposition=[0.18,0.12,0.98,0.98]

for iph=0,2 do begin
  case iph of
    0:ytot=mhalo[*,iv,ig,isq,it,*]
    1:ytot=mstar[*,iv,ig,isq,it,*]
    2:ytot=SFR[*,iv,ig,isq,it,*]
  endcase
  xyouts,0.05,(!p.position[1]+!p.position[3])/2,ytit[iph],$
   	   alignment=0.5,orientation=90,/normal
  for ir=1,3 do begin
    plot,tlist,ytot[*,3,ig,isq,it,1],/ylog,/nodata,$
         xrange=[0.1,13.6],yrange=[min(ytot)+1,max(ytot)*1.2]
    if iph eq 0 then xyouts,1,min(ytot)*2,!smf.region[ir]
    for i=0,8 do begin
      oplot,tlist,ytot[*,i,ig,isq,it,ir],color=!myct.c9[i]
    endfor
    multiplot_Gan
  endfor
endfor
multiplot_Gan,/default
PhoenixTable
name=strmid(!PhoenixTable.name[iv],0,3)
labeling,0.2,0.85,0.05,0.04,name[0:3],/lineation,ct=!myct.c9[0:3]
labeling,0.2,0.55,0.05,0.04,name[4:8],/lineation,ct=!myct.c9[4:8]

end

;================================================================
;================================================================

