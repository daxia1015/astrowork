;==================================================================
;==================================================================
pro test_h5
dataname0=['nodeIndex','nodeMass','nodeVirialRadius','nodeTimeLastIsolated']
outputName='Output55'
PhoenixTable

for isim=1,19 do begin
  fid=h5f_open(!galacticus.treeEv[isim])
  data=nodedata_ReadMany(fid,outputName,dataName0,/Mstar,/pos,MagName='SDSS_r')
  Mhalo1=max(data.nodeMass,sn1)
  Mstar1=max(data.Mstar,sn2)
  Mag1=min(data.Mag,sn3)
  Rvir1=max(data.nodeVirialRadius,sn4)
;  print,isim,sn1,sn4,sn2,sn3
  pos=data.pos
  center=pos[*,sn1]
  R2=sqrt((pos[0,*]-center[0])^2+(pos[1,*]-center[1])^2+(pos[2,*]-center[2])^2)
  sn=where(R2 le Rvir1,n2)
;  print,isim,!PhoenixTable.R200[isim]/0.73-Rvir1,n1,n2
  print,isim,min(data.nodeTimeLastIsolated)
;  h1=histo(R1/Max(R1),nbin=11,xmax=1,/cumulative,/log10)
;  h2=histo(R2/Max(R2),nbin=11,xmax=1,/cumulative,/log10)
;  print,h1
;  print,h2
  
;  print,isim,pair_distance(center,pos[*,sn1])


  h5f_close,fid
endfor

end
