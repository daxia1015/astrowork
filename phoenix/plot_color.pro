pro plot_histogram_catacolor,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.group
ngroup=n_elements(group) & nstage=!galaxy.nstage
xr=[0.11,0.99];,[2e5,6e9],[2e-4,0.9]] 
xtit='g - r';,'M!doutflow!n / '+!Msun,'SFR/M!dstar!n / (Gyr!u-1!n)']

icol=0
hist=galaxyData_histogram(fid,group,'color_g_r',$
     	    /cumulative,nbin=nbin,xmin=xr[0],xmax=xr[1])
;icol++
;hv[*,*,icol]=galaxyData_histogram(fid,group,'outflowedMass',/lg,$
;     	    	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
;icol++		     
;hv[*,*,icol]=galaxyData_histogram(fid,group,'starFormationRate','stellarMass',/lg,$
;     	     	    /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
device,filename=psdir+'cata_color.eps',/color,/encapsul,xsize=8,ysize=15
multiplot,[1,3],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,$
    	  myposition=[0.2,0.1,0.98,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.03,!p.position[3]-0.04,!galaxy.stage[istage],/normal		 
  plot_histogram_panel,hist[*,istage],xrange=xr,yrange=yr,/cumulative
  multiplot
endfor
multiplot,/default
labeling,0.55,0.83,0.13,0.04,group,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)
end
;==================================================================
;==================================================================
pro plot_histogram_satecolor,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.sategroup
ngroup=n_elements(group) & nstage=!galaxy.nstage
;hi=dblarr(nbin)
;hv=replicate({xh:hi,yh:hi},ngroup,nstage,3)
xr=[0.41,0.99];,[2e6,9e8],[5e5,9e8],[2e-4,0.9]] 
xtit='g - r';,'CR / '+!MsunGyr,'M!doutflow!n / '+!Msun,'SFR/M!dstar!n / (Gyr!u-1!n)']

icol=0
hist=galaxyData_histogram(fid,group,'color_g_r',$
     	    	     /cumulative,nbin=nbin,xmin=xr[0],xmax=xr[1])
device,filename=psdir+'sate_color.eps',/color,/encapsul,xsize=10,ysize=8
plot_histogram_panel,hist[*,2],xrange=xr,yrange=yr,xtitle=xtit,ytitle=ytit,$
    	    position=[0.16,0.15,0.98,0.98],/cumulative
labeling,0.7,0.3,0.1,0.08,!galaxy.sateName,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)
end
;==================================================================
;==================================================================
