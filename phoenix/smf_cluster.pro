;====================================================================
;====================================================================
function SMFfit,mlow,mhigh,nbin,p,i=i,h=h
mlow=mlow+2*alog10(h) & mhigh=mhigh+2*alog10(h)
dbin=(mhigh-mlow)/(nbin-1)
lgm=findgen(nbin)*dbin+mlow

phi    =SchechterFunction(lgm,p[i].phis,    	     p[i].alpha,              p[i].lgms+2*alog10(h))
phi_up =SchechterFunction(lgm,p[i].phis+p[i].phisErr,p[i].alpha-p[i].alphaErr,p[i].lgms+p[i].lgmsErr+2*alog10(h))
phi_low=SchechterFunction(lgm,p[i].phis-p[i].phisErr,p[i].alpha+p[i].alphaErr,p[i].lgms+p[i].lgmsErr-2*alog10(h))
smf={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,smf
end
;====================================================================
;====================================================================
pro SMF_Vulcani2013,ICBS=ICBS,EDisCs=EDisCs,WINGS=WINGS,$
    	    	    PM2GC=PM2GC,weight=weight
;formula: Vulcani 2013
;ICBS: Mstar>10^10.5, z=0.33~0.45
;EDisCs: Mstar>10^10.2, z=0.4~1.0
;WINGS: Mstar>10^9.8, z=0.04~0.07, uncorrected for completeness
;PM2GC: Mstar>10^10.25, z=0.04~0.1, field
;without normalization
h=0.7 & omega0=0.3 & lambda0=0.7
p=replicate({name:'',lgms:0.,lgmsErr:0.,alpha,:0.,alphaErr:0.,phis:0.,phisErr:0.},8)
openr,lun,'SMF/data/Vulcani2013.txt',get_lun
  skip_lun,lun,1,/lines
  readf,lun,p,format='(a24,f7.2,f5.2,f7.2,f5.2,i5,i3)'
free_lun,lun
if n_elements(weight) eq 0 then weight=1e-2
p.phis=p.phis*weight
p.phisErr=p.phisErr*weight

;readcol,'SMF/data/Vulcani2013.txt',lgms,lgmsErr,alpha,alphaErr,phis,$
;    	phisErr,/silent,comment='#',format='x,f,f,f,f,f,f'
if arg_present(ICBS)   then   ICBS=smffit( 10.5,12.0, 9,p,i=0,h=h)
if arg_present(EDisCs) then EDisCs=smffit( 10.2,12.1, 9,p,i=3,h=h)
if arg_present(WINGS)  then  WINGS=smffit(  9.8,12.0,11,p,i=6,h=h)
if arg_present(PM2GC)  then  PM2GC=smffit(10.25,12.1, 9,p,i=7,h=h)
end
;====================================================================
;====================================================================
pro SMF_Calvi2013,WINGS=WINGS,PM2GC=PM2GC  ;low redshift
;formula: Calvi et al. 2013, corrected for completeness
;normalization for cluster: method of Vulcani et al. 2014
;WINGS: Mstar>>10^10.25, z=0.04~0.07, corrected for completeness, normalized with method of Vulcani et al. 2014
;PM2GC: Mstar>10^10.25, z=0.04~0.1, field
h=0.7 & omega0=0.3 & lambda0=0.7
p=replicate({name:'',alpha,:0.,alphaErr:0.,lgms:0.,lgmsErr:0.,phis:0.,phisErr:0.},6)
openr,lun,'SMF/data/Vulcani2013.txt',get_lun
  skip_lun,lun,1,/lines
  readf,lun,p,format='(a13,f6.1,f4.1,f7.2,f5.2,f9.3,f6.3)'
free_lun,lun

if arg_present(WINGS) then WINGS=smffit(10.25,12.0,9,p,i=1,h=h)
if arg_present(PM2GC) then PM2GC=smffit(10.25,12.0,9,p,i=2,h=h)
end

;====================================================================
;====================================================================
function SMF_Vulcani2011,lgm
;data: Vulcani et al. 2011, uncorrected for completeness
h=0.7 & omega0=0.3 & lambda0=0.7
if n_elements(lgm) eq 0 then begin 
  mlow=9.8+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif

phi    =SchechterFunction(lgm,1.552,      -0.987,      11.667+2*alog10(h))
phi_up =SchechterFunction(lgm,1.552+0.055,-0.987-0.009,11.667+0.052+2*alog10(h))
phi_low=SchechterFunction(lgm,1.552-0.055,-0.987+0.009,11.667-0.052+2*alog10(h))
wings={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,wings
end
;====================================================================
;====================================================================
function SMF_Vulcani2014,lgm,cluster=cluster
;data: Vulcani et al. 2011, uncorrected for completeness
;formula: Vulcani et al. 2014
h=0.7 & omega0=0.3 & lambda0=0.7 & z=0.06
;h=0.73 & omega0=0.25 & lambda0=0.75 & z=0.06
if n_elements(lgm) eq 0 then begin 
  mlow=9.8+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif

phi    =SchechterFunction(lgm,0.0069,       -0.7,    10.61+2*alog10(h))
phi_up =SchechterFunction(lgm,0.0069+0.0009,-0.7-0.1,10.61+0.07+2*alog10(h))
phi_low=SchechterFunction(lgm,0.0069-0.0009,-0.7+0.1,10.61-0.07+2*alog10(h))
if keyword_set(cluster) then begin
  dltc=(omega0*(1+z)^3+lambda0)/(omega0*(1+z)^3)
  f200=200*dltc
  c200=5.17 & x=c200*0.6 
  f206=200*gx(x)*dltc/gx(c200)/0.6^3
  phi=phi*f206
  phi_up=phi_up*f206
  phi_low=phi_low*f206
endif
wings={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,wings
end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
function SMF_Shapley,lgm,weight=weight
;formula: Mercurio 2012
h=0.7 ;& omega0=0.25 & lambda0=0.75 & z=0.06
if n_elements(lgm) eq 0 then begin 
  mlow=8.9 & mhigh=11.7 & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif
phi    =SchechterFunction(lgm,0.1,     -1.2,     11.14)
phi_up =SchechterFunction(lgm,0.1+0.01,-1.2-0.01,11.14+0.01)
phi_low=SchechterFunction(lgm,0.1-0.01,-1.2+0.01,11.14-0.01)

if n_elements(weight) ne 0 then begin
  phi=phi*weight
  phi_up=phi_up*weight
  phi_low=phi_low*weight
endif

shap={lgm:lgm,phi:phi,up:phi_up,low:phi_low}

return,shap
end
;====================================================================
;====================================================================
function SMF_Giodini2012,lgm,weight=weight 
;data: Giodini et al. 2012, COSMOS survey
;without normalization
;high mass groups, 6.7*10^13~8.2*10^13Msun
h=0.71  
V2=11.3*h^3  ;h^-3Mpc^3  z=0.2~0.4, Mhalo=8.2*10^13Msun
;V8=16.8*h^3  ;h^-3Mpc^3  z=0.8~1.0, Mhalo=6.7*10^13Msun
np=15
gio=replicate({lgm:0.,nall:0.,err:0.,merr:0.},np)
openr,lun,'SMF/data/Giodini2012.txt',/get_lun
  skip_lun,lun,3,/lines
  readf,lun,gio,format='(f7.3,105x,f8.2,f6.2,f7.2)'
free_lun,lun
phi    =gio.nall/v2
phi_up =(gio.nall+err)/v2
phi_low=(gio.nall-merr)/v2
giod={lgm:gio.lgm+2*alog10(h),phi:phi,up:phi_up,low:phi_low}
return,giod
end
;====================================================================
;====================================================================
function SMF_vanderBurg2013,lgm,weight=weight  ;at z~1
h=0.7 & omega0=0.3 & lambda0=0.7
;data: van der Burg et al. 2013, GCLASS cluster sample
;Gemini Cluster Astrophysics Spectroscopic Survey
;Mhalo=1*10^14~2.6*10^15 Msun
;R200=0.6~2.1 Mpc
;z~1
;without normalization
readcol,'SMF/data/vanderBurg2013.txt',lgm,Nall,Err,mErr,comment='#',$
    	/silent,format='f,i,i,i'
if n_elements(weight) eq 0 then weight=1.0 else weight=weight+0.0
van={lgm:lgm+2*alog10(h),phi:Nall*weight,up:(Nall+Err)*weight,$
     low:(Nall+mErr)*weight}

return,van
end
;====================================================================
;====================================================================
function SMF_GuoSIM,lgm,cluster=cluster
;formula: Vulcani et al. 2014
;alog10(Mhalo)=14.1
h=0.73 & omega0=0.25 & lambda0=0.75 & z=0.06
if n_elements(lgm) eq 0 then begin 
  mlow=9.4+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif

phi    =SchechterFunction(lgm,0.0128,       -1.081,      10.865+2*alog10(h))
phi_up =SchechterFunction(lgm,0.0128+0.0005,-1.081-0.004,10.865+0.004+2*alog10(h))
phi_low=SchechterFunction(lgm,0.0128-0.0005,-1.081+0.004,10.865-0.004+2*alog10(h))
if keyword_set(cluster) then begin
  dltc=(omega0*(1+z)^3+lambda0)/(omega0*(1+z)^3)
  f200=200*dltc
;  c200=5.17 & x=c200*0.6 
;  f206=200*gx(x)*dltc/gx(c200)/0.6^3
  phi=phi*f200
  phi_up=phi_up*f200
  phi_low=phi_low*f200
endif
guo={lgm:lgm,phi:phi,up:phi_up,low:phi_low}

return,guo
end
;====================================================================
;====================================================================
function SMF_Guo2011,field=field
h=0.73 & omega0=0.25 & lambda0=0.75

lgm=[ 7.125, 7.376, 7.618, 7.869, 8.120,$
      8.380, 8.622, 8.873, 9.124, 9.375,$
      9.625, 9.876,10.127,10.378,10.629,$
     10.871,11.122,11.372,11.623,11.874]
lgm=lgm+2*alog10(h)
     
;dN/d\lgm
phi=[4969.94,3714.64,2682.92,2339.42,1904.85,$
     1306.89,1101.20,912.129,693.520,509.550,$
     444.312,332.088,284.654,299.661,231.777,$
     100.148,40.4076,12.1857,7.94176,4.07250]
phi_up=[5231.95,3910.47,2873.14,2462.75,2039.91,$
        1423.73,1179.28,993.671,768.569,564.691,$
    	492.394,374.382,320.907,349.595,270.399,$
        122.995,54.0626,19.0204,13.7376,8.07893]
phi_low=2*phi-phi_up

V=4*!pi*1.46^3/3  ;R200=1.46 h^-1Mpc
phi=phi/V 
phi_up=phi_up/V
phi_low=phi_low/V
if keyword_set(field) then begin
; Weight=1/V200/(200[omega0*(1+z)^3+lambda0]/[omega0*(1+z)^3])
  phi=phi*omega0/200     ;dilute by 200/moega0
  phi_up=phi_up*omega0/200     ;dilute by 200/moega0
  phi_low=phi_low*omega0/200     ;dilute by 200/moega0
endif
guo2011={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,guo2011
end
;====================================================================
;====================================================================
