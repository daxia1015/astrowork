pro galaxyDistribution
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
print,'computing the galaxy type distribution...'
fid=h5f_create(!galacticus.path+'galaxyTypeDistribution.hdf5')

sSFRmin=[-15,-14.5,-14,-13.5,-13.5,-13.5] 
sSFRmax=[-9,-8.5,-8,-7.5,-7.5,-7.5]
nbin=[36,26]
hi={x:0.0,n:0.0d,f:0.0d}
bod =replicate(hi,nbin[0],!phoenix.nsim,!smf.nz,!smf.nr)
sSFR=replicate(hi,nbin[1],!phoenix.nsim,!smf.nz,!smf.nr)

name0=['diskStellarMass','spheroidStellarMass','nodeVirialRadius']
for isim=1,19 do begin
  print,'isim=',isim
  fidTree=h5f_open(!galacticus.treeEv[isim])
  for iz=0,!smf.nz-1 do begin
    iout=!smf.ioutv[iz]
    data=nodeData_ReadMany(fidtree,outputName[iout],Name0,/sfr,/pos)
    tot=data.spheroidStellarMass+data.diskStellarMass
    data.sfr=data.sfr*1e-9  ; Msun/yr
    R200=max(data.nodeVirialRadius,sn1)  ;& print,'R200=',R200
    center=data.pos[*,sn1]
    RC=pair_distance(data.pos,center)/R200
    
    for ir=0,!smf.nr-1 do begin
;      print,iz,ir
      rsn=regionOFgalaxy(RC,!smf.ccR,ir=ir)
      sn1=where((data.spheroidStellarMass gt 0)and(data.diskStellarMass gt 0)and(tot gt 9e7)and rsn,n1)
      sn2=where((data.sfr gt 0)and(tot gt 9e7)and rsn,n2)		 
      if n1 gt nbin[0] then begin
        a=alog10(data.spheroidStellarMass[sn1]/data.diskStellarMass[sn1])
    	h=histo(a,xmin=-3,xmax=4,locations=xh,nbin=nbin[0])
        bod[*,isim,iz,ir].x=xh
	bod[*,isim,iz,ir].n=h
	bod[*,isim,iz,ir].f=h/total(h)
      endif
      ;===========================================
      if n2 gt nbin[1] then begin
        a=alog10(data.sfr[sn2]/tot[sn2])
        h=histo(a,xmin=sSFRmin[iz],xmax=sSFRmax[iz],locations=xh,nbin=nbin[1])
        sSFR[*,isim,iz,ir].x=xh
        sSFR[*,isim,iz,ir].n=h	
        sSFR[*,isim,iz,ir].f=h/total(h) 
      endif
    endfor
  endfor
  h5f_close,fidTree
endfor
;=====================================
mi=histo_moment(/get_unit)
hi={n:replicate(mi,nbin[0]),f:replicate(mi,nbin[0])}
mom1=replicate(hi,2,!smf.nz,!smf.nr)
hi={n:replicate(mi,nbin[1]),f:replicate(mi,nbin[1])}
mom2=replicate(hi,2,!smf.nz,!smf.nr)
for ilev=0,1 do begin
  iv=!Phoenix.isimv[*,ilev]
  for iz=0,!smf.nz-1 do begin
    for ir=0,!smf.nr-1 do begin
      mom1[ilev,iz,ir].n=histo_moment( bod[*,iv,iz,ir].n,nbin[0])
      mom1[ilev,iz,ir].f=histo_moment( bod[*,iv,iz,ir].f,nbin[0])
      mom2[ilev,iz,ir].n=histo_moment(sSFR[*,iv,iz,ir].n,nbin[1])
      mom2[ilev,iz,ir].f=histo_moment(sSFR[*,iv,iz,ir].f,nbin[1])
    endfor
  endfor
endfor
h5write_manydata,fid,['BulgeOverDisk','specificStarFormationRate','moment_BoD',$
    	    	 'moment_sSFR'],bod,sSFR,mom1,mom2
h5f_close,fid
end
