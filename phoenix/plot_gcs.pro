pro plot_type
h5read_manydata,!galacticus.path+'galaxyTypeDistribution.hdf5',$
    	    	['BulgeOverDisk','specificStarFormationRate','moment_BoD',$
    	    	 'moment_sSFR'],bod,sSFR,mom1,mom2
device,filename='gcsoutput/sSFR.eps',/color,/encapsul,xsize=10,ysize=8
;multiplot_Gan,[2,2],mxtitle='log[sSFR / yr!u-1!n]',mytitle='Number'
lts=[0,2,1] & ilev=0
iv=!phoenix.isimv[*,ilev]
zname='z!m'+string(187b)+'!x'+!smf.zname[0:2]
ir=0
plot,findgen(10)-10,findgen(10)+1,/nodata,xrange=[-15,-8.01],$
     yrange=[2,3e3],/ylog,xtitle='log[sSFR / yr!u-1!n]',ytitle='Number',$
     position=[0.18,0.15,0.98,0.98]
for iz=0,2 do begin
  oplot,sSFR[*,ilev*2+1,iz,ir].x,mom2[ilev,iz,ir].n.average,linestyle=lts[iz]

endfor
labeling,0.02,0.22,0.12,0.07,zname,/lineation,linestyle=lts,thick=4

end
;==================================================
;==================================================

;device,filename='gcsoutput/bod.eps',/color,/encapsul,xsize=10,ysize=8
;plot,findgen(10)-5,findgen(10)+1,/nodata,xrange=[-4,4],yrange=[2,9e3],$
;     /ylog,xtitle='log[B / D]',ytitle='Fraction'
;for iz=0,2 do begin
;  for ir=0,0 do begin
;    iv=!phoenix.isimv[*,ilev]
;    oplot,bod[*,ilev*2+1,iz,ir].x,mom1[ilev,iz,ir].n.average,color=!myct.c4[ir],linestyle=iz
;    y=max(mom1[ilev,iz,ir].n.average,index)

;  endfor
;endfor

