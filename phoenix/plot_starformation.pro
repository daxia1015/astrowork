pro plot_histogram_catastar,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
dataName=['allGasMass','starFormationRate','stellarMass','outflowedMass']
;xrv=[[2e6,5e10],[2e4,5e10],[2e6,5e11]]
xrv=[[2e-4,3e-1],[2e-6,4e-2],[2e-6,2e-2],[2e-6,2e-2]]
xtitv=['M!dgas!n / M!dhalo!n','SFR / M!dhalo!n / (Gyr!u-1!n)','M!dstar!n / M!dhalo!n','M!doutflow!n / M!dhalo!n']


for igroup=0,2 do begin 
  device,filename=psdir+!galaxy.group[igroup]+'_star.eps',/color,/encapsul,xsize=18,ysize=6
  multiplot,[4,1],mytitle=ytit,mytitoffset=-1,myposition=[0.1,0.2,0.99,0.98]

  for icol=0,3 do begin
    xyouts,(!p.position[0]+!p.position[2])/2,!p.position[1]-0.16,xtitv[icol],/normal,alignment=0.5
    hv=galaxyData_histogram(fid,dataName[icol],'nodeMass',/lg,$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol],$
	     GroupName=!galaxy.group[igroup])
    plot_cata_panel,hv,/cumulative,xrange=xrv[*,icol],yrange=yr,/xlog   
    multiplot
  endfor  
  multiplot,/default
  labeling,0.01,0.9,0.05,0.1,!galaxy.stage,/lineation,ct=!myct.c3,linestyle=[2,1,0]
  xyouts,0.12,0.9,!galaxy.group[igroup],/normal
endfor
end
;==================================================================
;==================================================================
pro plot_histogram_satestar,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.sategroup
ngroup=n_elements(group) & nstage=!galaxy.nstage
hi=dblarr(nbin)
hv=replicate({xh:hi,yh:hi},ngroup,nstage,4)
;xrv=[[1.6,9.1],[5e6,9e9],[5e4,9e9],[5e6,9e10]]
xrv=[[1.6,9.1],[5e-5,3e-2],[3e-6,2e-2]]
xtitv=['t!dlife!n / Gyr','M!dgas!n / M!dhalo!n','SFR / M!dhalo!n / (Gyr!u-1!n)']

icol=0
hv[*,*,icol]=galaxyData_histogram(fid,group,'timeDuration',$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
icol++
hv[*,*,icol]=galaxyData_histogram(fid,group,'hotHaloMass','nodeMass',/lg,$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
icol++		     
hv[*,*,icol]=galaxyData_histogram(fid,group,'starFormationRate','nodeMass',/lg,$
     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])
;icol++		     
;hv[*,*,icol]=galaxyData_histogram(fid,group,'stellarMass',/lg,$
;     	     /cumulative,nbin=nbin,xmin=xrv[0,icol],xmax=xrv[1,icol])


device,filename=psdir+'sate_star.eps',/color,/encapsul,xsize=15,ysize=6
multiplot,[3,1],mytitle=ytit,myposition=[0.11,0.23,0.99,0.98],mytitoffset=-1
for istage=2,2 do begin
;  xyouts,0.78,!p.position[3]-0.04,!galaxy.stage[istage],/normal		 
  for icol=0,2 do begin
    if istage eq 2 then xyouts,(!p.position[0]+!p.position[2])/2,$
    	    	   !p.position[1]-0.18,xtitv[icol],/normal,alignment=0.5
    plot_histogram_panel,hv[*,istage,icol],xlog=icol ne 0,xrange=xrv[*,icol],yrange=yr,/cumulative
    multiplot
  endfor
endfor
multiplot,/default
labeling,0.1,0.38,0.06,0.1,!galaxy.SateName,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)

end
;==================================================================
;==================================================================
pro plot_histogram_cataOutflow,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.group
ngroup=n_elements(group) & nstage=!galaxy.nstage
xr=[2e-6,4e-2];,[2e-4,0.9]] 
xtit='M!doutflow!n / M!dhalo!n';,'SFR/M!dstar!n / (Gyr!u-1!n)']

hist=galaxyData_histogram(fid,group,'outflowedMass','nodeMass',/lg,$
     	    /cumulative,nbin=nbin,xmin=xr[0],xmax=xr[1])

device,filename=psdir+'cata_outflow.eps',/color,/encapsul,xsize=8,ysize=15
multiplot,[1,3],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,$
    	  myposition=[0.2,0.1,0.98,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.03,!p.position[3]-0.04,!galaxy.stage[istage],/normal		 
  plot_histogram_panel,hist[*,istage],/xlog,xrange=xr,yrange=yr,/cumulative
  multiplot
endfor
multiplot,/default
labeling,0.15,1,0.13,0.04,group,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)
end
;==================================================================
;==================================================================
pro plot_histogram_cataBHmass,psdir,fid
ytit='Cumulative Probability'& yr=[0.01,1.05]
nbin=30
group=!galaxy.group
ngroup=n_elements(group) & nstage=!galaxy.nstage
xr=[5e-9,7e-5];,[2e-4,0.9]] 
xtit='M!dBH!n / M!dhalo!n';,'SFR/M!dstar!n / (Gyr!u-1!n)']

hist=galaxyData_histogram(fid,group,'blackHoleMass','nodeMass',/lg,$
     	    /cumulative,nbin=nbin,xmin=xr[0],xmax=xr[1])

device,filename=psdir+'cata_BHmass.eps',/color,/encapsul,xsize=8,ysize=15
multiplot,[1,3],mxtitle=xtit,mytitle=ytit,mytitoffset=-1,$
    	  myposition=[0.2,0.1,0.98,0.99]
for istage=0,2 do begin
  xyouts,!p.position[0]+0.03,!p.position[3]-0.04,!galaxy.stage[istage],/normal		 
  plot_histogram_panel,hist[*,istage],/xlog,xrange=xr,yrange=yr,/cumulative
  multiplot
endfor
multiplot,/default
labeling,0,0.27,0.13,0.04,group,/lineation,ct=!myct.(ngroup-2),linestyle=indgen(ngroup)
end
;==================================================================
;==================================================================
