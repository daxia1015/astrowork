;====================================================================
;====================================================================
pro lf_write,fidr
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
print,' computing luminosity function...'
gid=h5g_create(fidr,'luminosityFunction')
LphiAll =dblarr(!lf.nbin,!Phoenix.nsim,3,3)
;first 3: all, red, blue
;second 3: all, central, satellite
LphiR200=dblarr(!lf.nbin,!Phoenix.nsim,3)
;first 3:  all, red, blue
Name0=['nodeVirialRadius','nodeIsIsolated']

level=strmid(!Phoenix.TreeName,3,1)
fidVW=h5f_open(!galacticus.path+'treeProperties.hdf5')
W0=h5read_dataset(fidVW,'VolumeWeight')
h5f_close,fidVW
h5write_dataset,gid,W0,'VolumeWeight'
Lsun=10^(-4.67/2.5)  ; solar luminosity of SDSS_r band in AB magnitude system
for isim=1,19 do begin
  fid=h5f_open(!galacticus.treeEv[isim])
  data=nodeData_ReadMany(fid,'Output55',Name0,lumiName='SDSS_r',/pos,$
       ColorName=['SDSS_g','SDSS_r'])
  data.lumi=data.lumi*h0^2/Lsun    ;h^-2 L_sun
;  if level[isim] eq '4' then W=min(W0[*,isim]) else W=max(W0[*,isim])
  W=W0[3,isim]
  ;==========================================
  for i=0,2 do begin
    if i eq 2 then lumi=data.lumi else begin
      sn1=where(data.nodeIsIsolated eq i)
      lumi=data.lumi[sn1]
    endelse
    Lphi=smf_histo(Lumi,!lf.nbin,!lf.low,!lf.high,weight=W,/yangLF,igroup=i)
    LphiSateCentAll[*,isim,i]=Lphi
  endfor
  ;==========================================
  h5f_close,fid
endfor
h5write_dataset,gid,LphiSateCentAll,'LphiSateCentAll'

mi=histo_moment(/get_unit)
mom=replicate(mi,!lf.nbin,2,3)
for ilev=0,1 do begin
  iv=!Phoenix.isimv[*,ilev]
  for ig=0,2 do begin
    Lphi=LphiSateCentAll[*,iv,ig]
    mom[*,ilev,ig]=smf_moment(Lphi,!lf.nbin)
  endfor
endfor
h5write_dataset,gid,mom,'moment_LphiSateCentAll'

lgL=histo(findgen(100)+1,nbin=!lf.nbin,xmin=!lf.low,xmax=!lf.high,/log10,/get_locations)
h5write_dataset,gid,lgL,'lgL'

h5g_close,gid
end
;====================================================================
;==================================================================
;pro lf_read,lgL=lgL,LphiSateCentAll=LphiSateCentAll,$
;    	    moment_LphiSateCentAll=moment_LphiSateCentAll
;fid=h5f_open(!galacticus.path+'ResultI.hdf5')
;gid=h5g_open(fid,'luminosityFunction');

;if arg_present(lgL) then lgL=h5read_dataset(gid,'lgL')
;if arg_present(LphiSateCentAll) then LphiSateCentAll=h5read_dataset(gid,'LphiSateCentAll')
;if arg_present(moment_LphiSateCentAll) then moment_LphiSateCentAll=h5read_dataset(gid,'moment_LphiSateCentAll')

;h5g_close,gid
;h5f_close,fid
;end
