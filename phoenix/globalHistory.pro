;====================================================================
;====================================================================
pro global_irec,data,isim,irec,mhalo,mstar,SFR,sSFR
for ig=0,!smf.ng-1 do begin
  gsn=groupOFgalaxy(data.iso,ig=ig)
  for isq=0,!smf.nsq-1 do begin
    sqsn=sqOFgalaxy(data.sfr,data.Mstar,!smf.sSFRc,isq=isq)
    for it=0,!smf.nt-1 do begin
      tsn=typeOFgalaxy(data.disk,data.spheroid,!smf.bodc,it=it)
      for ir=0,!smf.nr-1 do begin
    	rsn=regionOFgalaxy(data.RC,!smf.ccR,ir=ir)
	sn=where((data.Mstar gt 1)and gsn and sqsn and tsn and rsn,ngal)
	if ngal eq 0 then continue
    	mhalo[irec,isim,ig,isq,it,ir]=total(data.mhalo[sn])   ;Musn 
    	mstar[irec,isim,ig,isq,it,ir]=total(data.mstar[sn])  ;Musn  
    	SFR  [irec,isim,ig,isq,it,ir]=total(data.sfr[sn])  ;Musn/yr
    	sSFR [irec,isim,ig,isq,it,ir]= total(data.sfr[sn])/total(data.mstar[sn])
	
      endfor
    endfor
  endfor
endfor
end
;====================================================================
;====================================================================
pro global_normalize,fid,mhalo,mstar,SFR,sSFR,nrec,Wr
Name=['nodeMass','StellarMass','StarFormationRate','specificStarFormationRate']
h5write_manydata,fid,Name,mhalo,mstar,SFR,sSFR,$
    	    	 groupName='withoutNormalization'
;================================
for irec=0,nrec-1 do begin
  for isim=1,19 do begin
    for ir=0,!smf.nr-1 do begin
      W1=Wr[irec,isim,ir]
      mhalo[irec,isim,*,*,*,ir]=mhalo[irec,isim,*,*,*,ir]*W1
      mstar[irec,isim,*,*,*,ir]=mstar[irec,isim,*,*,*,ir]*W1
      SFR  [irec,isim,*,*,*,ir]=SFR  [irec,isim,*,*,*,ir]*W1
      sSFR [irec,isim,*,*,*,ir]=sSFR [irec,isim,*,*,*,ir]*W1
    endfor
  endfor
endfor

h5write_manydata,fid,[Name,'VolumeWeight'],mhalo,mstar,SFR,sSFR,Wr,$
    	         groupName='treeVolumeNormalized'
end
;====================================================================
;====================================================================
;find the history in different region?
pro globalHistory_write
common cosmology ;,omega0,lambda0,h0,omegab,nspec,sigma8
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
print,'computing the global history...'
fid=h5f_create(!galacticus.path+'globalHistory.hdf5')
nrec=nout/2 & print,nout,nrec
sn=(indgen(nrec)+(nout mod 2))*2
h5write_dataset,fid,tlist[sn],'historyTime'
mhalo=fltarr(nrec,!Phoenix.nsim,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
mstar=fltarr(nrec,!Phoenix.nsim,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
SFR  =fltarr(nrec,!Phoenix.nsim,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
sSFR =fltarr(nrec,!Phoenix.nsim,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
;nrec    :number of records
;nsim	 :number of simulations
;second 3: all, central, satellite
;Third  3: all, Star-Forming, Quiescent
;Fourth 3: all, Late, Early, lenticulars(S0), Ellipitcal 
;Fifth  4: all, (0,R200], [R200,3R200], [R200,infinite)
Wr=fltarr(nrec,!Phoenix.nsim,!smf.nr)
Name0=['nodeMass','diskStellarMass','spheroidStellarMass',$
       'nodeVirialRadius','nodeIsIsolated']
       
for isim=1,19 do begin
  print,'isim=',isim
  fidtree=h5f_open(!galacticus.treeEv[isim])
  level=strmid(!Phoenix.TreeName[isim],3,1)
  for irec=0,nrec-1 do begin
    iout=(irec+(nout mod 2))*2
    data=nodeData_ReadMany(fidtree,outputName[iout],Name0,/pos,/sfr)
    data.nodeVirialRadius=data.nodeVirialRadius*h0  ;h^-1 Mpc
    data.pos=data.pos*h0      ;h^-1 Mpc
    data.sfr=data.sfr*1e-9    ; Msun/yr
    tot=data.spheroidStellarMass+data.diskStellarMass
    if level eq '4' then WC=min(Weight0[*,isim]) else WC=max(Weight0[*,isim])
    R200=max(data.nodeVirialRadius,sn1)  ;& print,'R200=',R200
    center=data.pos[*,sn1]
    RC=pair_distance(data.pos,center)/R200

    V200=4*!pi*R200^3/3.0	    ;h^-3 Mpc^3
    Wr[irec,isim,*]=[WC,1.0/V200,1.0/(26*V200),1.0/(1.0/WC-27*V200)]
  
    data={disk:data.diskStellarMass,spheroid:data.spheroidStellarMass,$
    	  mhalo:data.nodeMass,Iso:data.nodeIsIsolated,Mstar:tot,$
	  sfr:data.sfr,RC:RC}
    global_irec,data,isim,irec,mhalo,mstar,SFR,sSFR
  endfor
  h5f_close,fidtree
endfor
help,mhalo,mdisk,msph,mstar,SFRd,SFRs,SFR,sSFR

global_normalize,fid,mhalo,mstar,SFR,sSFR,nrec,Wr

h5f_close,fid
end
;====================================================================
;==================================================================
;pro globalHistory_read,tlist,VolumeWeight=VolumeWeight,NodeMass=NodeMass,$
;    DiskStellarMass=DiskStellarMass,SpheroidStellarMass=SpheroidStellarMass,$
;    StellarMass=StellarMass,DiskStarFormationRate=DiskStarFormationRate,$
;    SpheroidStarFormationRate=SpheroidStarFormationRate,$
;    StarFormationRate=StarFormationRate

;fid=h5f_open(!galacticus.path+'globalHistory.hdf5')
;if n_elements(tlist) eq 0 then tlist=h5read_dataset(fid,'historyTime')

;if arg_present(VolumeWeight) then VolumeWeight=h5read_dataset(fid,'VolumeWeight')
;if arg_present(NodeMass) then NodeMass=h5read_dataset(fid,'NodeMass')
;if arg_present(DiskStellarMass) then DiskStellarMass=h5read_dataset(fid,'DiskStellarMass')
;if arg_present(SpheroidStellarMass) then SpheroidStellarMass=h5read_dataset(fid,'SpheroidStellarMass')
;if arg_present(StellarMass) then StellarMass=h5read_dataset(fid,'StellarMass')
;if arg_present(DiskStarFormationRate) then DiskStarFormationRate=h5read_dataset(fid,'DiskStarFormationRate')
;if arg_present(SpheroidStarFormationRate) then SpheroidStarFormationRate=h5read_dataset(fid,'SpheroidStarFormationRate')
;if arg_present(StarFormationRate) then StarFormationRate=h5read_dataset(fid,'StarFormationRate')

;h5f_close,fid
;end
