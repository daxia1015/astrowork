;$Id: //depot/idl/IDL_70/idldir/lib/fx_root.pro#1 $
;
; Copyright (c) 1994-2007, ITT Visual Information Solutions. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;       FX_ROOT
;
; PURPOSE:
;       This function computes real and complex roots (zeros) of
;       a univariate nonlinear function.
;
; CATEGORY:
;       Nonlinear Equations/Root Finding
;
; CALLING SEQUENCE:
;       Result = FX_ROOT(X, Func)
;
; INPUTS:
;       X :      A 3-element initial guess vector of type real or complex.
;                Real initial guesses may result in real or complex roots.
;                Complex initial guesses will result in complex roots.
;
;       Func:    A scalar string specifying the name of a user-supplied IDL
;                function that defines the univariate nonlinear function.
;                This function must accept the vector argument X.
;
; KEYWORD PARAMETERS:
;       DOUBLE:  If set to a non-zero value, computations are done in
;                double precision arithmetic.
;
;       ITMAX:   Set this keyword to specify the maximum number of iterations
;                The default is 100.
;
;       STOP:    Set this keyword to specify the stopping criterion used to
;                judge the accuracy of a computed root, r(k).
;                STOP = 0 implements an absolute error criterion between two
;                successively-computed roots, |r(k) - r(k+1)|.
;                STOP = 1 implements a functional error criterion at the
;                current root, |Func(r(k))|. The default is 0.
;
;       TOL:     Set this keyword to specify the stopping error tolerance.
;                If the STOP keyword is set to 0, the algorithm stops when
;                |x(k) - x(k+1)| < TOL.
;                If the STOP keyword is set to 1, the algorithm stops when
;                |Func(x(k))| < TOL. The default is 1.0e-4.
;
; EXAMPLE:
;       Define an IDL function named FUNC.
;         function FUNC, x
;           return, exp(sin(x)^2 + cos(x)^2 - 1) - 1
;         end
;
;       Define a real 3-element initial guess vector.
;         x = [0.0, -!pi/2, !pi]
;
;       Compute a root of the function using double-precision arithmetic.
;         root = FX_ROOT(x, 'FUNC', /double)
;
;       Check the accuracy of the computed root.
;         print, exp(sin(root)^2 + cos(root)^2 - 1) - 1
;
;       Define a complex 3-element initial guess vector.
;         x = [complex(-!pi/3, 0), complex(0, !pi), complex(0, -!pi/6)]
;
;       Compute a root of the function.
;         root = FX_ROOT(x, 'FUNC')
;
;       Check the accuracy of the computed root.
;         print, exp(sin(root)^2 + cos(root)^2 - 1) - 1
;
; PROCEDURE:
;       FX_ROOT implements an optimal Muller's method using complex
;       arithmetic only when necessary.
;
; REFERENCE:
;       Numerical Recipes, The Art of Scientific Computing (Second Edition)
;       Cambridge University Press
;       ISBN 0-521-43108-5
;
; MODIFICATION HISTORY:
;       Written by:  GGS, RSI, March 1994
;       Modified:    GGS, RSI, September 1994
;                    Added support for double-precision complex inputs.
;-

function ifx_root, xi, func, const, tol = tol

  on_error, 2 ;Return to caller if error occurs.

  x = xi + 0.0 ;Create an internal floating-point variable, x.
  sx = size(x)
  if sx[0] ne 2 then message,'x must be a (3,n) initial guess array.'
  if sx[1] ne 3 then message, 'x must be a 3-element initial guess vector in the first dimension.'
  if sx[3] ne 4 or sx[3] ne 5 then message,'solve for real root only!!!'
  ;Initialize keyword parameters.
;  if keyword_set(itmax)  eq 0 then itmax = 100
;  if keyword_set(stop)   eq 0 then stop = 0
  if keyword_set(tol)    eq 0 then tol = 1.0e-4

  ;Initialize stopping criterion and iteration count.
  cond = intarr(sx[2])  & root=fltarr(1,sx[2]);&  it = 0
  
  ;Begin to iteratively compute a root of the nonlinear function.
  sn1=where(cond eq 0,n1)
  while n1 ne 0 do begin
    print,'root not found=',n1
    q = (x[2,sn1] - x[1,sn1])/(x[1,sn1] - x[0,sn1])
    pls = (1 + q)
    const1=reform(const[sn1],1,n1)
    f0 = call_function(func, x[0,sn1],const1)
    f1 = call_function(func, x[1,sn1],const1)
    f2 = call_function(func, x[2,sn1],const1)
    a = q*f2 - q*pls*f1 + q^2*f0
    b = (2*q+1)*f2 - pls^2*f1 + q^2*f0
    c = pls*f2
    disc = b^2 - 4*a*c
    
    sn2=where(disc lt 0,n2)
    if n2 ne 0 then message,'no root'      
    rR0 = b + sqrt(disc)
    rR1 = b - sqrt(disc)
    div=rR0
    sn4=where(abs(rR0) le abs(rR1),n4)
    if n4 ne 0 then div[sn4]=rR1[sn4]     

    root[0,sn1]= x[2,sn1] - (x[2,sn1] - x[1,sn1]) * (2 * c/div)
    sn2=where(abs(root[0,sn1] - x[2,sn1]) le tol,n2)
    if n2 ne 0 then cond[sn1[sn2]]=1           ;Absolute error tolerance.
    evalFunc = call_function( func, root[0,sn1],const1 )
    sn2=where(abs(evalFunc) le tol,n2)
    if n2 ne 0 then cond[sn1[sn2]]=1          ;Functional error tolerance.
    x = [x[1,*], x[2,*], root[0,*]]
endwhile
return, root
end
