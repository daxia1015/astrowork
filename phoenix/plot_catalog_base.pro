;==================================================================
;==================================================================
pro

;==================================================================
;==================================================================
pro read_cata_num,fid
group=[!galaxy.group,!galaxy.sateGroup]
for i=0,6 do begin
  did=h5d_open(fid,group[i]+'/Final/nodeMass')
  sid=h5d_get_space(did)
  n=h5s_get_simple_extent_npoints(sid)
  print,group[i],n
  h5s_close,sid
  h5d_close,did
endfor
end
;==================================================================
;==================================================================
pro plot_cata_panel,hv,cumulative=cumulative,_extra=_extra
nstage=!galaxy.nstage
ct=!myct.(nstage-2)
x=findgen(100)*0.01+0.01 & y=x
plot,x,y,/nodata,_extra=_extra


for istage=0,nstage-1 do begin
  y=hv[istage].h
  if keyword_set(cumulative) then begin
    nbin=n_elements(y)
    y=y/y[nbin-1]
  endif else y=y/total(y)
  oplot,hv[istage].xh,y,linestyle=nstage-1-istage,color=ct[istage]

endfor
end
