function tlife_sim,isim
tra=twobody_read(!twobody.fnametra[isim],ngroup,totnstep,nsteps,IDs)
@include/twobody_nsteps.pro
lf=tra[step1].angl/tra[step0].angl  & print,min(lf),max(lf)
rf=tra[step1].d/tra[step1].Rhost

help,ngroup
sn1=where((IDs ge 3)and(lf lt 1),n1) & help,n1
step0=step0[sn1] & step1=step1[sn1]
tlife=replicate(!twobody.life,n1) 
tlife.t=tra[step1].t-tra[step0].t & print,min(tlife.t),max(tlife.t)
tlife.mri=tra[step0].msub/tra[step0].mhost & print,min(tlife.mri),max(tlife.mri)
tlife.yita=tra[step0].yita & print,min(tlife.yita),max(tlife.yita)
tlife.epson=tra[step0].epson & print,min(tlife.epson),max(tlife.epson)
return,tlife
end
;==================================================================
;==================================================================
pro plot_tlife
tlife=tlife_sim(1)
char=1.1
device,filename='twobody/tmerge.ps',/color,/encapsul,xsize=10,ysize=20
multiplot,[1,3],ygap=0.04,/doxaxis,mytitle='t /Gyr',mytitsize=char
!p.charsize=char & !x.charsize=char & !y.charsize=char

plot,tlife.mri,tlife.t,psym=1,xrange=[1e-6,0.1],/xlog,$
xtitle='m(0)/M(0)',/nodata
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.epson-0.9) lt 0.1))
oplot,tlife[sn].mri,tlife[sn].t,psym=1,color=!myct.red
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.epson-0.7) lt 0.1))
oplot,tlife[sn].mri,tlife[sn].t,psym=1,color=!myct.blue
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.epson-0.5) lt 0.1))
oplot,tlife[sn].mri,tlife[sn].t,psym=1,color=!myct.green
multiplot,/doxaxis


plot,tlife.yita,tlife.t,psym=1,xrange=[1e-1,9],/xlog,xtitle='!mh!x'
  multiplot,/doxaxis
plot,tlife.epson,tlife.t,psym=1,/ylog,xtitle='!me!x',/nodata
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.mri/1e-2-1) lt 1))
oplot,tlife[sn].epson,tlife[sn].t,psym=1,color=!myct.red
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.mri/1e-3-1) lt 1))
oplot,tlife[sn].epson,tlife[sn].t,psym=1,color=!myct.blue
sn=where((abs(tlife.yita-1) lt 0.1)and(abs(tlife.mri/1e-4-1) lt 1))
oplot,tlife[sn].epson,tlife[sn].t,psym=1,color=!myct.green


multiplot,/default

end
;==================================================================
;==================================================================
