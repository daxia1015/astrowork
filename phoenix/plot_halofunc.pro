pro plot_halofunc

read_halofunc,outx,halofunc,halofunc0
phname=strmid(!treefile,9,5)
remchar,phname,'-'
phlevel=fix(strmid(phname,3,1)) ;& help,phlevel
alist=snaplist(n_snap)
aname='a='+string(alist[!ilev_v],format='(f5.3)')

char=1.1 & nsim=n_elements(!treefile)
device,filename='halofunc/mfunc.ps',$
/color,/encapsul,xsize=20,ysize=25
multiplot,[4,5],mxtitle='m/M',mytitle='n(m/M)' ;'dN/dln(m/M)'
;!p.charsize=char & !x.charsize=char & !y.charsize=char
ihost=0
xtick0=['10!u-4!n','10!u-3!n','10!u-2!n','10!u-1!n'] 
ytick0=['1','10','10!u2!n'] 
multitick,[4,5],xtick0,ytick0,xtick,ytick

for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.m,halofunc0.m.ev.giocoli[*,4],linestyle=2,$      ;evolved, Giocoli 2007
  xrange=[5e-6,5e-2],yrange=[0.5,2e3],/xlog,/ylog,/nodata ;,$
  ;xtickname=xtick[*,isim] ;,ytickname=ytick[*,isim]
  for ilev=0,4 do begin
    y=halofunc.m[*,ilev,ihost,isim]; & dy=halofunc.m.err[*,ld,2,5]
    oplot,outx.m,y,color=!myct.c5[ilev] ;,thick=mlr*4
  endfor
  xyouts,5e-3,500,phname[isim]
  multiplot
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1
;===============================================================;
;===============================================================;
device,filename='halofunc/cmfunc.ps',$
/color,/encapsul,xsize=20,ysize=25
multiplot,[4,5],mxtitle='m/M',mytitle='N(>m/M)'
for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.m,halofunc0.m.ev.giocoli[*,4],linestyle=2,$      ;evolved, Giocoli 2007
  xrange=[5e-6,5e-2],yrange=[0.5,2e3],/xlog,/ylog,/nodata ;,$
  ;xtickname=xtick[*,isim] ;,ytickname=ytick[*,isim]
  for ilev=0,4 do begin
    y=halofunc.cm[*,ilev,ihost,isim]; & dy=halofunc.m.err[*,ld,2,5]
    oplot,outx.m,y,color=!myct.c5[ilev] ;,thick=mlr*4
  endfor
  xyouts,5e-3,500,phname[isim]
  multiplot,doxaxis=isim ge 15
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1

;===============================================================;
;===============================================================;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
device,filename='halofunc/vfunc.ps',/encapsul,/color,xsize=20,ysize=25
multiplot,[4,5],mxtitle='V!dmax,sub!n/V!dmax,host!n',mytitle='n(V!dmax,sub!n/V!dmax,host!n)'
for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.v,halofunc.v[*,0,0,0],$      ;evolved, Giocoli 2007
  xrange=[6e-3,0.5],yrange=[2,5e3],/xlog,/ylog,/nodata ;,$
;  xtickname=xtick[*,isim],ytickname=ytick[*,isim]
  for ilev=0,4 do begin
    y=halofunc.v[*,ilev,ihost,isim]; & dy=halofunc.m.err[*,ld,2,5]
;   dye=exp(dy/y)
    oplot,outx.v,y,color=!myct.c5[ilev] ;,thick=mlr*4
    ;if ilev eq 1 and isim eq 15 then print,y  
  endfor
  xyouts,0.15,900,phname[isim]
  multiplot,doxaxis=isim ge 15
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1

;===============================================================;
;===============================================================;
device,filename='halofunc/cvfunc.ps',/encapsul,/color,xsize=20,ysize=25
multiplot,[4,5],mxtitle='V!dmax,sub!n/V!dmax,host!n',mytitle='N(>V!dmax,sub!n/V!dmax,host!n)'
for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.v,halofunc.v[*,0,0,0],$      ;evolved, Giocoli 2007
  xrange=[6e-3,0.5],yrange=[2,5e3],/xlog,/ylog,/nodata ;,$
;  xtickname=xtick[*,isim],ytickname=ytick[*,isim]
  for ilev=0,4 do begin
    y=halofunc.cv[*,ilev,ihost,isim]; & dy=halofunc.m.err[*,ld,2,5]
;   dye=exp(dy/y)
    oplot,outx.v,y,color=!myct.c5[ilev] ;,thick=mlr*4
    ;if ilev eq 1 and isim eq 15 then print,y  
  endfor
  xyouts,0.15,900,phname[isim]
  multiplot,doxaxis=isim ge 15
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1

;===============================================================;
;===============================================================;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;===============================================================;
;------M2----------------------
mc=1  & char=1.1
device,filename='halofunc/crfunc.ps',/encapsul,/color,xsize=20,ysize=25 ;,xoffset=1.5,yoffset=10
multiplot,[4,5],mxtitle='r/R!dvir!n',mytitle='N(<r/R!dvir!n)' ;/N(<R!dvir!n)'
;!p.charsize=char & !x.charsize=char & !y.charsize=char
for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.r,halofunc0.r.DM,/xlog,/ylog,$                       ;unevolved, fitted by Giocoli 2007
  xrange=[0.02,1],yrange=[1,9e3],/nodata
  for ilev=0,4 do begin
    oplot,outx.r,halofunc.cr[*,mc,ilev,ihost,isim],color=!myct.c5[ilev] ;,thick=mlr*4
  endfor
  xyouts,0.025,2e3,phname[isim]
  multiplot,doxaxis=isim ge 15
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1
;===============================================================;
;===============================================================;
device,filename='halofunc/rfunc.ps',/encapsul,/color,xsize=20,ysize=25 ;,xoffset=1.5,yoffset=10
multiplot,[4,5],mxtitle='r/R!dvir!n',mytitle='n(r/R!dvir!n)'
;!p.charsize=char & !x.charsize=char & !y.charsize=char
for isim=0,nsim-1 do begin
  if (phlevel[isim] ne 2)and(phlevel[isim] ne 4) then continue
  plot,outx.r,halofunc0.r.DM,/xlog,/ylog,$                       ;unevolved, fitted by Giocoli 2007
  xrange=[0.02,1],yrange=[2,5e3],/nodata
  for ilev=0,4 do begin
    oplot,outx.r,halofunc.r[*,mc,ilev,ihost,isim],color=!myct.c5[ilev] ;,thick=mlr*4
  endfor
  xyouts,0.025,1e3,phname[isim]
  multiplot,doxaxis=isim ge 15
endfor
multiplot,/default
labeling,0.65,0.1,0.05,0.02,aname[0:4],ct=!myct.c5,/lineation ;,charsize=1



end
