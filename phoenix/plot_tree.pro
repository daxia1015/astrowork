pro plot_haloMF
fname=!galacticus.path+'haloMassFunction.hdf5'
h5Read_manydata,fname,['phiHalo','moment_phiHalo'],phiHalo,mom,groupName='tree0'
help,phiHalo,mom
xtit='log[M!dhalo!n / (h!u-1!n'+!Msun+')]'
ytit='dn/dlogM / (h!u3!nMpc!u-3!n)'
zname=['0.0','0.3','1.0']
device,filename='treeProperties/haloMF.eps',/color,/encapsul,xsize=18,ysize=7
multiplot_Gan,[3,1],mxtitle=xtit,mytitle=ytit,myposition=[0.11,0.19,0.99,0.9]

ilev=0 
for iz=0,2 do begin
  plot,findgen(10)+1,10^(-findgen(10)),/nodata,/ylog,xrange=[8.01,13.99],yrange=[2e-8,2e4]
  xyouts,(!p.position[0]+!p.position[2])/2,0.93,'z~'+zname[iz],alignment=0.5,/normal
  for ir=0,3 do begin 
    oplot,phiHalo[*,ilev*2+1,iz,ir].lgm,mom[*,ilev,iz,ir].average,$
    	  linestyle=ir,color=!myct.c4[ir]
    errplot,phiHalo[*,ilev*2+1,iz,ir].lgm,mom[*,ilev,iz,ir].low,$
    	    mom[*,ilev,iz,ir].up,color=!myct.c4[ir],width=0.02
  endfor
  multiplot_Gan
endfor
multiplot_Gan,/default
labeling,0.01,0.35,0.05,0.09,!smf.region,$
    	  /lineation,ct=!myct.c4,linestyle=indgen(4),thick=4

;====================================================================
device,filename='treeProperties/haloMF2.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[2,2],mxtitle='lg[M!dhalo!n / (h!u-1!n'+!Msun+')]',$
    	      mytitle='dn/dlogM / (h!u3!nMpc!u-3!n)';,/doxaxis,/doyaxis
for ir=0,3 do begin
  plot,findgen(10)+1,10^(-findgen(10)),/nodata,/ylog,$
       xrange=[8.01,13.99],yrange=[2e-7,9e3]
  for i=0,8 do begin
    isim=!Phoenix.isimv[i,ilev]
    oplot,phiHalo[*,isim,iz,ir].lgm,phiHalo[*,isim,iz,ir].phi,color=!myct.c9[i]
  
  endfor
  multiplot_Gan
endfor	      
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_spin
device,filename='treeProperties/spinDistribution.eps',/color,/encapsul,xsize=17,ysize=8
multiplot_Gan,[3,1],mxtitle='!ml!x',mytitle='fraction'
for isim=1,3 do begin
  print,isim
  plot,findgen(10)-5,findgen(10)*0.1,/nodata,xrange=[-0.32,0.32],yrange=[0,0.6]
  tree=readtree_ph(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos,itree=0)
  
  for ix=0,2 do begin
    print,ix,min(tree.spin[ix]),max(tree.spin[ix])
    h=histo(tree.spin[ix],nbin=21,xmin=-0.3,xmax=0.3,locations=xh)
    oplot,xh,h/total(h),color=!myct.c3[ix]
  endfor
multiplot_Gan
endfor
end
;====================================================================
;====================================================================
pro tree01
PhoenixTable

device,filename='treeProperties/tree01.eps',/color,/encapsul,xsize=18,ysize=8
multiplot_Gan,[2,1],mytitle='Fraction',xgap=0.02
log10=1
for isim=3,3 do begin
  fname=!Phoenix.TreeFile[isim] & Evname=!galacticus.treeEv[isim]
  tree=readtree_ph(fname,ntree,totnhalo,nhalos,/alltree)
;    sn=where(tree.snapnum eq !Phoenix.n_snap-1,n)
  center=tree[0].pos
  tree0=tree[1:nhalos[0]-1]
  tree1=tree[nhalos[0]:totnhalo-1]
  help,tree0,tree1
  m0=tree0.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  R0=pair_distance(tree0.pos,center)/!PhoenixTable.R200[isim]
  m1=tree1.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  R1=pair_distance(tree1.pos,center)/!PhoenixTable.R200[isim]
  print,min(m0),max(m0),min(R0),max(R0)
  print,min(m1),max(m1),min(R1),max(R1)

  h=histo(m0,nbin=11,xmin=min(m0)*1.1,xmax=0.1,log10=log10,locations=xh)  
  plot,10^xh,h/total(h),psym=10,xtitle='M!dhalo!n / M!d200!n',$
       xlog=log10,xrange=[4e-6,0.2],yrange=[0,0.49]
  h=histo(m1,nbin=11,xmin=min(m1)*1.1,xmax=max(m1)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10
  multiplot_Gan
  
  h=histo(R0,nbin=11,xmin=0.01,xmax=max(R0)*0.95,log10=log10,locations=xh)  
  plot,10^xh,h/total(h),psym=10,xtitle='d!dc!n / R!d200!n',$
       xlog=log10,xrange=[0.01,20],yrange=[0,0.49]
  h=histo(R1,nbin=11,xmin=min(R1)*1.1,xmax=max(R1)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10
  multiplot_Gan
      
endfor

multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro treeDensity
PhoenixTable
nbin=16 

device,filename='treeProperties/haloDis.eps',/color,/encapsul,xsize=8,ysize=15
multiplot_Gan,[1,3],mxtitle='d!dc!n / R!d200!n',/doyaxis
log10=1 & xr=[0.05,17]
for isim=5,5 do begin
  fname=!Phoenix.TreeFile[isim] 
  tree=readtree_ph(fname,ntree,totnhalo,nhalos,itree=0)
  center=tree[0]
  sn=where((tree.snapNum eq !Phoenix.n_snap-1)and(tree.len ne center.len),n0)
  tree=tree[sn]
  R200=!PhoenixTable.R200[isim]
  R=pair_distance(center.pos,tree.pos)/R200
  Rmin=min(R)*1.1 & Rmax=max(R)*0.95
  h=histo(R,nbin=nbin,xmin=Rmin,xmax=Rmax,dbin=dbin,log10=log10,locations=xh)  
  xh=10^xh & dbin2=10^dbin/2
  plot,xh,h/total(h),psym=10,ytitle='Number Fraction',$
       xlog=log10,xrange=xr,yrange=[0,0.39]
  multiplot_Gan,/doyaxis

  xV=4*!pi*R200^3*((xh+dbin2)^3-(xh-dbin2)^3)/3.0
  y=h/xV
  plot,xh,y,psym=10,ytitle='Number Density',$
       xlog=log10,xrange=xr,yrange=[0.011,90],/ylog
  multiplot_Gan,/doyaxis

  mass=tree.len*!PhoenixTable.mp[isim]
  h=histo(R,nbin=nbin,xmin=Rmin,xmax=Rmax,log10=log10,weight=mass)
  y=h/xV
  plot,xh,y,psym=10,ytitle='Mass Density',$
       xlog=log10,xrange=xr,yrange=[8e8,9.9e11],/ylog
  multiplot_Gan,/doyaxis
    
endfor

multiplot_Gan,/default

end

