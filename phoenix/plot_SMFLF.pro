;====================================================================
;====================================================================
pro plot_SMF_base
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8,12.1]
yr=[[2e-6,2e2],[2e-9,0.2]]
smf_Read,lgmAll=lgmAll,lgmR200=lgmR200,moment_phiR200=phi2,moment_phiAll=phiA,$
    	 phiAll=PhiAll,phiR200=phiR200,GroupName='stellarMassFunction'
;help,phi2,phi2,phiA
name=['Popesso2006','Li2009']
base=['within R!d200!n','whole cluster']
device,filename='SMF/SMF_base.eps',/color,/encapsul,xsize=17,ysize=8
multiplot_Gan,[2,1],mxtitle=xtit,mytitle=ytit,xgap=0.04,/doyaxis,$
    	      myposition=[0.08,0.17,1,0.9]

for ifig=0,1 do begin
  case ifig of 
    0:begin
        lgx=lgmR200
        y=SMF_Popesso2006()
        y1=phi2[*,*,0]
	y2=phiR200[*,*,0]
      end
    1:begin
        lgx=lgmAll
    	y=SMF_li2009()
	y1=phiA[*,*,0,0]
	y2=phiAll[*,*,0,0]
      end
  endcase
  plot,y.lgm,y.phi,/ylog,linestyle=1,thick=4,$
       xrange=xr,yrange=yr[*,ifig]
  errplot,y.lgm,y.low,y.up,width=0.02
  
  for ilev=0,1 do begin
    oplot,lgx,y1[*,ilev].average,color=!myct.c2[ilev]
    errplot,lgx,y1[*,ilev].low,y1[*,ilev].up,color=!myct.c2[ilev],width=0.02
  endfor
;  for isim=2,3 do oplot,lgx,y2[*,isim],color=!myct.c9[isim]
  xyouts,(!p.position[0]+!p.position[2])/2,0.93,base[ifig],alignment=0.5,/normal
  labeling,0.06,0.28,0.12,0.09,['Ph2','Ph4',name[ifig]],/lineation,$
    	   ct=[!myct.c2,0],linestyle=[0,0,1]
  multiplot_Gan,xgap=0.04,/doyaxis
endfor
multiplot_Gan,/default

end
;====================================================================
;====================================================================
pro plot_SMF_sca
li2009=SMF_li2009()
smf_Read,lgmAll=lgx,moment_phiAll=phi,GroupName='stellarMassFunction'
ilev=1

xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8.01,12.2] & yr=[5e-8,0.5]
gName=['All','Central','Satellite']
;fname=['SMF','LF']
c3=[0,!myct.red,!myct.blue]
device,filename='SMF/SMF_sca.eps',/color,/encapsul,xsize=18,ysize=7
multiplot_Gan,[3,1],mxtitle=xtit,mytitle=ytit,mtitle='whole cluster',$
    	    	mtitoffset=-0.5
for ig=0,2 do begin
  plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,/ylog,linestyle=1
  yang=SMF_Yang2009('SMF',ic=0,ig=ig)
  oplot,yang.lgx,yang.phi,linestyle=2
  for ilev=0,1 do begin
    oplot,lgx,phi[*,ilev,0,ig].average,color=!myct.c2[ilev]
    errplot,lgx,phi[*,ilev,0,ig].low,phi[*,ilev,0,ig].up,color=!myct.c2[ilev],width=0.02
  endfor
  xyouts,12,0.05,gName[ig],align=1
  multiplot_Gan
endfor
labeling,0.02,0.4,0.05,0.09,['Li2009','Yang2009','Ph2','Ph4'],/lineation,$
    	 ct=[0,0,!myct.c2],linestyle=[1,2,0,0]
multiplot_Gan,/default
end

;====================================================================
;====================================================================
pro plot_SMF_all
li2009=SMF_li2009()
smf_Read,lgmAll=lgx,moment_phiAll=phi,GroupName='stellarMassFunction'
ilev=1

xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8.01,12.2] & yr=[5e-8,0.5]
gName=['All','Central','Satellite']
;fname=['SMF','LF']
c3=[0,!myct.red,!myct.blue]
device,filename='SMF/SMF_all.eps',/color,/encapsul,xsize=18,ysize=7
multiplot_Gan,[3,1],mxtitle=xtit,mytitle=ytit,mtitle='whole cluster',$
    	    	mtitoffset=-0.5
for ig=0,2 do begin
  plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,/ylog,linestyle=1
  for it=0,2 do begin
    oplot,lgx,phi[*,ilev,it,ig].average,color=c3[it]
    errplot,lgx,phi[*,ilev,it,ig].low,phi[*,ilev,it,ig].up,color=c3[it],width=0.02
  endfor
  xyouts,12,0.05,gName[ig],align=1
  multiplot_Gan
endfor
labeling,0.02,0.4,0.05,0.09,['Li2009','All','Early','Late'],/lineation,$
    	 ct=[0,c3],linestyle=[1,0,0,0]
multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_SMF_R200
smf_Read,lgmR200=lgx,moment_phiR200=phi,GroupName='stellarMassFunction'
wings=SMF_wings2014(/cluster)
ilev=1

xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
tit=['within 0.6R!d200!n','within R!d200!n']
xr=[9.51,11.8] & yr=[2e-6,2e2]
fname=['SMF_R206','LF_R200']
c3=[0,!myct.red,!myct.blue]
device,filename='SMF/SMF_R200.eps',/color,/encapsul,xsize=10,ysize=8
;multiplot_Gan,[3,1],mxtitle=xtit,mytitle=ytit,mtitle='within R!d200!n',$
;    	    	mtitoffset=-0.5
plot,wings.lgm,wings.phi,linestyle=1,xtitle=xtit,ytitle=ytit,$
     title='within R!d200!n',xrange=xr,yrange=yr,/ylog
for it=0,2 do begin
  oplot,lgx,phi[*,ilev,it].average,color=c3[it]
  errplot,lgx,phi[*,ilev,it].low,phi[*,ilev,it].up,color=c3[it],width=0.02
endfor
labeling,0.05,0.4,0.1,0.08,['Vulcani2014','All','Early','Late'],/lineation,$
    	 ct=[0,c3],linestyle=[1,0,0,0]

end
;====================================================================
;====================================================================
pro plot_SMF_group
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='dN/dlogM!d!m*!x!n'
smf_Read,CataPhi=CataPhi,GroupName='stellarMassFunction'
	 
xr=[8.01,12.2] & yr=[2,2e4]
devite,filename='SMF/SMF_Group.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit
for ig=0,!galaxy.nGroup-1 do begin
  plot,findgen(10)+1,findgen(10)*10+1,$
       xrange=xr,yrange=yr,/ylog,/nodata
  for it=0,2 do begin
    oplot,cataPhi[*,ig,it].lgm,cataPhi[*,ig,it].phi,linestyle=2-it
  endfor
  multiplot_Gan
endfor

multiplot_Gan,/default
end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
pro plot_smf_Combine
smf_read,phiCombine=phiCombine,GroupName='stellarMassFunction'

xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8.01,12.2] & yr=[[2e-3,5e4],[2e-4,5e3],[9e-6,5]]
gName=['(0,R!d200!n]','(R!d200!n,3R!d200!n]','(3R!d200!n,max)']
zName='z='+string(!smf.zv,format='(f4.2)')
c3=[!myct.black,!myct.red,!myct.blue]
device,filename='SMF/SMF_Combine.eps',/color,/encapsul,xsize=18,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit
for ir=0,2 do begin
  for ig=0,0 do begin
;    plot,findgen(10)+1,findgen(10)*10+1,xrange=xr,yrange=yr[*,ir],/ylog,/nodata
    for iout=0,2 do begin
      plot,findgen(10)+1,findgen(10)*10+1,xrange=xr,yrange=yr[*,ir],/ylog,/nodata
      for it=0,2 do begin
        oplot,phiCombine[*,ir,ig,it,iout].lgm,phiCombine[*,ir,ig,it,iout].phi,$
      	      color=c3[it]
      endfor
      xyouts,12,yr[1,ir]*0.1,gname[ir],align=1
      xyouts,12,yr[1,ir]*0.01,zname[iout],align=1
      multiplot_Gan
    endfor
  endfor
endfor
labeling,0.02,0.79,0.05,0.04,['All','Early','Late'],/lineation,ct=c3

multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro plot_smf_sample
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[8,12.1] & yr=[2e-9,0.2]
li=SMF_li2009()
fname=!galacticus.path+['Field_e0.1.hdf5','Field_Benson2010.hdf5','Field_e1.0.hdf5']
output=['Output55','Output45','Output35']
device,filename='SMF/FieldSample.eps',/color,/encapsul,xsize=10,ysize=8
plot,li.lgm,li.phi,/ylog,linestyle=1,thick=4,xrange=xr,yrange=yr,$
     xtitle=xtit,ytitle=ytit
for i=0,2 do begin
  print,i
  y=smf_Sample(fname[i],output[i])
  oplot,y.lgm,y.phi,thick=i*3+1
endfor

end
;====================================================================
;====================================================================
pro plot_smf_t
xtit='log[M!d!m*!x!n / (h!u-2!n'+!Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlog!u-1!nM!d!m*!x!n)'
xr=[[8,12.1],[8,12.1]]
yr=[[2e-6,2e2],[2e-9,0.2]]
smf_Read,lgmAll=lgmAll,lgmR200=lgmR200,moment_phiR200=phi2,moment_phiAll=phiA,$
    	 phiAll=PhiAll,phiR200=phiR200,GroupName='smfConstraint'
;help,phi2,phi2,phiA
name=['Popesso2006','Li2009']
base=['within R!d200!n','whole cluster']
tName=['0.1','1.0','10']
device,filename='SMF/SMF_t.eps',/color,/encapsul,xsize=17,ysize=8
multiplot_Gan,[2,1],mxtitle=xtit,mytitle=ytit,xgap=0.03,/doyaxis,$
    	      myposition=[0.08,0.17,1,0.9]

for ifig=0,1 do begin
  case ifig of 
    0:begin
        lgx=lgmR200
        y=SMF_popesso2006()
        y1=phi2
	y2=phiR200
      end
    1:begin
        lgx=lgmAll
    	y=SMF_li2009()
	y1=phiA
	y2=phiAll
      end
  endcase
  plot,y.lgm,y.phi,/ylog,linestyle=1,thick=4,$
       xrange=xr[*,ifig],yrange=yr[*,ifig]
  errplot,y.lgm,y.low,y.up,width=0.02
  
  for it=0,2 do begin 
;    oplot,lgx,y1[*,it].average,thick=it*3+1
    oplot,lgx,y2[*,0,it],thick=it*3+1,color=!myct.pink
  endfor
  xyouts,(!p.position[0]+!p.position[2])/2,0.93,base[ifig],alignment=0.5,/normal
  labeling,0.05,0.35,0.12,0.08,[name[ifig],tName],/lineation,$
    	   thickv=[0,1,4,7],linestyle=[1,0,0,0]
  multiplot_Gan,/doyaxis
endfor
multiplot_Gan,/default



end
