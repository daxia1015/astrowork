;====================================================================
;====================================================================
function SMF_li2009,lgm,ig=ig
;SDSS
h=0.73
if n_elements(lgm) eq 0 then begin 
  mlow=8.0 & mhigh=12.0 & nbin=26
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif
range=[8,9.33,10.67,12]
para=[[0.0146,-1.13,  9.61],$
      [0.0132,-0.9 , 10.37],$
      [0.0044,-1.99, 10.71]]
phi=lgm & phi[*]=1e-10
for i=0,2 do begin
  sn=where((lgm ge range[i])and(lgm le range[i+1]),n)
  if n ne 0 then begin
    phi[sn]=SchechterFunction(lgm[sn],para[0,i],para[1,i],para[2,i])
  endif
endfor
moms=10^lgm/4.5e10
error=0.001*exp(-moms)/moms^0.3+1.0e-7
phi_up=phi+error
phi_low=phi-error
sn=where(phi_low le 0,n)
if n ne 0 then phi_low[sn]=phi[sn]/100
li2009={lgm:lgm,phi:phi,err:error,up:phi_up,low:phi_low}

return,li2009
end
;====================================================================
;====================================================================
function SMF_Yang2009,lgm,Fname=Fname,ig=ig,ic=ic,fit=fit
;data: Yang et al. 2009, SDSS
;ig=0,1,2: all, central, satellite
;ic=0,1,2: all, red, blue
if n_elements(ig) eq 0 then ig=0
if n_elements(ic) eq 0 then ic=0
h=0.73 & moega0=0.238 & lambda0=0.762
if keyword_set(fit) then begin
  if n_elements(lgm) eq 0 then begin 
    mlow=8.2 & mhigh=11.6 & nbin=19
    dbin=(mhigh-mlow)/(nbin-1)
    lgm=findgen(nbin)*dbin+mlow
  endif
  irow=ig*3+ic
  if(Fname eq 'SMF') then skip=11+irow else skip=1+irow
  
  p={phis:0.,alpha:0.,lgms:0.}
  openr,lun,'SMF/data/Yang2009/Yang2009fit.txt',/get_lun
    skip_lun,lun,skip,/lines
    readf,lun,p,format='(f7.5,f7.3,f7.3)'
  free_lun,lun
  yang=SchechterFunction(lgm,p.phis,p.alpha,p.lgms)
endif else begin 
  u=1e-2
  fname='SMF/data/Yang2009/Yang2009_'+Fname+'.txt'
  a=read_ascii(fname,comment='#')
  a=a.field01
  lgm=a[0,*]
  a=a*u

  icol=ig*6+ic*2+1
  y=a[icol,*] & ye=a[icol+1,*]
  yang={phi:y,up:y+ye,low:y-ye,lgm:lgm}
endelse
return,Yang
end
;====================================================================
;====================================================================
function SMF_CYang2009,mlow=mlow,mhigh=mhigh,nbin=nbin,ig=ig,ic=ic,$
    	 ih=ih,weight=weight
;data: Yang et al. 2009, SDSS
;ig: all, central, satellite
;ic: all, red, blue
;ih: halo mass range, for top to bottom, 0~8
if n_elements(ig) eq 0 then ig=0
if n_elements(ic) eq 0 then ic=0
fname='SMF/data/Yang2009/Yang2009_CSMF.txt'
skip=3+ic*10+ih
p={phis:0.,phisE:0.,alpha:0.,alphaE:0.,lgms:0.,lgmsE:0.,sigmac:0.,sigmacE:0.}
openr,lun,fname,/get_lun
  skip_lun,lun,skip,/lines
  readf,lun,p,format='(20x,f7.2,f5.2,f7.2,f5.2,f8.3,f6.3,f7.3,f6.3)'
free_lun,lun
h=0.73 & moega0=0.238 & lambda0=0.762
dbin=(mhigh-mlow)/(nbin-1)
lgm=dindgen(nbin)*dbin+mlow

;central
phi1    =LogNormalFunction(lgm,1.0,p.sigmac,p.lgms)
phi_up1 =LogNormalFunction(lgm,1.0,p.sigmac+p.sigmacE,p.lgms+p.lgmsE)   ;? error maybe wrong
phi_low1=LogNormalFunction(lgm,1.0,p.sigmac-p.sigmacE,p.lgms-p.lgmsE)   ;? error maybe wrong
;satellite
phi2    =SchechterFunctionYang(lgm,p.phis,p.alpha,p.lgms-0.25)
phi_up2 =SchechterFunctionYang(lgm,p.phis+p.phisE,p.alpha-p.alphaE,p.lgms-0.25+p.lgmsE)
phi_low2=SchechterFunctionYang(lgm,p.phis-p.phisE,p.alpha+p.alphaE,p.lgms-0.25-p.lgmsE)

case ig of
  0:yang={lgm:lgm,phi:phi1+phi2,up:phi_up1+phi_up2,low:phi_low1+phi_low2}
  1:yang={lgm:lgm,phi:phi1,up:phi_up1,low:phi_low1}
  2:yang={lgm:lgm,phi:phi2,up:phi_up2,low:phi_low2}
  else: message,'ig must be 0, 1, or 2'
endcase

if n_elements(weight) ne 0 then begin
  yang.phi=yang.phi*weight
  yang.up =yang.up *weight
  yang.low=yang.low*weight
endif
return,yang
end
;====================================================================
;====================================================================
function SMF_Baldry2012,ic=ic
;data: Baldry et al. 2012, MNRAS.421.621, Galaxy And Mass Assembly, GAMA
h=0.7  & omega0=0.3 & lambda0=0.7
readcol,'SMF/data/Baldry2012.txt',lgm,phi,err,comment='#',$
    	/silent,format='f,x,f,f'
u=1e-3/h^3
bald={lgm:lgm+2*alog10(h),phi:phi*u,up:(phi+err)*u,low:(phi-err)*u}
;phi=phi/h^3, unit?
return,bald
end
;====================================================================
;====================================================================
function SMF_Moustakas2013,iz=iz,isq=isq,ig=ig
;data: Moustakas et al. 2013, 2013ApJ...767...50M.pdf
;SDSS-GALEX at z~0.1
;from z=1 to z=0.2: PRism MUlti-object Survey, COSMOS, XMM-SXDS, 
;XMM-CFHTLS, CDFS, ELAIS-SI
;iz= 0,       1,          2,          3,          4,          5,          6
;   0.1, 0.20<z<0.30,0.30<z<0.40,0.40<z<0.50,0.50<z<0.65,0.65<z<0.80,0.80<z<1.00
;isq=0,1,2: all, Star-Forming, Quiescent;  seperated by SFR-Mstar diagram
if n_elements(iz) eq 0 then iz=0
if n_elements(isq) eq 0 then isq=0

h=0.7 & omega0=0.3 & lambda0=0.7
start=[3,39,74,108] & Num=[31,33,32,28]
if iz eq 0 then begin
  a=read_ascii('SMF/data/Moustakas2013.txt',data_start=start[0],Num_Records=Num[0])
  a=a.FIELD01   
  lgm=a[0,*]
  icol=isq*5+1
  lgphi=a[icol,*]	
  lgup=lgphi+a[icol+1,*]
  lglow=lgphi+a[icol+2,*]
  mous={lgm:lgm+2*alog10(h),phi:10^lgphi/h^3,up:10^lgup/h^3,low:10^lglow/h^3}
endif else begin 
  a=read_ascii('SMF/data/Moustakas2013.txt',data_start=start[isq+1],Num_Records=Num[isq+1])
  a=a.FIELD01   
  lgm=a[0,*]
  icol=(iz-1)*5+1
  lgphi=a[icol,*]	
  lgup=lgphi+a[icol+1,*]
  lglow=lgphi+a[icol+2,*]
  mous={lgm:lgm+2*alog10(h),phi:10^lgphi/h^3,up:10^lgup/h^3,low:10^lglow/h^3}
endelse
return,mous
end
;====================================================================
;====================================================================
function SMF_Bernardi2013,model=model,it=it
;data: Bernardi et al. 2013, http://arxiv.org/abs/1304.7778
;SDSS, analized with different light profile
;it=0,	    1,	    2,	3,   4,    5,    6
; All, Elliptical, S0, Sab, Scd, Late, Early
if n_elements(model) eq 0 then model='SerExp'
if n_elements(it) eq 0 then it=0
h=0.7  & omega0=0.3 & lambda0=0.7
a=read_ascii('SMF/data/Bernardi2013/MsF_'+Model+'.dat',comment='#')
a=a.FIELD01   ;& help,a
lgm=a[0,*] ;& help,lgm
if it le 4 then begin
  icol=it*2+1
  lgphi=a[icol,*]
  lgerr=a[icol+1,*]
endif else begin
  if it eq 5 then icol=7
  if it eq 6 then icol=3
  lgphi=alog10((10^a[icol,*]+10^a[icol+2,*]))
  lgerr=a[2,*]
endelse
bern={lgm:lgm+2*alog10(h),phi:10^lgphi/h^3,up:10^(lgphi+lgerr)/h^3,$
      low:10^(lgphi-lgerr)/h^3}
;phi=phi*h^-3, unit?
return,bern
end
;====================================================================
;====================================================================
function SMF_Davidzon2013,iz=iz
;data: Davidzon et al. 2013, VIPERS, VIMOS Public Extragalactic Redshift Survey
;Global SMF / SMF in field
;iz= 0   	1	   2   	      3 	 4	    5
;0.5<z<0.6, 0.6<z<0.7, 0.7<z<0.8, 0.8<z<0.9, 0.9<z<1.1, 1.1<z<1.3
h=0.7
x3='x,x,x,'
xx=''
if iz ge 1 then begin
  for i=0,iz-1 do xx=xx+x3
endif else xx=''
readcol,'SMF/data/Davidzon2013.txt',lgm,lgphi,Err,mErr,comment='#',$
    	/silent,format='f,'+xx+'f,f,f'
dav={lgm:lgm,phi:10^lgphi,up:10^(lgphi+Err),$
     low:10^(lgphi+mErr)}
return,dav
end
;====================================================================
;====================================================================
function SMF_Mortlock2011,iz=iz,ic=ic
;data: Mortlock et al. 2011, GOODS NICMOS survey
;field SMF
;iz=0	    1	    	2   	3   	4
;1.0~1.5  1.5~2.0  2.0~2.5  2.5~3.0  3.0~3.5 
;ic=0,   1,   2
; all, blue, red
h=0.7
skip=[3,18,31,43,54]
nline=[14,12,11,10,11]
openr,lun,'SMF/data/Mortlock2011.txt',/get_lun
  skip_lun,lun,skip[iz],/line
  out=fltarr(3,nline[iz])
  readf,lun,out,format='(f4.1,f8.2,f6.2)'
free_lun,lun
mor={lgm:out[0,*],phi:10^out[1,*],up:10^(out[1,*]+out[2,*]),low:10^(out[1,*]-out[2,*])}
return,mor
end
;====================================================================
;====================================================================
function SMF_Santini2012,lgm,iz=iz
;data: Santini et al. 2012, WFC3
;field SMF
;iz=0	     1	       2   	3   	 4  	  5
;0.6~1.0  1.0~1.4   1.4~1.8  1.8~2.5  2.5~3.5  3.5~4.5 
h=0.7 & omega0=0.3 & lambda0=0.7
mlowv=[8.2,8.4,9.18,9.22,9.4,10.1]
p=replicate({alpha:0.,alphaErr:0.,lgms:0.,lgmsErr:0.,phis:0.,$
    	     phisErr:0.},6)
openr,lun,'SMF/data/Santini2012.txt',/get_lun
  skip_lun,lun,3,/line
  readf,lun,p,format='(20x,f5.2,f5.2,f7.2,f5.2,f7.2,f5.2)'
free_lun,lun
p.lgms=p.lgms+2*alog10(h)
p.lgmsErr=p.lgmsErr+2*alog10(h)
p.phis=10^p.phis/h^3
p.phisErr=10^p.phisErr/h^3

san=SchechterFit(p[iz],lgm=lgm,mlow=mlowv[iz],mhigh=12.0,nbin=15-iz)
return,san
end
;====================================================================
;====================================================================
;====CLUSTER Following=====================================================
;====================================================================
pro SMF_Vulcani2013,lgm,ICBS=ICBS,EDisCs=EDisCs,WINGS=WINGS,$
    	    	    PM2GC=PM2GC,ir=ir,icolor=icolor,weight=weight
;formula: Vulcani 2013
;ICBS: Mstar>10^10.5, z=0.33~0.45, cluster, R200=1.04~2.03Mpc
;EDisCs: Mstar>10^10.2, z=0.4~0.8, cluster, R200=0.7~1.99Mpc
;WINGS: Mstar>10^9.8, z=0.04~0.07, uncorrected for completeness
;PM2GC: Mstar>10^10.25, z=0.04~0.1, field
;without normalization
;ir=1,2,3:  Cluster_regions, Cluster_outskirts, Field
;icolor=0,1,2: all, blue, red
if n_elements(ir) eq 0 then ir=1
if n_elements(icolor) eq 0 then icolor=0

h=0.7 & omega0=0.3 & lambda0=0.7
pi={name:'',lgms:0.,lgmsErr:0.,alpha:0.,alphaErr:0.,phis:0.,phisErr:0.}
fmt='(a26,f5.2,f5.2,f7.2,f5.2,i5,i3)'
openr,lun,'SMF/data/Vulcani2013.txt',/get_lun
if arg_present(ICBS) then begin
  WC=[0.04,2e-3,2e-6]
  skip=2+(ir-1)*3+icolor
  skip_lun,lun,skip,/lines
  readf,lun,pi,format=fmt
  if n_elements(weight) eq 0 then weight=WC[ir-1]
  ICBS=SchechterFit(pi,lgm=lgm,mlow=10.7,mhigh=12.0,nbin=9,h=h,weight=weight)
endif
if arg_present(EDisCs) then begin
  WC=[0.04,2e-3,2e-6]
  skip=12+(ir-1)*3+icolor
  skip_lun,lun,skip,/lines
  readf,lun,pi,format=fmt
  if n_elements(weight) eq 0 then weight=WC[ir-1]
  EDisCs=SchechterFit(pi,lgm=lgm,mlow=10.5,mhigh=12.0,nbin=11,h=h,weight=weight)
endif
if arg_present(WINGS) then begin
  skip_lun,lun,22,/lines
  readf,lun,pi,format=fmt
  WINGS=SchechterFit(pi,lgm=lgm,mlow=10.25,mhigh=12.0,nbin=9,h=h,weight=0.1)
endif
if arg_present(PM2GC) then begin
  skip_lun,lun,24,/lines
  readf,lun,pi,format=fmt
  PM2GC=SchechterFit(pi,lgm=lgm,mlow=10.25,mhigh=12.0,nbin=9,h=h,weight=0.1)
endif

free_lun,lun
end
;====================================================================
;====================================================================
pro SMF_Calvi2013,lgm,WINGS=WINGS,PM2GC=PM2GC,weight=weight,it=it  ;low redshift
;formula: Calvi et al. 2013, corrected for completeness
;normalization for cluster: method of Vulcani et al. 2014
;WINGS: Mstar>>10^10.25, z=0.04~0.07, corrected for completeness, normalized with method of Vulcani et al. 2014
;it=0,1,2,3,4: all, Late, Early, lenticulars(S0), Elliptical
;PM2GC: Mstar>10^10.25, z=0.04~0.1, field
if n_elements(it) eq 0 then it=0
h=0.7 & omega0=0.3 & lambda0=0.7
p=replicate({name:'',alpha:0.,alphaErr:0.,lgms:0.,lgmsErr:0.,phis:0.,$
    	     phisErr:0.},10)
openr,lun,'SMF/data/Calvi2013.txt',/get_lun
  skip_lun,lun,2,/lines
  readf,lun,p,format='(a13,f6.1,f4.1,f7.2,f5.2,f9.3,f6.3)'
free_lun,lun

if arg_present(WINGS) then WINGS=SchechterFit(p[it+1],lgm=lgm,mlow=10.25,mhigh=12.0,nbin=9,h=h,weight=weight)
if arg_present(PM2GC) then PM2GC=SchechterFit(p[6],lgm=lgm,mlow=10.25,mhigh=12.0,nbin=9,h=h,weight=weight)
end

;====================================================================
;====================================================================
function SMF_Vulcani2011,lgm
;data: Vulcani et al. 2011, uncorrected for completeness
;table 4?
h=0.7 & omega0=0.3 & lambda0=0.7
if n_elements(lgm) eq 0 then begin 
  mlow=9.8+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif

phi    =SchechterFunction(lgm,1.552,      -0.987,      11.667+2*alog10(h))
phi_up =SchechterFunction(lgm,1.552+0.055,-0.987-0.009,11.667+0.052+2*alog10(h))
phi_low=SchechterFunction(lgm,1.552-0.055,-0.987+0.009,11.667-0.052+2*alog10(h))
wings={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,wings
end
;====================================================================
;====================================================================
function SMF_Vulcani2014,lgm,field=field
;WIde-field Nearby Galaxy-cluster Survey
;data: Vulcani et al. 2011, uncorrected for completeness
;formula: Vulcani et al. 2014
h=0.7 & omega0=0.3 & lambda0=0.7 & z=0.06
;h=0.73 & omega0=0.25 & lambda0=0.75 & z=0.06
if n_elements(lgm) eq 0 then begin 
  ;only mass above 10^10.25 is completed
  mlow=10.25+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif

phi    =SchechterFunction(lgm,0.0069,       -0.7,    10.61+2*alog10(h))
phi_up =SchechterFunction(lgm,0.0069+0.0009,-0.7-0.1,10.61+0.07+2*alog10(h))
phi_low=SchechterFunction(lgm,0.0069-0.0009,-0.7+0.1,10.61-0.07+2*alog10(h))

if keyword_set(field) then begin
  wings={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
endif else begin  
  dltc=(omega0*(1+z)^3+lambda0)/(omega0*(1+z)^3)
;  print,dltc,1/omega0
  f200=200*dltc
  c200=5.17 & x=c200*0.6 
  f206=200*gx(x)*dltc/gx(c200)/0.6^3
  phi=phi*f206
  phi_up=phi_up*f206
  phi_low=phi_low*f206
  wings={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
endelse
return,wings
end
;====================================================================
;====================================================================
function SMF_Merluzzi2010,lgm,weight=weight
;formula: Merluzzi 2010; Mercurio 2012, Shapley supercluster
h=0.7 ;& omega0=0.25 & lambda0=0.75 & z=0.048
p={alpha:-1.2,alphaErr:0.01,lgms:11.16,lgmsErr:0.02,phis:3.5,phisErr:0.02}
shap=SchechterFit(p,lgm=lgm,mlow=8.75,mhigh=11.8,nbin=15,weight=weight)
;shap={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,shap
end
;====================================================================
;====================================================================
function SMF_Giodini2012,lgm,ig=ig,iz=iz,isq=isq
;data: Giodini et al. 2012, COSMOS survey
;without normalization
;low  mass group: 2.1*10^13~4.2*10^13Msun
;high mass group: 6.7*10^13~8.2*10^13Msun
;ig=0,1,2: low  mass group, high mass group, field 
;iz=0	     1	      2        3
;0.2~0.4, 0.4~0.6, 0.6~0.8, 0.8~1.0 
;isq=0,1,2, All, Star-forming, Passive; seperated by color
if n_elements(iz) eq 0 then iz=0
if n_elements(isq) eq 0 then isq=0
h=0.71 & omega0=0.27 & lambda0=0.73
mlowV=[8.6,8.1,9.6,9.9];+2*alog10(h)
nbinV=[16,13,11,9]
a=read_ascii('SMF/data/Giodini2012SF.txt',COMMENT_SYMBOL='#')
a=a.FIELD01   ;star-forming
b=read_ascii('SMF/data/Giodini2012P.txt',COMMENT_SYMBOL='#')
b=b.FIELD01   ;passive
irow=ig*4+iz
VW=1.0/(a[1,*]*h^3)
weight=[1,1,1.5]
Wi=VW[irow]*weight[ig]

p1={alpha:a[8,irow],alphaErr:a[9,irow],lgms:a[5,irow],lgmsErr:a[6,irow],$
    phis:a[2,irow],phisErr:a[3,irow]}
p2={alpha:b[8,irow],alphaErr:0.0,lgms:b[5,irow],lgmsErr:b[6,irow],$
    phis:b[2,irow],phisErr:b[3,irow]}
G1=SchechterFit(p1,lgm=lgm,mlow=mlowV[iz],mhigh=11.6,nbin=nbinv[iz],$
    	    	weight=Wi)
G2=SchechterFit(p2,lgm=lgm,mlow=mlowV[iz],mhigh=11.6,nbin=nbinv[iz],$
    	    	weight=Wi)

case isq of
  0:Gio={lgm:G1.lgm,phi:(G1.phi+G2.phi),up:(G1.up+G2.up),low:(G1.low+G2.low)}
  1:Gio=G1
  2:Gio=G2
  else: message,'ig must be 0, 1, or 2'
endcase
return,gio
end
;====================================================================
;====================================================================
function SMF_vanderBurg2013,weight=weight,cluster=cluster,$
    	 field=field,isq=isq
;data: van der Burg et al. 2013, GCLASS cluster sample
;Gemini Cluster Astrophysics Spectroscopic Survey
;Mhalo=1*10^14~2.6*10^15 Msun  ;at z~1
;R200=0.6~2.1 Mpc
;z~1
;isq=0,1,2: all, Star-forming,  Quiescent;  seperated by color
;without normalization
if n_elements(isq) eq 0 then isq=0
h=0.7 & omega0=0.3 & lambda0=0.7
a=read_ascii('SMF/data/vanderBurg2013.txt',COMMENT_SYMBOL='#')
a=a.FIELD01   
lgm=a[0,*]
if keyword_set(cluster) then begin
  case isq of 
    0:icol=1
    1:icol=9
    2:icol=5
  endcase
  phi=a[icol,*]
  up=phi+a[icol+1,*]
  low=phi+a[icol+2,*]
  if n_elements(weight) eq 0 then weight=0.1
  van={lgm:lgm+2*alog10(h),phi:phi*weight,up:up*weight,low:low*weight}
endif
if keyword_set(field) then begin
  case isq of 
    0:icol=13
    1:icol=17
    2:icol=15
  endcase
  phi=a[icol,*]
  up=phi+a[icol+1,*]
  low=phi-a[icol+1,*]
  u=1e-5/h^3
  van={lgm:lgm+2*alog10(h),phi:phi*u,up:up*u,low:low*u}
endif
return,van
end
;====================================================================
;====================================================================
;function SMF_GuoSIM,lgm,cluster=cluster
;formula: Vulcani et al. 2014
;alog10(Mhalo)=14.1
;h=0.73 & omega0=0.25 & lambda0=0.75 & z=0.06
;if n_elements(lgm) eq 0 then begin 
;  mlow=9.4+2*alog10(h) & mhigh=11.7+2*alog10(h) & nbin=13
;  dbin=(mhigh-mlow)/(nbin-1)
;  lgm=findgen(nbin)*dbin+mlow
;endif

;phi    =SchechterFunction(lgm,0.0128,       -1.081,      10.865+2*alog10(h))
;phi_up =SchechterFunction(lgm,0.0128+0.0005,-1.081-0.004,10.865+0.004+2*alog10(h))
;phi_low=SchechterFunction(lgm,0.0128-0.0005,-1.081+0.004,10.865-0.004+2*alog10(h))
;if keyword_set(cluster) then begin
;  dltc=(omega0*(1+z)^3+lambda0)/(omega0*(1+z)^3)
;  f200=200*dltc
;  c200=5.17 & x=c200*0.6 
;  f206=200*gx(x)*dltc/gx(c200)/0.6^3
;  phi=phi*f200
;  phi_up=phi_up*f200
;  phi_low=phi_low*f200
;endif
;guo={lgm:lgm,phi:phi,up:phi_up,low:phi_low}

;return,guo
;end
;====================================================================
;====================================================================
;function SMF_Guo2011,field=field
;h=0.73 & omega0=0.25 & lambda0=0.75;

;lgm=[ 7.125, 7.376, 7.618, 7.869, 8.120,$
;      8.380, 8.622, 8.873, 9.124, 9.375,$
;      9.625, 9.876,10.127,10.378,10.629,$
;     10.871,11.122,11.372,11.623,11.874]
;lgm=lgm+2*alog10(h)
     
;dN/d\lgm
;phi=[4969.94,3714.64,2682.92,2339.42,1904.85,$
;     1306.89,1101.20,912.129,693.520,509.550,$
;     444.312,332.088,284.654,299.661,231.777,$
;     100.148,40.4076,12.1857,7.94176,4.07250]
;phi_up=[5231.95,3910.47,2873.14,2462.75,2039.91,$
;        1423.73,1179.28,993.671,768.569,564.691,$
;    	492.394,374.382,320.907,349.595,270.399,$
;        122.995,54.0626,19.0204,13.7376,8.07893]
;phi_low=2*phi-phi_up

;V=4*!pi*1.46^3/3  ;R200=1.46 h^-1Mpc
;phi=phi/V 
;phi_up=phi_up/V
;phi_low=phi_low/V
;if keyword_set(field) then begin
; Weight=1/V200/(200[omega0*(1+z)^3+lambda0]/[omega0*(1+z)^3])
;  phi=phi*omega0/200     ;dilute by 200/moega0
;  phi_up=phi_up*omega0/200     ;dilute by 200/moega0
;  phi_low=phi_low*omega0/200     ;dilute by 200/moega0
;endif
;guo2011={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
;return,guo2011
;end
;====================================================================
;====================================================================
