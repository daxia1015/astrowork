pro count_halo,nn,write=write

nfile=n_elements(!treefile)
nn=lonarr(!n_snap,nfile)
fname='counting/halonumber'
if keyword_set(write) then begin
  for ifile=0, do begin
    breaktree,!treefile[ifile],snapnum,tag=['snapnum']
    for i=0,!n_snap-1 do begin
      sn=where((snapnum eq i),n)
      nn[i,ifile]=n
    endfor
  endfor
  openw,lun,fname,/get_lun
  writeu,lun,nn
  free_lun,lun
endif else begin
  openr,lun,fanme,/get_lun
  readu,lun,nn
  free_lun,lun

end
