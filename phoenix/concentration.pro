function gxox_eq,x,const
p=gx(x)/x-const
return,p
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;==============================================================
function gxox_eq_p,x
pp=(x*gx_p(x)-gx(x))/x^2
return,pp
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;==============================================================
function gxox_root,const
print,min(const),max(const)
sn0=where(const ge 0.216217,n0)
print,'over maxium=', n0
it=0  & eps=1e-3 & root=fix(const) & root[*]=0
ci=1./const  & f=ci & fp=ci

    sn1=where(root eq 0,n1) & print,n1
    n3=n1
    while n1 ne 0 do begin
      if n1 lt n3 then begin
        print,n1
	n3=n3/2
      endif
      f[sn1]=concen_eq(ci[sn1],const[sn1])
      fp[sn1]=concen_eq_p(ci[sn1])
      dx=abs(f/fp) & dx0=ci*eps ;> eps
      sn2=where((root eq 0)and(dx le dx0),n2)    ;or(abs(f) le eps)),n2)
      if n2 ne 0 then root[sn2]=1
      sn2=where((root eq 0)and(dx gt dx0),n2)  
      sign=-f/abs(f)
      ;sn3=where((root eq 0)and(dx gt dx0)and(f gt 0),n3)
      if n2 ne 0 then ci[sn2]=ci[sn2]+dx[sn2]*sign[sn2]
      ;if n3 ne 0 then ci[sn3]=ci[sn3]-dx[sn3]/2
      ;if !enprint then print,rx,dx,format='(2e15.6)'
      sn1=where(root eq 0,n1)
;      if n2 ne 0 then begin
;        fp[sn2]=rx_eq_p(rx[sn2],csub[sn2])
;      endif else break    
;    endif else break
    it++
  endwhile
help,it
return,ci
end
