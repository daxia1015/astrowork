function assoctree,fname,ntree,totnhalo,nhalos
@include/readtree_readhead.pro
head=8L+4L*ntree
tree=assoc(lun,!halostruct,head)
return,tree
end
;==================================================================
;==================================================================
;The properties of the first halo in each tree:
;They are identified at snapnum=71 (a=1, z=0) and all have no Descendant.
;Their FirstProgenitor is eithor 1 or -1.
;Their  NextProgenitor all eq -1.
;They all belong to the group of the first halo (haloID=0) in the whole tree,
;i.e., their FirstHaloInFOFgroup all equal 0.
;==================================================================
;All trees are not self-contained except the first tree.
;==================================================================
function readtree,fname,ntree,totnhalo,nhalos,alltree=alltree,itree=itree,$
    	    	  skip_nhalo=skip_nhalo
@include/readtree_readhead.pro
if keyword_set(alltree) then begin
  tree=replicate(!halostruct,totnhalo)
;  tree=assoc(tree_lun,!halostruct,8L+4L*ntree)
  readu,lun,tree
endif else begin
  skip_nhalo=0
  if itree ge 1 then skip_nhalo=total(nhalos[0:itree-1],/int)
  skip=8L+4L*ntree+104L*skip_nhalo
  point_lun,lun,skip  
;    for tr=0L,Ntree-1  do begin  ; read different tree
  Tree = replicate(!HaloStruct, Nhalos[itree]);every halo's formation in the tree
  readu,lun,Tree
endelse

;if eof(lun) then message,'end of '+fname,/continue
free_lun,lun
return,tree
end
;==================================================================
;==================================================================
pro breaktree4,fname,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,halosn=halosn,tag=tag,alltree=alltree
ntag=n_elements(tag) & nhalo=n_elements(halosn) 
if (nhalo ne 0)and(keyword_set(alltree)) then message, $
   'only one of keyword "halosn" or "alltree" can be set.'
if keyword_set(alltree) then nhalo=0

tree=readtree(fname,/alltree)
      tag=strupcase(tag) ;& tagsn=intarr(ntag)
      for ii=0,ntag-1 do begin
        tagi=tag[ii]
	sn2=where(!tagname eq tagi,n2)
	if n2 ne 1 then message,'tag name not match!!!' 
    	if nhalo ne 0 then array=tree[halosn].(sn2) else array=tree.(sn2)
        case ii of 
	  0: v0=array
      	  1: v1=array
    	  2: v2=array
    	  3: v3=array
    	  4: v4=array
    	  5: v5=array
    	  6: v6=array
    	  7: v7=array
    	  8: v8=array
    	  9: v9=array
          else: message,'variables not enough!'
        endcase
      endfor
print,'%breaktree4: ',tag,'  have been read out.'
tree=0
end
;==================================================================
;==================================================================
pro breaktree,fname,tree,totnhalo,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,$
    	    	     halosn=halosn,tag=tag,alltree=alltree
p1=strpos(fname,'/trees_sf1_071.0')		     
if strmid(fname,p1-1,1) eq '4' then begin
  breaktree4,fname,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,halosn=halosn,tag=tag,alltree=alltree
  return
endif
nhalo=n_elements(halosn) 	     
nhalo0=nhalo
if (nhalo ne 0)and(keyword_set(alltree)) then message, $
   'only one of keyword "halosn" or "alltree" can be set.'
if (nhalo eq 0)or(keyword_set(alltree)) then nhalo=totnhalo


ntag=n_elements(tag) 
      tag=strupcase(tag) & tagsn=intarr(ntag)
      for ii=0,ntag-1 do begin
        tagi=tag[ii]
	sn2=where(!tagname eq tagi,n2)
	if n2 ne 1 then message,'tag name not match!!!' 
	tagsn[ii]=sn2       
        n3=n_elements(!halostruct.(sn2))
        if n3 eq 1 then array=replicate(!halostruct.(sn2),nhalo) else array=rebin(!halostruct.(sn2),n3,nhalo)
        case ii of 
	  0: v0=array
      	  1: v1=array
    	  2: v2=array
    	  3: v3=array
    	  4: v4=array
    	  5: v5=array
    	  6: v6=array
    	  7: v7=array
    	  8: v8=array
    	  9: v9=array
          else: message,'variables not enough!'
        endcase
      endfor

for k=0L,nhalo-1 do begin
  if  nhalo0 eq 0 then i=k else i=halosn[k]
  haloi=tree[i]
  for ii=0,ntag-1 do begin
    itag=tagsn[ii]
    n3=n_elements(!halostruct.(itag))
    case ii of
      0: if n3 eq 1 then v0[k]=haloi.(itag) else v0[*,k]=haloi.(itag)
      1: if n3 eq 1 then v1[k]=haloi.(itag) else v1[*,k]=haloi.(itag)
      2: if n3 eq 1 then v2[k]=haloi.(itag) else v2[*,k]=haloi.(itag)
      3: if n3 eq 1 then v3[k]=haloi.(itag) else v3[*,k]=haloi.(itag)
      4: if n3 eq 1 then v4[k]=haloi.(itag) else v4[*,k]=haloi.(itag)
      5: if n3 eq 1 then v5[k]=haloi.(itag) else v5[*,k]=haloi.(itag)
      6: if n3 eq 1 then v6[k]=haloi.(itag) else v6[*,k]=haloi.(itag)
      7: if n3 eq 1 then v7[k]=haloi.(itag) else v7[*,k]=haloi.(itag)
      8: if n3 eq 1 then v8[k]=haloi.(itag) else v8[*,k]=haloi.(itag)
      9: if n3 eq 1 then v9[k]=haloi.(itag) else v9[*,k]=haloi.(itag)
    endcase
  endfor
endfor
print,'%breaktree: ',tag,'  have been read out.'
end


;==================================================================
;==================================================================

;nhalo0=n_elements(halosn)
;@include/readtree_readhead.pro
;head=8L+4L*ntree
;for k=0L,nhalo-1 do begin
;  if  nhalo0 eq 0 then i=k else i=halosn[k]
;  skip_halo=head+104L*i
;  for ii=0,ntag-1 do begin
;    itag=tagsn[ii]
;    skip=0
;    if itag ge 1 then skip=total(!tagsize[0:itag-1],/int)
;    skip=skip_halo+skip
;    point_lun,lun,skip
;    array=!halostruct.(itag)
;    readu,lun,array
;    n3=n_elements(!halostruct.(itag))
;    case ii of
;      0: if n3 eq 1 then v0[k]=array else v0[*,k]=array
;      1: if n3 eq 1 then v1[k]=array else v1[*,k]=array
;      2: if n3 eq 1 then v2[k]=array else v2[*,k]=array
;      3: if n3 eq 1 then v3[k]=array else v3[*,k]=array
;      4: if n3 eq 1 then v4[k]=array else v4[*,k]=array
;      5: if n3 eq 1 then v5[k]=array else v5[*,k]=array
;      6: if n3 eq 1 then v6[k]=array else v6[*,k]=array
;    endcase
;  endfor
;endfor
