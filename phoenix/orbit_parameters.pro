;x=r/r_s
;V(r1,r2)=int(vec{f_g},x1,x2)=-int(GM(<r)/r^2,r1,r2)    ;NFW profile
;   	    =(GM_vir/gx(c)/r_s)*(ln(1+x)/x)|_x1^x2
;V(r,inf)=-(GM_vir/gx(c)/r_s)*(ln(1+x)/x)
;Vorb_circular^2=(GM_vir/gx(c)/r_s)*(g(x)/x)
;e_circular=-(GM_vir/gx(c)/r_s)*(ln(1+x)/x+1/(1+x))/2
function potential_r,r,mhost,rs_host,chost,hprofile=hprofile
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if hprofile ne 2 then message,'error found'    ;only valid for NFW profile
gconst=gaussc*mhost/gx(chost)/rs_host
x=r/rs_host
potential=-gconst*alog(1+x)/x             ;only valid for NFW profile
return,potential
end
;========================================================
;=========%%%=++++++++++++============+++++++++++++=======
function energy_r,r,mhost,rs_host,chost,Vorb,circular=circular,hprofile=hprofile
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
if hprofile ne 2 then message,'error found'    ;only valid for NFW profile
gconst=gaussc*mhost/gx(chost)/rs_host
x=r/rs_host
V_r_inf=-gconst*alog(1+x)/x              ;only valid for NFW profile
if keyword_set(circular) then begin
  energy=V_r_inf+gconst*gx(x)/x/2.
;  energy=-gconst*(alog(1+x)/x+1/(1+x))/2
endif else begin
  energy=V_r_inf+Vorb^2/2.
endelse
return,energy
end
;========================================================
;=========%%%=++++++++++++============+++++++++++++=======
function yita_eq,r,mhost,rs_host,chost,ei
;f=yitag(x)+const
f=energy_r(r,mhost,rs_host,chost,/circular,hprofile=2)
f=f-ei
return,f
end
;========================================================
;=========%%%=++++++++++++============+++++++++++++====
function yita_eq_p,r,mhost,rs_host,chost
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;fp=-gx(x)/x^2+1/(1+x)^2
gconst=gaussc*mhost/gx(chost)/rs_host
x=r/rs_host
gconst=gconst/2./rs_host
fp=gconst*(gx(x)/x^2-1./(1+x)^2)
return,fp
end
;========================================================
;========================================================
pro orbit_circularity,mhost,rs_host,chost,ji,ei,$
    	    	      yita,epson,boundsn=boundsn;,hprofile=hprofile
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
;if hprofile ne 2 then message,'error found'    ;only valid for NFW profile
;ei=energy_r(r,mhost,rs_host,chost,Vorb,hprofile=hprofile)
;x=r/rs_host
yita=ei & epson=ei
yita[*]=-1. & epson[*]=-1.
gconst=gaussc*mhost/gx(chost)/rs_host
;const=2*ei/gconst
boundsn=where(ei le -1e-3,nbound)   & print,nbound
;const=const[boundsn]                ;calculate for bound orbit only
;chost1=chost[boundsn] 
;print,'const=',min(const),max(const)
r_cir=inewton('yita_eq','yita_eq_p',rs_host[boundsn],[4,3],mhost[boundsn],$
    	      rs_host[boundsn],chost[boundsn],ei[boundsn],eps=1e-3,$
	      lower=1e-3)
yita[boundsn]=r_cir/chost[boundsn]/rs_host[boundsn]           ;orbital energy parameter

x_cir=r_cir/rs_host[boundsn]
;v_cir=Vvir[boundsn]*sqrt(chost1*gx(x_cir)/x_cir/gx(chost1))  
v_cir=sqrt(gconst[boundsn]*gx(x_cir)/x_cir)
epson[boundsn]=ji[boundsn]/r_cir/v_cir          ;orbital angular momentum parameter, circularity
sn=uniq(yita,sort(yita))
print,'yita=  ',yita[sn[1]],max(yita)
sn=uniq(epson,sort(epson))
print,'epson= ',epson[sn[1]],max(epson)
end
;========================================================
;========================================================
function ecc_eq,r,mhost,rs_host,chost,ji,ei
potential=potential_r(r,mhost,rs_host,chost,hprofile=2)
f=potential+(ji/r)^2/2-ei
return,f
end
;========================================================
;========================================================
function ecc_eq1,r,mhost,ji,ei
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
potential=-gaussc*mhost/r
f=potential+(ji/r)^2/2-ei
return,f
end
;========================================================
;========================================================
function ecc_eq_p,r,mhost,rs_host,chost,ji
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
gconst=gaussc*mhost/gx(chost)/rs_host
x=r/rs_host
fp=gconst*gx(x)/x^2-ji^2/rs_host^2/x^3
fp=fp/rs_host
return,fp
end
;========================================================
;========================================================
pro orbit_eccentricity,mhost,rs_host,chost,ji,ei,Rorb,$
    	    	      ecc,boundsn=boundsn;,hprofile=hprofile
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
ecc=ei & ecc[*]=-1.
gconst=gaussc*mhost/gx(chost)/rs_host
boundsn=where(ei lt 0,nbound)   & print,nbound

rs_hostp=rs_host[boundsn]
mhostp=mhost[boundsn]
chostp=rs_host[boundsn]
jip=ji[boundsn]
eip=ei[boundsn]

ri=rs_hostp
while 1 do begin
  f=ecc_eq(ri,mhostp,rs_hostp,chostp,jip,eip)
  sn=where(f lt 0,n)
  if n ne 0 then ri[sn]=ri[sn]/2. else break
endwhile
r_peri=inewton('ecc_eq','ecc_eq_p',ri,[5,4],mhostp,$
    	      rs_hostp,chostp,jip,eip,$
	      eps=1e-3,lower=1e-6)

ri=rs_hostp
while 1 do begin
  f=ecc_eq(ri,mhostp,rs_hostp,chostp,jip,eip)
  sn=where(f lt 0,n)
  if n ne 0 then ri[sn]=ri[sn]*2. else break
endwhile
r_apo=inewton('ecc_eq','ecc_eq_p',ri,[5,4],mhostp,$
    	      rs_hostp,chostp,jip,eip,$
	      eps=1e-3,lower=Rorb)

print,min(r_peri),max(r_peri),min(r_apo),max(r_apo)

ecc[boundsn]=(r_apo-r_peri)/(r_apo+r_peri)
sn=uniq(ecc,sort(ecc))
print,'ecc= ',ecc[sn[1]],max(ecc)

end
;========================================================
;========================================================
pro test_orbit
common astronomy ;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
r=findgen(500)+1
mhost=1e12 & z=0.1
rvir=r_virial(mhost,z)
Vvir=sqrt(gaussc*mhost/rvir)

chost=haloConcen(mhost,z)
rs_host=rvir/chost
ji=rvir*vvir*2
ei=-1e5
help,rvir,vvir,chost,rs_host,ji
;f=yita_eq(r,mhost,rs_host,chost,ei)
f =ecc_eq1(r,mhost,ji,ei)
fp=ecc_eq_p(r,mhost,rs_host,chost,ji)
plot,r,f,/xlog



n=1000
mhost=randomu(seed1,n)*1e13
z=randomu(seed2,n)
rvir=r_virial(mhost,z)
Vvir=sqrt(gaussc*mhost/rvir)
chost=haloConcen(mhost,z)
rs_host=rvir/chost
Rorb=randomu(seed3,n)*rvir*2
Vorb=randomu(seed4,n)*Vvir*2
ji=randomu(seed5,n)*Rorb*Vorb
;ei=-1e5*randomu(seed6,n)
ei=energy_r(Rorb,mhost,rs_host,chost,Vorb,hprofile=2)

;orbit_eccentricity,mhost,rs_host,chost,ji,ei,Rorb,ecc,boundsn=boundsn;,hprofile=hprofile
;orbit_circularity,mhost,rs_host,chost,ji,ei,yita,circ,boundsn=boundsn 





end
