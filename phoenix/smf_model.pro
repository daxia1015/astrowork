function smf_histo,Mstar,nbin,low,high,Weight=weight,locations=lgm,$
    	 log10=log10,li2009=li2009,wings=wings,yangSMF=yangSMF,$
	 iband=iband,ig=ig,ic=ic,downfactor=downfactor

phi=histo(Mstar,nbin=nbin,xmin=low,xmax=high,weight=weight,$
    	  dbin=dlgm,locations=lgm,log10=log10)
phi=phi/dlgm
sn1=where(phi lt 0,n1)
if n1 ne 0 then message,'error'
sn1=where(phi eq 0,n1)
if n1 ne 0 then begin
  if n_elements(downfactor) eq 0 then downfactor=1.0
  if keyword_set(li2009) then begin
    li=SMF_li2009(lgm)
    phi[sn1]=li.phi[sn1]*downfactor
  endif  
  if keyword_set(wings) then begin
    SMF_Calvi2013,lgm,WINGS=wings,it=0
;    wings=smf_Vulcani2014(lgm,/cluster)
    phi[sn1]=wings.phi[sn1]*downfactor 
  endif
endif
return,phi
end
;====================================================================
;==================================================================
pro smf_iz,data,isim,iz,phiAll,phiAllC,nGalaxy,li2009,wings
;  z~  0.0,   0.3,   1.0
Mmax=[[1e12,  9e11,  8e11],$  ;whole sky
      [1e12,  8e11,  6e11],$  ;<R200
      [7e11,  9e11,  7e11],$  ;[R200,3R200]
      [8e11,  9e11,  1e12]]   ;>3R200 [4e13,9e12,2e13,3e13]
Mmin=Mmax/1e5
df0=[1,1,1e-2,1e-2]
df1=[1e-3,1e-3,1e-5,1e-4]
for ig=0,!smf.ng-1 do begin
  gsn=groupOFgalaxy(data.iso,ig=ig)
  for isq=0,!smf.nsq-1 do begin
    sqsn=sqOFgalaxy(data.sfr,data.Mstar,!smf.sSFRc,isq=isq)
    for it=0,!smf.nt-1 do begin
      tsn=typeOFgalaxy(data.disk,data.spheroid,!smf.bodc,it=it)
      for ir=0,!smf.nr-1 do begin
    	rsn=regionOFgalaxy(data.RC,!smf.ccR,ir=ir)
	sn=where((data.Mstar gt 1)and gsn and sqsn and tsn and rsn,ngal)
;	print,'ngal=',ngal
	nGalaxy[isim,iz,ig,isq,it,ir]=ngal
	if ngal eq 0 then continue
        mstar=data.Mstar[sn]
    	phi=smf_histo(Mstar,!smf.nbin,Mmin[iz,ir],Mmax[iz,ir],$
	    	      weight=data.Wr[ir],/log10,locations=lgm)
	phiAll [*,isim,iz,ig,isq,it,ir].lgm=lgm	      
    	phiAll [*,isim,iz,ig,isq,it,ir].phi=phi
	phiAllC[*,isim,iz,ig,isq,it,ir].lgm=lgm	      
    	sn1=where(phi eq 0,n1)
	if(ngal gt !smf.nbin)and(n1 ne 0)then begin
	  if(ig+isq+it eq 0)then df=df0[ir] else df=df1[ir]
	  if(ir eq 1)or(ir eq 2)then begin
    	    SMF_Calvi2013,lgm,WINGS=wings,it=0
	    phi[sn1]=wings.phi[sn1]*df 
	  endif else begin
    	    li2009=SMF_li2009(lgm)
	    phi[sn1]=li2009.phi[sn1]*df
	  endelse
	endif
    	phiAllC[*,isim,iz,ig,isq,it,ir].phi=phi
      endfor
    endfor
  endfor  
endfor
end
;====================================================================
;==================================================================
pro smf_isim,fname,isim,phiAll,phiAllC,nGalaxy
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
;print,'  computing stellar mass function of satellite, central and all galaxies...'
Name0=['diskStellarMass','spheroidStellarMass','nodeVirialRadius','nodeIsIsolated']
level=strmid(!Phoenix.TreeName[isim],3,1)
p0=strpos(fname[isim],'preset')
preset=strmid(fname[isim],p0+6,1)
fid=h5f_open(fname[isim])
;Lsun=10^(-4.67/2.5)  ; solar luminosity of SDSS_r band in AB magnitude system
for iz=0,!smf.nz-1 do begin
  iout=!smf.ioutv[iz]
;  print,'iz=',iz
  data=nodeData_ReadMany(fid,outputName[iout],Name0,/pos,/sfr)
;       lumiName='SDSS_r',ColorName='SDSS_g-SDSS_r')
  data.nodeVirialRadius=data.nodeVirialRadius*h0  ;h^-1 Mpc
  data.pos=data.pos*h0      ;h^-1 Mpc
  data.sfr=data.sfr*1e-9    ; Msun/yr
  tot=data.spheroidStellarMass+data.diskStellarMass
  if preset eq '0' then begin
    tot=tot*h0^2   ; h^-2 M_sun
    WC=max(Weight0[*,isim])*5.0
  endif else begin
    if level eq '4' then begin
      tot=tot*h0^2   ; h^-2 M_sun
      WC=min(Weight0[*,isim])
    endif else WC=max(Weight0[*,isim])
  endelse
  R200=max(data.nodeVirialRadius,sn1)  ;& print,'R200=',R200
  center=data.pos[*,sn1]
  RC=pair_distance(data.pos,center)/R200

  V200=4*!pi*R200^3/3.0	    ;h^-3 Mpc^3
  Wr=[WC,1.0/V200,1.0/(26*V200),1.0/(1.0/WC-27*V200)]
  
  data={disk:data.diskStellarMass,spheroid:data.spheroidStellarMass,$
    	Iso:data.nodeIsIsolated,Mstar:tot,sfr:data.sfr,RC:RC,Wr:Wr}
  smf_iz,data,isim,iz,phiAll,phiAllC,nGalaxy
endfor
h5f_close,fid
end
;====================================================================
;====================================================================
pro smf_moment_write,gidSMF,phiAll,phiAllC,nGalaxy
;print,'  computing moment of stellar mass function...'
mi=histo_moment(/get_unit)

All=replicate(mi,!smf.nbin,2,!smf.nz,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
;nbin    :number of bin
;2	 :number of resolution levels
;first  3: z~0.0, 0.3, 1.0, 3.0
;second 3: all, central, satellite
;Third  3: all, Star-Forming, Quiescent
;Fourth 3: all, Late, Early, lenticulars(S0), Ellipitcal 
;Fifth  4: all, (0,R200], [R200,3R200], [R200,infinite)
all_lg=replicate(mi,!smf.nbin,2,!smf.nz,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
lgmV=fltarr(!smf.nbin,!smf.nz,!smf.nr)
sn1=indgen(5)+17
for ilev=0,1 do begin
  iv=!Phoenix.isimv[*,ilev]
  for iz=0,!smf.nz-1 do begin
    if iz le 1 then calf=1.0 else calf=0.5
    for ig=0,!smf.ng-1 do begin
      for isq=0,!smf.nsq-1 do begin
        for it=0,!smf.nt-1 do begin
          for ir=0,!smf.nr-1 do begin
            lgm=phiAllC[*,1,iz,ig,isq,it,ir].lgm
	    lgmV[*,iz,ir]=lgm
            phi=phiAllC[*,iv,iz,ig,isq,it,ir].phi
            All[*,ilev,iz,ig,isq,it,ir]=histo_moment(phi,!smf.nbin)
	    nv=nGalaxy[iv,iz,ig,isq,it,ir]
	    sn2=where(nv lt !smf.nbin,n2)
	    if n2 eq 0 then All_lg[*,ilev,iz,ig,isq,it,ir]=histo_moment(phi,!smf.nbin,log10=1)
	    if(ig+isq+it+ir eq 0)and(n2 eq 0)then begin
     	      li2009=SMF_li2009(lgm)
	      li2009=li2009.phi
 	      All[sn1,ilev,iz,ig,isq,it,ir].average=li2009[sn1]*calf
	      All[sn1,ilev,iz,ig,isq,it,ir].up=li2009[sn1]*calf+All[sn1,ilev,iz,ig,isq,it,ir].deviation
	      All[sn1,ilev,iz,ig,isq,it,ir].low=li2009[sn1]*calf-All[sn1,ilev,iz,ig,isq,it,ir].deviation
	      All_lg[sn1,ilev,iz,ig,isq,it,ir].average=li2009[sn1]*calf
	      All_lg[sn1,ilev,iz,ig,isq,it,ir].up=li2009[sn1]*calf*All_lg[sn1,ilev,iz,ig,isq,it,ir].deviation
	      All_lg[sn1,ilev,iz,ig,isq,it,ir].low=li2009[sn1]*calf/All_lg[sn1,ilev,iz,ig,isq,it,ir].deviation
    	    endif
	  endfor
    	endfor
      endfor
    endfor
  endfor
endfor
h5write_manydata,gidSMF,['moment_phiAll','moment_lg_phiAll','moment_lgm'],$
    	    	 All,All_lg,lgmV
end
;====================================================================
;====================================================================
pro smf_imodel,fname,gidSMF
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
hi={lgm:0.0,phi:0.0d}
phiAll =replicate(hi,!smf.nbin,!Phoenix.nsim,!smf.nz,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
phiAllC=replicate(hi,!smf.nbin,!Phoenix.nsim,!smf.nz,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
;nbin    :number of bin
;nsim	 :number of simulations
;first  3: z~0.0, 0.3, 1.0, 3.0
;second 3: all, central, satellite
;Third  3: all, Star-Forming, Quiescent
;Fourth 3: all, Late, Early, lenticulars(S0), Ellipitcal 
;Fifth  4: all, (0,R200], [R200,3R200], [R200,infinite)
nGalaxy=lonarr(!Phoenix.nsim,!smf.nz,!smf.ng,!smf.nsq,!smf.nt,!smf.nr)
;==========================================
for isim=1,19 do begin
  print,'isim=',isim
  ;==========================================
  smf_isim,fname,isim,phiAll,phiAllC,nGalaxy
  ;==========================================
endfor
h5write_manydata,gidSMF,['phiAll','phiAll_Calibrated','nGalaxy'],$
    	    	    	phiAll,phiAllC,nGalaxy
smf_moment_write,gidSMF,phiAll,phiAllC,nGalaxy

end
;====================================================================
;====================================================================
pro smf_write
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
print,'computing stellar mass function...'
post=['preset1_shift0']
fid=h5f_create(!galacticus.path+'stellarMassFunction.hdf5')
h5write_dataset,fid,Weight0,'VolumeWeight'
n=n_elements(post)
for i=0,n-1 do begin
  fname=!galacticus.path+!Phoenix.TreeName+'_first_ev_'+post[i]+'.hdf5'
  gid=h5g_create(fid,post[i])
  smf_imodel,fname,gid
  h5g_close,gid
endfor
h5f_close,fid
end
;====================================================================
;====================================================================
function smf_itest,fname,weight=weight,calibrate=calibrate
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
fidVW=h5f_open(!galacticus.path+'treeProperties.hdf5')
weight0=h5read_dataset(fidVW,'VolumeWeight')
h5f_close,fidVW
tn=strmid(fname,34,4)
isim=where(!Phoenix.TreeName eq tn)
print,'isim=',isim,fname
WC=max(Weight0[*,isim])
if n_elements(weight) ne 0 then WC=WC*weight


fid=h5f_open(fname)
Name0=['nodeVirialRadius']
data=nodeData_ReadMany(fid,'Output55',Name0,/mstar,/pos)
data.nodeVirialRadius=data.nodeVirialRadius*h0  ;h^-1 Mpc
data.mstar=data.mstar*h0^2   ; h^-2 M_sun

R200=max(data.nodeVirialRadius,sn1)  ;& print,'R200=',R200
W200=3.0/(4*!pi*R200^3)	    ;h^-3 Mpc^3
center=data.pos[*,sn1]
RC=pair_distance(data.pos,center)/R200

phiAll=smf_histo(data.Mstar,!smf.nbin,!smf.all.low,!smf.all.high,$
	    	      weight=WC,/log10,locations=lgm,li2009=calibrate)

sn0=where(RC le 1)
mstar=data.mstar[sn0]
phiR200=smf_histo(data.Mstar,!smf.nbin,!smf.all.low,!smf.all.high,$
	    	      weight=W200,/log10,wings=calibrate)
h5f_close,fid
return,{lgm:lgm,All:phiAll,R200:phiR200}
end
;====================================================================
;====================================================================
function smf_test,fname,log10=log10,weight=weight
n=n_elements(fname)
phi=dblarr(!smf.nbin,n,2)
for i=0,n-1 do begin
  phii=smf_itest(fname[i],/calibrate,weight=weight)
  phi[*,i,0]=phii.all
  phi[*,i,1]=phii.R200
endfor
All=histo_moment(phi[*,*,0],!smf.nbin,log10=log10)
R200=histo_moment(phi[*,*,1],!smf.nbin,log10=log10)

return,{lgm:phii.lgm,All:All.average,R200:R200.average}
end

;====================================================================
;====================================================================
function smf_Sample,fname,outputName
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
fid=h5f_open(fname)
Count=h5read_dataset(fid,'mergerTreeCount',GroupName='Outputs/'+outputName)
Start=h5read_dataset(fid,'mergerTreeStartIndex',GroupName='Outputs/'+outputName)
W0   =h5read_dataset(fid,'mergerTreeWeight',GroupName='Outputs/'+outputName)
;====================================================================
dataname=['diskStellarMass','spheroidStellarMass']
Mstar=NodeData_Read(fid,outputName,dataname=dataname,/sum)    ; M_sun
Weight=Mstar
ntree=n_elements(W0)
for i=0L,ntree-1 do Weight[Start[i]:Start[i]+Count[i]-1]=W0[i]
phi=smf_histo(Mstar,!smf.nbin,!smf.all.low,!smf.all.high,$
    	      weight=Weight,locations=lgm,/log10,/li2009)
h5f_close,fid
return,{lgm:lgm,phi:phi}
end
;====================================================================
;====================================================================
;pro smf_Catalog,gidSMF
;common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
;print,'  computing stellar mass function of different galaxies...'
;hi={lgm:0.0,phi:0.0d}
;nbin=21
;CataPhi=replicate(hi,nbin,!galaxy.ngroup,!galaxy.nstage)
;fidCata=h5f_open(!galacticus.path+'CatalogInfo.hdf5')

;for ilev=1,1 do begin
;  for ig=0,!galaxy.nGroup-1 do begin
;    for it=0,!galaxy.nstage-1 do begin
;      data=galaxy_catalog_read(fidCata,ilev,!galaxy.Group[ig],!galaxy.Stage[it])
;      mstar=data.mstar*h0^2 ; h^-2 M_sun
;    
;      print,min(mstar),max(mstar)
;      mlow=min(mstar)*1.1 > 1e6 & mhigh=max(mstar)*0.95
;      phi=smf_histo(mstar,nbin,mlow,mhigh,locations=lgm,/log10)
;      CataPhi[*,ig,it].lgm=lgm
;      CataPhi[*,ig,it].phi=phi
;    endfor
;  endfor
;endfor
;h5write_dataset,gidSMF,CataPhi,'CataPhi'

;h5f_close,fidCata
;end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;pro sate_read,FinalOverInfallHisto=FinalOverInfallHisto,LifeTimeHisto=LifeTimeHisto,$
;    	      FinalOverInfallLocation=FinalOverInfallLocation,LifeTimeLocation=LifeTimeLocation
;fid=h5f_open(!galacticus.path+'ResultI.hdf5')
;gid=h5g_open(fid,'SatelliteStatistics');

;if arg_present(FinalOverInfallHisto) then FinalOverInfallHisto=h5read_dataset(gid,'FinalOverInfallHisto')
;if arg_present(LifeTimeHisto) then LifeTimeHisto=h5read_dataset(gid,'LifeTimeHisto')
;if arg_present(FinalOverInfallLocation) then FinalOverInfallLocation=h5read_dataset(gid,'FinalOverInfallLocation')
;if arg_present(LifeTimeLocation) then LifeTimeLocation=h5read_dataset(gid,'LifeTimeLocation')

;h5g_close,gid
;h5f_close,fid
;end

;====================================================================
;====================================================================
;====================================================================
;====================================================================
;function GalacticusWeight,fid,outputName,Mstar
;common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
;sn=where((Mstar ge 1e7)and(Mstar lt 1e12))
;dataname=['positionX','positionY','positionZ']
;pos=NodeData_Read(fid,outputName,dataname=dataname,/readpos)
;pos=pos[*,sn]
;center=[max(pos[0,*])+min(pos[0,*]),max(pos[1,*])+min(pos[1,*]),max(pos[2,*])+min(pos[2,*])]/2.0
;R=sqrt((pos[0,*]-center[0])^2+(pos[1,*]-center[1])^2+(pos[2,*]-center[2])^2)
;VC=4.0*!pi*max(R)^3/3.0 
;x0=max(pos[0,*])-min(pos[0,*])
;x1=max(pos[1,*])-min(pos[1,*])
;x2=max(pos[2,*])-min(pos[2,*])

;weight={cubic:1.0/(x0*x1*x2)/h0^3,sphere:1.0/VC/h0^3}
;return,weight
;end
;====================================================================
;==================================================================
;pro smflf_R200,isim,data,phiR200,phiR206,LphiR200
;common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
;;print,'  computing stellar mass function of galaxies within R_200 of cluster...'
;R200=max(data.nodeVirialRadius,sn1)
;center=data.pos[*,sn1]
;R=pair_distance(data.pos,center)
;help,R200,Center,data.pos,R
;for i=0,1 do begin
;  RC=R200*0.6^i
;  sn1=where(R le RC)
;  WC=3.0/(4.0*!pi*RC^3) ;/(200/omega0)  
; Weight=1/V200/(200[omega0*(1+z)^3+lambda0]/[omega0*(1+z)^3])
;  for it=0,2 do begin
;    if it eq 0 then sn2=sn1 else begin
;      sn2=where(data.IsLate[sn1] eq it-1,ngal)
;      if ngal gt !smf.R200.nbin then sn2=sn1[sn2] else continue
;    endelse  
;    Mstar=data.Mstar[sn2]
;    phi=smf_histo(Mstar,!smf.R200.nbin,!smf.R200.low,!smf.R200.high,$
;    	    	  weight=WC,/log10,/wings)
;    if i then begin 
;      phiR206[*,isim,it]=phi
;    endif else begin
;      phiR200[*,isim,it]=phi
;      lumi=data.lumi[sn2]
;      Lphi=smf_histo(Lumi,!lf.nbin,!lf.low,!lf.high,weight=WC,$
;    	    	     /log10,/Popesso,ic=it)
;      LphiR200[*,isim,it]=Lphi
;    endelse
;  endfor
;endfor
;end
;====================================================================
;====================================================================
;====================================================================
;====================================================================
;pro smf_combine,gidSMF
;common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
;OutputList,!galacticus.treeEv[3],nout,OutputName=OutputName
;hi={lgm:0.0,phi:0.0d}
;nbin=21
;phiCombine=replicate(hi,nbin,4,3,3,3)
;first  4: region<R200, R200<region<3R200, region>3R200, whole
;second 3: all central satellite
;third  3: all early late
;fourth 3: output time
;fidCata=h5f_open(!galacticus.path+'CatalogInfo.hdf5')
;for ii=0,2 do begin
;  iout=!smf.ioutv[ii] & print,iout
;  data=h5read_dataset(fidCata,outputName[iout],groupName='CombinedData')
;  data.Mstar=data.Mstar*h0^2   ; h^-2 M_sun
  ;====================================
;  for ir=0,3 do begin
;    if ir eq 3 then sn1=where(data.Mstar gt 1,ngal) else $
;    	    sn1=where((data.ccR gt !smf.ccR[ir])and(data.ccR le !smf.ccR[ir+1]),ngal)
;    for ig=0,2 do begin
;      if ig eq 0 then sn2=sn1 else begin
;        sn2=where(data[sn1].nodeIsIsolated eq 2-ig,ngal)
;    	if ngal gt nbin then sn2=sn1[sn2] else continue
;      endelse
;      for it=0,2 do begin
;        if it eq 0 then sn3=sn2 else begin
;          sn3=where(data[sn2].IsLate eq it-1,ngal)
;	  if ngal gt nbin then sn3=sn2[sn3] else continue
;        endelse
;        mstar=data[sn3].Mstar
;        mlow=min(mstar)*1.1 >1e7  & mhigh=max(mstar)*0.95
;        phi=smf_histo(Mstar,nbin,mlow,mhigh,weight=data[sn3].weight[ir],$
;     	    	      /log10,locations=lgm)
;        phiCombine[*,ir,ig,it,ii].lgm=lgm
;        phiCombine[*,ir,ig,it,ii].phi=phi
;      endfor
;    endfor  
;  endfor   
;endfor
;h5write_dataset,gidSMF,phiCombine,'phiCombine';

;h5f_close,fidCata
;end
;====================================================================
;====================================================================
;function smf_moment,phi,nbin,log10=log10
;if keyword_set(log10) then begin 
;  mom=histo_moment(alog10(phi),nbin)
;  for i=0,3 do mom.(i)=10^mom.(i)
;endif else mom=histo_moment(phi,nbin)
;return,mom
;end
;====================================================================
;====================================================================
