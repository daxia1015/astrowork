function rh2c_f,c,rh_rvir
f=gx(rh_rvir*c)/gx(c)-0.5
return,f
end
;==================================================================
;==================================================================
function rh2c_fp,c,rh_rvir
gc=gx(c)
fp=ghx_p(c,rh_rvir)/gc-gx_p(c)*gx(rh_rvir*c)/gc^2
return,fp
end
;==================================================================
;==================================================================
function rh2rs_NFW,rh,rvir,ci
rh_rvir=rh/rvir

c=inewton('rh2c_f','rh2c_fp',ci,[1,1],rh_rvir,eps=1e-6)
rs=rvir/c
help,rh,rvir,c,rs
print,min(c),max(c)
print,min(rs),max(rs)
return,rs
end
;==================================================================
;==================================================================
pro rh2rs_test
device,retain=2
mhalo=1e12 & z=2.
c=chalo(mhalo,z)
rvir=r_virial(mhalo,z)
rs=rvir/c
rh=50.
r=findgen(rvir)+1
print,c,rvir,rs
x=findgen(500)*0.01
plot,x,gx(x)/gx(10.)
for i=0,8 do oplot,x,gx(x)/gx(i+1.),linestyle=i
oplot,x,fltarr(500)+0.5
;plot,rs,rh2rs_f(rs,rh,rvir),/xlog

end
;==================================================================
;==================================================================
pro rh2rs
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common simulation  ;,particlemass,unit
!except=1
zlist=snaplist(/redshift)
isim=3
tree=assoctree(!treefile[isim],ntree,totnhalo,nhalos)
nhalo=nhalos[0]
halosn=lindgen(nhalo)
breaktree,!treefile[isim],tree,totnhalo,$
    	       mhalo,snapnum,rh, $
	       tag=['len','snapnum','HalfMassRadius'],halosn=halosn
mhalo=temporary(mhalo)*particlemass[isim]
rh=temporary(rh)*unit.d
z=zlist[snapnum]
rvir=r_virial(mhalo,z)
ci=chalo(mhalo,z)
sn=where(ci le 1,n)
print,nhalo,n
;print,z[sn]
print,min(rh),max(rh)
print,min(rvir),max(rvir)
ci=ci > 4.
rs=rh2rs_NFW(rh,rvir,ci)
help,rs

end
