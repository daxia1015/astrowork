pro test_rp
mhalo=1e12
c=chalo(mhalo,0)
rvir=r_vir(mhalo,0)
rs=rvir/c
n=100.
r=findgen(n)*rs/n/10+rs/n/10
ro=ro_r(r,rs,c)
mr=mhalo_r(r,rs,c,mhalo)
p=mr*ro/r
help,c,rs

device,filename='test/test_rp.ps',/color,/encapsul
plot,r,p,/ylog



end
