;====================================================================
;====================================================================
;pro h5_test
;fname=!galacticus.treeEv[3]
;a=Galacticus_GlobalHistory(fname,nhistory)
;Galacticus_Outputs,fname,info=info,nodedata=nodedata,dataname=['diskScaleLength','spheroidScaleLength']
;MAH=Galacticus_MAH(fname)
;para=Galacticus_para(fname,paraName,npara)

;help,a,nhistory,info,nodedata,para,npara,/struc
;help,nhistory
;for i=0,npara-1 do print,paraName[i]
;fid=h5f_open(fname)
;gid1=h5g_open(fid,'Outputs')
;gid2=h5g_open(gid1,'Output1')
;gid3=h5g_open(gid2,'nodeData')
;gid4=h5g_open(fid,'Outputs/Output1')
;gid5=h5g_open(fid,'Outputs/Output1/nodeData')
;did1=h5d_open(gid3,'blackHoleCount')
;did2=h5d_open(gid5,'blackHoleCount')
;print,gid1,gid2,gid3,gid4,gid5,did1,did2
;h5f_close,fid;
;
;end
;GroupName='globalHistory'
;fileid = H5F_OPEN(fname) 
;nset=h5g_get_Nmembers(fileid,GroupName)
;dataname=strarr(nset)
;for iset=0,nset-1 do dataname[iset]=h5g_get_member_name(fileid,GroupName,iset)
;print,dataname
;out=h5read_dataset(fileid,DataName,GroupName=GroupName)
;help,out,/struct
;a=h5_parse(fileid,GroupName,/read_data)
;message,'Read out '+a._comment,/continue

;groupname='Parameters'
;a=h5g_get_num_objs(fileid)
;gid=h5g_open(fileid,groupname)
;b=h5g_get_num_objs(gid)
;c=h5g_get_nmembers(fileid,groupname)
;help,a,b,c
;help,a,/struc
;help,a.(6),/struc
;help,a.(6)._data
;print,tag_names(a)
;print,tag_names(a.(6))
;h5f_close,fileid
