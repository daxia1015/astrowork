;====================================================================
;====================================================================
;Schechter luminosity function or stellar mass function
;\phi(m)=\phi_s*(m/ms)^alpha*exp(-m/ms)/ms
;return dn/dlgm=\phi(m)*dm/dlgm=phi(m)*m*ln10 
;   	       =\phi_s*(m/ms)^(alpha+1)*exp(-m/ms)*ln10
function SchechterFunction,lgm,phis,alpha,lgms ;,log10=log10
;print,phis,alpha,lgms
m=10^lgm
ms=10^lgms
moms=m/ms
phi=phis*moms^(alpha+1.)*exp(-moms)*alog(10)
;print,phi
return,phi
end
;====================================================================
;====================================================================
function SchechterFunctionYang,lgm,phis,alpha,lgms ;,log10=log10
;if keyword_set(log10) then begin
m=10^lgm
ms=10^lgms
;endif
moms=m/ms
phi=phis*moms^(alpha+1.)*exp(-moms^2)*alog(10)
return,phi
end

;====================================================================
;====================================================================
function LogNormalFunction,lgm,A,sigmaC,lgms
phi=A*exp(-(lgm-lgms)^2/(2*sigmaC^2))/(sqrt(2*!pi)*sigmaC)
;phi=phi*alog10(10)
return,phi
end
;====================================================================
;====================================================================
function SchechterFit,p,lgm=lgm,mlow=mlow,mhigh=mhigh,nbin=nbin,$
	 log10=log10,h=h,weight=weight
if n_elements(lgm) eq 0 then begin
  if keyword_set(log10) then begin
    mlow=alog10(mlow)
    mhigh=alog10(mhigh)
  endif
  if n_elements(h) ne 0 then begin
    mlow=mlow+2*alog10(h)
    mhigh=mhigh+2*alog10(h)
  endif  
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif
if n_elements(h) ne 0 then p.lgms=p.lgms+2*alog10(h)

phi    =SchechterFunction(lgm,p.phis,          p.alpha,           p.lgms)
phi_up =SchechterFunction(lgm,p.phis+p.phisErr,p.alpha-p.alphaErr,p.lgms+p.lgmsErr)
phi_low=SchechterFunction(lgm,p.phis-p.phisErr,p.alpha+p.alphaErr,p.lgms-p.lgmsErr)
if n_elements(weight) ne 0 then $
  smf={lgm:lgm,phi:phi*weight,up:phi_up*weight,low:phi_low*weight} $
  else smf={lgm:lgm,phi:phi,up:phi_up,low:phi_low}
return,smf
end
;====================================================================
;====================================================================
