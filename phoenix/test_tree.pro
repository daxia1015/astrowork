pro tree01
PhoenixTable

device,filename='treeProperties/tree01.eps',/color,/encapsul,xsize=18,ysize=8
multiplot_Gan,[2,1],mytitle='Fraction',xgap=0.02
log10=1 ;& print,log10
for isim=3,3 do begin
  fname=!Phoenix.TreeFile[isim] & Evname=!galacticus.treeEv[isim]
  tree=readtree_ph(fname,ntree,totnhalo,nhalos,/alltree)
;    sn=where(tree.snapnum eq !Phoenix.n_snap-1,n)
  center=tree[0].pos
  tree0=tree[1:nhalos[0]-1]
  tree00=tree0[where(tree0.snapnum eq !phoenix.n_snap-1)]
  tree1=tree[nhalos[0]:totnhalo-1]
  tree10=tree[where(tree1.snapnum eq !phoenix.n_snap-1)]
  help,tree0,tree00,tree1,tree10
  m0=tree0.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  m00=tree00.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  R0=pair_distance(tree0.pos,center)/!PhoenixTable.R200[isim]
  R00=pair_distance(tree00.pos,center)/!PhoenixTable.R200[isim]
  m1=tree1.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  m10=tree10.len*!PhoenixTable.mp[isim]/!PhoenixTable.M200[isim]
  R1=pair_distance(tree1.pos,center)/!PhoenixTable.R200[isim]
  R10=pair_distance(tree10.pos,center)/!PhoenixTable.R200[isim]

  h=histo(m0,nbin=11,xmin=min(m0)*1.1,xmax=0.1,log10=log10,locations=xh)  
  plot,10^xh,h/total(h),psym=10,xtitle='M!dhalo!n / M!d200!n',$
       xlog=log10,xrange=[4e-6,0.2],yrange=[0,0.49]
  h=histo(m1,nbin=11,xmin=min(m1)*1.1,xmax=max(m1)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10
  h=histo(m00,nbin=11,xmin=min(m00)*1.1,xmax=max(m00)*0.95,log10=log10,locations=xh)  
  
  oplot,10^xh,h/total(h),psym=10,color=!myct.red
  h=histo(m10,nbin=11,xmin=min(m10)*1.1,xmax=max(m10)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10,color=!myct.red
  multiplot_Gan
  
  h=histo(R0,nbin=11,xmin=0.01,xmax=max(R0)*0.95,log10=log10,locations=xh)  
  plot,10^xh,h/total(h),psym=10,xtitle='d!dc!n / R!d200!n',$
       xlog=log10,xrange=[0.01,20],yrange=[0,0.49]
  h=histo(R1,nbin=11,xmin=min(R1)*1.1,xmax=max(R1)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10
  h=histo(R00,nbin=11,xmin=min(R00)*1.1,xmax=max(R00)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),psym=10,color=!myct.red
  h=histo(R10,nbin=11,xmin=min(R10)*1.1,xmax=max(R10)*0.95,log10=log10,locations=xh)  
  oplot,10^xh,h/total(h),linestyle=1,psym=10,color=!myct.red
  multiplot_Gan
endfor

multiplot_Gan,/default
end
;====================================================================
;====================================================================
pro treeDensity
PhoenixTable
nbin=16 

device,filename='treeProperties/haloDis.eps',/color,/encapsul,xsize=8,ysize=15
multiplot_Gan,[1,3],mxtitle='d!dc!n / R!d200!n',/doyaxis
log10=1 & xr=[0.05,17]
for isim=5,5 do begin
  fname=!Phoenix.TreeFile[isim] 
  tree=readtree_ph(fname,ntree,totnhalo,nhalos,itree=0)
  center=tree[0]
  sn=where((tree.snapNum eq !Phoenix.n_snap-1)and(tree.len ne center.len),n0)
  tree=tree[sn]
  R200=!PhoenixTable.R200[isim]
  R=pair_distance(center.pos,tree.pos)/R200
  Rmin=min(R)*1.1 & Rmax=max(R)*0.95
  h=histo(R,nbin=nbin,xmin=Rmin,xmax=Rmax,dbin=dbin,log10=log10,locations=xh)  
  xh=10^xh & dbin2=10^dbin/2
  plot,xh,h/total(h),psym=10,ytitle='Number Fraction',$
       xlog=log10,xrange=xr,yrange=[0,0.39]
  multiplot_Gan,/doyaxis

  xV=4*!pi*R200^3*((xh+dbin2)^3-(xh-dbin2)^3)/3.0
  y=h/xV
  plot,xh,y,psym=10,ytitle='Number Density',$
       xlog=log10,xrange=xr,yrange=[0.011,90],/ylog
  multiplot_Gan,/doyaxis

  mass=tree.len*!PhoenixTable.mp[isim]
  h=histo(R,nbin=nbin,xmin=Rmin,xmax=Rmax,log10=log10,weight=mass)
  y=h/xV
  plot,xh,y,psym=10,ytitle='Mass Density',$
       xlog=log10,xrange=xr,yrange=[8e8,9.9e11],/ylog
  multiplot_Gan,/doyaxis
    
endfor

multiplot_Gan,/default

end

