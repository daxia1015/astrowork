pro plot_galacticus
;isimv=[19,15,3];['phi4','phg4','pha4']
;print,!galacticus.treeEv[isimv]
gcs='gcsoutput/'
interaction='interaction/'
isim=21

evfile=!galacticus.treeEv[isim]
OutputList,evfile,totnout,OutputName=OutputName,tlist=tlist
;==testing galacticus...
;plot_morph,gcs,evfile,istepv,outputName,tlist
;plot_cm,gcs,evfile,istepv,outputName,tlist
plot_color,gcs
plot_global,gcs
;plot_Rdisk,gcs

;fid0=h5f_open(!galacticus.treeEv[isim])
;fid1=h5f_open(!galacticus.catalogInfo[isim])
;fid2=h5f_open(!galacticus.catalogData[isim])


;read_cata_num,fid2
;plot_cata_num,interaction,fid1
;==plotting star formation
;plot_histogram_catastar,interaction,fid2
;plot_histogram_satestar,interaction,fid2
;plot_histogram_cataOutflow,interaction,fid2
;plot_histogram_cataBHmass,interaction,fid2

;==plotting color distribution
;plot_histogram_catacolor,interaction,fid2
;plot_histogram_satecolor,interaction,fid2

;==plotting morphology
;plot_histogram_catamorph,interaction,fid2
;plot_cata_mass_size,interaction,fid2
;plot_sateGroup_mass_size,interaction,fid2
;plot_cata_spheroid_disk,interaction,fid2
;plot_sateGroup_spheroid_disk,interaction,fid2

;==plotting dependence of spatial distribution
;plot_histogram_spatial,interaction,fid2
;plot_iso_sCCR_star,interaction,fid2
;plot_host_sCCR_star,interaction,fid2
;plot_sate_sCCR_star,interaction,fid2
;plot_sateGroup_sCCR_star,interaction,fid2
;plot_cata_sCCR_color,interaction,fid2
;plot_sateGroup_sCCR_color,interaction,fid2
;plot_cata_sCCR_morph,interaction,fid2
;plot_sateGroup_sCCR_morph,interaction,fid2

;==plotting metallicity
;plot_histogram_cataGasMetal,interaction,fid2
;plot_histogram_cataStellarMetal,interaction,fid2
;plot_histogram_sateGasMetal,interaction,fid2
;plot_histogram_sateStellarMetal,interaction,fid2



;plot_galacticus2,psdir,isimv
;plot_galacticus3,psdir,istepv,outputName,tlist
;plot_galacticus4,psdir,19,outputName,tlist

;h5f_close,fid0
;h5f_close,fid1
;h5f_close,fid2
end
