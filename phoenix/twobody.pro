function twobody_initialize,totnhalo,FirstProgenitor,FirstHaloInFOFgroup,$
    	    	    	    m200,snapnum,ngroup,minsnap
sub=-1L & host=-1L
index=lindgen(totnhalo)
hostv=where((FirstHaloInFOFgroup eq index)and(m200 ge 10),nhost)
snapv= snapnum[hostv]  &  minsnap=min(snapv)
hostv=hostv[sort(snapv)]    & print,'nhost=',nhost
sn1=where((snapnum ge minsnap)and(FirstProgenitor eq -1))
fhifg=FirstHaloInFOFgroup[sn1]

for i=0L,nhost-1 do begin
  hosti=hostv[i]
  if i mod 1000 eq 0 then print,'hosti=',hosti
  sn2=where(fhifg eq hosti,nsub)
  if nsub ne 0 then begin
    sub=[temporary(sub),sn1[sn2]]
    host=[temporary(host),lonarr(nsub)+hosti]
  endif
endfor
n1=n_elements(sub) & n2=n_elements(host)
if n1 ne n2 then message,'error found'
print,'ngroup=',n1-1
sn=where((sub ne host)and(snapnum[sub] eq snapnum[host]),ngroup)
print,'ngroup_valid=',ngroup
track_ini=replicate(!twobody.track,ngroup)
track_ini.sub=sub[sn] &  track_ini.host=host[sn]
return,track_ini
end
;==================================================================
;==================================================================
pro twobody_find,Descendant,FirstProgenitor,FirstHaloInFOFgroup,snapnum,$
    	    	    	   ngroup,maxstep,nsteps,IDs,track
nsteps=intarr(ngroup)+1 & IDs=fltarr(ngroup)    ;ID=0, unidentified
track_f=track[*,0]
sn1=where((snapnum[track_f.sub] eq  !Phoenix.n_snap-1)and $
    	    	  (FirstProgenitor[track_f.sub] eq -1),n1)
if n1 ne 0 then IDs[sn1]=1                                                        ;Survive
;help,n1,maxstep
for istep=0,maxstep-1 do begin
  track_f=track[*,istep]
  sn0=where((track_f.sub ne -1)and(track_f.host ne -1)and(IDs eq 0),n0)
  if istep mod 5 eq 0 then print,'unidentified=',n0
  if n0 ne 0 then begin
    track_f=track_f[sn0]
    IDs0=IDs[sn0]
  endif else break

  sub=Descendant[track_f.sub] & host=Descendant[track_f.host]
  
  sn1=where((sub eq -1)and(host eq -1)and(IDs0 eq 0),n1)
  if n1 ne 0 then IDs0[sn1]=3                                                          ;Disappear
;  help,n1
  sn1=where((sub eq  -1)and(host ne -1)and(IDs0 eq 0),n1) 
  if n1 ne 0 then IDs0[sn1]=3.1                                                         ;Disappear
;  help,n1
  sn1=where((sub ne  -1)and(host eq -1)and(IDs0 eq 0),n1)
  if n1 ne 0 then IDs0[sn1]=3.2                                                          ;Disappear
;  help,n1  
  
  sn1=where((sub eq host)and(IDs0 eq 0),n1)
  if n1 ne 0 then IDs0[sn1]=4                                                         ;Unresolved
;  help,n1
  
  sn1=where((snapnum[sub] ne snapnum[host])and(IDs0 eq 0),n1)
  if n1 ne 0 then IDs0[sn1]=2.1                                                              ;Escape
;  help,n1  
  sn1=where((FirstHaloInFOFgroup[sub] ne host)and(IDs0 eq 0),n1)
  if n1 ne 0 then IDs0[sn1]=2.2                                                             ;Escape
;  help,n1
     
  ;the following statements should be the last one
  sn1=where((snapnum[sub] eq  !Phoenix.n_snap-1) and(IDs0 eq 0) ,n1)
  if n1 ne 0 then IDs0[sn1]=1                                                         ;Survive
  ;help,n1
  
  sn1=where(IDs0 lt 3,n1)
  if n1 ne 0 then begin
    track[sn0[sn1],istep+1].sub=sub[sn1]  
    track[sn0[sn1],istep+1].host=host[sn1]
    nsteps[sn0[sn1]]=nsteps[sn0[sn1]]+1
  endif
  IDs[sn0]=IDs0  
endfor
sn1=where(IDs eq 0,n1)
if n1 ne 0 then message,'error found' 
end
;==================================================================
;==================================================================
function tra_sim2sam,isim,track,totnstep,len,m200,pos,velocity,Vmax,snapnum,$
    	    	    	    	 hprofile=hprofile
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
common simulation  ;,!Phoenix.mp,unit
zlist=snaplist(/redshift)
tra=replicate(!twobody.tra,totnstep)
sub=track.sub & host=track.host
tra.sub=sub & tra.host=host
zv=zlist[snapnum[track.host]]
vecd=(pos[*,sub]-pos[*,host])*PhoenixUnit.d
vecvel=(velocity[*,sub]-velocity[*,host])*PhoenixUnit.v
vecangl=crosspn(vecd,vecvel)                    ;specific angular momentum, vector
vmax_host=Vmax[host]*PhoenixUnit.v

tra.t=tGy(zv)
tra.mhost=m200[host]*PhoenixUnit.m & tra.msub=m200[sub]*PhoenixUnit.m
sn1=where(tra.msub eq 0)
tra[sn1].msub=len[sub[sn1]]*!Phoenix.mp[isim]
sn1=where(tra.mhost eq 0,n1) ;& help,n1
tra[sn1].mhost=len[host[sn1]]*!Phoenix.mp[isim]

tra.d=sqrt(total(vecd^2,1))                       ;radial position
tra.Vorb=sqrt(total(vecvel^2,1))     	    	  ;orbital velocity
tra.angl=sqrt(total(vecangl^2,1))                           ;angular mementum
tra.chost=chalo(tra.mhost,zv)                  ;concentration of host halo
Vvir2=vmax_host^2*gx(tra.chost)/tra.chost/0.216217             ;NFW profile
tra.Vvir=sqrt(Vvir2)	    	    	    	     ;virial velocity of host halo
tra.Rhost=gaussc*tra.mhost/Vvir2	    	    	    ;virial radius of host halo

rs_host=tra.Rhost/tra.chost
ros_host=ros_profile(zv,tra.chost,hprofile=hprofile)
mhost_r=mhalo_r(tra.d,rs_host,tra.chost,tra.mhost,hprofile=hprofile)  ;host halo mass within tra.d
ro_host_r=ro_r(tra.d,rs_host,tra.chost,ros_host,hprofile=hprofile)

tra.energy=energy_r(tra.d,tra.mhost,rs_host,tra.chost,tra.Vorb,hprofile=hprofile)
tra.f_g=gaussc*mhost_r/tra.d^2                          ;gravity at radius tra.d
tra.lnlda=lnLambda(tra.msub,tra.Vorb,tra.d)       ;Coulomb logarithm
tra.f_df=f_DynaFric(tra.msub,ro_host_r,tra.Vorb,tra.d,rs_host,vmax_host,$
    	    	    	      tra.lnlda,hprofile=hprofile)     ;dynamical friction

orbit_parameters,tra.mhost,rs_host,tra.chost,tra.energy,$
    	    	    	  tra.angl,tra.Vvir,yita,epson,sn=boundsn 
tra[boundsn].yita=yita
tra[boundsn].epson=epson
return,tra
end
;==================================================================
;==================================================================
pro twobody_isim,isim,ngroup,totnstep,nsteps,IDs,tra
tree=assoctree(!Phoenix.TreeFile[isim],ntree,totnhalo,nhalos)
breaktree,!Phoenix.TreeFile[isim],tree,totnhalo,Descendant,FirstProgenitor,FirstHaloInFOFgroup,$
    	       len,m200,pos,velocity,Vmax,snapnum, $
	       tag=['Descendant','FirstProgenitor','FirstHaloInFOFgroup',$
	       'len','m200','pos','vel','vmax','snapnum']
track_ini=twobody_initialize(totnhalo,FirstProgenitor,FirstHaloInFOFgroup,$
    	    	    	    	    	  m200,snapnum,ngroup,minsnap)
maxstep=!Phoenix.n_snap-minsnap
track=replicate(!twobody.track,ngroup,maxstep)
track[*,0]=track_ini
twobody_find,Descendant,FirstProgenitor,FirstHaloInFOFgroup,snapnum,$
    	    	     ngroup,maxstep,nsteps,IDs,track	
index=lindgen(ngroup,maxstep) ;& help,index
index=reform(transpose(index),ngroup*maxstep)	  ;& help,index			  
track=track[index]   ;& help,track
sn1=where((track.host ne -1)and(track.sub ne -1),totnstep)
track=track[sn1]	;& help,track			  
if total(nsteps,/int) ne totnstep then message,'error found'	
message,'two body finding completed!',/continue	     	
tra=tra_sim2sam(isim,track,totnstep,len,m200,pos,velocity,Vmax,snapnum,$
    	    	    	  hprofile=!key.hprofile)

close,/all
end
;==================================================================
;==================================================================
pro twobody_write

for isim=16,19 do begin
  print,'isim=',!Phoenix.TreeFile[isim]
  twobody_isim,isim,ngroup,totnstep,nsteps,IDs,tra
 ; help,ngroup,totnstep,nsteps,IDs,tra
  openw,lun,!twobody.fnametra[isim],/get_lun
    writeu,lun,ngroup,totnstep
    writeu,lun,nsteps,IDs
    writeu,lun,tra
  free_lun,lun
endfor
end
;==================================================================
;==================================================================
function twobody_read,fname,ngroup,totnstep,nsteps,IDs
ngroup=0L & totnstep=0L

openr,lun,fname,/get_lun
  readu,lun,ngroup,totnstep
  nsteps=intarr(ngroup) & IDs=fltarr(ngroup)
  array=replicate(!twobody.tra,totnstep)
  readu,lun,nsteps,IDs
  readu,lun,array
  if eof(lun) then print,'end of ',fname
free_lun,lun
return,array
end
;==================================================================
;==================================================================
pro twobody_test
!except=2

for isim=1,19 do begin
  track=twobody_read(!twobody.fnametra[isim],ngroup,totnstep,nsteps,IDs)
  n0=double(ngroup)
  ;help,ngroup ;,totnstep,nsteps,IDs,track
  sn1=where(IDs eq 1,n1)
  sn21=where(IDs eq 2.1,n21)
  sn22=where(IDs eq 2.2,n22)
  sn3=where(IDs eq 3,n3)
  sn31=where(IDs eq 3.1,n31)
  sn32=where(IDs eq 3.2,n32)
  sn4=where(IDs eq 4,n4)
  print,n1/n0,n21/n0,n22/n0,n3/n0,n31/n0,n32/n0,n4/n0
  if (n1+n21+n22+n3+n31+n32+n4) ne ngroup then message,'error'
endfor

end

;==================================================================
;==================================================================
;pro twobody_refine,FirstProgenitor,ngroup,maxstep,nsteps,IDs,track
;sub0=track[*,0].sub & sub1=track[*,1:maxstep-1].sub
;sn1=where(FirstProgenitor[sub0] ne -1,n1)
;sn2=where(FirstProgenitor[sub0] eq -1,n2)
;sub0=sub0[sn1]

;sn3=-1
;for i=0L,n1-1 do begin
;  if i mod 1000 eq 0 then print,'i=',i
;  sn4=where(sub1 eq sub0[i],n4)
;  if n4 eq 0 then sn3=[sn3,sn1[i]]  
;endfor
;n3=n_elements(sn3)-1
;sn3=sn3[1:n3]
;sn=[sn2,sn3] & ngroup=n2+n3
;track=track[sn,*] & nsteps=nsteps[sn] & IDs=IDs[sn]
;index=lindgen(ngroup,maxstep) ;& help,index
;index=reform(transpose(index),ngroup*maxstep)	  ;& help,index			  
;track=track[index]   ;& help,track
;sn1=where((track.sub ne -1)or(track.host ne -1))
;track=track[sn1]				  ;

;if total(nsteps,/int) ne n_elements(track)  then message,'error found'
;end
