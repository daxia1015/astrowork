pro animate
xfigure,2,1.5,0.04
xh=800 & yh=600

;xinteranimate,20,set=[xh,yh,!Phoenix.n_snap],/mpeg_open,mpeg_quality=100,$
;mpeg_filename='sinx.mpg',title='Example'
alist=snaplist(n_snap)
aname=string(alist,format='(f5.3)')
gifname='gif/'+strmid(!Phoenix.TreeFile,9,5)+'.gif'
remchar,gifname,'-'
for ifile=4,18,2 do begin
  breaktree,!Phoenix.TreeFile[ifile],snapnum,pos,tag=['snapnum','pos']
  xrange=[min(pos[0,*]),max(pos[0,*])]
  yrange=[min(pos[1,*]),max(pos[1,*])]
  zrange=[min(pos[2,*]),max(pos[2,*])]

  for i=0,!Phoenix.n_snap-1 do begin
    sn=where((snapnum eq i),n)
    if n ge 2 then begin
      window,0,xsize=xh,ysize=yh
      erosolo_plot3d,pos[0,sn],pos[1,sn],pos[2,sn],$
      xrange=xrange,yrange=yrange,zrange=zrange,$
      xtitle='Mpc/h',ytitle='Mpc/h',ztitle='Mpc/h';,pcolor='0000ff'xl ;,$
    
      xyouts,0.25,0.78,'a='+aname[i],/normal
  ;    tvlct,r,g,b,/get
      write_gif,gifname[ifile],tvrd(80,30,640,480),/multiple,delay_time=30
    endif
  endfor
  write_gif,/close
endfor
end
