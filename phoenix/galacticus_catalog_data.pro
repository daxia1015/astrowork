pro galData_write,galaxyDataName,galData,tailHead=tailHead,igroup=igroup
common galaxy_catalog_data_block,fid0,fid1
common galaxy_catalog_run_block,nout,outputName,tlist,zlist
ngal=n_elements(galData)/3
if keyword_set(tailHead) then begin
  t0=galData[0:ngal-1]
  t2=galData[2*ngal:3*ngal-1]
  tlife=t2-t0
endif
for istage=0,2 do begin
  if keyword_set(tailHead) then data1=tlife else $
    	data1=galData[istage*ngal:(istage+1)*ngal-1]
  groupName=!galaxy.group[igroup]+'/'+!galaxy.stage[istage]
  h5write_dataset,fid0,data1,galaxyDataName,GroupName=GroupName
endfor

end
;==================================================================
;==================================================================
pro sateData_write,galaxyDataName,sate,sateData,tailHead=tailHead
common galaxy_catalog_data_block,fid0,fid1
common galaxy_catalog_run_block,nout,outputName,tlist,zlist
nsate=n_elements(sate)/3 & nsate1=n_elements(sateData)/3
if nsate ne nsate1 then message,'error'
sate0=sate[0:nsate-1]

if keyword_set(tailHead) then begin
  t0=sateData[0:nsate-1]
  t2=sateData[2*nsate:3*nsate-1]
  tlife=t2-t0
endif

igroup=0
for im=0,1 do begin
  for ie=0,2 do begin
    for istage=0,2 do begin
      if istage eq 0 then begin    
        sn1=where((sate0.mratio gt !galaxy.mr[im])and(sate0.mratio le !galaxy.mr[im+1]) $
      	    	and(sate0.ecc ge !galaxy.ecrit[ie])and(sate0.ecc lt !galaxy.ecrit[ie+1]),n1)
      endif
      if n1 ne 0 then begin
    	if keyword_set(tailHead) then data1=tlife[sn1] else begin
	  data0=sateData[istage*nsate:(istage+1)*nsate-1]     
    	  data1=data0[sn1]
	endelse
	GroupName=!galaxy.sateGroup[igroup]+'/'+!galaxy.stage[istage]
    	h5write_dataset,fid0,data1,galaxyDataName,GroupName=GroupName
      endif    
    endfor
    igroup++
  endfor
endfor

end
;==================================================================
;==================================================================
pro galData_make,galaxyDataName,nodeDataName,iso,central,sate,$
    	    	 _extra=_extra
common galaxy_catalog_data_block,fid0,fid1
common galaxy_catalog_run_block,nout,outputName,tlist,zlist
print,'writting ',galaxyDataName
isoData=double(iso.mhalo)
centralData=double(central.mhalo)
sateData=double(sate.mhalo)

for iout=0,nout-1 do begin
  sn1=where(iso.iout eq iout,n1)
  sn2=where(central.iout eq iout,n2)
  sn3=where(sate.iout eq iout,n3)
  if (n1+n2+n3 eq 0) then continue
  data=nodeData_ReadOne(fid1,outputName[iout],nodeDataName,_extra=_extra)
  if n1 ne 0 then isoData[sn1]    =data[iso[sn1].sn]
  if n2 ne 0 then centralData[sn2]=data[central[sn2].sn]
  if n3 ne 0 then sateData[sn3]   =data[sate[sn3].sn]
endfor

galData_write,galaxyDataName,isoData,igroup=0
galData_write,galaxyDataName,centralData,igroup=1
galData_write,galaxyDataName,sateData,igroup=2
sateData_write,galaxyDataName,sate,sateData
end
;==================================================================
;==================================================================
pro galaxy_catalog_data,isim
common galaxy_catalog_data_block,fid0,fid1
common galaxy_catalog_run_block,nout,outputName,tlist,zlist

fid0=h5f_create(!galacticus.CatalogData[isim])
fid1=h5f_open(!galacticus.treeEv[isim])
h5write_group,fid0,[!galaxy.group,!galaxy.sategroup],!galaxy.stage

fid2=h5f_open(!galacticus.CatalogInfo[isim])
iso=h5read_dataset(fid2,'galaxyThreeTrace/IsolatedGalaxies')
central=h5read_dataset(fid2,'galaxyThreeTrace/CentralGalaxies')
sate=h5read_dataset(fid2,'galaxyThreeTrace/SatelliteGalaxies')
h5f_close,fid2


galaxyDataName='timeDuration'
galData_write,galaxyDataName,tlist[iso.iout],igroup=0,/tailHead
galData_write,galaxyDataName,tlist[central.iout],igroup=1,/tailHead
galData_write,galaxyDataName,tlist[sate.iout],igroup=2,/tailHead
sateData_write,galaxyDataName,sate,tlist[sate.iout],/tailHead

galaxyDataName='nodeMass'
galData_write,galaxyDataName,iso.mhalo,igroup=0
galData_write,galaxyDataName,central.mhalo,igroup=1
galData_write,galaxyDataName,sate.mhalo,igroup=2
sateData_write,galaxyDataName,sate,sate.mhalo

galaxyDataName='specificClusterCentricRadius'
galData_write,galaxyDataName,iso.cRR,igroup=0
galData_write,galaxyDataName,central.cRR,igroup=1
galData_write,galaxyDataName,sate.cRR,igroup=2
sateData_write,galaxyDataName,sate,sate.cRR

galaxyDataName='unityBase'
data=iso.mhalo & data[*]=1
galData_write,galaxyDataName,data,igroup=0
data=central.mhalo & data[*]=1
galData_write,galaxyDataName,data,igroup=1
data=sate.mhalo & data[*]=1
galData_write,galaxyDataName,data,igroup=2
sateData_write,galaxyDataName,sate,data




galaxyDataName='color_g_r'
nodeDataName='g-r'
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/gcolor


nodeDataName='diskStellarMass'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original

nodeDataName='spheroidStellarMass'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original

galaxyDataName='stellarMass'
nodeDataName=['diskStellarMass','spheroidStellarMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum


nodeDataName='nodeVirialRadius'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original


nodeDataName='outflowedMass'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original


nodeDataName='blackHoleMass'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original


galaxyDataName='coldGasMass'
nodeDataName=['diskGasMass','spheroidGasMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum

galaxyDataName='starFormationRate'
nodeDataName=['diskStarFormationRate','spheroidStarFormationRate']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum


nodeDataName='hotHaloMass'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original


galaxyDataName='galaxyAllMass'
nodeDataName=['diskStellarMass','spheroidStellarMass','diskGasMass','spheroidGasMass','hotHaloMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum



galaxyDataName='diskHalfMassRadius'
nodeDataName='diskScaleLength'
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/Rhalf,/Exponential

galaxyDataName='spheroidHalfMassRadius'
nodeDataName='spheroidScaleLength'
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/Rhalf,/Hernquist

galaxyDataName='allGasMass'
nodeDataName=['diskGasMass','spheroidGasMass','hotHaloMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum


nodeDataName='diskStellarLuminosity:SDSS_r:rest:z0.0000'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original

nodeDataName='spheroidStellarLuminosity:SDSS_r:rest:z0.0000'
galaxyDataName=nodeDataName
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original


galaxyDataName='stellarLuminosity:SDSS_r:rest:z0.0000'
nodeDataName=['diskStellarLuminosity:SDSS_r:rest:z0.0000',$
    	      'spheroidStellarLuminosity:SDSS_r:rest:z0.0000']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/original,/sum


galaxyDataName='coldGasMetallicity_12plusOoverH'
nodeDataName=['diskGasMetals','spheroidGasMetals','diskGasMass','spheroidGasMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/metal,/p12OoverH


galaxyDataName='hotGasMetallicity_12plusOoverH'
nodeDataName=['hotHaloMetals','hotHaloMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/metal,/p12OoverH

galaxyDataName='outflowedMetallicity_12plusOoverH'
nodeDataName=['outflowedMetals','outflowedMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/metal,/p12OoverH


galaxyDataName='allGasMetallicity_12plusOoverH'
nodeDataName=['diskGasMetals','spheroidGasMetals','hotHaloMetals','diskGasMass','spheroidGasMass','hotHaloMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/metal,/p12OoverH

galaxyDataName='stellarMetallicity_12plusOoverH'
nodeDataName=['diskStellarMetals','spheroidStellarMetals','diskStellarMass','spheroidStellarMass']
galData_make,galaxyDataName,nodeDataName,iso,central,sate,/metal,/p12OoverH






h5f_close,fid0
h5f_close,fid1
print,'finish writting galaxy catalog data.'
end
;==================================================================
;==================================================================
