pro parameters
common cosmology,omega0,lambda0,h0,omegab,nspec,sigma8
common astronomy,ro_c0,dltc0,pc,Mpc,staryr,HC0,h0_inv,msun,gaussc,rsun,lsun
common unit_ex,kpcGyr2kms,kms2kpcGyr
common simulation,PhoenixUnit

;---cosmological parameters
omega0=0.25
lambda0=0.75
h0=0.73
nspec=1.
sigma8=0.9

;----the astronomical constant
ro_c0=2.77671e11             ;cosmic critic density, 3*HC0^2/(8*pi*G))=2.77671e11 h0^{2}M_{sun}Mpc^{-3}
;ro_c0=27.7671              ;cosmic critic density, 10^10 h0^{2}M_{sun}Mpc^{-3}
dltc0=200.
pc=3.08567802e16              ;1pc=3.08567802e16 m
Mpc=3.08567802e19              ;1pc=3.08567802e19 km
staryr=3.1558149984e7         ;1 star yr=3.1558149984e7 s
HC0=0.1022729843*h0              ;HC0=100 h0 km s^-{1}/Mpc=0.1022729843 h0 Gyr^{-1}
H0_inv=9.7777532/h0              ;H0^{-1}=9.7777532/h0 Gyr
msun=1.989e30                 ;msun=1.989e30 kg
gaussc=1.32712497e11       ;Gaussian constant, GM_sun=1.32712497e20m^3 s^{-2}=1.32712497e11km^3 s^{-2}=4.4986582e-6kpc^3 Gyr^{-2}
rsun=6.9600e8                 ;rsun=6.9600e8 m
lsun=3.846e26                 ;lsun=3.827e26 W, bolometric luminosity
;help,ro_c,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun

;----units exchange
kpcGyr2kms=0.97777532         ;1kpc/Gyr=0.97777532km/s
kms2kpcGyr=1.022729843        ;1km/s=1.022729843kpc/Gyr
;help,kpcGyr_kms,kms_kpcGyr

;--simulation parameters
PhoenixUnit={m:1e10,d:1e3,v:kms2kpcGyr}

end
