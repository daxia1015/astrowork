pro plot_pair_num,psdir
pairfile=!galacticus.pair[21]
ngroup=!galaxypair.ngroup
fid=h5f_open(pairfile)
n_series=h5read_dataset(fid,'n_series')
;help,n_series,n_series.npair
;help,n_series,n_series.npair,/struc
for iout=0,27 do print,iout,n_series[iout].nhalo,n_series[iout].totnpair,n_series[iout].totnhaloinpair

h5f_close,fid
pairname=['total','mC','mVC','MC','MVC']
device,filename=psdir+'pairNumber.ps',/color,/encapsul,xsize=10,ysize=8
plot,n_series.t,n_series.totnpair,/ylog,yrange=[2,2e4],$
     xtitle='t / Gyr',ytitle='Number of Pairs',$
     ytickname=['10','10!u2!n','10!u3!n','10!u4!n'],$
     position=[0.18,0.17,0.98,0.98]
for igroup=0,ngroup-1 do begin
  oplot,n_series.t,n_series.npair[igroup],color=!myct.c4[igroup],$
    	linestyle=igroup+1

endfor
labeling,0.05,0.4,0.12,0.09,pairname,ct=[!myct.black,!myct.c4],$
    	 linestyle=indgen(ngroup+1),/lineation
;==================================================================
device,filename=psdir+'pairFraction.ps',/color,/encapsul,xsize=10,ysize=8
plot,n_series.t,findgen(28)*0.01+1e-3,/nodata,yrange=[9e-3,0.99],/ylog,$
     xtitle='t / Gyr',ytitle='Pair Fraction'
nhalo=double(n_series.totnhaloinpair) ;& print,nhalo
nhalo[0]=1.
for igroup=0,ngroup-1 do begin
;  help,n_series.nhaloinpair[igroup],nhalo
  oplot,n_series.t,n_series.nhaloinpair[igroup]/nhalo,color=!myct.c4[igroup],$
    	linestyle=igroup+1

endfor
labeling,0.05,0.4,0.12,0.09,pairname[1:4],ct=!myct.c4,$
    	 linestyle=indgen(ngroup)+1,/lineation

end
;==================================================================
;==================================================================
pro plot_pair_kinetic,psdir



end
;==================================================================
;==================================================================
pro plotxy3_sub,ihost,nbin,ngroup,iout,data,_extra=_extra
x=data[0,ihost,iout].xh & y=data[0,ihost,iout].yh 
plot,x,y/y[nbin-1],/nodata,yrange=[1e-3,1.02],_extra=_extra
for i=0,ngroup-1 do begin
  x=data[i,ihost,iout].xh & y=data[i,ihost,iout].yh 
  oplot,x,y/y[nbin-1],color=!myct.c4[i]
endfor
;xyouts,xrange[0]*2,0.85,tname[iout]
multiplot
end
;==================================================================
;==================================================================
pro plotxy3,psfile,xtit,ytit,tname,nbin,ngroup,nout,data,$
    	    xrange=xrange,_extra=_extra
device,filename=psfile,/color,/encapsul,xsize=9.5,ysize=15
multiplot,[1,3],mxtitle=xtit,mytitle=ytit
for iout=0,nout-1 do $
  plotxy3_sub,1,tname,nbin,ngroup,iout,data,xrange=xrange,_extra=_extra

multiplot,/default
labeling,0.7,0.13,0.1,0.03,!galaxypair.name,ct=!myct.c4,/lineation
end
;==================================================================
;==================================================================
pro plot_pair_histogram,psdir,istepv,outputName0,tlist
isim=21
nbin=20
ngroup=!galaxypair.ngroup
outputName=outputName0[istepv]
nout=n_elements(outputName)
tname='t='+strtrim(string(tlist[istepv],format='(f5.2)'),2)
print,tname
pair_histogram,isim,nbin,outputName,h_sfr=h_sfr,h_stellar=h_stellar,h_gas=h_gas
;    	    ,h_disksize=h_disksize,h_spheroidsize=h_spheroidsize,h_BHmass=h_BHmass;,h_spheroidsfr=h_spheroidsfr,$
;	  h_disksfr=h_disksfr;,h_ro5=h_ro5,h_gr=h_gr

ytit='Cumulative Probability'
name=['sub','host']

;plotxy3,psdir+'pair_disksize.ps','R!ddisk!n / R!dvir!n',ytit,tname,nbin,ngroup,nout,$
;     	h_disksize,/xlog,xrange=[2e-4,5e-1]

;plotxy3,psdir+'pair_spheroidsize.ps','R!dspheroid!n / R!dvir!n',ytit,tname,nbin,ngroup,nout,$
;    	h_spheroidsize,/xlog,xrange=[2e-5,3e-1]
	
;plotxy3,psdir+'pair_BHmass.ps','M!dBH!n/ M!d*!n',ytit,tname,nbin,ngroup,nout,$
;    	h_BHmass,/xlog,xrange=[2e2,3e8]

for ihost=0,1 do begin
  device,filename=psdir+name[ihost]+'_sfr.ps',/color,/encapsul,xsize=16,ysize=14
  multiplot,[3,3],mytitle=ytit,myposition=[0.12,0.1,0.98,0.98]
  for iout=0,nout-1 do begin
    if iout eq 0 then xyouts,(!p.position[0]+!p.position[2])/2,0.02,'M!d*!n / M!dh!n',/normal,alignment=0.5
    plotxy3_sub,ihost,nbin,ngroup,iout,h_stellar,xrange=[2e-3,9e-1],/xlog
    if iout eq 0 then xyouts,(!p.position[0]+!p.position[2])/2,0.02,'M!dgas!n / M!d*!n',/normal,alignment=0.5
    plotxy3_sub,ihost,nbin,ngroup,iout,h_gas,xrange=[2e-6,9],/xlog
    if iout eq 0 then xyouts,(!p.position[0]+!p.position[2])/2,0.02,'sSFR / Gyr!u-1!n',/normal,alignment=0.5
    plotxy3_sub,ihost,nbin,ngroup,iout,h_sfr,xrange=[2e-6,9e-1],/xlog
  endfor
  multiplot,/default
endfor
labeling,0.9,0.13,0.05,0.03,!galaxypair.name,ct=!myct.c4,/lineation

;plotxy3,psdir+'pair_disksfr.ps','sSFR,disk / Gyr!u-1!n',ytit,tname,nbin,ngroup,nout,$
;     	h_disksfr,/xlog,xrange=[2e-4,5e-1]

;plotxy3,psdir+'pair_spheroidsfr.ps','sSFR,spheroid / Gyr!u-1!n',ytit,tname,nbin,ngroup,nout,$
;    	h_spheroidsfr,/xlog,xrange=[2e-5,3e-1]

;plotxy3,psdir+'pair_gr.ps','g-r',ytit,tname,nbin,ngroup,nout,$
;        h_gr,xrange=[0.56,0.9]

;plotxy3,psdir+'pair_ro5.ps','!mr!x!d5!n / Mpc!u-3!n',ytit,tname,nbin,ngroup,nout,$
;        h_ro5,/xlog,xrange=[0.2,9e2]
end
;==================================================================
;==================================================================
pro plot_pair_history,psdir,outputName,tlist
isim=21
ngroup=!galaxypair.ngroup
name=['sub','host']
pair_history,isim,outputName,dataName=dataName,h_sfr=h_sfr

device,filename=psdir+'pair_sfr_ev.ps',/color,/encapsul,xsize=10,ysize=16
multiplot,[1,2],mxtitle='t / Gyr',mytitle='sSFR / (Gyr!u-1!n)';,myposition=[0.12,0.1,0.98,0.98]
for ihost=0,1 do begin
  plot,tlist,h_sfr[*,0,ihost],/nodata,/ylog
  for igroup=0,ngroup-1 do begin
    oplot,tlist,h_sfr[*,igroup,ihost],color=!myct.c4[igroup]

  endfor
  multiplot
endfor
multiplot,/default

end
