function pair_distance,pos,sub,host
pos0=pos[0,sub]-pos[0,host]
pos1=pos[1,sub]-pos[1,host]
pos2=pos[2,sub]-pos[2,host]
d2=sqrt(pos0^2+pos1^2+pos2^2)
return,d2
end
;==================================================================
;==================================================================
function pair_energy,mhalo,velocity,zi,d,sub,host
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
common astronomy;,ro_c0,dltc0,pc,staryr,h0_inv,msun,gaussc,rsun,lsun
m1m2=mhalo[sub]*mhalo[host]
Relvel=pair_distance(velocity,sub,host)
KE=m1m2*Relvel^2/(mhalo[sub]+mhalo[host])/2.
ez2=ez(zi)
;HEE=0.0125*HC0^2*ez2*d^5*!pi*omega0*ro_c0*ez2
PE=-gaussc*m1m2/d+0.0125*HC0^2*ez2^2*d^5*!pi*omega0*ro_c0 
Etot=KE+PE
return,Etot
end
;==================================================================
;==================================================================
pro pair_match,mhalo,pos,velocity,zi,rvir,sn0,n0,sub,host,d,nok,$
    	       overRedshift=overRedshift,energy=energy
sub=-1L & host=-1L & d=-1. 
for i=0,n0-2 do begin
  sub0=sn0[i+1:n0-1] & host0=sn0[i]
  d0=pair_distance(pos,sub0,host0)  
  dR0=d0/(rvir[sub0]+rvir[host0])                ;criterion 3: distance
  if keyword_set(overRedshift) then begin
    d0=temporary(d0)/(1+zi)
    dR0=temporary(dR0)/(1+zi)
  endif  
  sn1=where((dR0 le 1.)and(d0 gt 0.),n1)
  if n1 eq 0 then continue
  if keyword_set(energy) then begin
    Etot=pair_energy(mhalo,velocity,zi,d0[sn1],sub0[sn1],host0)  ;criterion 4: energy
    sn2=where(Etot lt 0,n1)
    if n1 eq 0 then continue
    sn1=sn1[sn2]
  endif
  sub=[temporary(sub),sub0[sn1]] 
  host=[temporary(host),replicate(sn0[i],n1)]
  d=[temporary(d),d0[sn1]]
endfor
nok=n_elements(sub)-1
if nok eq 0 then return
;nsub=n_elements(sub) & 
nhost=n_elements(host)-1 
if nok ne nhost then message,'error'
sub=sub[1:nok] & host=host[1:nok] & d=d[1:nok]
end
;==================================================================
;==================================================================
pro pair_swap,mhalo,sub,host
msub=mhalo[sub] & mhost=mhalo[host]
sn=where(mhost lt msub,n)
if n ne 0 then begin
  temp=sub[sn]
  sub[sn]=host[sn]
  host[sn]=temp
endif
;mhalo=0 & msub=0 & sn=0
end
;==================================================================
;==================================================================
function pair_nhalo,sub,host,returnsn=returnsn
sn1=[sub,host]
sn2=uniq(sn1,sort(sn1))
nhaloinpair=n_elements(sn2)
if keyword_set(returnsn) then return,sn1[sn2]
return,nhaloinpair
end
;==================================================================
;==================================================================
function halo_pair_isim,isim,zlist
common simulation  ;,particlemass,unit
tree=assoctree(!treefile[isim],ntree,totnhalo,nhalos)
nhalo=nhalos[0] & print,'nhalo=',nhalo
halosn=lindgen(nhalo)
breaktree,!treefile[isim],tree,totnhalo,$
    	       mhalo,pos,velocity,snapnum, $
	       tag=['len','pos','vel','snapnum'],halosn=halosn
mhalo=temporary(mhalo)*particlemass[isim]
pos=temporary(pos)*unit.d
velocity=temporary(velocity)*unit.v
;help,mhalo,pos,velocity

step=2
mcrit=particlemass[isim]*100
n_series=replicate(!halopair.ni,!n_snap/step)
openw,lun,'halopair/data/temp',/get_lun
ilev=0
for isnap=1,!n_snap-1,step do begin
;  print,'isnap=',isnap
  zi=zlist[isnap]
  n_series[ilev].z=zi
  
  sn0=where((mhalo gt mcrit)and(snapnum eq isnap),n0)
  n_series[ilev].nhalo=n0
  if n0 lt 2 then continue
;  print,'n0=',n0
  rvir=r_virial(mhalo,zi)
  pair_match,mhalo,pos,velocity,zi,rvir,sn0,n0,sub,host,d,nok,/overRedshift
  if nok eq 0 then continue
  n_series[ilev].Npair=nok
  n_series[ilev].NhaloInPair=pair_nhalo(sub,host)

  dR=d/(rvir[sub]+rvir[host])
  Etot=pair_energy(mhalo,velocity,zi,d,sub,host)
  pair_swap,mhalo,sub,host  
  pair=replicate(!halopair.pair,nok)
  pair.sub=sub & pair.host=host
  pair.msub=mhalo[sub] & pair.mhost=mhalo[host]
  pair.d=d & pair.dR=dR & pair.E=Etot
  writeu,lun,pair

  print,isnap,n0,n_series[ilev].Nhaloinpair,n_series[ilev].Npair
  ilev++
endfor
free_lun,lun
return,n_series
end
;==================================================================
;==================================================================
pro halo_pair_write
zlist=snaplist(/redshift)
for isim=5,19,2 do begin
  print,!treefile[isim],isim
  n_series=halo_pair_isim(isim,zlist)
  openw,lun1,'halopair/data/'+!treename[isim],/get_lun
  openr,lun2,'halopair/data/temp',/get_lun
    writeu,lun1,n_elements(n_series),total(n_series.npair,/int)
    writeu,lun1,n_series
    copy_lun,lun2,lun1,/eof    
  free_lun,lun1,lun2
endfor
end
;==================================================================
;==================================================================
pro pair_read,fname,nlev,npairtot,n_series,pair,outputName=outputName
;group='g'+['0','1','2','3']
nlev=0L & npairtot=0LL
openr,lun,fname,/get_lun
  readu,lun,nlev,npairtot
  n_series=replicate(!halopair.ni,nlev)
  readu,lun,n_series
  nout=n_elements(outputName)
  if nout ne 0 then begin
    ns=n_series.npair ;& print,ns
    for iout=0,nout-1 do begin
      len=strlen(outputName[iout])
      ilev=fix(strmid(outputName[iout],6,len-6))-1
      skip=12+nlev*size_struct(!halopair.ni)
      if ilev gt 0 then skip=skip+total(ns[0:ilev-1],/int)*size_struct(!halopair.pair)
      point_lun,lun,skip
      pairi=replicate(!halopair.pair,ns[ilev])
;      print,iout,ilev,ns[ilev]
      readu,lun,pairi
      if iout eq 0 then pair=create_struct(outputName[iout],pairi) else $
        pair=create_struct(pair,outputName[iout],pairi)
    endfor
    if (nlev eq nout)and(eof(lun) eq 0) then message,'error'
  endif
free_lun,lun
;help,nlev,npairtot,n_series,pair

end


;  print,min(Etot),max(Etot)
;  sn3=where((d lt 1000)and(Etot lt 0),n3)
;  pair.KE=KE & pair.PE=PE

;  sn1=where(Etot lt 0,n1)
;  n_series[ilev].npair[1]=n1        ;d<1000kpc, E<0, m>1e10
;  n_series[ilev].nhaloinpair[1]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[1])and(Etot lt 0),n1)
;  n_series[ilev].npair[2]=n1        ;d<500kpc, E<0, m>1e10
;  if n1 eq 0 then n_series[ilev].nhaloinpair[2]=0 else $
;  n_series[ilev].nhaloinpair[2]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[2])and(Etot lt 0),n1)
;  n_series[ilev].npair[3]=n1        ;d<250kpc, E<0, m>1e10
;  if n1 eq 0 then n_series[ilev].nhaloinpair[3]=0 else $
;  n_series[ilev].nhaloinpair[3]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  
;  sn1=where((Etot lt 0)and(pair.msub gt !halopair.mcrit[1]),n1)
;  n_series[ilev].npair[4]=n1        ;d<1000kpc, E<0, m>1e11
;  if n1 eq 0 then n_series[ilev].nhaloinpair[4]=0 else $
;  n_series[ilev].nhaloinpair[4]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[1])and(Etot lt 0)and(pair.msub gt !halopair.mcrit[1]),n1)
;  n_series[ilev].npair[5]=n1        ;d<500kpc, E<0, m>1e11
;  if n1 eq 0 then n_series[ilev].nhaloinpair[5]=0 else $
;  n_series[ilev].nhaloinpair[5]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[2])and(Etot lt 0)and(pair.msub gt !halopair.mcrit[1]),n1)
;  n_series[ilev].npair[6]=n1        ;d<250kpc, E<0, m>1e11
;  if n1 eq 0 then n_series[ilev].nhaloinpair[6]=0 else $
;  n_series[ilev].nhaloinpair[6]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;    
;  sn1=where((Etot lt 0)and(pair.msub gt !halopair.mcrit[2]),n1)
;  n_series[ilev].npair[7]=n1        ;d<1000kpc, E<0, m>1e12
;  if n1 eq 0 then n_series[ilev].nhaloinpair[7]=0 else $
;  n_series[ilev].nhaloinpair[7]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[1])and(Etot lt 0)and(pair.msub gt !halopair.mcrit[2]),n1)
;  n_series[ilev].npair[8]=n1        ;d<500kpc, E<0, m>1e12
;  if n1 eq 0 then n_series[ilev].nhaloinpair[8]=0 else $
;  n_series[ilev].nhaloinpair[8]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;  sn1=where((d lt !halopair.dcrit[2])and(Etot lt 0)and(pair.msub gt !halopair.mcrit[2]),n1)
;  n_series[ilev].npair[9]=n1        ;d<250kpc, E<0, m>1e12
;  if n1 eq 0 then n_series[ilev].nhaloinpair[9]=0 else $
;  n_series[ilev].nhaloinpair[9]=pair_nhalo(pair[sn1].sub,pair[sn1].host)
;
