function doubleSchechter,lgL,phis,ab,lgLb,af,lgLf ;,log10=log10
h=0.7 & omega0=0.3 & lambda0=0.7
;lgLb=(Mb-4.67)/(-2.5)+2*alog10(h) ; h^-2 L_sun
;lgLf=(Mf-4.67)/(-2.5)+2*alog10(h) ; h^-2 L_sun
b=SchechterFunction(lgL,phis,ab,lgLb)
f=SchechterFunction(lgL,phis,af,lgLf)
phi=b+f
return,phi
end
;====================================================================
;====================================================================
function LF_Popesso2006,lgL,phis,iband=iband,ic=ic
;iband: g r i z
;ic: all early late
h=0.7 & omega0=0.3 & lambda0=0.7

p=fltarr(8,10)
openr,lun,'SMF/data/Popesso2006.txt',/get_lun
skip_lun,lun,2,/lines
readf,lun,p,format='(f8.2,f6.2,f8.2,f6.2,f8.2,f6.2,f8.2,f6.2)'
free_lun,lun
for row=1,9,2 do begin
  for col=0,6,2 do begin
    p[col,row]=(p[col,row]-4.67)/(-2.5)+2*alog10(h) ; h^-2 L_sun
    p[col+1,row]=p[col+1,row]/(-2.5)
  endfor
endfor

if n_elements(lgL) eq 0 then begin
  low=7.8d & high=11.0d & nbin=17
  dbin=(high-low)/(nbin-1)
  lgL=findgen(nbin)*dbin+low
;  print,lgL
endif else lgL=double(lgL)
if n_elements(phis) eq 0 then phis=1.6

icol=iband*2
p0=p[icol,*] & pe=p[icol+1,*]
case ic of 
  0:begin
    y  =doubleSchechter(lgL,phis,p0[0],p0[1],p0[2],p0[3])
    up =doubleSchechter(lgL,phis,p0[0]-pe[0],p0[1]+pe[1],p0[2]-pe[2],p0[3]+pe[3])
    low=doubleSchechter(lgL,phis,p0[0]+pe[0],p0[1]-pe[1],p0[2]+pe[2],p0[3]-pe[3])
  end
  1:begin
    y  =doubleSchechter(lgL,phis,p0[4],p0[5],p0[6],p0[7])
    up =doubleSchechter(lgL,phis,p0[4]-pe[4],p0[5]+pe[5],p0[6]-pe[6],p0[7]+pe[7])
    low=doubleSchechter(lgL,phis,p0[4]+pe[4],p0[5]-pe[5],p0[6]+pe[6],p0[7]-pe[7])
  end
  2:begin
    y  =SchechterFunction(lgL,phis,p0[8],p0[9])
    up =SchechterFunction(lgL,phis,p0[8]-pe[8],p0[9]+pe[9])
    low=SchechterFunction(lgL,phis,p0[8]+pe[8],p0[9]-pe[9])
  end
endcase
popesso={lgL:lgL,phi:y,up:up,low:low}
return,popesso
end
;====================================================================
;====================================================================
function SMF_Popesso2006,lgm,phis
;z-band luminosity function, Popesso et al. 2006
;M/L transfer: Baldry et al. 2008
if n_elements(lgm) eq 0 then begin 
  mlow=8.0 & mhigh=11.5 & nbin=16
  dbin=(mhigh-mlow)/(nbin-1)
  lgm=findgen(nbin)*dbin+mlow
endif
lgLz=lgm-0.2  ;h^-2 L_sun, h^-2 M_sun
popL=LF_Popesso2006(lgLz,phis,iband=1,ic=0)
pop={lgm:lgm,phi:popL.phi,up:popL.up,low:popL.low}
return,pop
end
;====================================================================
;====================================================================
