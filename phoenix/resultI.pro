pro ResultI_Write
common ResultBlockI,nout,OutputName,tlist,zlist,weight0
OutputList,!galacticus.treeEv[3],nout,OutputName=OutputName,tlist=tlist,zlist=zlist
fidVW=h5f_open(!galacticus.path+'treeProperties.hdf5')
weight0=h5read_dataset(fidVW,'VolumeWeight')
h5f_close,fidVW

;for isim=0,19 do begin
;  level=strmid(!Phoenix.TreeName[isim],3,1)
;  w0=weight0[3,isim]
;  if level eq '4' then w1=min(Weight0[*,isim]) else W1=max(Weight0[*,isim])
;  w2=max(weight0[*,isim])*5
;  print,!Phoenix.treename[isim],w0,w1,w2,format='(a4,3e15.6e3)'
;endfor
;!except=2
;treeProperties_write
;SelfContained_write
;haloDistribution,weight0
;galaxyDistribution
;==================================
;globalHistory_write
;Constraint_write,fidr
smf_write
;MassiveBright_write
;lf_write,fidr
;stellarHalo_write,fidr
;galaxyExample_write,fidr

;==================================
end
;====================================================================
;==================================================================
;====================================================================
;==================================================================
