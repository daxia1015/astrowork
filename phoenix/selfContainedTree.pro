function SelfContainedIndex,gid,nhalo,tree
index=lindgen(nhalo)
sn1=where((tree.firsthaloinfofgroup eq index)and(tree.descendant eq -1)and(tree.SnapNum lt !Phoenix.n_snap-1),ntree)
h5write_attribute,gid,ntree,'IsolatedTreeNum'
h5write_dataset,gid,sn1,'IsolatedTreeIndex'
h5write_dataset,gid,tree[sn1].SnapNum,'IsolatedBeginSnap'

progenitor=TreeHostProgenitor(tree,sn1,nstep)
h5write_dataset,gid,progenitor,'IsolatedProgenitor'
h5write_dataset,gid,nstep,'NstepBeforeIsolated'
;print,ntree
;==================================================================
sn1=where((tree.firsthaloinfofgroup eq index)and(tree.descendant eq -1)and(tree.SnapNum eq !Phoenix.n_snap-1),ntree)
h5write_attribute,gid,ntree,'CentralTreeNum'
h5write_dataset,gid,sn1,'CentralTreeIndex'

progenitor=TreeHostProgenitor(tree,sn1,nstep)
h5write_dataset,gid,progenitor,'CentralProgenitor'
h5write_dataset,gid,nstep,'NstepInCentral'

;print,ntree
return,sn1
end
;==================================================================
;==================================================================
pro SelfContainedMatch,IndexToMatch,host,treeID
match2,IndexToMatch,host,sub,sn1
sn1=where((sub ne -1)and(treeID eq -1),n1)
if n1 ne 0 then treeID[sn1]=treeID[host[sub[sn1]]]
end
;==================================================================
;==================================================================
pro SelfContainedID,gid,nhalo,Index,tree

treeID=lonarr(nhalo)-1
followed=intarr(nhalo)
treeID[Index]=Index

while 1 do begin
  hosthalo=where((treeID ne -1)and(followed eq 0),nhost)
;  if nhost lt 1000 then print,'nhost=',nhost
  if nhost eq 0 then break else begin
    SelfContainedMatch,tree.firsthaloinfofgroup,hosthalo,treeID
    SelfContainedMatch,tree.descendant,hosthalo,treeID
    followed[hosthalo]=1
  endelse
endwhile
h5write_dataset,gid,treeID,'TreeID',GroupName=Namei
end
;==================================================================
;==================================================================
pro SelfContainedWeight,isim,tree,zlist,weight
common cosmology;,omega0,lambda0,h0,omegab,nspec,sigma8
x0=max(tree.pos[0])-min(tree.pos[0])
x1=max(tree.pos[1])-min(tree.pos[1])
x2=max(tree.pos[2])-min(tree.pos[2])
;V200=4.0*!pi*!PhoenixTable.R200[isim]^3/3.0
;V500=4.0*!pi*!Phoenix.R500[isim]^3/3.0
center=tree[0].pos
R=pair_distance(tree.pos,center)
Rmax=max(R)
VC=4.0*!pi*Rmax^3/3.0 
;R=pair_distance(tree.pos,center,zlist=zlist[tree.snapNum])
;print,x0,x1,x2,Rmax
weight[*,isim]=1.0/[x0^3,x1^3,x2^3,x0*x1*x2,VC]
print,weight[*,isim]
if isim mod 2 eq 0 then print,max(weight[*,isim]) else print,min(weight[*,isim])
end
;==================================================================
;==================================================================
pro mah0,isim,gid,tree,zlist
host=0
hostBranch=TreeHostBranch(tree,host,nstep)
mah=replicate({z:0.,mhalo:0.},nstep)
mah.z=zlist[hostBranch.snap]
mah.mhalo=tree[hostBranch.host].len*!PhoenixTable.mp[isim]
;print,mah.z,mah.mhalo
h5write_attribute,gid,nstep,'MainBranchNstep'
h5write_dataset,gid,mah,'MainBranchMAH'
end
;==================================================================
;==================================================================
pro SelfContained_write
PhoenixTable
zlist=snaplist(/redshift)
fid=h5f_create(!galacticus.path+'SelfContainedTree.hdf5')
h5write_group,fid,!Phoenix.TreeName
weight=fltarr(5,!Phoenix.nsim)
for isim=1,19 do begin
  fname=!Phoenix.TreeFile[isim] & print,'isim=',isim
  tree=readtree_ph(fname,ntree,totnhalo,nhalos,itree=0)
  nhalo=nhalos[0]
  gid=h5g_open(fid,!Phoenix.TreeName[isim])
;  treeIndex=SelfContainedIndex(gid,nhalo,tree)
  ;  SelfContainedID,gid,nhalo,treeIndex,tree
  SelfContainedWeight,isim,tree,zlist,weight
;  mah0,isim,gid,tree,zlist

  h5g_close,gid
endfor
h5write_dataset,fid,weight,'VolumeWeight'
h5f_close,fid
end
;====================================================================
;====================================================================
;====================================================================
