;======++++++===+++======+++++++======++++++==+++======+++++++++
function fepson,es,zentner=zentner,jiang=jiang
a=2.22
case 1 of
  keyword_set(zenter): f=gamma(2*a)*es^(a-1)*(1-es)^(a-1)/gamma(a)^2   ;probability density profile provided by Zenter
  keyword_set(jiang):  f=2.77*es^1.19*(1.55-es)^2.99  ;probability density profile provided by Jiang
  else: f=2.77*es^1.19*(1.55-es)^2.99     ;message,'keyword /zenter or /jiang should be setted!'
endcase
return,f
end
;~!@#$$%^&*()__+-====-099876543221``~!@@##$$%%^^&&**((())___=+/*---+.
;=====================================================================
pro plot_orbit_para
isim=1
tra=twobody_read(!twobody.fnametra[isim],ngroup,totnstep,nsteps,IDs)
@include/twobody_nsteps.pro
epson=tra[step0].epson & print,min(epson),max(epson)
yita=tra[step0].yita & print,min(yita),max(yita)
nbin=11
device,filename='twobody/epson.ps',/color,/encapsul,xsize=10,ysize=8
h=histo(epson,0.05,0.95,nbin,locations=ex)
;print,ex,h
plot,ex,h*nbin/total(h,/double),xtitle='!me!x',ytitle='f(!me!x)'
;oplot,ex,fepson(ex,/zentner),linestyle=1
oplot,ex,fepson(ex,/jiang),linestyle=2
;=====================================================================
device,filename='twobody/yita.ps',/color,/encapsul,xsize=10,ysize=8
h=histo(yita,0.3,1,nbin,locations=yx)
;print,yx,h
plot,yx,h*nbin/total(h,/double),xtitle='!mh!x',ytitle='f(!mh!x)'





end
