pro plot_halopair
isimv=[19,15,3];['phi4','phg4','phb4']
ngroup=4
pairfile='halopair/data/'+!treename
device,filename='halopair/npair.ps',/color,/encapsul,xsize=16,ysize=11
multiplot,[2,2],xgap=0.04,ygap=0.03,/doxaxis,/doyaxis,$
mxtitle='t / Gyr',mytitle='Number of Pairs'

for igroup=0,3 do begin
  isim=isimv[0]
  pair_group,pairfile[isim],ngroup,n_series,pairNumber=pairNumber
  y=pairNumber.group[*,igroup].npair
  sn=uniq(y,sort(y))
  plot,tGy(n_series.z),y,/ylog,yrange=[y[sn[1]],max(y)*1.1],/nodata;  xrange=[6,13.6]
  oplot,tGy(n_series.z),y,color=!myct.c3[0]
  for i=1,2 do begin
    isim=isimv[i]
    pair_group,pairfile[isim],ngroup,n_series,pairNumber=pairNumber
    y1=pairNumber.group[*,igroup].npair    
    oplot,tGy(n_series.z),y1,color=!myct.c3[i]
  endfor
  xyouts,12,y[sn[1]]*1.5,!halopair.name[igroup],alignment=1
  multiplot,/doxaxis,/doyaxis
endfor
multiplot,/default
labeling,0.25,0.75,0.05,0.05,!treename[isimv],ct=!myct.c3,/lineation
;print,n_series.npair,' ',pairNumber.tot.npair
;================================================================
;================================================================
device,filename='halopair/pairFraction.ps',/color,/encapsul,xsize=16,ysize=10
multiplot,[2,2],xgap=0.05,ygap=0.03,/doxaxis,/doyaxis,$
mxtitle='t / Gyr',mytitle='Pair Fraction'

for igroup=0,3 do begin
  isim=isimv[0]
  pair_group,pairfile[isim],ngroup,n_series,pairNumber=pairNumber
  nhalo=double(pairNumber.tot.nhaloinpair)
  sn1=where(nhalo gt 0,n1) ;& print,n1
  y=double(pairNumber.group[*,igroup].nhaloinpair)
  y[sn1]=y[sn1]/nhalo[sn1]
  sn=uniq(y,sort(y))
  plot,tGy(n_series.z),y,/ylog,yrange=[y[sn[1]],max(y)*1.2],/nodata;  xrange=[6,13.6]
  oplot,tGy(n_series.z),y,color=!myct.c3[0]
  for i=1,2 do begin
    isim=isimv[i]
    pair_group,pairfile[isim],ngroup,n_series,pairNumber=pairNumber
    nhalo=double(pairNumber.tot.nhaloinpair)
    sn1=where(nhalo gt 0,n1) ;& print,n1
    y1=double(pairNumber.group[*,igroup].nhaloinpair)
    y1[sn1]=y1[sn1]/nhalo[sn1]
    oplot,tGy(n_series.z),y1,color=!myct.c3[i]
;    help,nhalo,n_series.z,n_series.nhaloinpair[igroup]
  endfor
  xyouts,12,y[sn[1]]*1.5,!halopair.name[igroup],alignment=1
  multiplot,/doxaxis,/doyaxis
endfor
multiplot,/default
labeling,0.3,0.7,0.06,0.05,!treename[isimv],ct=!myct.c3,/lineation
;================================================================
;================================================================
  
end
;device,filename='halopair/MAH.ps',/color,/encapsul,xsize=10,ysize=8
;plot,findgen(100)*0.1,findgen(100)*0.01,/ylog,xrange=[1,13.58],yrange=[1e-2,1.1],/nodata
;for i=0,2 do begin
;  isim=isimv[i] 
;  MAH=Galacticus_MAH(!galacticus.treeEv[isim])
;  oplot,MAH.nodeTime,MAH.nodeMass/MAH.nodeMass[0],color=!myct.c3[i]
;endfor
