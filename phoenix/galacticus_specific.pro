function specific,outputName,nodeData,dataTag,scaleTag,scale=scale
nout=n_elements(outputName)
ndata=n_elements(dataTag)
nscale=n_elements(scaleTag)
if keyword_set(scale) and (nscale eq 0) then message,'error'
tagname=tag_names(nodeData.(0))
dataTag=strupcase(dataTag)
;print,tagname
for iout=0,nout-1 do begin
  data1=0.
  for i=0,ndata-1 do begin
    itag=where(tagname eq dataTag[i])
    data1=data1+nodeData.(iout).(itag)
  endfor
  if keyword_set(scale) then begin
    base=0.
    for i=0,nscale-1 do base=base+nodeData.(iout).(scaleTag[i])
    sn=where(base gt 0)
    data1[sn]=data1[sn]/base[sn]
  endif
  if iout eq 0 then data=create_struct(outputName[iout],data1) else $
    data=create_struct(data,outputName[iout],data1)
endfor 
return,data
end
;==================================================================
;==================================================================
pro Galacticus_specific,fname,outputName,scale=scale,$
    sfr=sfr,stellar=stellar,gas=gas,BHmass=BHmass,outflowMass=outflowMass,$
    metal=metal,disksize=disksize,spheroidsize=spheroidsize,disksfr=disksfr,$
    spheroidsfr=spheroidsfr,BHAccRate=BHAccRate,BHfeedback=BHfeedback,$
    coolingRate=coolingRate,BoundMass=BoundMass
dataname=['diskStellarMass','spheroidStellarMass','nodeMass','nodeVirialRadius']
stellarName=dataName[0:1]
if arg_present(sfr) then begin
  sfrName=['diskStarFormationRate','spheroidStarFormationRate']
  dataName=[dataname,sfrName]
endif	 
if arg_present(gas) then begin
  gasName=['diskGasMass','spheroidGasMass']
  dataName=[dataname,gasName]
endif
if arg_present(BHmass) then begin
  BHname='blackHoleMass'
  dataName=[dataname,BHname]
endif
if arg_present(outflowMass) then begin
  outflowName='outflowedMass'
  dataName=[dataname,outflowName]
endif
if arg_present(disksize) then begin
  disksizename='diskScaleLength'
  dataName=[dataname,disksizeName]  
endif
if arg_present(spheroidsize) then begin
  spheroidsizename='spheroidScaleLength'
  dataName=[dataname,spheroidsizeName]  
endif
if arg_present(disksfr) then begin
  disksfrname='diskStarFormationRate'
endif
if arg_present(spheroidsfr) then begin
  spheroidsfrname='spheroidStarFormationRate'
endif
if arg_present(BHAccRate) then begin
  BHAccRateName='blackHoleAccretionRate'
  dataName=[dataname,BHAccRateName]    
endif
if arg_present(BHfeedback) then begin
  BHfeedbackName='blackHoleRadiativeEfficiency'
  dataName=[dataname,BHfeedbackName]    
endif
if arg_present(coolingRate) then begin
  coolingRateName='hotHaloCoolingRate'
  dataName=[dataname,coolingRateName]    
endif
if arg_present(BoundMass) then begin
  BoundMassName='nodeBoundMass'
  dataName=[dataname,BoundMassName]    
endif

Galacticus_Outputs,fname,outputName=outputName,nodeData=nodeData,dataname=dataname

if arg_present(stellar) then stellar=specific(outputName,nodeData,stellarName,2,scale=scale)  
if arg_present(sfr) then sfr=specific(outputName,nodeData,sfrName,[0,1],scale=scale)	 
if arg_present(gas) then  gas=specific(outputName,nodeData,gasName,[0,1],scale=scale)	 
if arg_present(BHmass) then  BHmass=specific(outputName,nodeData,BHname,[0,1],scale=scale)	 
if arg_present(outflowMass) then outflowMass=specific(outputName,nodeData,outflowName,[0,1],scale=scale)	 
if arg_present(disksize) then disksize=specific(outputName,nodeData,disksizename,[3],scale=scale)	 
if arg_present(spheroidsize) then spheroidsize=specific(outputName,nodeData,spheroidsizename,[3],scale=scale)	 
if arg_present(disksfr) then disksfr=specific(outputName,nodeData,disksfrname,0,scale=scale)	 
if arg_present(spheroidsfr) then spheroidsfr=specific(outputName,nodeData,spheroidsfrname,1,scale=scale)	 
if arg_present(BHAccRate) then BHAccRate=specific(outputName,nodeData,BHAccRatename,[0,1],scale=scale)
if arg_present(BHfeedback) then BHfeedback=specific(outputName,nodeData,BHfeedbackname,scale=0)
if arg_present(coolingRate) then coolingRate=specific(outputName,nodeData,coolingRateName,[0,1],scale=scale)
if arg_present(BoundMass) then BoundMass=specific(outputName,nodeData,BoundMassName,2,scale=scale)


end
;==================================================================
;==================================================================
;pro Galacticus_ScaleLength,fname,outputName,scale=scale,$
;    disk=disk,spheroid=spheroid

;dataname=['diskScaleLength','spheroidScaleLength','nodeVirialRadius']


;if keyword_set(specific) then Galacticus_Outputs,fname,nodedata=r0,outputName=outputName,dataname='nodeVirialRadius'

;if arg_present(disk) then begin
;  Galacticus_Outputs,fname,nodedata=disk,outputName=outputName,$
;  dataname='diskScaleLength'
;  if keyword_set(specific) then $
;  for iout=0,nout-1 do disk.(iout)=disk.(iout)/r0.(iout)
;endif

;if arg_present(spheroid) then begin
;  Galacticus_Outputs,fname,nodedata=spheroid,outputName=outputName,$
;  dataname='spheroidScaleLength'
;  if keyword_set(specific) then $
;  for iout=0,nout-1 do spheroid.(iout)=spheroid.(iout)/r0.(iout)
;endif
;end
;==================================================================
;==================================================================
