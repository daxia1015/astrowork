;====================================================================
;====================================================================
pro plot_SMF
;@include/stellarMassFunc.pro
dir0='SMF/'
Msun='M'+sunsymbol()
xtit='log!d10!n[M!d!m*!x!n / (h!u-2!n'+Msun+')]'
ytit='!mf!x(M!d!m*!x!n) / (h!u3!nMpc!u-3!nlg!u-1!nM!d!m*!x!n)'
xr=[7.01,12.2] & yr=[1.01e-9,0.99]
iv2=[1,indgen(8)*2+4]
iv4=indgen(9)*2+3

li2009=SMF_li2009()
wings=SMF_wings()
guo2011=SMF_guo2011()
stellarMassFunc_Read,lgm,phiAll,phiR200,mom_All,mom_R200

device,filename=dir0+'SMF_all.eps',/color,/encapsul,xsize=17,ysize=15
multiplot_Gan,[3,3],mxtitle=xtit,mytitle=ytit

plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,psym=5,/ylog
errplot,li2009.lgm,li2009.low,li2009.up,width=0.02
for isim=1,3 do begin
  oplot,lgm,phiAll[*,isim],color=!myct.c3[isim-1],thick=3
  oplot,lgm,phiR200[*,isim],color=!myct.c3[isim-1],thick=3,linestyle=2
endfor
oplot,lgm,phiAll[*,20],color=!myct.cyan,thick=3
labeling,0.1,0.5,0.15,0.1,['Li2009','Field','PhA2','PhA3','PhA4'],$
    	 /lineation,ct=[!myct.black,!myct.cyan,!myct.c3],$
	 linestyle=[1,0,0,0,0]

multiplot_Gan

i=4
for ifig=1,8 do begin
  plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,psym=5,/ylog
  errplot,li2009.lgm,li2009.low,li2009.up,width=0.02
  oplot,wings.lgm,wings.phi,psym=4
  errplot,wings.lgm,wings.low,wings.up,width=0.02
  for isim=i,i+1 do begin
    oplot,lgm,phiAll[*,isim],color=!myct.c2[isim-i],thick=3
    oplot,lgm,phiR200[*,isim],color=!myct.c2[isim-i],thick=3,linestyle=2
  endfor
  labeling,0.1,0.35,0.15,0.1,['<R!d200!n',!Phoenix.treeName[i:i+1]],$
    	 /lineation,ct=[!myct.black,!myct.c2],$
	 linestyle=[2,0,0]
  i=i+2
  multiplot_Gan
endfor
multiplot_Gan,/default
;====================================================================

device,filename='paperI/SMF.eps',/color,/encapsul,xsize=18,ysize=7
multiplot_Gan,[3,1],mxtitle=xtit,mytitle=ytit
plot,li2009.lgm,li2009.phi,psym=5,/ylog
errplot,li2009.lgm,li2009.low,li2009.up,width=0.02
for isim=1,3 do begin
  oplot,lgm,phiAll[*,isim],color=!myct.c3[isim-1],thick=3
endfor
oplot,lgm,phiAll[*,20],color=!myct.cyan,thick=3
oplot,[8,8],[1e-4,1],linestyle=1
labeling,0.1,0.5,0.15,0.1,['Li2009','PhA2','PhA3','PhA4','Field Sample'],$
    	 /lineation,ct=[!myct.black,!myct.c3,!myct.cyan],$
	 linestyle=[1,0,0,0,0]
multiplot_Gan

plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,psym=5,/ylog
errplot,li2009.lgm,li2009.low,li2009.up,width=0.02
for i=0,1 do begin
  oplot,lgm,mom_All[*,i].ave,color=!myct.c2[i],thick=3
  errplot,lgm,mom_All[*,i].low,mom_All[*,i].up,color=!myct.c2[i],thick=3,width=0.02
endfor
oplot,[8,8],[1e-4,1],linestyle=1

labeling,0.1,0.4,0.15,0.1,['Li2009','Ph2','Ph4'],/lineation,$
    	 ct=[!myct.black,!myct.c2],linestyle=[1,0,0]
;xyouts,xr[0]+0.3,yr[0]*2.5,'whole cluster'
xyouts,0.1,0.06,'whole cluster(40-50Mpc)'
multiplot_Gan

;plot,li2009.lgm,li2009.phi,xrange=xr,yrange=yr,linestyle=1,thick=5
plot,wings.lgm,wings.phi,xrange=xr,yrange=yr,psym=4,/ylog
errplot,wings.lgm,wings.low,wings.up,width=0.02
;oplot,wings.lgm,wings.phi,linestyle=2,thick=5
oplot,guo2011.lgm,guo2011.phi,linestyle=2,thick=5
;plot,wings.lgm,wings.phi,xrange=xr,yrange=yr,linestyle=2,thick=5
for i=0,1 do begin
  oplot,lgm,mom_R200[*,i].ave,color=!myct.c2[i],thick=3
  errplot,lgm,mom_R200[*,i].low,mom_R200[*,i].up,color=!myct.c2[i],thick=1,width=0.02
;  shadowing,lgm,mom_R200[*,i].low,mom_R200[*,i].up,color=!myct.c2[i],thick=1,density=2
endfor
oplot,[9.8,9.8],[1e-4,1],linestyle=1
labeling,0.1,0.5,0.15,0.1,['Vulcani2011','Guo2011','Ph2','Ph4'],/lineation,$
    	 ct=[!myct.black,!myct.black,!myct.c2],linestyle=[2,2,0,0]
xyouts,0.1,0.06,'within R!d200!n (1.4-2.2Mpc)'
multiplot_Gan,/default

end
