pro refine_bbl  ;refine to author-year mode.
source='pair/bbl0.tex'
target='pair/pair.bbl'
;copy = file0+'2.tex'
str='' & key0='' & key=''
nline=0L  & nkey=0L  & en=0
openr,lun0,source,/get_lun
openw,lun1,target,/get_lun
;openw,lun2,copy,/get_lun
  while eof(lun0) eq 0 do begin
    readf,lun0,str
    str_copy=str
    p0=strpos(str,'\bibitem')
    if p0 eq 0 then begin
      en=0
      p1=strpos(str,'[',strlen('\bibitem'))
      p2=strpos(str,']',p1+6)
      p3=strpos(str,'{',p2) 
      p4=strpos(str,'}',p3+4)
      p5=strpos(str,',',p4+1)
      len=strlen(str)
      str0=strmid(str,0,p3+1) & str1=strmid(str,p4,len-p4)
      key0i=strmid(str,p3+1,p4-p3-1)
;      key0=[key0,key0i]
      author=strmid(str,p4+1,p5-p4-1) ;& print,author
      author=strlowcase(author) ;& print,author
      bauthor=byte(author)
      bauthor=bauthor[where((bauthor ge 97)and(bauthor le 122))]
      author=string(bauthor)    
      ay=strmid(str,p1+1,p2-p1-1)
      p11=strpos(ay,'(',3) & p12=strpos(ay,')',p11+1)
      year=strmid(ay,p11+1,p12-p11-1)
;      year=strmid(year,2,strlen(year)-2)
      keyi=author+year
;      key=[key,keyi]
      str=str0+keyi+str1
      nkey++
      print,key0i,keyi,format='(2a19)'
    endif else begin
      if p0 eq 1 then en=1    
    endelse
    if en then continue
    nline++
    printf,lun1,str
;    printf,lun2,str_copy
  endwhile
free_lun,lun0,lun1
print,nline,nkey

end
