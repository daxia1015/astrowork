\documentclass[useAMS,usenatbib]{mn2e}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{texnansi}
\usepackage{afterpage}
\usepackage{placeins}
\input{macros.tex}



\newcommand{\gcs}{{\it Galacticus}~}


\title[GSMFs in Clusters]
{Characterizing the Evolution of Galaxy Stellar Mass Functions in Clusters}

\author[Gan et al.]
       {Jianling Gan$^{1}$\thanks{E-mail: jlgan@shao.ac.cn} \\
  $^1$ Key Laboratory for Research in Galaxies and Cosmology,
       Shanghai Astronomical Observatory,\\
       Chinese Academy of Sciences, 80 Nandan RD, Shanghai, 200030, China\\
       }
%%  $^3$ The Purple Mountain Observatory, Chinese Academy of Sciences, \\ 
%%       2 West Beijing Road, Nanjing, 210008, China\\
%%  $^5$ National Astronomical Observatory, Chinese Academy of Sciences }

\begin{document}
\date{Accepted 2011 July 15. Received 2011 July 14; in original
form 2011 October 11}

\pagerange{\pageref{firstpage}--\pageref{lastpage}} \pubyear{2002}

\maketitle

\label{firstpage}

\begin{abstract}
Using a semi-analytical model of Galacticus and the dark matter 
halo merger trees output by Phoenix simulation, we study the various
effects of galaxy interaction in the rich galaxy cluster. The main 
physical process about the galaxy interaction are the ram pressure 
stripping and tidal stripping on the satellite galaxies when they 
travel in the galaxy cluster. In the results, we select a large 
sample of isolated, central and satellite from the modeling 
clusters and compare the probability distribution of star formation 
rate (SFR), color and morphology of different galaxy group. We find 
that the central galaxies strip gas from their satellite and the SFR 
increase remarkably at the early stage, while at the later stage, 
the SFR decrease as there are strong feedback. We find that the star 
formation in satellite are strongly suppressed by gas stripping. The 
satellite life time, which are determined by their formation time and 
the dynamical friction, also play an important role in the evolution 
of satellite SFR. Those satellites have shorter life time will be 
less affected by the gas stripping and have higher SFR. In the group 
environment of galaxy cluster, the galaxies are moving toward the 
cluster and the gas stripping tend to be more efficiently, which 
increases the star formation of central galaxies and decrease that 
of satellite galaxies, respectively.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{keywords}
methods: N-body simulations --- 
galaxies: haloes --- 
galaxies: mass function --- 
cosmology: dark matter
\end{keywords}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\label{sec:intro}
The observations by Sloan Digital Sky Survey (SDSS) showed the
enhancement of star formation induced by close galaxy encounter
\citep{patton2010}
and the high fraction of active galactic nuclei (AGN) triggered by galaxy
mergers \citep{ellison2011}.  
It's found that the galaxies within the group environment tend to have
lower star formation rate and be redder than the field galaxies
\citep{alonso2012}. It's also found that the fraction of late type galaxies
decreases with the local density of where the galaxies stay
\citep{park2009}. 
The problem is that what kind of physical process leads to the galaxy 
properties as observed?


In the hierarchical scenario of galaxy formation, the interactions or
mergers between galaxies (and their associated dark matter haloes) are 
frequent, and they play an important role in 
the star formation, color, morphology and nuclear activity of the galaxies.
The mechanisms between the interacting galaxies, especially the cluster 
galaxies, contain many physical processes and are complicated, For
example, the high-speed encounter, harassment, tidal interaction, ram-pressure stripping,
dynamical friction, cannibalism and strangulation etc.\citep{park2009,mo2010}. 
This bring us
some questions that how do these processes act on galaxies and how can we 
investigate and interpret them.


A galaxy encounter with another galaxy in high speed, which is higher
than its internal velocity dispersion, can be impulsively heated and become
expanded\citep{binney2008}. The cluster galaxies usually have high speed, and the
cumulative effect of mutiple high-speed encounters is refered to as galaxy
harassment\citep{mo2010}. The harassment may have an strong impact on the
morphology of late-type (Sc-Sd) galaxy that transforms the disk
into speroidal component, when the galaxy disk has relatively low surface
density. For the compact early-type (Sa-Sb) disk galaxies, they can be
significantly heated and become more easily to be stripped by the tidal force or
ram-pressure. 

When a satellite halo moves in a bigger galactic halo, its mass outside
a critical radius, which is called the tidal radius, will become unbound and
be stripped gradually\citep{taylor2001,gan2010}. The satellite haloes may 
lose a large amount of mass due to tidal stripping at early time, 
so they cannot accrete enough gas for further star formation. 




The effects of galaxy interaction are  widely studied by theorist
using simulations or semi-analytical model
\citep[e.g.,][]{font2008,book2010,tonnesen2012}.  



%ram pressure stripping, tidal force stripping, orbital evolution, 
%galaxy merger (the remant size and the formation of supper massive
%black hole)
Fist of all, what kind of physical processes can happen during the 
galaxies interaction?
The main reason is that the gas in the satellite galxies is
removed and the star formation within them is quenched.



The possible mechanisms for the gas loss are the ram pressure
stripping and tidal force stripping from the galaxies as they 
interact with the 
intracluster medium (ICM). 


%galaxy mass, pair distance, orbital energy (bound or unbound), 
%orbital eccentricity, local environment (cluster-centric radius)
The other issue is which quantities can determine the strength of galaxies 
interaction and bring out the diversity of observed phenomena.



In this paper, we apply the semi-analytical model of \gcs
\citep{benson2012} to the
evolutionary track of dark matter haloes in 9 clusters as output by
the Phoenix simulation \citep{gao2012}. By doing this, we can obtain a
large catalog of evolving galaxies and
investigate the properties of interacting galaxies in the diverse 
and complex circumstance. 
The model ingredients include the ram pressure
stripping, tidal force stripping, galaxies merger and the formation of
SMBH (Section~\ref{sec:model}). We then extract a large sample of galaxy 
pairs and isolated galaxies associated with their mass, orbital
properties and cluster-centric radial position from the modeling clusters
(Section~\ref{sec:pair}). In the results (Section~\ref{sec:result}), 
we compared the star formation, color, morphology and nulear activity of
galaxies in the individual group and  the different group of pairs.
Finally, we conclude our model and discuss some open problems
(Section~\ref{sec:conc}).


\section{Model}
\label{sec:model}
In order to investigate the effects of galaxy interaction in clusters,
we make use of the dark matter halo merger trees output by the Phoenix 
simulation \citep{gao2012} and the \gcs model \citep{benson2012} 
for modelling the galaxy formation and their interaction. Such model 
does not require heavy compuation and allow us to study the physical 
process in more detail. Below we describe the simulation 
and model.


\subsection{Simulation}
\label{subsec:sim}
The Phoenix Project \citep{gao2012} simulates 9 different galaxy clusters 
with viral
masses in the range from $5.495 \times 10^{14} \Msunh$ to $2.427 \times 10^{15}
\Msunh$. The 9 clusters were named with PHA, PHB, ..., PHI. They were randomly selected from the Millennium 
Simulation
friends-of-friends (FOF) group catalog at $z=0$ and resimulated individually
at varying resolution of four levels.  
The simulations were run with the P-Gadget-3 code \citep{springel2008}, 
which is an improved version of Gadget-2 \citep{springel2005}.
The simulation outputs were stored in $72$ snapshots uniformly spaced in
$\log_{10} a$, starting at $a=0.017$, where $a=1/(1+z)$ is the expansion
factor. The large number of outputs allow us to follow the details of 
baryonic evolution within the semi-analytic model. The simulated box was
not limited to the virial range of cluster, but extend as far as 
$40 \mpch - 65 \mpch$, which is about $20R_{200} - 30R_{200}$. Here, 
$R_{200}$ is the virial radius of cluster within which the mean
density is $200$ times of the critical density of the universe. The
statistics on whole cluster means a wide range of the whole sky is
counted.

The simulations adopt the same cosmological parameters as
the Millennium Simulation \citep{springel2005} and Aquarius project
 \citep{springel2008}: $\Omega_{\rm M}=0.25$,
$\Omega_{\Lambda}=0.75$, $\sigma_8=0.9$, $n_s=1$, and a present-day
value of the Hubble constant $H_0=100\,h\,km s^{-1} Mpc^{-1} =73 
km s^{-1} Mpc^{-1}$. Although these parameters differ slightly from 
latest cosmic microwave background observations 
\citep{komatsu2011,hinshaw2012}, as shown by \citet{guo2013}, they 
predicted the similar evolution of galaxy population, which will be 
investigated in this paper.  

In this work, we use the evolutionary history of mass, position and 
velocity of each halo in the merger trees of the 9 clusters. With 
these data, we can explore the effects of dynamical interaction on 
galaxies within the cluster environment. We can also see the average
properties of galaxies in different clusters and their 
cluster-to-cluster variation. We mainly use the high resolution data 
(level 2), which have the particle mass with 
$4.425\times 10^6 - 1.841 \times 10^7 \Msunh$, the softening length with
$0.32 \kpch$ and the convergence radius with $2.4 - 3.2 \kpch$. 
%For a comparision, we also show the results of low resolution (level 4) somewhere.


\subsection{Semi-Analytical Model}
\label{subsec:sam}
The semi-analytical model we used is the open-source \gcs code
\citep{benson2012}, which has incorporated a wide range of physical
processes. In particular, the model considers in each halo the accretion 
of gas into the hot halo, the gas cooling, the star formation and the 
feedback by supernova or black hole etc..  
Then the galaxies are modelled to interact and
merge with each other in the hierachical formation scenario. Below, 
we briefly describe the interaction and merger considered in
this work, where we have made some modifications.


\subsubsection{Ram Pressure Stripping and Tidal Stripping}
\label{subsubsec:stripping}
When a satellite galaxy travels around its central galaxy, it loses gas
due to ram pressure and tidal force. The ram pressure stripping radius 
($r_{\rm rp}$) is 
the radial distance relative to satellite center outside which the ram
pressure exerted by the ICM exceeds the gravitational
restoring force of the satellite \citep[e.g.,][]{gunn1972,roediger2009}. As
concluded by \citet{mccarthy2008}, $r_{\rm rp}$ can be given by 
\begin{equation}
 \label{eq:RamPressure}
  \alpha_{\rm rp} {{\rm G} M_{\rm sat, tot}(r_{\rm rp}) \rho_{\rm sat, gas}
  (r_{\rm rp}) \over r_{\rm rp} } = \rho_{\rm host, gas}(R_{\rm orb}) 
  V_{\rm orb}^2  \; ,
\end{equation}
where $\alpha_{\rm rp}=2.0$ is a geometric factor,  $M_{\rm sat, tot}(r)$ 
is the total mass of the satellite within radius $r$.
$\rho_{\rm sat, gas}(r)$ and $\rho_{\rm host, gas}$
are the density profile of the hot gas halo of the satellite and its
host galaxy, respectively. Here, we implement a isothermal profile for 
the gas halo. $R_{\rm orb}$ and $V_{\rm orb}$ are the radial 
position and velocity of the satellite halo. 


When the satellite galaxy is close to the host center and is equivalently 
massive ($M_{sat}/M_{host} > 0.1$, see \citet{mccarthy2008}) to its host 
galaxy, the gas loss due to tidal force is also important. We find this
situation is common in the cluster environment. We add this
process to the model, while it was not included in the release version of
\gcs.
The tidal stripping radius ($r_{\rm tf}$)is radial 
position relative to satellite center outside which the external
differential force from the host halo exceeds the binding force of the 
satellite\footnote{This is similar to the tidal
stripping on dark matter subhalo.} \citep{taylor2001,gan2010}. 
The $r_{\rm tf}$ approximately follows the equation:
\begin{equation}
 \label{eq:TidalForce}
  r_{\rm tf}^3 = \frac{G M_{\rm sat, gas}(r_{\rm tf})}
  {\omega_{\rm orb}^2 + G\left[ 2M_{\rm host, tot}(R_{\rm orb})
  /R_{\rm orb}^3 - 4\pi\rho_{\rm host, tot}(R_{\rm orb}) \right] } \, ,
\end{equation}
with $\omega$  the orbital angular velocity of the satellite, and 
$\rho_{\rm host, tot}$ the dark matter density profile of the host halo
(see Section~\ref{subsubsec:modification}).


After finding the above two stripping radius, a minimum of them is
adopted as the truncation radius: 
\begin{equation}
 r_{\rm trunc}={\rm min}(r_{\rm rp}, r_{\rm tf}) \; .
\end{equation}
Then the gas of satellite outside $r_{\rm trunc}$ is stripped and
transferred to the gas halo of its host galaxy at an rate: 
\begin{equation}
 \label{eq:strip}
  \frac{{\rm d}M_{sat,gas}}{{\rm d}t} = - \epsilon_{\rm strip} 
  \frac{M_{sat,gas}(>r_{rm trunc})}{\tau_{\rm dyn}} \; ,
\end{equation}
where $\tau_{\rm dyn}$ is the dynamical time of the satellite halo, 
$\epsilon_{\rm strip}$ means the stripping efficency, which is 
a free parameter. This process of gas stripping 
is somewhat similar to that in the original \gcs model, which modeled the 
shrinking of a outer radius of the gas halo.

We note here that the finding of the gas stripping have been simplified. 
We have ignored the complicated structures of dark matter 
halo and gas halo, i.e., we treat them as spherical. When
regarding to the motion of the satellite, we suppose that it is a
particle and its orbit is Keplerian. So the stripping radius given by
the Equation~\ref{eq:RamPressure} and \ref{eq:TidalForce} are only rough
approximation. Besieds, the stripped gas from satellite still remains 
in the vicinity of the satellite, and it will perturb kinematics of
satellite and affect the gas stripping \citep{e.g.,}{}[fellhauer2007,haan2014].
In the model, these uncertainties are simply parameterized by the factor 
$\epsilon_{\rm strip}$ in Equation~\ref{eq:strip}, which implies an
average mass loss rate contributed by various factors. We adopt a value
\footnote{The default value in \gcs is $\epsilon_{\rm strip}=0.1$ as
concluded by \citet{font2008}, in which the tidal stripping on gas halo
was not included.}
$\epsilon_{\rm strip}=0.335$ as concluded by \citet{benson2010}.

%The available value for $\epsilon_{\rm strip}$ is between $(0.0,1.0]$ \citep{benson2010}.
%In the work of \citet{gan2010}, they proposed a stripping model for the
%dark matter subhaloes, which is similar to the model here. They found a
%stripping rate to be $3.5$ by constraining the merging timescale and
%mass funciton of dark matter subhaloes. Suppose the baryonic component
%is $10\%$ of the dark matter in a galaxy, then the stripping efficiency
%for gas halo can be adopted as $0.35$. This value is also closed to the one ($0.335$) as concluded by \citet{benson2010}.


\subsubsection{Galaxy Merger and Transformation}
\label{subsubsec:merger}
dynamical friction lead to merger


The \gcs model divides the 
galactic gas and star into two component. One is the disk where the
cooling gas is expected to form star there firstly. Another is the 
spheroid which is formed via disk instability, spheroid star formation
and galaxy mergers.

when mergeing, mass movement, remnant sizes?



\subsubsection{Model Settings}
\label{subsubsec:modification}
The fiducial model and most of the parameters of \gcs have been calibrated 
well by the developer \citep{benson2014}, who has tested the model with 
the Monto-Carlo merger trees \citep{parkinson2008}. In this work, we have 
employed quite different
merger trees given by the N-body simulation, so some 
special settings are configured as follows.

\begin{itemize}
 \item We make use of the mass, position and velocity of each halo at 
       each output 
       time given by the high resolution simulation. So the data contain
       \begin{itemize}
        \item the mass evolution of subhlaloes,
	\item the mass accretion history of the main host haloes (i.e.,
	      the haloes lie in the most massive branch of the merger 
	      trees),
	\item the orbital properties and the spational evolution of
	      subhaloes,
	\item the merging time when a subhalo will merge with another
	      halo, which may be a subhalo or host halo.
       \end{itemize}
       Therefore, the above data are preset before evolving the galaxy 
       physics. In the default model, most of them are placed with some
       empirical formulae. 
 \item The same cosmological model are setted as in the Phoenix simulation.
 \item As measured by \citet{gao2012}, the density profile of dark
       matter halo in 2 clusters (name with PHD and PHF) are better 
       described by NFW profile \citep{navarro1996,navarro1997}, while
       the other 7 clusters better by Einasto profile \citep{einasto1965}. 
       So we utilize the corresponding profile as they found. 
 \item As stated in Section~\ref{subsubsec:stripping}, we add the
       process of tidal stripping and propose an alternative
       mechanics of gas stripping on the gas halo of satellites. 
       We set $\epsilon_{\rm strip}=0.335$, which is different from the
       default value $0.1$.
\end{itemize}


\section{Galaxy Stellar Mass Function in Clusters}
\subsection{Comparision of GSMF to Observation}
\label{sec:SMFcompare}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure*}
 \centering
 \includegraphics[width=0.95\hsize]{SMFcompare.eps}
 \caption{Comparision of GSMFs to observation. In general, the model
 predictions (color lines) agree well with observations (black lines) at
 redshift from $1.0$ to $0.0$. 
 In model, the GSMFs of whole sky are counted on whole cluster up to
 $20R_{200} - 30R_{200}$, while the cluster GSMFs are within $R_{200}$.
 The lines are the results averaged on 9 Phoenix clusters and the error 
 bars indicate the scatters due to cosmic variance and cluster-to-cluster 
 variations.  In observations, the shaded area in the upper-left panel 
 shows the field GSMFs at $z \simeq 0.0$ determined by various
 measurements, with a upper limit by \citet{bernardi2013} and lower limit 
 by \citet{li2009}. The other results are given by
 \citet{giodini2012,moustakas2013,vanderburg2013,calvi2013,vulcani2013},
 respectively (see text for more details). 
 For the GSMFs in observed cluster, they are
 normalized arbitrarily so that there are comparable.}
 \label{fig:SMFcompare}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For an examination on the model, we compare the predicted GSMF to the 
observation's. The GSMF 
$\phi (M_{\ast})$ means the number of galaxies with stellar mass
$M$ per unit volume per unit logarithmic mass interval. Although a single 
constraint from GSMF is not adequate, it's one of the most fundamental
way to check if a model can reproduce the observed properties of the 
galaxy population. Moreover, there have been many data from high to
low redshift contributing to the study of GSMF, which allow us to make a
detail comparision of the galaxy evolution of different types in
different environments. 


In Fig.~\ref{fig:SMFcompare}, we compare the GSMFs of 
cluster's and field galaxies predicted by our model to that by 
observations at redshift $z \simeq 0.0 - 1.0$. In observation, the
galaxies in field means the galaxies in a wide range of the whole sky.
In model, the GSMFs of whole sky are counted on whole cluster up to
$20R_{200} - 30R_{200}$ (about $40 \mpch - 65 \mpch$, see 
Section~\ref{subsec:sim}), while the GSMFs of cluster are within $R_{200}$.
The color lines show the averaged GSMFs on 9 Phoenix clusters and the
corresponding error bars indicate the scatters due to cosmic variance and 
cluster-to-cluster variations. The observed results are obtained from 
SDSS \citep{bernardi2013,li2009,moustakas2013}, 
COSMOS survey \citep{giodini2012,moustakas2013},
PRism MUlti-object Survey \citep{moustakas2013},
Gemini Cluster Astrophysics Spectroscopic Survey \citep{vanderburg2013},
WIde-field Nearby Galaxy-Cluster Survey \citep{calvi2013,vulcani2013},
ESO Distant Cluster Survey \citep{vulcani2013}, etc.. 
As the exact size of observed clusters were not well measured
\citep[e.g.,][]{calvi2013,vanderburg2013,vulcani2013}, 
their GSMFs are normalized arbitrarily so that they are comparable to 
the model clusters.

In general, Fig.~\ref{fig:SMFcompare} shows that the prediected GSMF 
is in good agreement with observation's. 1) The GSMFs have a small scatter 
for low-mass galaxies and a large scatter for massive galaxies.
2) The large scatter is caused by the massive galaxies within
$R_{200}$ of clusters. 
3) The GSMFs evolve very little from $z\simeq 1.0$ to $z\simeq 0.0$.
 
For the GSMFs in field (whole sky), the different observations match 
well with each other below $M_{\ast} \simeq 10^{11}h^{-2}M_{\odot}$, 
but differ significantly at massive end. In the upper-left panel of 
Fig.~\ref{fig:SMFcompare}, we plot this fact as a shaded area 
determined by various measurements, with a upper limit by 
\citet{bernardi2013} and a lower limit by \citet{li2009}. 
For the GSMFs in cluster, there still lack observable low-mass 
galaxies, and also show large discrepancies for massive galaxies.
This is mainly caused by the diversity in the mass estimates of massive 
galaxies in clusters. It may be due to the sky-subtraction problems
\citep[e.g.,][]{bell2003,bernardi2010}, or the different choice of 
mass-to-light ratio ($M_{\ast}/L$) and initial mass function
\citep[e.g.,][]{li2009,baldry2012,bernardi2010,moustakas2013}. 
Besides, \citet{bernardi2013} found that the 
stellar mass density has strong dependence on the light profile to fit 
for massive galaxies. Using different light profile, one would integrate
the stellar component with different mass profile to different radius.
However, the outer radius of massive galaxies were not well
measured \citep[e.g.][]{mcgee2010,gonzalez2013} and 
there is still debated on which profile is the most appropriate
\citep{bernardi2013}. 

The predicted GSMFs at massive end
\citet{kravtsov2014}

evolve little, refer to \citet{giodini2012,moustakas2013}


\subsection{Evolution of Galaxy Stellar Function}
\label{sec:result}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure*}
 \centering
 \includegraphics[width=0.95\hsize]{SMFev.eps}
 \caption{Evolution of halo mass functions (HMFs) and GSMFs in
 different environment. The lines with error bars indicate the average 
 value on 9 clusters and their scatters due to cosmic variance and 
 cluster-to-cluster variations. Kinds of lines show the halo mass 
 functions or GSMFs in different environment: high density region 
 ($< R_{200}$, dotted-dashed, red),intermediate density region 
 ($within [R_{200},3R_{200}]$, dashed, green) and low density region 
 ($> 3R_{200}$, dotted, blue). The HMFs and GSMFs have almost the same 
 shape in the above environments. }
 \label{fig:SMFev}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In Fig.~\ref{fig:SMFev}, we compare the evolution of halo mass function
(HMFs) and GSMFs in Phoenix Clusters. The halo mass function 
$\phi (M_{\rm h}$ is defined as the number of haloes with virial mass
$M$ per unit volume per unit logarithmic mass interval.  
We seperate the cluster into 
three regions: high density region ($<R_{200}$),intermediate density 
region (within $ [R_{200},3R_{200}]$), low density region ($>3R_{200}$).
Figure~\ref{fig:SMFev} shows that the HMFs and GSMFs in different 
environments have almost the same shape from $z\simeq 1.0$ to 
$z\simeq 0.0$. 

vetical: halo mass function to stellar mass function

horizontal: evolution with redshift


compare to \citet{vulcani2013}



\FloatBarrier

\afterpage{\clearpage}
\subsection{Star-forming and Quiescent Galaxies}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure}
 \centering
 \includegraphics[width=0.95\hsize]{sSFR.eps}
 \caption{Average distribution of specific star formation rate of
 galaxies in 9 Phoenix clusters.}
 \label{fig:sSFR}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\afterpage{\clearpage}

The galaxies can be simply seperated to be star-forming or quiescent at 
$sSFR = 10^{-11} yr^{-1}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure*}
 \centering
 \includegraphics[width=0.95\hsize]{SMFsq.eps}
 \caption{Evolution of GSMFs of star-forming and quiescent galaxies in
 different environments.}
 \label{fig:SMFsq}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





\afterpage{\clearpage}

\subsection{Dependence on Morphologies}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure*}
 \centering
 \includegraphics[width=0.95\hsize]{SMFtype.eps}
 \caption{Evolution of GSMFs of star-forming and quiescent galaxies in
 different environments.}
 \label{fig:SMFtype}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







\FloatBarrier













\section{Conclusions and Discussions}
\label{sec:conc}

singel level hierachy in \gcs at present.

satellite-satellite interaction are not included at present, it will be considered
in future once \gcs is able to model the mutiple level hierachy merger. 



\vspace{2cm}

\section*{Acknowledgements}

This work was supported by {\it 973 Program} 2014 CB845702; the Strategic 
Priority Research Program {\it The Emergence of Cosmological Structures}
of the Chinese Academy of Sciences (Grant No. XDB09010100).


\bibliographystyle{mnras}
\bibliography{pair}


\appendix

\section[]{Input of \gcs Model}


\section[]{Output of \gcs Model}

\bsp

\label{lastpage}
\end{document}
