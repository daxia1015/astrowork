%% 《天文学进展》杂志 文档类
%% `TWXJZ' class v0.1 for LaTeX 2e
%% 版权 王伟华 2005,2006,2007,2008年 
%% Copyright (C) Wang Weihua 2005, 2006, 2007, 2008
%%
%% for LaTeX version 2E, by Leslie Lamport
%% `TWXJZ' 文档类主要根据 'article', 'AA', 'MN', 和''
%%  等文档类编写而成。

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{twxjz}
          [2007/01/26 V0.1 TWXJZ article document class]

\newcommand{\TWXJZ@version}{0.1}
\typeout{`TWXJZ' article document class V\TWXJZ@version, released 2007 Jan 26}

\newif\ifTWXJZ@dvips   \TWXJZ@dvipsfalse
\DeclareOption{dvips}{\TWXJZ@dvipstrue}
\DeclareOption{dvipdfm}{\TWXJZ@dvipsfalse}
\newif\ifTWXJZ@english \TWXJZ@englishfalse
\DeclareOption{english}{\TWXJZ@englishtrue}
\DeclareOption{chinese}{\TWXJZ@englishfalse}
\newif\ifTWXJZ@print   \TWXJZ@printtrue
\DeclareOption{Print}{\TWXJZ@printtrue}
\DeclareOption{screen}{\TWXJZ@printfalse}
\newif\ifTWXJZ@referee \TWXJZ@refereefalse
\DeclareOption{referee}{\TWXJZ@refereetrue}
\DeclareOption{final}{\TWXJZ@refereefalse}
\DeclareOption{twocolumn}%
    {\ClassError{twxjz}{Option `twocolumn' not supported}{}}
\DeclareOption{titlepage}%
    {\ClassError{twxjz}{Option `titlepage' not supported}{}}
\PassOptionsToClass{onecolumn}{article}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ctexart}}
\ProcessOptions
\LoadClass[a4paper,twoside,fancyhdr,psfont]{ctexart}[2004/08/14]
%%     math packages
\RequirePackage{amsmath,amsfonts,amssymb}
%%     graphics packages
\RequirePackage{graphicx}
%%     other packages
\RequirePackage{url}
%%     check pdfTeX mode
\RequirePackage[sort&compress,numbers]{natbib}
\RequirePackage{ifpdf}
%%     hyperref package
\ifpdf   % We're running pdfTeX in PDF mode
    \RequirePackage[pdftex]{hyperref}
\else    % We're not running pdfTeX, or running pdfTeX in DVI mode
    \ifTWXJZ@dvips
        \RequirePackage[dvips]{hyperref}
    \else
        \RequirePackage[dvipdfm]{hyperref}
    \fi
    \AtBeginDvi{\special{pdf:tounicode GBK-EUC-UCS2}} % GBK -> Unicode
\fi
\hypersetup{CJKbookmarks,%
            bookmarksnumbered,%
            colorlinks,%
            linkcolor=blue,%
            citecolor=blue,%
            plainpages=false,%
            pdfstartview=FitH}
%%  Print or Screen
\ifTWXJZ@print
    \hypersetup{colorlinks=false}
\fi

%===================== paper size ===============
%% the global environments
\setlength{\oddsidemargin}{0pt}       
\if@twoside
    \setlength{\evensidemargin}{0pt}
\else
    \setlength{\evensidemargin}{0pt}
\fi
\setlength{\topmargin}{-0.5cm}
\setlength{\headsep}{0.6cm}
\headheight 15pt
\ifTWXJZ@referee
    \setlength{\textwidth}{140mm}       
    \setlength{\textheight}{215mm} 
\else
    \setlength{\textwidth}{148mm}     
    \setlength{\textheight}{210mm}      
\fi
\parskip .5ex plus .25ex minus .25ex
\def\@listi{\leftmargin\leftmargini
            \topsep    6\p@ \@plus2\p@ \@minus4\p@
            \parsep    \z@
            \itemsep   \parsep}
\let\@listI\@listi
\@listi
\def\@listii {\leftmargin\leftmarginii
              \labelwidth\leftmarginii
              \advance\labelwidth-\labelsep
              \topsep    4\p@ \@plus2\p@ \@minus\p@
              \parsep    \z@
              \itemsep   \parsep}
\def\@listiii{\leftmargin\leftmarginiii
              \labelwidth\leftmarginiii
              \advance\labelwidth-\labelsep
              \topsep    2\p@ \@plus\p@ \@minus\p@
              \parsep    \z@
              \partopsep \p@ \@plus\z@ \@minus\p@
              \itemsep   \parsep}
%% Chinese or English
%% the chinese or english environments
\def\chinesematter{%
    \ifTWXJZ@referee
        \def\baselinestretch{1.8}\selectfont
    \else
        \def\baselinestretch{1.3}\selectfont
    \fi    
    \def\TWXJZ@abstract{摘要：}
    \def\TWXJZ@keywords{\ziju{1.05}关键词:}
    \def\CTEX@appendixname{附录~}
    \def\acknowledgementsname{致谢}
    \renewcommand{\indexname}{作者索引}
}
\def\englishmatter{%
    \ifTWXJZ@referee
        \def\baselinestretch{1.5}\selectfont
    \else
        \def\baselinestretch{1}\selectfont
    \fi    
    \def\TWXJZ@abstract{Abstract:}
    \def\TWXJZ@keywords{Key words:}
    \def\CTEX@figurename{Figure}
    \def\CTEX@tablename{Table}
    \def\CTEX@bibname{References}
    \def\CTEX@appendixname{Appendix\space}
    \def\acknowledgementsname{Acknowledgements}
    \renewcommand{\indexname}{Author Index}
}
\AtBeginDocument{
  \ifTWXJZ@english
    \englishmatter
  \else
    \chinesematter
  \fi}
%%
%% Define some Command For TWXJZ
\newcommand{\TWXJZ@serialnumbername}{文章编号}
\newcommand{\TWXJZ@labelnumbername}{中图分类号}
\newcommand{\TWXJZ@IDcodename}{文献标识码}
\newcommand{\TWXJZ@receive}{收稿日期}
\newcommand{\TWXJZ@accept}{修改日期}
\newcommand{\TWXJZ@chnjournalname}{天文学进展}
\newcommand{\TWXJZ@engjournalname}{PROGRESS IN ASTRONOMY}
\newcommand*{\TWXJZ@publishvol}{00}
\newcommand*{\TWXJZ@publishnum}{0}
\newcommand*{\TWXJZ@publishyear}{0000}
\gdef\TWXJZ@publishmonth{0}
\newcommand{\TWXJZ@Date}{\textbf{收稿日期:} 0000/00/00; \textbf{修改日期:} 0000/00/00}
\newcommand{\TWXJZ@serialnumber}{0000-0000}
\newcommand{\TWXJZ@labelnumber}{P000.0}
\newcommand{\TWXJZ@IDcode}{X}
\def\TWXJZ@shortauthor{Must given short author}
\def\TWXJZ@shorttitle{Must given short title}
\newcommand*\Volume[2]{%
  \renewcommand*{\TWXJZ@publishvol}{#1}%
  \renewcommand*{\TWXJZ@publishnum}{#2}%
}
\newcommand*\PublishDate[2]{%
  \renewcommand*{\TWXJZ@publishyear}{#1}%
  \gdef\TWXJZ@publishmonth{#2}%
}
\newif\ifTWXJZ@yjlw   \TWXJZ@yjlwfalse
\newif\ifTWXJZ@yjkb   \TWXJZ@yjkbfalse
\newcommand*{\yjlw}{\TWXJZ@yjlwtrue\TWXJZ@yjkbfalse}
\newcommand*{\yjkb}{\TWXJZ@yjkbtrue\TWXJZ@yjlwfalse}
\newcommand*{\ShortAuthor}[1]{\gdef\TWXJZ@shortauthor{#1}}
\newcommand*{\ShortTitle}[1]{\gdef\TWXJZ@shorttitle{#1}}
\newcommand{\SerialNumber}[1]{\renewcommand{\TWXJZ@serialnumber}{#1}}
\newcommand{\LabelNumber}[1]{\renewcommand{\TWXJZ@labelnumber}{#1}}
\newcommand{\IDCode}[1]{\renewcommand{\TWXJZ@IDcode}{#1}}
%%
\newcommand*{\TWXJZ@engmonth}[1]{%
  \ifcase#1 Month\or Jan.\or Feb.\or Mar.\or Apr.\or May.\or Jun.\or Jul\or
  Aug.\or Sep.\or Oct.\or Nov.\or Dec.\else Wronginput\fi}
%%
\newcommand{\timenow}{%
  \@tempcnta=\time \divide\@tempcnta by 60 \number\@tempcnta:\multiply
  \@tempcnta by 60 \@tempcntb=\time \advance\@tempcntb by -\@tempcnta
  \ifnum\@tempcntb <10 0\number\@tempcntb\else\number\@tempcntb\fi}
%%
%% 增加 \upcite 命令使显示的引用为上标形式
\def\@cite#1{\mbox{$\m@th^{\hbox{\@ove@rcfont[#1]}}$}}
%\renewcommand\@biblabel[1]{#1} %去掉[]
\newcommand{\upcite}[1]{\textsuperscript{\textsuperscript{\cite{#1}}}}
\renewcommand{\footnoterule}
    {\noindent\rule{10\ccwd}{0.45mm}\vspace{0.2cm}}
\setlength{\footnotesep}{12.65pt}    
%%
\let\@date\undefined
\ifTWXJZ@referee
   \newcommand*{\support}[1]{\begingroup
    \def\thefootnote{}% initial style
    \protect\footnotetext[\c@footnote]{\ignorespaces#1}%
    \endgroup\ignorespaces}
\else
   \renewcommand*{\date}[1]{\begingroup
        \def\thefootnote{}% initial style
        \protect\footnotetext[\c@footnote]{\ignorespaces#1}%
        \endgroup\ignorespaces}
   \newcommand*{\support}[1]{\begingroup
    \def\thefootnote{}% initial style
    \protect\footnotetext[\c@footnote]{\ignorespaces#1}%
    \endgroup\ignorespaces}
\fi

\def\TWXJZ@titlemark{}
\newcommand*\titlemark[1]{\def\TWXJZ@titlemark{#1}}
\renewcommand*{\title}[3][\@empty]{%
    \gdef\@title{#2\@titlenote{#3}}}
\newcommand*{\@titlenote}[1]{%
    \def\TWXJZ@title@footnote{#1}%
    \ifx \TWXJZ@title@footnote
        \@empty
    \else
        \footnote{#1}%
    \fi}

\def\inst#1{\unskip$^{#1}$}
\gdef\@institute{}
\newcommand*{\institute}[2][]{%
  \ifx \@institute\@empty
    \gdef\@institute{\@instituteline{#1}{#2}}
  \else
    \expandafter\gdef\expandafter\@institute\expandafter
      {\@institute\tabularnewline\@instituteline{#1}{#2}}
  \fi}
\def\@instituteline#1#2{\@institutecr\hb@xt@1ex{\textsuperscript{#1}\hss}#2}
\def\@institutecr{\def\\{\tabularnewline\@institutecr\hbox to1ex{}\ignorespaces}}
%% make title
\renewcommand{\maketitle}{\par%
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\hbox{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \global\@topnum\z@   % Prevents figures from going at top of page.
    \TWXJZ@makeheadbox%
%    \vspace{2mm}
    \@maketitle
    \thispagestyle{empty}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\@title\@empty
  \global\let\@author\@empty
  \global\let\@institute\@empty
  \global\let\@thanks\@empty
}
\newcommand{\makeothertitle}{\par%
  \bigskip
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\hbox{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \global\@topnum\z@   % Prevents figures from going at top of page.
    \@maketitle
    \@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\@title\@empty
  \global\let\@author\@empty
  \global\let\@institute\@empty
  \global\let\@thanks\@empty
}
\def\@maketitle{%
  \begingroup\centering%
  \let\footnote\thanks
    {\huge\zihao{2}\ziju{0.2}\bf\heiti\@title \par}%
    \vspace*{0.6cm}
%    \vskip 3ex%
    {\Large\zihao{4}\songti
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vspace*{0.1cm}
%    \vskip 1ex%
    {\normalfont\normalsize\kaishu\zihao{-5}
      \begin{tabular}[t]{@{}l@{}}%
        \@institute
      \end{tabular}\par}%
  \endgroup}
%%
%% Environment English Title
\newenvironment{chinesetitle}
    {\chinesematter}
    {\vskip 10pt}
\newenvironment{englishtitle}
    {\englishmatter}
    {\vskip 10pt}
\AtBeginDocument{\null\vskip -1.7cm}
\def\TWXJZ@abstract{Abstract}
\def\TWXJZ@keywords{Keywords}
\renewenvironment{abstract}
  {\smallskip
   \trivlist\itemindent\parindent\item\relax%
   \zihao{-5}\noindent%
   \leftskip=2\ccwd \rightskip=2\ccwd
   \textbf{\TWXJZ@abstract}\quad\ignorespaces}
  {\endtrivlist}
\newcommand*\keywords[1]{%
  \vskip .5ex\zihao{-5}\noindent
  \leftskip=2\ccwd \rightskip=2\ccwd
  \textbf{\TWXJZ@keywords}\quad#1\par}
\newcommand*\LabelIDcode{%
  \vskip .5ex\zihao{-5}\noindent
  \leftskip=2\ccwd \rightskip=2\ccwd
  \textbf{\TWXJZ@labelnumbername:}\quad \TWXJZ@labelnumber
  \qquad\textbf{\TWXJZ@IDcodename:}\TWXJZ@IDcode\par\vspace*{1mm}}
\setcounter{secnumdepth}{5}
\renewcommand{\floatpagefraction}{0.80}
%%
\newenvironment{acknowledgements}{%
      \section*{\acknowledgementsname}
      \@mkboth{\MakeUppercase\acknowledgementsname}{\MakeUppercase\acknowledgementsname}}%
{\par\smallskip}
%%
% Change definition of `thebibliography' environment
\renewenvironment{thebibliography}[1]
  {\section*{\refname：\@mkboth{\refname}{\refname}}%      !!!
   \list{\@biblabel{\@arabic\c@enumiv}}%
        {\settowidth\labelwidth{\@biblabel{#1}}%
         \leftmargin\labelwidth
         \advance\leftmargin\labelsep
         \@openbib@code
         \usecounter{enumiv}%
         \let\p@enumiv\@empty
         \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \vskip 6\p@
      \itemsep 4\p@ \@plus2\p@ \@minus\p@
      \def\baselinestretch{1}\selectfont
      \sloppy
      \zihao{6}%                                              !!!
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}

\newcommand\TWXJZ@logo{%
    \begin{minipage}{52.7mm}
       \normalsize
       \begin{tabular}{c}
          \hline\\[-5mm]
          {\songti\ziju{1}\TWXJZ@chnjournalname} \\
          \TWXJZ@engjournalname \\
          \hline\\[-5mm]
       \end{tabular}
    \end{minipage}
} 
%%
\newcommand*{\TWXJZ@makeheadbox}{%
\zihao{-5} \noindent
\ifTWXJZ@referee
    \let\TWXJZ@savebaselinestretch=\baselinestretch
    \parbox{\textwidth}{
    \def\baselinestretch{1.28}
    \begin{minipage}{0.5\textwidth}
       \normalsize
       \textbf{稿\quad 件:}\ \jobname.tex \\ 
       \textbf{打\quad 印:}\ \today;\ \timenow
    \end{minipage}\hfill\TWXJZ@logo}
    \rule[0.8cm]{\textwidth}{2pt}
    \let\baselinestretch=\TWXJZ@savebaselinestretch
\else
    \let\TWXJZ@savebaselinestretch=\baselinestretch
    \def\baselinestretch{1.28}
    \parbox{15cm}{
    \parbox{3.2cm}{\centering{第~\TWXJZ@publishvol~卷　第~\TWXJZ@publishnum~期}}\hfill
    \parbox{7.0cm}{\centering{\songti\ziju{1}\zihao{-4}\TWXJZ@chnjournalname}}\hfill
    \parbox{3.2cm}{\centering{Vol.\TWXJZ@publishvol,\quad No.~\TWXJZ@publishnum}}}
    \parbox{15cm}{
    \parbox{3.2cm}{\centering{\TWXJZ@publishyear~年~\TWXJZ@publishmonth~月}}\hfill
    \parbox{7.0cm}{\centering\zihao{-4}{\rm PROGRESS IN ASTRONOMY}}\hfill
    \parbox{3.2cm}{\centering{\TWXJZ@engmonth{\TWXJZ@publishmonth},  \TWXJZ@publishyear}}}
    \vspace*{0.6cm}
    \begin{tabular*}{\textwidth}{cc}
        \hline
        \hline
    \end{tabular*}
    \\[-6mm]
    {\small{\heiti\TWXJZ@serialnumbername:} \TWXJZ@serialnumber\hfill\par}
    \ifTWXJZ@yjlw
        \begin{flushleft}
        \includegraphics[width=0.2\textwidth]{twxjz_yjlw.eps}\hfill\\[1mm]
        \vspace{1mm}
        \end{flushleft}
    \else
        \ifTWXJZ@yjkb
            \begin{flushleft}
            \includegraphics[width=0.2\textwidth]{twxjz_yjkb.eps}\hfill\\[1mm]
%            \vspace{1mm}
            \end{flushleft}
        \else
            \vspace{2mm}
        \fi
    \fi
    \let\baselinestretch=\TWXJZ@savebaselinestretch
\fi
}
%%
%% set default \pagestyle to Fancy
\def\TWXJZ@evenhead{\thepage \hfill{\songti\zihao{-5}\ziju{0.32}\TWXJZ@chnjournalname} 
                  \hfill\TWXJZ@publishvol{}~卷
                  \hspace*{-1.0\textwidth}{\rule[-0.25cm]{\textwidth}{0.2mm}}}
\def\TWXJZ@oddhead{\TWXJZ@publishnum{}~期\hfill {\songti\zihao{-5}\ziju{0.1}\TWXJZ@shortauthor{}：\TWXJZ@shorttitle} 
                   \hfill\thepage\hspace*{-1.0\textwidth}{\rule[-0.25cm]{\textwidth}{0.2mm}}}
%%
\pagestyle{fancy}
\fancyhf{}  %清除以前对页眉页脚的设置
\fancyhead[E]{\iftopfloat{}{\TWXJZ@evenhead}}
\fancyhead[O]{\iftopfloat{}{\TWXJZ@oddhead}}
\renewcommand{\headrulewidth}{0.0pt}
%%
%% the section formats
\def\CTEX@section@format{\bfseries\zihao{4}}
\def\CTEX@section@indent{\z@}
\def\CTEX@section@beforeskip{-1.5ex \@plus -.5ex \@minus -.2ex}
\def\CTEX@section@afterskip{.5ex \@plus .1ex}

\def\CTEX@subsection@format{\bfseries\zihao{-4}}
\def\CTEX@subsection@indent{\z@}
\def\CTEX@subsection@beforeskip{-1.25ex \@plus -.5ex \@minus -.2ex}
\def\CTEX@subsection@afterskip{.4ex \@plus .1ex}

\def\CTEX@subsubsection@format{\bfseries\zihao{-4}}
\def\CTEX@subsubsection@indent{\z@}
\def\CTEX@subsubsection@beforeskip{-1ex \@plus -.5ex \@minus -.2ex}
\def\CTEX@subsubsection@afterskip{.3ex \@plus .1ex}

\def\CTEX@preparagraph{}
\def\CTEX@postparagraph{)}
\def\CTEX@paragraph@format{\bfseries\normalsize}
\def\CTEX@paragraph@aftername{\hskip 1ex}
\def\CTEX@paragraph@beforeskip{-.75ex \@plus -.25ex \@minus -.2ex}
\def\CTEX@paragraph@afterskip{.2ex \@plus .1ex}

\def\CTEX@presubparagraph{(}
\def\CTEX@postsubparagraph{)}
\def\CTEX@subparagraph@format{\normalfont\normalsize}
\def\CTEX@subparagraph@aftername{\hskip 1ex}
\def\CTEX@subparagraph@beforeskip{-.5ex \@plus -.25ex \@minus -.2ex}
\def\CTEX@subparagraph@afterskip{.1ex \@plus .1ex}
\renewcommand\theparagraph    {\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\@alph\c@subparagraph}
\bibliographystyle{plain}
%%
%% Astronomy and Astrophysics symbol macros
\newcommand\getsto{\mathrel{\mathchoice {\vcenter{\offinterlineskip
\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr\gets\cr\to\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr\gets
\cr\to\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr\gets
\cr\to\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
\gets\cr\to\cr}}}}}

\newcommand\cor{\mathrel{\mathchoice {\hbox{$\widehat=$}}{\hbox{$\widehat=$}}
{\hbox{$\reset@font\scriptstyle\hat=$}}
{\hbox{$\reset@font\scriptscriptstyle\hat=$}}}}

\newcommand\lid{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr<\cr\noalign{\vskip1.2pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr<\cr
\noalign{\vskip1.2pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr<\cr
\noalign{\vskip1pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
<\cr
\noalign{\vskip0.9pt}=\cr}}}}}

\newcommand\gid{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr>\cr\noalign{\vskip1.2pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr>\cr
\noalign{\vskip1.2pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr>\cr
\noalign{\vskip1pt}=\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
>\cr
\noalign{\vskip0.9pt}=\cr}}}}}

\newcommand\sol{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr\sim\cr<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr\sim\cr
<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr\sim\cr
<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
\sim\cr<\cr}}}}}

\newcommand\sog{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr\sim\cr>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr\sim\cr
>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
\sim\cr>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
\sim\cr>\cr}}}}}

\newcommand\lse{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr<\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
<\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
<\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
<\cr\simeq\cr}}}}}

\newcommand\gse{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr>\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
>\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
>\cr\simeq\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
>\cr\simeq\cr}}}}}

\newcommand\grole{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr>\cr\noalign{\vskip-1.5pt}<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
>\cr\noalign{\vskip-1.5pt}<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
>\cr\noalign{\vskip-1pt}<\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
>\cr\noalign{\vskip-0.5pt}<\cr}}}}}

\newcommand\leogr{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr<\cr\noalign{\vskip-1.5pt}>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
<\cr\noalign{\vskip-1.5pt}>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
<\cr\noalign{\vskip-1pt}>\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
<\cr\noalign{\vskip-0.5pt}>\cr}}}}}

\newcommand\loa{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr<\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
<\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
<\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
<\cr\approx\cr}}}}}

\newcommand\goa{\mathrel{\mathchoice {\vcenter{\offinterlineskip\halign{\hfil
$\reset@font\displaystyle##$\hfil\cr>\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\textstyle##$\hfil\cr
>\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptstyle##$\hfil\cr
>\cr\approx\cr}}}
{\vcenter{\offinterlineskip\halign{\hfil$\reset@font\scriptscriptstyle##$\hfil\cr
>\cr\approx\cr}}}}}

\newcommand\sun{\hbox{$\odot$}}
\newcommand\earth{\hbox{$\oplus$}}
\newcommand\degr{\hbox{$^\circ$}}
\newcommand\diameter{{\ifmmode\mathchoice
{\ooalign{\hfil\hbox{$\reset@font\displaystyle/$}\hfil\crcr
{\hbox{$\reset@font\displaystyle\mathchar"20D$}}}}
{\ooalign{\hfil\hbox{$\reset@font\textstyle/$}\hfil\crcr
{\hbox{$\reset@font\textstyle\mathchar"20D$}}}}
{\ooalign{\hfil\hbox{$\reset@font\scriptstyle/$}\hfil\crcr
{\hbox{$\reset@font\scriptstyle\mathchar"20D$}}}}
{\ooalign{\hfil\hbox{$\reset@font\scriptscriptstyle/$}\hfil\crcr
{\hbox{$\reset@font\scriptscriptstyle\mathchar"20D$}}}}
\else{\ooalign{\hfil/\hfil\crcr\mathhexbox20D}}%
\fi}}

\newcommand\sq{\ifmmode\squareforqed\else{\unskip\nobreak\hfil
\penalty50\hskip1em\null\nobreak\hfil\squareforqed
\parfillskip=0pt\finalhyphendemerits=0\endgraf}\fi}
\newcommand\squareforqed{\hbox{\rlap{$\sqcap$}$\sqcup$}}

\newcommand{\romn}[1] {{\mathrm #1}}

\newcommand\fd{\hbox{$.\!\!^{\reset@font\romn d}$}}
\newcommand\fh{\hbox{$.\!\!^{\reset@font\romn h}$}}
\newcommand\fm{\hbox{$.\!\!^{\reset@font\romn m}$}}
\newcommand\fs{\hbox{$.\!\!^{\reset@font\romn s}$}}
\newcommand\fdg{\hbox{$.\!\!^\circ$}}
\newcommand\farcm{\hbox{$.\mkern-4mu^\prime$}}
\newcommand\farcs{\hbox{$.\!\!^{\prime\prime}$}}
\newcommand\fp{\hbox{$.\!\!^{\reset@font\reset@font\scriptscriptstyle\romn p}$}}
\newcommand\arcmin{\hbox{$^\prime$}}
\newcommand\arcsec{\hbox{$^{\prime\prime}$}}

\def\micron{\hbox{$\umu$m}}

%====================================================================
% standard abbreviations of the mostly-used astro/astrophys-journals
%====================================================================
\let\jnl@style=\rmfamily 
\def\ref@jnl#1{{\jnl@style#1}}% 
\newcommand\aj{\ref@jnl{AJ}}% 
          % Astronomical Journal 
\newcommand\araa{\ref@jnl{ARA\&A}}% 
          % Annual Review of Astron and Astrophys 
\newcommand\apj{\ref@jnl{ApJ}}% 
          % Astrophysical Journal 
\newcommand\apjl{\ref@jnl{ApJ}}% 
          % Astrophysical Journal, Letters 
\newcommand\apjs{\ref@jnl{ApJS}}% 
          % Astrophysical Journal, Supplement Series
\newcommand\ao{\ref@jnl{Appl.~Opt.}}% 
          % Applied Optics 
\newcommand\apss{\ref@jnl{Ap\&SS}}% 
          % Astrophysics and Space Science 
\newcommand\aap{\ref@jnl{A\&A}}% 
          % Astronomy and Astrophysics           
\newcommand\aapr{\ref@jnl{A\&A~Rev.}}% 
          % Astronomy and Astrophysics Reviews 
\newcommand\aaps{\ref@jnl{A\&AS}}% 
          % Astronomy and Astrophysics, Supplement Series
\newcommand\azh{\ref@jnl{AZh}}% 
          % Astronomicheskii Zhurnal 
\newcommand\baas{\ref@jnl{BAAS}}% 
          % Bulletin of the AAS 
\newcommand\chjaa{\ref@jnl{ChJAA}}
          % Chinese Journal Astronomy Astrophysics   
\newcommand\ibvs{\ref@jnl{IBVS}}                 
          % Infoormation Bulletin on Variable Stars 
\newcommand\jrasc{\ref@jnl{JRASC}}% 
          % Journal of the RAS of Canada 
\newcommand\memras{\ref@jnl{MmRAS}}% 
          % Memoirs of the RAS 
\newcommand\mnras{\ref@jnl{MNRAS}}% 
          % Monthly Notices of the RAS 
\newcommand\pra{\ref@jnl{Phys.~Rev.~A}}% 
          % Physical Review A: General Physics 
\newcommand\prb{\ref@jnl{Phys.~Rev.~B}}% 
          % Physical Review B: Solid State 
\newcommand\prc{\ref@jnl{Phys.~Rev.~C}}% 
          % Physical Review C 
\newcommand\prd{\ref@jnl{Phys.~Rev.~D}}% 
          % Physical Review D 
\newcommand\pre{\ref@jnl{Phys.~Rev.~E}}% 
          % Physical Review E 
\newcommand\prl{\ref@jnl{Phys.~Rev.~Lett.}}% 
          % Physical Review Letters 
\newcommand\pasp{\ref@jnl{PASP}}% 
          % Publications of the ASP 
\newcommand\pasj{\ref@jnl{PASJ}}% 
          % Publications of the ASJ 
\newcommand\qjras{\ref@jnl{QJRAS}}% 
          % Quarterly Journal of the RAS 
\newcommand\skytel{\ref@jnl{S\&T}}% 
          % Sky and Telescope 
\newcommand\solphys{\ref@jnl{Sol.~Phys.}}% 
          % Solar Physics 
\newcommand\sovast{\ref@jnl{Soviet~Ast.}}% 
          % Soviet Astronomy 
\newcommand\ssr{\ref@jnl{Space~Sci.~Rev.}}% 
          % Space Science Reviews 
\newcommand\zap{\ref@jnl{ZAp}}% 
          % Zeitschrift fuer Astrophysik 
\newcommand\nat{\ref@jnl{Nature}}% 
          % Nature 
\newcommand\iaucirc{\ref@jnl{IAU~Circ.}}% 
          % IAU Cirulars 
\newcommand\aplett{\ref@jnl{Astrophys.~Lett.}}% 
          % Astrophysics Letters 
\newcommand\apspr{\ref@jnl{Astrophys.~Space~Phys.~Res.}}% 
          % Astrophysics Space Physics Research 
\newcommand\bain{\ref@jnl{Bull.~Astron.~Inst.~Netherlands}}% 
          % Bulletin Astronomical Institute of the Netherlands 
\newcommand\fcp{\ref@jnl{Fund.~Cosmic~Phys.}}% 
          % Fundamental Cosmic Physics 
\newcommand\gca{\ref@jnl{Geochim.~Cosmochim.~Acta}}% 
          % Geochimica Cosmochimica Acta 
\newcommand\grl{\ref@jnl{Geophys.~Res.~Lett.}}% 
          % Geophysics Research Letters 
\newcommand\jcp{\ref@jnl{J.~Chem.~Phys.}}% 
          % Journal of Chemical Physics 
\newcommand\jgr{\ref@jnl{J.~Geophys.~Res.}}% 
          % Journal of Geophysics Research 
\newcommand\jqsrt{\ref@jnl{J.~Quant.~Spec.~Radiat.~Transf.}}% 
          % Journal of Quantitiative Spectroscopy and Radiative Trasfer 
\newcommand\memsai{\ref@jnl{Mem.~Soc.~Astron.~Italiana}}% 
          % Mem. Societa Astronomica Italiana 
\newcommand\nphysa{\ref@jnl{Nucl.~Phys.~A}}% 
          % Nuclear Physics A 
\newcommand\physrep{\ref@jnl{Phys.~Rep.}}% 
          % Physics Reports 
\newcommand\physscr{\ref@jnl{Phys.~Scr}}% 
          % Physica Scripta 
\newcommand\planss{\ref@jnl{Planet.~Space~Sci.}}% 
          % Planetary Space Science 
\newcommand\procspie{\ref@jnl{Proc.~SPIE}}% 
          % Proceedings of the SPIE 
\newcommand\sci{\ref@jnl{Sci.}}          
          % Science
\let\astap=\aap 
\let\aas=\aaps
\let\apjlett=\apjl 
\let\apjsupp=\apjs 
\let\applopt=\ao
\newcommand\phn{\phantom{0}}% 
\newcommand\phd{\phantom{.}}% 
\newcommand\phs{\phantom{$-$}}% 
\newcommand\phm[1]{\phantom{#1}}% 

%============================================================
% Chinese astronomical affiliates:
%============================================================
\newcommand{\cas}{The Chinese Academy of Sciences}
\newcommand{\bao}{Beijing Astronomical Observatory, 
Chinese Academy of Sciences, Beijing 100012}
\newcommand{\pmo}{{ Purple Mountain Observatory,  Chinese Academy of 
          Sciences, Nanjing 210008}\\}
\newcommand{\sho}{{ Shanghai Observatory,  Chinese Academy of 
          Sciences, Shanghai 200030}}
\newcommand{\yuo}{{ Yunnan Observatory,  Chinese Academy of 
          Sciences, Kunming 650011}}
\newcommand{\ust}{{ Centre for Astrophysics, University of 
          Sciences \& Technology of China, Hefei 230026}\\}
\newcommand{\ess}{{ Department of Earth and Space Sciences, University of 
        Sciences \& Technology\\ of China, Hefei 230026}\\}
\newcommand{\gra}{{ Graduate School, University of Sciences 
          \& Technology of China, Beijing 100039}\\}
\newcommand{\nua}{{ Department of Astronomy, Nanjing University, 
          Nanjing 210008}\\}
\newcommand{\nnup}{{ Department of Physics, Nanjing Normal University,
          Nanjing 210097}\\}
\newcommand{\nup}{{ Department of Physics, Nanjing University, 
          Nanjing 210008}\\}
\newcommand{\geo}{{ Department of Geophysics, Peking University, 
          Beijing 100871}\\}
\newcommand{\phy}{{ Department of Physics, Peking University 
         }}
\newcommand{\hep}{{ Institute of High Energy Physics, Chinese 
          Academy of Sciences, Beijing 100039}\\}
\newcommand{\itp}{{ Institute of Theoretical Physics, Chinese 
          Academy of Sciences, Beijing 100080}\\}
\newcommand{\bnua}{{ Department of Astronomy, Beijing Normal 
          University, Beijing 100875}\\}
\newcommand{\bnup}{{ Department of Physics, Beijing Normal 
          University, Beijing 100875}\\}
\newcommand{\sxo}{{ Shaanxi Observatory, The Chinese Academy of 
          Sciences, Lintong 710600}\\}
\newcommand{\jlos}{{ Joint Laboratory for Optical Astronomy, 
         The Chinese Academy of Sciences, Shanghai 200030}\\}
\newcommand{\jlrn}{{ Joint Laboratory for Radio Astronomy, 
        The Chinese Academy of Sciences, Nanjing 210008}\\}
\newcommand{\css}{{ Center for Space Science and Applied
        Research, Beijing 100080}\\}
\newcommand{\rajl}{{ Radio Astronomy Joint Laboratory, Beijing, 
        100080}\\}
\newcommand{\urum}{{ Urumqi Astronomical Station, Urumqi,
        830011}\\}
\newcommand{\jtap}{{ Department of Applied Physics, Shanghai Jiaotong
        University, Shanghai 200240}\\}
\newcommand{\ispa}{{ Institute of Space Physics and Astrophysics, 
        Shanghai Jiaotong Univeristy, Shanghai 200030}\\}
\newcommand{\jbac}{{ CAS-Peking University Joint Beijing 
        Astrophysical Center, Beijing 100871}\\}
\newcommand{\nao}{National Astronomical Observatories, 
         Chinese Academy of Sciences, Beijing 100012}
\newcommand{\hua}{{ Department of Physics, Huazhong University of 
         Science and Technology, Wuhan 430074}\\}
\newcommand{\iam}{{ Institute of Applied Mathematics, Chinese Academy of
         Sciences, Beijing 100080}\\}
\newcommand{\heb}{{ Department of Physics, Hebei Normal University, 
         Shijiazhuang 050016}\\}


\endinput
