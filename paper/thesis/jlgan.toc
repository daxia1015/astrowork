\let \CTEX@spaceChar \relax 
\contentsline {chapter}{摘\CTEX@spaceChar \CTEX@spaceChar 要}{i}{chapter*.1}
\contentsline {chapter}{Abstract}{iii}{chapter*.2}
\contentsline {chapter}{目\CTEX@spaceChar \CTEX@spaceChar 录}{v}{chapter*.3}
\contentsline {chapter}{\numberline {第一章\hspace {0.3em}}引论}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}基本概念}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}基本观测量}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}矮星系、伴星系与暗晕次结构}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}研究次结构的一些物理量}{5}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}研究方法}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}SDSS简介}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}数值模拟}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}半解析模型}{8}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}研究进展}{9}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}观测进展}{9}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}数值模拟}{9}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}半解析模型}{12}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}遗失的伴星系问题}{14}{subsection.1.3.4}
\contentsline {section}{\numberline {1.4}本文内容}{16}{section.1.4}
\contentsline {chapter}{\numberline {第二章\hspace {0.3em}}暗晕形成理论}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}宇宙学基础}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}宇宙结构的形成}{19}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}扰动的线性增长}{19}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}非线性塌缩：球蹋缩模型}{21}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}原初密度扰动分布和功率谱的演化}{22}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}星系形成}{23}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}暗晕的性质}{23}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}暗晕的结构：NFW密度轮廓}{23}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}质量分布函数}{24}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}Press-Schechter理论}{24}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}扩展的Press-Schechter理论}{25}{subsubsection.2.3.2.2}
\contentsline {section}{\numberline {2.4}暗晕的形成历史：并合树}{26}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}暗晕的并合}{26}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}并合树的产生}{27}{subsection.2.4.2}
\contentsline {chapter}{\numberline {第三章\hspace {0.3em}}暗晕次结构的动力学演化模型}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}引力势}{31}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}一般结论}{31}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}星系暗晕}{32}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}演化势}{33}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}动力磨擦}{34}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}动力磨擦的形成}{34}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}库仑对数}{36}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}次结构的轨道演化}{37}{section.3.3}
\contentsline {section}{\numberline {3.4}潮汐效应}{37}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}潮汐半径与潮汐质量剥离}{38}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}潮汐加热}{39}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}次结构间的相互作用}{41}{section.3.5}
\contentsline {section}{\numberline {3.6}关于潮汐瓦解的讨论}{41}{section.3.6}
\contentsline {chapter}{\numberline {第四章\hspace {0.3em}}动力摩擦时标的研究}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}引言}{45}{section.4.1}
\contentsline {section}{\numberline {4.2}相关的参数}{46}{section.4.2}
\contentsline {section}{\numberline {4.3}以前的结果}{47}{section.4.3}
\contentsline {section}{\numberline {4.4}模型检验}{48}{section.4.4}
\contentsline {section}{\numberline {4.5}模型解析}{49}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}$T_{\rm df}$\hspace {0.25em plus 0.125em minus 0.08em}\ignorespaces 对潮汐剥离效率$A$的依赖性}{49}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}$T_{\rm df}$\hspace {0.25em plus 0.125em minus 0.08em}\ignorespaces 对轨道圆度$\varepsilon $的依赖性}{53}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}拟合库仑对数}{53}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}模型说明}{54}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}模型M1的结果}{55}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}模型M2的结果}{56}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}拟合库仑对数}{57}{subsection.4.6.4}
\contentsline {section}{\numberline {4.7}检验拟合的库仑对数}{59}{section.4.7}
\contentsline {section}{\numberline {4.8}小结}{60}{section.4.8}
\contentsline {chapter}{\numberline {第五章\hspace {0.3em}}银河系大小暗晕中次结构的分布}{63}{chapter.5}
\contentsline {section}{\numberline {5.1}引言}{63}{section.5.1}
\contentsline {section}{\numberline {5.2}模型参量}{63}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}宇宙学参数}{63}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}暗晕的吸积历史}{64}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}暗晕轮廓}{65}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}初始轨道分布}{65}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}模型说明}{66}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}模型特点}{67}{subsection.5.2.6}
\contentsline {section}{\numberline {5.3}质量函数}{67}{section.5.3}
\contentsline {section}{\numberline {5.4}径向分布}{69}{section.5.4}
\contentsline {section}{\numberline {5.5}关于径向分布的进一步讨论}{71}{section.5.5}
\contentsline {section}{\numberline {5.6}小结}{72}{section.5.6}
\contentsline {chapter}{\numberline {第六章\hspace {0.3em}}从矮星系到星系团中次结构的分布}{75}{chapter.6}
\contentsline {section}{\numberline {6.1}引言}{75}{section.6.1}
\contentsline {section}{\numberline {6.2}主暗晕质量的影响}{75}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}质量函数}{75}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}径向分布}{77}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}暗晕占据分布}{77}{section.6.3}
\contentsline {section}{\numberline {6.4}小结}{78}{section.6.4}
\contentsline {chapter}{\numberline {第七章\hspace {0.3em}}总结与展望}{81}{chapter.7}
\contentsline {section}{\numberline {7.1}本文的结果}{81}{section.7.1}
\contentsline {section}{\numberline {7.2}未来的工作}{82}{section.7.2}
\contentsline {chapter}{参考文献}{83}{chapter*.6}
\contentsline {chapter}{发表文章目录}{91}{chapter*.7}
\contentsline {chapter}{简\CTEX@spaceChar \CTEX@spaceChar 历}{93}{chapter*.8}
\contentsline {chapter}{致\CTEX@spaceChar \CTEX@spaceChar 谢}{95}{chapter*.9}
