\documentclass[a4paper,10pt]{article}
\usepackage[utf8x]{inputenc}

%opening
\title{}
\author{}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section{The Distribution of Subhalo Population}
\label{subsec:subpop}
%
\begin{figure*}
\centerline{\psfig{figure=fig8.eps,width=0.95\hsize}}
\caption{Subhalo mass function (SHMF,  upper panels) and radial number
  distribution (lower panels) in a  Milky-Way type halo.  Panels a and
  d basically show the predictions from model M2 (solid) with \abest{}
  and \CL{} of Equation~(\ref{eq:fitting}).  In panel a, the unevolved
  and  evolved SHMFs from  simulation are  shown as  dashed-dotted and
  long dashed lines, respectively. In panel d, we also plot the radial 
  number distribution of simulated
  subhaloes  (hatch  area,  upper  limit:  Via  Lactea;  lower  limit:
  Aquarius) and  observational MW  satellites (squares).  In  panels b
  and e,  we compare the  results of simulation  to the model  M1 with
  $\ln \Lambda=-\ln \mu$  and \AAA, while in panels c  and f, the used
  parameters  are  $\ln  \Lambda=3,  4,  5$  and  \abest.   The  model
  predictions  with different  parameters  are plotted  in lines  with
  varying line style as indicated.}
\label{fig:subpop}
\end{figure*}
%

In Section~\ref{subsec:dynamics}, we have introduced in detail the model 
for the evolution of  subhalo,   including  its   mass, radial  position   
and  merging time-scales. In Section~\ref{subsec:tuning},  we tune the 
model parameters  to fit the
dynamical evolution of subhalo  predicted by simulations. Couple with the
merger trees,  the  model is  ready  to  produce the  subhalo
catalogue in the host halo. As described in Section~\ref{subsec:tree},  
our model employs $100$ realizations of merger trees of the MW type halo, 
and each realization specifies a random assembly history of dark matter 
haloes (Fig.~\ref{fig:MAH}). We follow the dynamical evolution of the 
accreted subhaloes [with masses $m(z_{\rm acc}) \ge 10^8 M_{\odot}$] by 
the main branch of merger tree, and investigate the
distribution of subhalo population at $z=0$, including the subhalo
mass function (SHMF)  and their radial distribution. We also compare the
model prediction with the simulation  results and  the observed  
distribution of  the MW satellites.


Fig.\ref{fig:subpop} show the SHMF  and the radial number distribution
in the upper and lower panels, respectively. For panels in each column,
the same  set of model parameters  is used and indicated  in the lower
panel.  The SHMF from N-body  simulations is well described by a power
law, with  index between $-0.8$  and $-1.0$ (Springel \etal  2001; Gao
\etal 2004b; Kang \etal 2005; Diemand \etal 2004; Giocoli \etal 2008;
2010).  In  panel a,  we show  the simulated one  from Giocoli  et al.
(2008) as  the long-dashed line and  the model prediction  is shown as
the solid line. It can be  seen that our fiducial model (Model M2 with
$A=3.5$ and Coulomb logarithm from Equation~(\ref{eq:fitting})) produces
a fair match to the simulation result.


As the SHMF is for an  evolved population of accreted subhaloes, it is
important to check  if the unevolved SHMF, which  is the mass function
of subhaloes at their accretion  times, is reproduced by the EPS based
merger  tree  employed  in   our  model.   The  unevolved  SHMFs  from
simulation and  the EPS  model are shown  as dashed-dotted  and dotted
lines, respectively. Their good agreement indicates that the model for
the  dynamical evolution  of subhalo  is not  biased by  the formation
history of the host halo.


We  further  check  if  our  model predictions  are  affected  by  the
assumptions for the dynamical processes  of subhalo. Panel b and c show
the predictions from our Model M1, with the dependence on $A$ (panel b)
and Coulomb  logarithm (panel c).  It  is found that  the SHMF depends
strongly on tidal stripping efficiency  $A$, but weakly on Coulomb 
logarithm. This
can be understood  from that, as shown by van  den Bosch \etal (2005),
the subhaloes  population at  present day is  dominated by  the recent
(the last  $\sim 1-2$ Gyr) accretion  history of the host  halo. It is
already  shown  in  Fig.~\ref{fig:modelA}  that subhalo  mass  depends
strongly on $A$  at the first few Gyrs, but with  a weak dependence on
Coulomb logarithm. Thus the results indicate that SHMF can not be used
to constrain the mass evolution of subhalo after a few Gyrs, while the
dynamical evolution $j$  can set strong constraints on  the late stage
evolution of subhalo, as shown in Section~\ref{subsec:tuning}.


The  radial number  distribution of  subhaloes is  shown in  the lower
panels of  Fig.\ref{fig:subpop}.  In panel  d, the hatched  area shows
the spanned  distribution from  simulations (upper limit:  Via Lactea;
lower limit: Aquarius). The observed distribution of the MW satellites
is shown  as the empty  squares (data are  from Mateo 1998;  Kroupa et
al. 2005; Metz et al. 2007; Metz et al.  2009; Martin et al. 2008).  A
clear discrepancy  is that  the distribution of  the MW  satellites is
more concentrated  than the  subhaloes from N-body  simulations, which
has  already been  noted before  (e.g. Taylor  et al.  2005b).   Such a
discrepancy  could  be  due  to  the  incompleteness  of  observations
(Willman et  al.  2004), or  the observed satellites present  a biased
population of subhaloes from simulations (Kravtsov et al.  2004b; Madau
et al.  2008).  We leave more discussion to Section~\ref{sec:discuss}.


The fiducial model  prediction is shown as the solid  line in panel d.
Compared to the simulation result, the model predicts a more centrally
concentrated  distribution of  subhaloes.  A  Similar  discrepancy was
also noted by  Taylor \& Babul (2005b) although  their model prediction
is  slightly lower  than ours.   However, Z05  found that  their model
predicts a well  match to simulation result, and  they argued that the
discrepancy  noted by  Taylor \&  Babul (2005b)  is not  from numerical
effects of  simulation but the  model assumptions for  subhalo merging
and disruption.  There still lacks  detailed studies on  this issue.
Here we firstly explore if  the predicted distribution of subhaloes is
affected by the  model assumption.  The predictions from  our Model M1
are shown in  panel e and panel f, with dependence  on $A$ and Coulomb
logarithm,  respectively.  Surprisingly,  we find  that  the predicted
distribution is  similar to  that obtained from  our Model M2,  and it
also has no dependence on the model parameter $A$ and $\ln \Lambda$.


In principle,  the final spatial  distribution of subhaloes  is mainly
determined by (1) their  initial positions at accretion, (2) dynamical
processes governing  subhalo evolution, and (3) criteria  on where and
when subhalo disappears. The results in panel e and f suggest that (2)
has no  significant effects on  the radial distribution  of subhaloes.
Since the low-mass subhaloes dominate the subhaloes population, varying 
the strength of the dynamical friction and tidal stripping will not 
change the spatial distribution of subhaloes much. 
In addition, Kang  (2008) has shown that the  formation history of the
host halo from the EPS theory  is very similar to that of simulations.
As   the   mass   and   radius   of  halo   are   close   related   by
Equation~(\ref{eq:delta}),  the   initial  positions  of  subhaloes at
accretion (the  virial radius of host  halo) from the  EPS merger tree
should be similar to the simulation results. Thus effect (1) will also
contribute little to the  discrepancy on the final radial distribution
of subhaloes.



It is then reasonable to conclude that the over-predicted subhaloes at
small  radii   is  because   either  simulations  still   lack  enough
resolutions to  resolve subhaloes  in the central  region of  the host
halo,   or   the  model   neglecting   subhalo   disruption  is   not
realistic. With  respect to simulation,  Springel et al.   (2008) have
shown  that  increasing  the  resolution does  resolve  more  low-mass
subhaloes,  but  the number  of  subhaloes  converges  for given  mass
limit.  Thus   it  is   implausible  that  simulation   resolution  is
responsible for this discrepancy.  With respect to the model, defining
a  subhalo  to be  disrupted  (or  unbound)  is very  subjective,  for
example, most authors assume that  subhalo is tidally disrupted if its
mass is less than the initial mass within a radius $f_{dis}r_{s}$, but
with  a wide range  of $f_{dis}$  between $0.1\sim  2.0$. As  shown by
Wetzel \&  White (2010), varying $f_{dis}$  has a huge  impacts on the
final radial distribution of subhaloes.


In fact,  there are  more effects which  can affect the  abundance of
subhaloes  and  their  radial   distribution. 
(i) Host  halo  formed  in
cosmological simulation always contains more than one subhalo, and the
interaction  between  subhaloes  will  accelerate  the  disruption  of
subhaloes and reduce  the number of subhaloes at  the inner host halo
(e.g., Tormen et al. 1998; Gnedin et al. 2004;  Angulo et al. 2009). 
(ii) Ludlow  et  al.   (2009)  have shown  that  small
subhaloes are more likely to be ejected out to larger distances during
the virialization of the host halo, thus producing a less concentrated
distribution.  
(iii) Subhalo-subhalo mergers may be  also effective to reduce the 
abundance of subhaloes (e.g., Kim et al. 2009), especially for the less 
massive  subhaloes (e.g., Angulo et al. 2009).
Unfortunately,  these processes  are  difficult to  be
included in the analytical model.



\end{document}
