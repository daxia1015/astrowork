\documentclass[a4paper]{article}

\newcommand{\dft}{$T_{\rm df}$~}
\newcommand{\CL}{Coulomb logarithm~}
\newcommand{\apj}{ApJ}
\newcommand{\mnras}{MNRAS}
\newcommand{\nat}{Nature}
\newcommand{\aj}{AJ}

\setlength{\hoffset}{0.1cm}
\setlength{\marginparwidth}{1cm}
\setlength{\oddsidemargin}{0.5cm}
\setlength{\evensidemargin}{0.3cm}
\setlength{\textwidth}{15cm}
\setlength{\voffset}{0.1cm}
\setlength{\topmargin}{0.5cm}
\setlength{\headheight}{0.5cm}
\setlength{\headsep}{0.5cm}
\setlength{\textheight}{23cm}

%opening
\title{Responses to the Reviewer and List of Modifications}
\author{J. Gan et al.}

\begin{document}

\maketitle


\section{Response to the Reviewer}

We greatly appreciate the referee for careful reading on this paper.
The comments and suggestions are helpful for improving
our paper. 

Before giving the responses to the referee, we firstly note that the 
derivation of dynamical friction
timescale (\dft) is affected by various factors, including the halo
profile, the accurate form of dynamical friction, the treatment of tidal
field and the evolution of baryonic component etc..  It's difficult to
include all the effects in one paper. In this paper, our main
motivation is neither to get a consistent result 
with simulation or other models, nor to derive a fully reasonable \dft,
but to see how the model predictions
are affected  by various physical processes.  This  will tell us
which  process  dominates  the  predicted  \dft,  and  how  to
interpret  the discrepancies  among the previous  studies.  


\subsection{Response to the Major Points}
\begin{itemize}
   \item
    {\bf Q/Cs (Questions or Comments from the reviewer):}
    Equation (10) assumes always a Maxwellian velocity distribution for 
    the main halo dark matter particles, which further, are assumed to be 
    isotropically distributed in velocity space. Neither of the two 
    assumptions hold in the case being treated here, in NFW or similar 
    dark matter density profiles resulting from cosmological simulations, 
    velocity distribution functions are typically anisotropic, with radial
    orbits dominating, and have non-Maxwellian velocity distributions. 
    (e.g. see Salvador-Sole et al. 2005, MNRAS, 358, 901, Manrique et al.
    2003, ApJ, 593, 26, Williams, Liliya L. R. et al.  2004, ApJ, 604, 18, 
    Bellovary et al. 2008, ApJ, 685, 739).
    
    
    {\bf Re (Response):}
    We agree that the accurate velocity-distribution of dark matter 
    particles is not Maxwellian and anisotropic (e.g., references from the
    referee's report, and are now cited in the paper), 
    but it is still a good approximation for haloes from N-body 
    simulations (e.g., Kang et al. 2002 \mnras, 336, 892; Hayashi et al. 
    2003, \apj, 584, 541). A Maxwellian and isotropic distribution is often
    adopted as  the 
    dynamical friction formula can be solved analytically. In our paper, 
    we just follow most authors (LC93; C99;  T03; Zentner et al. 2005; 
    BK08) to use the dynamical friction formula 
    derived by BT87. This is to assure a consistent comparison with 
    others work.   On the other hand, BK08 also noted that the velocity 
    distribution in their
    simulation is well described by an isotropic distribution. We also 
    add a short discussion on this issue in Section~3.2.
    
   

   \item
    {\bf Q/Cs:}
    Mass striping is treated as independent to all other effects, this is 
    clearly not valid. If 20\% of the mass of a satellite halo is stripped, 
    the remaining satellite mass will not simply constitute the central 
    80\% of the initial satellite dark matter profile, but will react 
    dynamically to the loss of the outer 20\% by swelling a certain amount,
    a bit like the tidal heating effect already considered. This internal 
    readjustment to mass loss will occur on a dynamical timescale for the 
    remaining satellite, much shorter than its orbital timescale and will 
    therefore have to be taken into account.
    
    {\bf Re:}
    As pointed out by the referee, the dynamical response to the mass 
    stripping is partly 
    incorporated in our paper by the tidal heating using the fitting 
    formula of Hayashi et al. (2003) to 
    describe the density profile of subhalo. In another aspect, the model 
    parameter     $\alpha$ (the efficiency of tidal stripping) should have 
    incorporated all the uncertainties of tidal mass stripping. 
    
    
    In Section~3.3 of the revised version, we firstly give the formalism
    of mass stripping  proposed by Taylor \& Babul (2001) \apj, 559, 716. 
    We add a paragraph
    discussing   the uncertainties of mass stripping, then give our
    model for mass stripping based on this discussion.


   \item
    {\bf Q/Cs:}
    Similar to what happens to open stellar clusters, if too much mass is 
    lost, the  satellite halo will become unbound and dissipate, much 
    before what the authors currently assume.
    
    {\bf Re:}
    For open stellar cluster, this is true. But for dark matter dominated 
    system, N-body simulations have shown that disruption happens very 
    rare (e.g., Kazantzidis et al. 2004 \apj, 608, 663; Bullock \& Johnston
    2005 \apj, 635, 931; Pe\~narrubia et al. 2008 \apj, 673, 226; 
    Diemand et al. 2007 \apj, 667, 859; Springel et al. 2008 
    \mnras, 391, 1685, BK08, private communication). Thus in this paper, 
    we simply assume that subhalo is not disrupted by tidal force, this is
    also to make a consistent comparison to other model results on 
    dynamical friction time scales, as introducing tidal disruption will 
    make it difficult to define a accurate merger time between subhalo 
    and host halo.
    
    
  
    
   \item
    {\bf Q/Cs:}
    The changing of the satellite profile due to tidal heating and mass
    stripping (plus accompanying dynamical adjustments) brings into 
    question the use of a constant value for the Coulomb logarithm, 
    although the effect of this will only be of a logarithmic correction.
    
    {\bf Re:} 
    The use of the constant value of \CL is for a full comparison with the
    results of T03, J08 and BK08. In another paper (Gan et al. 2010,
    MNRAS, in press), we have considered a time-dependent value of \CL. 
    This paper will appear in astro-ph in few weeks.

   \item
    {\bf Q/Cs:}
    DF depends explicitly on both halo density profile and velocity 
    distribution function, comparison amongst studies taking widely 
    different hypothesis on both of the above aspects of dark halo 
    structure, (e.g. the Maxwellian, isotropic and isothermal halos of 
    LC93, and the non-Maxwellian, radially dominated and NFW halos of J08 
    and BK08) should be treated much more carefully than at present. 

    {\bf Re:} 
    We agree that the halo profile can affect the result of \dft. However,
    the simulation of BK08 indicated that using a different profile
    of halo had a change in \dft  of only $5\%$. In Section~4.1 of the
    revised paper, we give the predicted \dft with isothermal sphere 
    profile and NFW
    profile. The difference between them are small and negilible. As we
    conclude in this paper, the dominant factor in deriving \dft is the
    treament of tidal stripping. The Maxwellian distribution is only an
    approximation, but it is accepted by most authors of LC93, C99, T03,
    J08 and BK08. In Section~2.1 of BK08, they clearly noted that the 
    velocity distribution in their simulation is isotropic. 

   \item
    {\bf Q/Cs:}
    The case of J08, where star formation and gas cooling are included 
    must really be treated separately, the extra physics included in that 
    study makes any quantitative comparison with the present study rather 
    dubious. 

    {\bf Re:} 
    The simulation of J08 is rather different from the other simulations or
    models. They have consider the mechanisms of gas cooling and star
    formation, which are difficult to be included in the analytical 
    model. In this paper, we never try to compare our model prediction with
    their results, but attempt to explain the discrepancy between
    BK08 and J08. The neglect of the effects of baryon in our model is 
    motivated by the results of BK08, who found that the inclusion of 
    baryon contributed less to the predicted \dft.


   \item
    {\bf Q/Cs:}
    The close to 25\% offset between the results presented and those of 
    LC93 seen in fig3 is evidence of this point, and suggests an internal 
    error of at least this magnitude on the present results.

    {\bf Re:} 
    The difference between our result and LC93's prediction is from that
    the different halo profiles are employed. We think that this difference
    is small and negligible, and it has no significant effect on our 
    conclusions. 

   \item
    {\bf Q/Cs:}
    The reality of centrally cuspy dark matter profiles is far from 
    assured, with numerous indications of dark matter haloes having
    constant density central cores e.g. Gilmore et al. (2007) NuPhS. 173, 
    15, de Blok et al. (2008) AJ, 136, 2648,  Kuzio de Naray, et al. 
    (2009) ApJ, 692, 1321,  Gebhardt \& Thomas (2009)ApJ, 700, 1690,  
    and Hernandez \& Lee (2010), MNRAS, 404, L6. If dark matter haloes have 
    constant density cores, then dynamical friction timescales would be 
    much longer, (and tidal stripping much reduced)e.g.  nchez-Salcedo 
    et al. (2006), MNRAS, 370, 1829, Inoue (2009), MNRAS, 397, 709, with 
    orbital decay practically stalling on reaching the constant density 
    central region. This would have the effect of significantly reducing 
    merger rates for dark matter haloes. It would be interesting to include
    a constant density central core example for comparison, or at least to 
    mention and make explicit that the DF timescales  being calculated 
    apply only to NFW profiles.

    {\bf Re:} 
    The typical size of a constant density core in the dark matter halo
    is usually less than $1$~kpc. In our model, the satellite would
    never go through this region in the host halo, except for highly
    eccentric orbits (i.e., $\varepsilon \to 0$) or the final stage of
    satellite evolution when the satellite has lost more than $99\%$ of
    its angular momentum.   The effect of the constant density core 
    may be remarkable for the evolution of  globular clusters in a
    dwarf galaxy (e.g., S\'anchez-Salcedo et al. 2006, /mnras, 370, 1829),
    but not for the evolution of satellite halo in a 
    Milky-Way sized halo.   On the other hand, the previous studies of 
    LC93, C99, T03, J08 and BK08, with whom we compare, have never taken 
    this issue into account.    Thus it's not necessary to 
    consider a constant density core of dark matter halo in this
    paper.    In Section~3.1, we add a short discussion on the
    halo profile.

   \item
    {\bf Q/Cs:}
    The inclusion of baryon physics could substantially modify the picture,
    through e.g. reducing the degree of central concentration of the dark 
    matter haloes, e.g. Governato, et al. 2010 Natur, 463, 20. Although 
    baryon physics might lie beyond the scope of the present paper, 
    mention of the issues involved in going from DM halo DF timescales to 
    galaxy merger timescales is appropriate.
    
    {\bf Re:} 
    There are debates about how baryon will change the central concentration 
    of dark matter halo. Some found that central density  increases
    (e.g., Blumenthal et al. 1986 \apj, 301, 27; Gnedin et al. 2004 
    \apj, 616, 16), but some disagreed with it. 
    Gnedin et al. (2004) found that the halo will become more
    concentrated as baryons condense in the radiative cooling, and the
    contraction of halo is dependent on the amount of baryon. While 
    Abadi et al. (0902.2477) found that the response of halo contraction 
    depends not only on how much baryon mass has been deposited  by the
    halo, but also on the mode of its deposition (also see Tissera et
    al. 0911.2316). They showed that strong feedback by
    supernovae can significantly decrease the central density of halo (also
    see Pedrosa et al. 0910.4380 and the references given by the reviewer). 

    In fact, T03 and BK08 both showed that the halo concentration affects
    the predicted \dft weakly.  They found that varying
    $c_{sat}/c_{host}$ between $1$ and $2$ results in a change in \dft
    of only $\approx 20\%$.  
    
    In the revised paper, we add discussion on the effects of
    baryon in the last section. 

\end{itemize}  



\subsection{Response to the Minor Points}
\begin{enumerate}
  \item
  {\bf Q/Cs:}
  In many of the figures where \dft was calculated against certain 
  parameter, e.g. ${\rm R_m}$ in figure 4, \dft was calculated only for a 
  small set 
  of values of the parameter, and the results obtained were then joined 
  by straight lines, with obvious discrete kinks resulting in the plotted
  ``curves''. 

  {\bf Re:} 
  The \dft is calculated against a larger set of values of the parameter 
  ${\rm R_m}$, and the plots look more beautiful now.

  \item
  {\bf Q/Cs:}
  The use of English requires a thorough revision.

  {\bf Re:} 
  The English of this paper has been revised thoroughly.

  \item
  {\bf Q/Cs:}
  Although the study is announced as "analytical" in the abstract, even 
  the highly simplistic treatment of the various effects included required 
  a numerical solving of the equations involved, which in my opinion 
  invalidates the use of the term "analytical model".
  
  {\bf Re:} 
  The term ``analytical'' is modified to ``semi-analytical''.

\end{enumerate}

\subsection{Additional Comments}
We are sorry that some of the reviewer's suggestions (e.g.,
Non-Maxwellian distribution of dark matter particles; a constant-density 
core in dark matter halo; the evolution of baryon) for the improvement 
of the model are not incorporated to this paper. Our model only
estimates the \dft approximately. Nevertheless, our motivation is not to
predict a reasonable timescale, but to see the effect of each
physical process on the prediction of \dft. The suggestions from the 
reviewer are important, and we will take them into account in the future
work. 


\section{List of Modifications}
\begin{itemize}
\item  In the Abstract,
\begin{itemize}
 \item  ``we present an analytical model ...'' is modified to
        ``we present a semi-analytical model ...'' 
	
 \item  ``We found that treatment ...'' is modified to ``We find that the 
         treatment ...''
\end{itemize}


\item  In the Introduction,
\begin{itemize}
 \item  ``...the massive one (host halo)'' is modified to
         ``...the more massive one (host halo)''
 \item  ``... is defined to be the time interval between entry and
         merging with the host center'' is modified to ``... is  defined  
	 as  the time  interval between  entry   and  merger  with  the  
	 host   center''
 \item	 ``The  simple application of  Chandrasekhar's formula is to 
         drive \dft for  a rigid satellite  is  by ...'' is modified to 
	 ``The  simple application of  Chandrasekhar's formula  to drive 
	 \dft for  a rigid satellite  is given by ...''
 \item	 ``Deriving  an analytical formula for \dft for  live satellite
         ...'' is modified to ``Deriving  an analytical formula of \dft 
	 for a live satellite ...''
 \item  ``Colpi et al. (1999) firstly
         questioned...'' is modified to ``Colpi et al. (1999, hereafter C99)
         firstly  questioned...''
 \item	 ``the prediction of T03 are quantitatively ...'' is 
         modified to ``the prediction of T03 is quantitatively ...''
 \item  ``Our main motivation is not to tune the
        model parameters to fit the results from simulations, but see how...''
        is modified to ``Our main motivation is neither to get a consistent 
          result with simulation or other model, nor to derive a fully 
         reasonable \dft, but to see how...''
 \item	 ``This will motivate us which ...'' is modified to ``This  will 
         tell us which ...''
\end{itemize}
 

\item  In Section 2.2,  ``Applying this formula is difficult as it depends the
       presumed value for $f_m$.'' is modified to ``It's difficult to use this 
       formula  as the \dft depends on the presumed value for $f_m$.''
	 

\item  In Section 3.1, 
\begin{itemize}
 \item   We add a description on the isothermal profile.
 
 \item   We add a paragraph that ``Note that there are  still debates ...''
 
 \item   We add a paragraph that ``Except for Section~4.1  ...''

 \item   ``For our study, we select...'' is modified to ``In our study, 
         we select...''
\end{itemize}


\item  In Section 3.2, 
\begin{itemize}
 \item  ``If we assume that ..., the dynamical friction can be given by 
        ...'' is modified to ``By assuming that ..., BT87 gave the formula
	of dynamical friction as ... ''
 
 \item  we add ``For ISO profile, $\sigma(r)\equiv V_{\rm vir}/\sqrt{2}$''
  
 \item  we add a paragraph that ``The Equation~(12) was derived ...''
\end{itemize}
	
\item  In Section 3.3,
\begin{itemize}
 \item   The original formula of mass stripping from Taylor \& Babul (2001)
         is added.

 \item   We add discussion on the uncertainties of mass stripping, which
         is ``There are some uncertainties in ...''

 \item   We introduce our model for mass stripping based on the above
         discussion: ``Owing to these uncertainties, numerical 
	 simulations ...''
\end{itemize}



\item  In Section 3.5,	 ``Here we write explicitly ...'' is modified to 
        `` Here we present explicitly ...''
 
\item  In Section 4.1, 
\begin{itemize}
 \item  In Fig.~2 and Fig.~3, we add description on the isothermal
      	profile and NFW profile.
	
 \item	We add ``LC93 derived \dft using Equation~(12) and ISO 
      	profile for the host halo.''	
	
 \item  We add discussion on the NFW profile and isothermal profile.
	
\end{itemize}

\item  In Footnote 3,  ``we found that different definition has no
       significant effects.'' is modified to ``We find that different 
       definitions have   no significant  effects.''

\item   In Section 4.2, 
\begin{itemize}
 \item	 ``... the previous results are in disagreement with each other
           quantitatively.'' is modified to ``... the previous results  
          disagree  with  each other quantitatively.''
 \item	 ``T03  used   Equation~(12)  to   describe  the  mass   loss,  
          but  with  $\alpha=1.0$ that is too low.'' is modified to ``T03 
	  also used Equation~(15)  to   describe  the  mass   loss,  
	  but  with   $\alpha=1.0$ which is too low.''
 \item	 ``a   higher value $\alpha=3.5$  is  required to  better fit the  
         subhalo  mass function  from  simulations.'' is modified
         to ``a higher value that  $\alpha=3.5$  is  required to  better 
	 fit  the    subhalo  mass function  from  simulations (also see
	 Gan et al. 2010)''
\end{itemize}

\item  In Section 4.3, ``It is not a surprise as C99 only considers only 
        minor mergers'' is modified to ``It is  not  a surprise, as C99 
	only considers minor mergers''.


  
\item  In the Conclusion,
\begin{itemize}
 \item   The title is modified to ``Conclusion and Discussion''.
 \item	 ``The  model predictions agree well with the LC93 result, for 
         the amplitude of ...'' is modified to ``The  model predictions 
	 agree well with the LC93 result on the amplitude of ...''
 \item	 ``A  higher $\alpha$ lead to rapid  loss ...'' is modified to ``A  
          higher $\alpha$ leads to rapid  loss ...''
 \item	 ``Thus  this results in a slower  down of ...'' is modified to 
         ``Thus this results in a slower  decay of ...''
 \item   We add discussion on the effects of baryon.
\end{itemize}

\item  Figure 1, 2, 3 and 4 are replotted and they look 
       more beautiful than before.
       
\item  In the Acknowledgements, we add ``We thank the referee for 
       constructive comments which significantly improve our paper.''


\item  The following references are added:
  \begin{enumerate}
    \item Fellhauer, M., \& Lin, D.~N.~C.\ 2007, \mnras, 375, 604 
    \item Fukushige, T., \& Makino, J.\ 2001, \apj, 557, 533 
    \item Navarro, J.~F., et al.\ 2004, \mnras, 349, 1039 
    \item Stoehr, F.\ 2006, \mnras, 365, 147 
    \item Springel, V., et al.\ 2008, \mnras, 391, 1685 
    \item Hernquist, L.\ 1990, \apj, 356, 359 
    \item Gan, J. L., et al. 2010,  MNRAS, in press
    \item Manrique, A., Raig, A., Salvador-Sol{\'e}, E., Sanchis, T., 
      	  \& Solanes, J.~M.\ 2003, \apj, 593, 26 
    \item Williams, L.~L.~R., Babul, A., \& Dalcanton, J.~J.\ 2004, 
      	  \apj, 604, 18    
    \item Salvador-Sol{\'e}, E., Manrique, A., \& Solanes, J.~M.\ 2005, 
      	  \mnras, 358, 901    
    \item Bellovary, J.~M., Dalcanton, J.~J., Babul, A., Quinn, T.~R., 
      	  Maas, R.~W., Austin, C.~G., Williams, L.~L.~R., \& Barnes, 
	  E.~I.\ 2008, \apj, 685, 739   
    \item Sheth, R.~K.\ 1996, \mnras, 279, 1310    
    \item Seto, N., \& Yokoyama, J.\ 1998, \apj, 492, 421    
    \item Kang, X., Jing, Y.~P., Mo, H.~J.,  B\"orner, G.\ 2002, \mnras, 
      	  336, 892 
    \item Blumenthal, G.~R., Faber, S.~M., Flores, R., \& Primack, 
          J.~R.\ 1986, \apj, 301, 27 
    \item Tissera, P.~B., White, S.~D.~M., Pedrosa, S., \& Scannapieco, 
      	  C.\ 2009, arXiv:0911.2316    
    \item Pedrosa, S., Tissera, P.~B., \& Scannapieco, C.\ 2009, 
      	  \mnras, 1877  
    \item Governato, F., et al.\ 2010, \nat, 463, 203     
    \item Gilmore, G., Wilkinson, M.~I., Wyse, R.~F.~G., Kleyna, J.~T., 
      	  Koch, A., Evans, N.~W., \& Grebel, E.~K.\ 2007, \apj, 663, 948     
    \item de Blok, W.~J.~G., Walter, F., Brinks, E., Trachternach, C., 
      	  Oh, S.-H., \& Kennicutt, R.~C.\ 2008, \aj, 136, 2648     
    \item Kuzio de Naray, R., McGaugh, S.~S., \& Mihos, J.~C.\ 2009, 
      	  \apj, 692, 1321     
    \item Gebhardt, K., \& Thomas, J.\ 2009, \apj, 700, 1690     
    \item Hernandez, X., \& Lee, W.~H.\ 2010, \mnras, 404, L6     
    \item S{\'a}nchez-Salcedo, F.~J., Reyes-Iturbide, J., \& Hernandez, 
      	  X.\ 2006, \mnras, 370, 1829    
    \item Inoue, S.\ 2009, \mnras, 397, 709     
  \end{enumerate}     
\end{itemize}


\end{document}
